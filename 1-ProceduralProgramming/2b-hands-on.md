---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Procedural programming](./0-main.ipynb) - [Hands-on 1](./4b-hands-on.ipynb)


## Introduction

### How to do the hands-on?

This [notebook](../HandsOn/HowTo.ipynb) explains very briefly your options to run the hands-ons.

### The problem we'll deal with throughout the hands-ons

Any real number can be approximated by the ratio between two integers. 

We will choose here to approximate any real `r` by an expression `numerator / 2^exponent`, where both `numerator` and `exponent` are integers.
$$
\forall r \, \in \mathbb{R}, \quad r \simeq \frac{a}{2^b} \quad \text{with} \quad [a, \, b] \in \mathbb{Z}^2.
$$

In practice, if we fix the value for the exponent $b$ we can compute the numerator $a$ through $a = r \cdot 2^b$, which gives an approximation of $r$ as $r_{\text{approx}} = a \cdot 2^{-b}$.

The higher the numerator and exponent values, the more accurate the approximation can be. For example, 0.65 can be approximated successively by:  
* 1 / 2<sup>1</sup> = 0.5  
* 3 / 2<sup>2</sup> = 0.75  
* 5 / 2<sup>3</sup> = 0.625
* ... 

The highest possible numbers will therefore be chosen, within the limits set by the system, i.e. by the number of bits available to encode these numbers.

As part of the hands-on, the number of bits allowed to store the numerator will be arbitrarily fixed, and the effect on the accuracy of the approximation will be calculated. Note that if you have N bits to store an integer, the largest possible integer is 2<sup>N</sup> - 1.

The file you need to start is [provided](../HandsOn/1-ProceduralProgramming/initial_file.cpp) in the _HandsOn_ folder of ProceduralProgramming lectures.

*Note*: you may found more about the context of the formation exercises in those articles:
* ["Exposing Floating Points](https://ciechanow.ski/exposing-floating-point/)
* ["What Every Computer Scientist Should Know About Floating-Point Arithmetic"](https://dl.acm.org/doi/pdf/10.1145/103162.103163). 



### __EXERCISE 1: Adding a loop__

Write a loop that displays the like of above up to the 2<sup>8</sup> for 0.65.

_Expected result_:

0.65 ~ 1 / 2^1  
0.65 ~ 3 / 2^2  
0.65 ~ 5 / 2^3  
0.65 ~ 10 / 2^4  
0.65 ~ 21 / 2^5  
0.65 ~ 42 / 2^6  
0.65 ~ 83 / 2^7  
0.65 ~ 166 / 2^8



[© Copyright](../COPYRIGHT.md)   




