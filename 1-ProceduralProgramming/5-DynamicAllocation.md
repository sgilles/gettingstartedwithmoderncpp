---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Procedural programming](./0-main.ipynb) - [Dynamic allocations](./5-DynamicAllocation.ipynb)


## Introduction

In C++, we can finely control the life cycle of objects and manage the memory allocated to them. This is what makes it possible to create more powerful applications than with many other languages, but it is also the main source of errors in the language. Pointers and dynamic memory management: watch out for danger!

## Stack

The ordinary variables of C++ have a lifetime limited to the current instruction block, whether it is the current function, or an instruction block attached to an `if`, `for` or just independent.

The memory allocated to them is located in an area called a **stack**, and is automatically relieved when exiting the current block using the **last in, first out** principle.

If you want to learn more about memory layout, have a look [here]( https://www.geeksforgeeks.org/memory-layout-of-c-program/).

```c++
{
    {
        int a { 5 };
        double b { 7.4 };
    } // at the end of this block, b is released first and then a - but 99.99 % of the time you shouldn't care
      // about that order!

    // a and b are not available here    
}
```

There are few limitations with the stack:

* The number of memory you can allocate on the stack is rather limited. On a current POSIX OS the order of magnitude is ~ 8 MB (on Unix type `ulimit -s` in a terminal to get this information). If you allocate more you will get a **stack overflow** (and now you know why the [most popular developers forum](https://stackoverflow.com/) is named this way!)
* The information is very local; you can't use it elsewhere. If you pass the variable as argument in a function for instance a copy is made (or if you're using a reference or a pointer you have to be sure all is done when the block is exited!)
* Stack information must be known at compile time: if you're allocating an array on the stack you must know its size beforehand.


## Heap and free store

You can in fact also explicitly place a variable in another memory area called **heap** or **free store**; doing so overcomes the stack limitations mentioned above.

This is done by calling the `new` operator, which reserves the memory and returns its address, so that the user can store it _with a pointer_.

The **heap** is independent of the **stack** and the variable thus created exists as long as the `delete` operator is not explicitly called. The creation and destruction of this type of variable is the responsibility of the programmer. 




```c++
#include <iostream>

{
    int* n = new int(5); // variable created on the heap and initialized with value 5.
    
    std::cout << *n << std::endl;
    
    delete n; // deletion must be explicitly called; if not there is a memory leak!
}
```

What is especially tricky is that:

* Creating and destroying can be done in places very disconnected in your program.
* You must ensure that whatever the runtime path used in your program each variable allocated on the heap:
    - is destroyed (otherwise you get a **memory leak**)
    - is only destroyed once (or your program will likely crash with a message about **double deletion**).
    
In sophisticated programs, this could lead in serious and tedious bookkeeping to ensure all variables are properly handled, even if tools such as [Valgrind](http://www.valgrind.org/) or [Address sanitizer](https://github.com/google/sanitizers/wiki/AddressSanitizer) may help to find out those you will probably have forgotten somewhere along the way.

To be honest, C++ gets quite a bad name due to this tedious memory handling; fortunately the RAII idiom provides a neat way to automate nicely memory management (which we'll study [later](../5-UsefulConceptsAndSTL/2-RAII.ipynb)) and some vocal critics on forums that regret the lack of [garbage collection](https://en.wikipedia.org/wiki/Garbage_collection_(computer_science)) might actually not be aware of this fundamental (from my point of view at least) idiom.


### Free store?

**Free store** is very similar in functionality to the **heap** (to the point I had to [check the difference](https://stackoverflow.com/questions/1350819/c-free-store-vs-heap) before writing this...) , and more often than not one word might be used as the other. If you want to be pedantic:

* When memory is handled by `new`/`delete`, you should talk about **free store**.
* When memory is handled by `malloc`/`free` (the C functions), you should talk about **heap**.

Pedantry aside, the important thing to know is to never mix both syntax: if you allocate memory by `new` don't use `free` to relieve it.



## Arrays on heap

If you want to init an array which size you do not know at compile time or that might overflow the stack, you may to do with `new` syntax mixed with `[]`:



```c++
#include <random>

int* throw_dice(std::size_t ndigit) {    

    // Don't bother much here - this is lifted from https://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution
    std::random_device rd;  // a seed source for the random number engine
    std::mt19937 gen(rd()); // mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> distrib(1, 6);
   
    int* dice_result = new int[ndigit];

    for (std::size_t i = 0; i < ndigit; ++i)
        dice_result[i] = distrib(gen);

    return dice_result;
}
```

```c++
#include <iostream>

auto Ndice = 5ul;

int* throw_5_dices = throw_dice(Ndice);

for (std::size_t i = 0; i < Ndice; ++i)
    std::cout << throw_5_dices[i] << std::endl;

delete[] throw_5_dices;
```

```c++
#include <iostream>

Ndice = 3;

int* throw_7_dices = throw_dice(Ndice);

for (std::size_t i = 0; i < Ndice; ++i)
    std::cout << throw_7_dices[i] << std::endl;

delete[] throw_7_dices;
```

Please notice that:

* No value can be assigned in construction: you must first allocate the memory for the array and only in a second time fill it.
* A `[]` **must** be added to the **delete** instruction to indicate to the compiler this is actually an array that is destroyed.

In fact, my advice would be to avoid entirely to deal directly with such arrays and use containers from the standard library such as `std::vector`:



```c++
#include <vector>

{
    std::vector<int> pi_first_five_digits { 3, 1, 4, 1, 5 };
}
```

that does the exact same job in a shorter way and is much more secure to use (spoiler: `std::vector` is built upon the RAII idiom mentioned briefly in this notebook).

We shall see `std::vector` more deeply [later](../5-UsefulConceptsAndSTL/3-Containers.ipynb) but will nonetheless use it before this as it is a rather elementary brick in most C++ codes.



[© Copyright](../COPYRIGHT.md)   


