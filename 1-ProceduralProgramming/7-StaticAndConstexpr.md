---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](/) - [Procedural programming](./0-main.ipynb) - [Static and constexpr](./7-StaticAndConstexpr.ipynb)


## Static keyword


`static` may be seen as _at compile time_, whereas `dynamic` may be seen as _at runtime_.

A reply to this [StackOverflow question](https://stackoverflow.com/questions/572547/what-does-static-mean-in-c) gives a rather good summary of what `static` means in C (we shall see [later](../2-ObjectProgramming/5-static.ipynb) a C++-only keyword that unfortunately share the same moniker):

* Static defined local variables do not lose their value between function calls. In other words they are global variables, but scoped to the local function they are defined in.
* Static global variables are not visible outside of the C file they are defined in.
* Static functions are not visible outside of the C file they are defined in.

Only the first one is really relevant in C++, as for variables that should be accessible only in the current file C++ provides a concept of his own: the [**unnamed namespace**](../6-InRealEnvironment/5-Namespace.ipynb#Unnamed-namespace).

Let's see this first case in action:




```c++
#include <iostream>

void FunctionWithStatic()
{
    static int n = 0; // This initialisation occurs only at first call
                      // But `n` is not destroyed when the end bracket is reached and remains available
                      // in subsequent calls. However, `n` is available only inside this function.
    std::cout << "The function has been called " << ++n << " times." << std::endl;    
}
```

```c++
{
    for (int i = 0; i < 5; ++i)
        FunctionWithStatic();    
}
```

It might be used for instance if you need to initialize something on the very first call of a function:

```c++
// Pseudo-code

void FunctionWithStatic()
{
    static bool is_first_call = true;
    
    if (is_first_call)
    {
        // Init stuff here on first call only, for instance something that requires heavy computation.
        // ...        
        is_first_call = false;
    }
    
    // ... code executed at each call
}
```

## Constexpr

We've seen the allocation of an array on the stack follows this syntax:

```c++
int array[5ul];
```

where the size of the array is available at compile time (if not you have to use an array allocated on the heap at runtime).

Now imagine we want to init such an array with a size that results from a computation; let's say a Fibonacci series:

```c++
// Recursive function - Xeus cling may not appreciate if you call this cell several times.

auto Fibonacci (std::size_t n) 
{
    if (n == 0)
        return 0;
    if (n == 1)
        return 1;
    
    return Fibonacci(n-1) + Fibonacci(n-2);
}
```

```c++
#include <iostream>
std::cout << Fibonacci(5) << std::endl;
std::cout << Fibonacci(10) << std::endl;
```

```c++
double array[Fibonacci(5)]; // COMPILATION ERROR!
```

This doesn't seem outlandish: a computation is involved and this computation happens at runtime - even if in fact all the required elements to perform it were available at compile time (there were for instance no argument read from command line involved).

In C++ 03, you had two choices to resolve this

- Using a macro...
- Or using [metaprogramming](../4-Templates/4-Metaprogramming.ipynb) which is a tad overkill... (and involves boilerplate code).

C++ introduced the keyword `constexpr`, which indicates something happens at compile time. It might be used for just a variable, or for more complicated construct such as functions or classes. The only requirement is that all the subparts used are themselves `constexpr`.

```c++
constexpr auto FibonacciConstexpr (std::size_t n) 
{
    if (n == 0)
        return 0;
    if (n == 1)
        return 1;
    
    return FibonacciConstexpr(n-1) + FibonacciConstexpr(n-2);
}
```

```c++
double array[FibonacciConstexpr(5)]; // Ok!
```

`constexpr` function may also be used as runtime function, but in this case their results can't of course be used at compile time.

```c++
#include <iostream>

int i = 7;
++i; // i is by no stretch a compile time variable!
std::cout << FibonacciConstexpr(i) << std::endl;
```

`constexpr` becomes increasingly powerful over time:

- The function `FibonacciConstexpr` above does not in fact work before C++ 14: this version of the standard introduced the possibility to provide several `return` in a `constexpr` function.
- `constexpr` is added wherever possible in the STL. This is still a work in progress: if you look for instance the [CppReference page](https://en.cppreference.com/w/cpp/algorithm/count) for `std::count` algorithm, you will see this algorithm becomes `constexpr` in C++ 20.

We will see another use of `constexpr` in a [later notebook](../4-Templates/2-Specialization.ipynb#If-constexpr).



[© Copyright](../COPYRIGHT.md)   


