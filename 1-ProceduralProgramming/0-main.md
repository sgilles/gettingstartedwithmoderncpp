---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Procedural programming](./0-main.ipynb)


* [Variables, initialisation, assignment](./1-Variables.ipynb)
* [Condition and loops](./2-Conditions-and-loops.ipynb)
    * [Hands-on 1](./2b-hands-on.ipynb)
* [Predefined types](./3-Types.ipynb)
* [Functions](./4-Functions.ipynb)
    * [Hands-on 2](./4b-hands-on.ipynb)
* [Dynamic allocation](./5-DynamicAllocation.ipynb)
* [Input/output](./6-Streams.ipynb)
    * [Hands-on 3](./6b-hands-on.ipynb)
* [Static and constexpr](./7-StaticAndConstexpr.ipynb)



[© Copyright](../COPYRIGHT.md)   


