---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Procedural programming](./0-main.ipynb) - [Conditions and loops](./2-Conditions-and-loops.ipynb)


## Conditions


### `if` condition followed by a single statement


In C++, a condition is the `if` command followed by a condition in parenthesis `()`. The single instruction (that ends at the next `;`) _or_ the block (in braces `{}`) that follows is then executed only if the condition is true.

```c++
#include <iostream>

{
    int a { 2 };
    
    if (a > 0)
        std::cout << a << " is greater than 0" << std::endl;

    if (a < 0)
        std::cout << "This line won't be executed so nothing will be printed." << std::endl;    
        std::cout << "But this line will be printed: without braces `{}` only the first instruction depends "
        "on the condition!" << std::endl;
    
}
```

Of course, the precedent code is embarrassing but is due to the poor indenting used: you have to remember that in C++ indenting is just for the programmer: it is much easier to read if a program is properly indented, but it doesn't matter from the compiler standpoint!

Our case above would be much clearer however with a more logical indenting and spacing:

```c++
#include <iostream>

{
    int a { 2 };
    
    if (a < 0)
        std::cout << "This line won't be executed so nothing will be printed." << std::endl;    
    
    std::cout << "It's much clearer now that this line is not encompassed by the condition!" << std::endl;
    
}
```

### `if` condition followed by a block

And if you need several lines to be encompassed by the condition, just use braces!

```c++
#include <iostream>

{
    int a { 2 };
    
    if (a < 0)
    {
        std::cout << "Should not be printed: the condition is false!" << std::endl;
        ++a; // won't be executed: the condition is false
    }
    
    std::cout << "a was not modified and is still 2: " << a << std::endl;    
}
```

### No semicolon at the end of the `if` condition

__BEWARE__: do not put a `;` at the end of an `if` statement! If you do so, the statement executed if the condition is true is the empty statement `;` which does nothing... The risk is rather mitigated: any compiler worth its salt will warn you if you do this mistake.

```c++
{
    int a { 2 };
    if (a == 0);
        std::cout << "Will be printed: the statement after the condition is ';', which does nothing..." << std::endl;
}
```

```c++
{
    int a { 2 };
    if (a == 2)
        ; // putting the semicolon in a different line silences the warning; there are legitimate cases in
          // which it's useful to do so and the risk is
          // slim that this was written by mistake. 
}
```

### if ... else if ... else

It is rather usual to foreseen two (or more...) courses of action depending on the results of one or several conditions. The syntax in this case is the following:

```c++
#include <iostream>

{
    int a { 2 };
    
    if (a < 0)
        std::cout << a << " is negative." << std::endl;
    else if (a == 0)
    {
        std::cout << a << " is zero." << std::endl;
    }
    else if (a < 10)
        std::cout << a << " is positive but lower than 10." << std::endl;
    else if (a < 20)
        std::cout << a << " is in the interval [10, 20[." << std::endl;
    else
    {
        std::cout << a << "is greater than 20." << std::endl;
    }
        
    
}
```

Please notice that:
* `else if` and `else` syntax is the same as the one for `if` and you may choose a single statement or a block in each branch of the condition.
* As soon as one condition is fulfilled, the execution of the condition blocks ends. I therefore didn't have to spell out in the (`a < 20`) case that `a` had to be greater than 10.


### The ternary operator


C++ has inherited from C the so-called ternary operator, which is basically an if/else case packed in one line:

```c++
#include <iostream>

{
    int i { 5 };
    std::cout << "Is " << i << " even? -> " << (i % 2 == 0 ? "true" : "false") << std::endl;
}
```

It is really the same as:

```c++
#include <iostream>

{
    int i { 5 };
    std::cout << "Is " << i << " even? -> ";
    if (i % 2 == 0)
        std::cout << "true" << std::endl;
    else
        std::cout << "false" << std::endl;
}
```

There are nonetheless some cases in which a ternary operator performs a task that is not otherwise reachable by an if/else statement; please consider for instance the initialisation of a `const` variable:

```c++
{
    int i { 5 };
    const int is_strictly_positive = (i > 0 ? 1 : 0);
    
    std::cout << is_strictly_positive << std::endl;
}
```

With if/else this doesn't work: both ways to do so that might come to mind are flawed:

```c++
{
    int i { 5 };
    const int is_strictly_positive;
    
    if (i > 0)
        is_strictly_positive = 1; // COMPILATION ERROR: can't assign to const value!
    // ... 
}
```

```c++
{    
    int i { 5 };

    
    if (i > 0)
        const int is_strictly_positive = 1;
    else
        const int is_strictly_positive = 0;
    
    std::cout << is_strictly_positive << std::endl; // COMPILATION ERROR: is_strictly_positive not 
                                                    // in the current scope.
}
```

### `switch` statement

Very briefly: there is also a `switch` statement that can be used when:

* The variable is an integer, an enum (see [next notebook](./3-Types.ipynb#Enumerations)) or might be convertible into one of those.
* The relationship considered is an equality.

I present it quickly in [appendix](../7-Appendix/Switch.ipynb) but we do not have yet seen all the elements needed to explain its interest (which remains fairly limited compared to the vastly more powerful `switch` present in other languages...)



## Logical operators

A condition might be an amalgation of several conditions. The way to glue it is to use logical operators.

The operators are:

* `&&` for the **and** operator.
* `||` for the **or** operator.
* `!` for the **not** operator.

Actually there are so-called __alternative representations__ using the english name directly for each of them but their usage is not widely spread and not advised from a stylistic standpoint (many StackOverflow threads debate on this topic...).

```c++
#include <iostream>

int a { 2 };
int b { 3 };

if (a == 2 || b == 5)
    std::cout << "Ok: first condition is true." << std::endl;
```

```c++
if (a == 2 or b == 5)
    std::cout << "Same as above illustrating the alternative representation." << std::endl;
```

```c++
if (a == 2 && b == 5)
    std::cout << "Not printed: one condition is false." << std::endl;
```

```c++
if (!(a < 0))
    std::cout << "Ok: a < 0 is false so the `not` operator returns true." << std::endl;
```

You may combine several of them in a more complex condition. `and` takes precedence over `or`, but anyway it's usually better to disambiguate using parenthesis:

```c++
if (a == 5 || a == 2 && b == 3)
    std::cout << "(a == 5) and (a == 2 && b == 3) are evaluated separately and the latter is true so this text is printed" << std::endl;
```

```c++
if ((a == 5) || (a == 2 && b == 3))
    std::cout << "Same but easier to grasp." << std::endl;
```

Builtin operators `&&` and `||` perform short-circuit evaluation: they do not evaluate the second operand if the result is known after evaluating the first. 

Please notice this means these operators are **not commutative**!

```c++
#include <iostream>

int a { 2 };
int b { 3 };

if (a < 0 && b++ == 3)
    ;
    
std::cout << "b was not incremented!: " << b << std::endl;
```

The story is different if the ordering of the conditions to test is inverted:

```c++
if (b++ == 3 && a < 0)
    ;

std::cout << "b was incremented: " << b << 
    " (as first operation is true second one is evaluated)." << std::endl;
```

Please notice it's true **only for built-in operators**: later in this tutorial we will deal with operators overloading, and such operators always evaluate both operands.


## Loops


### `while` loop


The `while` instruction allows you to execute a block of instructions in a loop
as long as a condition is true. The condition is checked **before** each
iteration.

The rules about the instructions belonging to the loop are exactly the same as the ones described for `if` statements.

```c++
#include <iostream>

{
    int a { };
    
    while (a++ < 5)
        std::cout << a << std::endl;    
}
```

If the condition is never true in the first place, we never go inside the loop:

```c++
#include <iostream>

{
    int a { };
    
    while (a-- < 0)
        std::cout << a << std::endl;

    std::cout << "Done!" << std::endl;
}
```

### `do`...`while` loop


The `do` instruction allows you to execute a block of instructions in a loop as long as
that a condition is true. The condition is verified **after** each
iteration.

```c++
#include <iostream>

{
    int a { };
    
    do 
    {
        std::cout << a << std::endl;
    } while (a++ < 0); // there is a semicolon here.
}
```

### `for` loop


#### Historical `for` loop


The historical `for` instruction allows you to execute a block of instructions as long as a
condition is true; the difference with the `while` loop is that there are fields explicitly detailing:
* The initial situation.
* The condition to check at the end of each loop.
* What should be changed in next loop if one is called for.

Syntax is:

for (_initial situation_ ; _end loop condition_ ; *evolution for next loop*)

```c++
#include <iostream>

{
    for (int i = 0; i < 5; ++i)
        std::cout << i << std::endl;    
}
```

Any of these fields might be left empty:

```c++
#include <iostream>

{
    int i { }; // i might also be declared and initialized outside the block
    for ( ; i < 5; ) // completely equivalent to `while(i < 5)`
    {
        ++i; // i might also be modified within the block
        std::cout << i << std::endl;        
    }
    std::cout << "`for` loop stopped for i = " << i << std::endl;
}
```

#### New `for` loop


The `for`syntax we just saw is still very useful, but C++ 11 introduced a new syntax when for instance you want to iterate through all the items in a container, clearly taking inspiration from syntax present in languages such as Python:

for (_type_ _element_ : *container*)

```c++
#include <iostream>
#include <vector> // we'll present this one more in detail later

{
    std::vector<int> v { 2, 3, 5, 7 };
    
    for (int item : v)
        std::cout << item << std::endl;
}
```

It might not seem much, but just for the record doing the same before C++ 11 was not for the faint of heart...:

```c++
#include <iostream>
#include <vector> // we'll present this one more in detail later

{
    std::vector<int> v { 2, 3, 5, 7 }; // and we're cheating here: C++ 03 syntax was here much worse as well...
    
    for (std::vector<int>::const_iterator it = v.cbegin(), 
         end = v.cend();
         it != end;
         ++it)
    {
        std::cout << *it << std::endl;
    }
}
```

### continue, break and infinite loop


A danger with a loop is to make it infinite: you have to make sure an exit way is foreseen:

```c++
#include <iostream>

// WARNING: if you run this you will have to restart your kernel! (in Kernel menu)

{
    int i { 2 };
    
    while (i > 0) // condition that is true for quite a long time... 
        std::cout << ++i << " ";
}
```

The best is to write a palatable condition to end it, but when some loops become increasingly complex you may have to resort to `break`. Be aware it is slightly frowned upon by some programmers, for the very same reasons [goto](https://en.wikipedia.org/wiki/Goto) instructions are avoided.

```c++
#include <iostream>

{
    int i { 2 };
    
    while (i > 0)
    {
        std::cout << i++ << " ";
        
        if (i > 20)
            break;
    }
        
}
```

In this trivial case writing the condition more properly would be of course much better:

```c++
#include <iostream>

{
    int i { 2 };
    
    while (i > 0 && i <= 20)
        std::cout << i++ << " ";
}
```

but honestly in more complex cases `break` can help keep the code more readable. 

`continue` is related: it is useful when in some conditions you want to skip the rest of the current loop iteration to go directly to the next:

```c++
#include <iostream>

{
    for (int i = 2; i < 20; ++i)
    {
        if (i == 2)
        {
            std::cout << i << " is even and prime (hello 2!)." << std::endl;
            continue;
        }

        if (i % 2 == 0)
        {
            std::cout << i << " is even." << std::endl;
            continue; // goes directly at the condition checking step in the loop, 
                      // skipping the remaining code below.
        }
        
        std::cout << i << " is odd";
        
        bool is_prime = true;
        
        for (int j = 2; j < i / 2; ++j)
        {
            if (i % j == 0) // % returns the remainder of the division
            {
                is_prime = false;
                break; // this break cuts the inner loop 'for (int j = 1; j < i / 2; ++j)'
            }
        }

        std::cout << (is_prime ? " and prime." : ".") << std::endl; // the ternary operator
    }
}
```

Of course, in a trivial example like this one we could have written it much more cleanly without any `continue`, but in more complex cases it is really handful to use it: not using it could lead to code much more complicated to understand, and you really should always strive for code that is the most expressive for a reader.


### So which loop should I use?


Whichever you want in fact!

They are mostly interchangeable:

* `while` and (historical) `for` are completely interchangeable, as:
    - A `while` loop is exactly like a `for ` loop with only the middle term.
    - You can transform a `for` loop into a `while` one: putting the first term before the loop and the third one inside the loop to do so.
* `do..while` behaves slightly differently, but you can always mimic the behaviour with another type of loop.

Lots of programming language define these guys (at least `for` and `while`) so it's useful to know about them, but you can choose one and stick with it as well.



[© Copyright](../COPYRIGHT.md)   


