---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Procedural programming](./0-main.ipynb) - [Hands-on 3](./7b-hands-on.ipynb)


### __EXERCISE 8: Multiplication by an integer__

Write the `Multiply()` function that calculates the product of an approximated real by an integer coefficient and returns an integer. 

*Reminder*: a real is approximated by `numerator / 2^exponent`; your function should rely upon `ComputePowerOf2Approx()` and `TimesPowerOf2()` functions.

The arguments of `Multiply()` are the maximum number of bits for approximation, the real and the integer coefficient.

The new main will be:

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{ 
    for (int nbits = 2; nbits <= 8; nbits += 2)
        DisplayPowerOf2Approx(nbits, 0.65) ;
    
    std::cout << std::endl;

    for (int nbits = 2; nbits <= 8; nbits += 2)
        DisplayPowerOf2Approx(nbits, 0.35) ;
    
    std::cout << std::endl;

    for (int nbits = 1; nbits <= 8; ++nbits)
    {
        double exact = 0.65 * 3515;
        int rounded = RoundAsInt(exact);
        int approx = Multiply(nbits, 0.65, 3515);
        std::cout << "[With " << nbits << " bits]: 0.65 * 3515 = " 
            << rounded << " ~ " << approx << std::endl;
     }

    return EXIT_SUCCESS;
}
```

_Expected result:_

```
[With 2 bits]: 0.65 ~ 0.75 (3 / 2^2)  [error = 15/100]
[With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 4/100]
[With 6 bits]: 0.65 ~ 0.65625 (42 / 2^6)  [error = 1/100]
[With 8 bits]: 0.65 ~ 0.648438 (166 / 2^8)  [error = 0/100]

[With 2 bits]: 0.35 ~ 0.375 (3 / 2^3)  [error = 7/100]
[With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 2/100]
[With 6 bits]: 0.35 ~ 0.351562 (45 / 2^7)  [error = 0/100]
[With 8 bits]: 0.35 ~ 0.349609 (179 / 2^9)  [error = 0/100]

[With 1 bits]: 0.65 * 3515 = 2285 ~ 1757
[With 2 bits]: 0.65 * 3515 = 2285 ~ 2636
[With 3 bits]: 0.65 * 3515 = 2285 ~ 2196
[With 4 bits]: 0.65 * 3515 = 2285 ~ 2196
[With 5 bits]: 0.65 * 3515 = 2285 ~ 2306
[With 6 bits]: 0.65 * 3515 = 2285 ~ 2306
[With 7 bits]: 0.65 * 3515 = 2285 ~ 2279
[With 8 bits]: 0.65 * 3515 = 2285 ~ 2279
```


### __EXERCISE 9: display sum of two Multiply__

Write a `DisplaySumOfMultiply` function which will write the computation of the sum of two approximated real numbers that have each been multiplied by a different integer coefficient.

The function is expected to compute and display the following:
$$
\displaystyle r_1 \cdot c_1 + r_2 \cdot c_2 \simeq (a_1 \cdot 2^{-b_1}) \cdot c_1 + (a_2 \cdot 2^{-b_2}) \cdot c_2
$$

This function will take 5 arguments:

* The number of bits to use.
* Two real values.
* Their associated coefficients.

New main will look like:

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{  
    for (int nbits = 2; nbits <= 8; nbits += 2)
        DisplayPowerOf2Approx(nbits, 0.65);

    std::cout << std::endl;

    for (int nbits = 2; nbits <= 8; nbits += 2)
        DisplayPowerOf2Approx(nbits, 0.35);
    
    std::cout << std::endl;
    
    for (int nbits = 1; nbits <= 8; ++nbits)
        DisplaySumOfMultiply(nbits, 0.65, 3515, 0.35, 4832); // to compute 0.65 * 3515 + 0.35 * 4832
        
    return EXIT_SUCCESS;
}
```

*Expected result*:

    [With 2 bits]: 0.65 ~ 0.75 (3 / 2^2)  [error = 15/100]
    [With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 4/100]
    [With 6 bits]: 0.65 ~ 0.65625 (42 / 2^6)  [error = 1/100]
    [With 8 bits]: 0.65 ~ 0.648438 (166 / 2^8)  [error = 0/100]

    [With 2 bits]: 0.35 ~ 0.375 (3 / 2^3)  [error = 7/100]
    [With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 2/100]
    [With 6 bits]: 0.35 ~ 0.351562 (45 / 2^7)  [error = 0/100]
    [With 8 bits]: 0.35 ~ 0.349609 (179 / 2^9)  [error = 0/100]

    [With 1 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 2965
    [With 2 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4448
    [With 3 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4008
    [With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857
    [With 5 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3967
    [With 6 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4004
    [With 7 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3977
    [With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968


### __EXERCISE 10: print error in `DisplaySumOfMultiply()`__

Modify slightly the function defined above to add display of the error; we will express it over 1000 (see exercise 7 which was roughly the same!)

*Expected result*:

```
[With 2 bits]: 0.65 ~ 0.75 (3/2^2)  [error = 15/100]
[With 4 bits]: 0.65 ~ 0.625 (10/2^4)  [error = 4/100]
[With 6 bits]: 0.65 ~ 0.65625 (42/2^6)  [error = 1/100]
[With 8 bits]: 0.65 ~ 0.648438 (166/2^8)  [error = 0/100]

[With 2 bits]: 0.35 ~ 0.375 (3/2^3)  [error = 7/100]
[With 4 bits]: 0.35 ~ 0.34375 (11/2^5)  [error = 2/100]
[With 6 bits]: 0.35 ~ 0.351562 (45/2^7)  [error = 0/100]
[With 8 bits]: 0.35 ~ 0.349609 (179/2^9)  [error = 0/100]

[With 1 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 2965  [error = 254/1000]
[With 2 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4448  [error = 119/1000]
[With 3 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4008  [error = 8/1000]
[With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 30/1000]
[With 5 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3967  [error = 2/1000]
[With 6 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4004  [error = 7/1000]
[With 7 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3977  [error = 0/1000]
[With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2/1000]
```



### [optional] __EXERCISE 11: write in output file__

Modify the program so that the `DisplayPowerOf2Approx` and `DisplaySumOfMultiply` functions take an additional argument: the output stream to which the content should be written.

The following `main()` which writes part of the outputs in a file should work:

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{  
    std::ofstream out("/tmp/approx_0.65.txt");

    for (int nbits = 2; nbits <= 8; nbits += 2)
        DisplayPowerOf2Approx(out, nbits, 0.65);

    for (int nbits = 2; nbits <= 8; nbits += 2)
        DisplayPowerOf2Approx(std::cout, nbits, 0.35);
    
    std::cout << std::endl;
    
    for (int nbits = 1; nbits <= 8; ++nbits)
        DisplaySumOfMultiply(std::cout, nbits, 0.65, 3515, 0.35, 4832); // to compute 0.65 * 3515 + 0.35 * 4832
    
    return EXIT_SUCCESS;
}
```

*Expected result*:

```
[With 2 bits]: 0.35 ~ 0.375 (3 / 2^3)  [error = 7/100]
[With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 2/100]
[With 6 bits]: 0.35 ~ 0.351562 (45 / 2^7)  [error = 0/100]
[With 8 bits]: 0.35 ~ 0.349609 (179 / 2^9)  [error = 0/100]

[With 1 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 2965  [error = 254/1000]
[With 2 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4448  [error = 119/1000]
[With 3 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4008  [error = 8/1000]
[With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 30/1000]
[With 5 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3967  [error = 2/1000]
[With 6 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4004  [error = 7/1000]
[With 7 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3977  [error = 0/1000]
[With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2/1000]
```


and `cat /tmp/approx_0.65.txt` should return:

```
[With 2 bits]: 0.65 ~ 0.75 (3 / 2^2)  [error = 15/100]
[With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 4/100]
[With 6 bits]: 0.65 ~ 0.65625 (42 / 2^6)  [error = 1/100]
[With 8 bits]: 0.65 ~ 0.648438 (166 / 2^8)  [error = 0/100]
```


### [optional] __EXERCISE 12: function pointers__

Create a `Loop()` function that takes as an argument :

* The output stream
* An initial number of bits
* A final number of bits
* An increment to be applied to the number of bits
* A pointer to a function to be executed for each number of bits

You will need the following intermediate function to be able to use them in `Loop()` (as a specific signature is expected):

```c++
// Declarations
void Display_065(std::ostream& out, int nbits);

void Display_035(std::ostream& out, int nbits);

void Display_065_3515_035_4832(std::ostream& out, int nbits);


// Definitions
void Display_065(std::ostream& out, int nbits)
{ 
    DisplayPowerOf2Approx(out, nbits, 0.65); 
}

void Display_035(std::ostream& out, int nbits)
{
    DisplayPowerOf2Approx(out, nbits, 0.35); 
}

void Display_065_3515_035_4832(std::ostream& out, int nbits)
{ 
    DisplaySumOfMultiply(out, nbits, 0.65, 3515, 0.35, 4832); 
}


// Main
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{       
    std::ofstream out("/tmp/approx_0.65.txt");

    Loop(out, 2, 8, 2, Display_065);
    Loop(std::cout, 2, 8, 2, Display_035);

    std::cout << std::endl;

    Loop(std::cout, 1, 8, 1, Display_065_3515_035_4832);
    
    return EXIT_SUCCESS;
}


```


[© Copyright](../COPYRIGHT.md)   


