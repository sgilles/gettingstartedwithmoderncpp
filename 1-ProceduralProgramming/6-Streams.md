---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Procedural programming](./0-main.ipynb) - [Input and output streams](./6-Streams.ipynb)


## Predefined streams

The standard C++ includes an input/output library that specifies a common interface for all data exchanges with the outside world, based in particular on the insertion `<<` and extraction `>>` operators.

### `std::cout`

We have already dealt profusely with `std::cout` which provide the link to the Unix channel `stdout`:

```c++
#include <iostream>

{
    std::cout << "Hello world!" << std::endl;
}
```

### `std::cerr`

There is also `std::cerr`, which is related to Unix `stderr`:

```c++
#include <iostream>

{
    int n = -4;
    
    if (n < 0)
        std::cerr << "Positive or null value expected!" << std::endl;
        
}
```

### `std:cin`

And finally `std::cin`, related to Unix channel `stdin`. Line crossings are ignored (assimilated to spaces and tabs).
    
**WARNING** This works only with a recent version of Xeus-cling.

```c++
#include <random>
#include <iostream>

{
    std::random_device rd;  // Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); // Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(0, 100);
    
    auto hidden = dis(gen);
    
    int guess = -1;
    
    while (guess != hidden)
    {
        std::cout << "Find the value between 0 and 100: ";
        
        std::cin >> guess;
        
        if (guess > hidden)
            std::cout << " Too high!" << std::endl;
        else if (guess < hidden)
            std::cout << " Too low!" << std::endl;
    }
    
    std::cout << "Congratulations! You have found the hidden number!" << std::endl;
}
```

`std::cin` is a bit more tricky to use than the others, as the risk the operation fails is really higher. For instance, if you give a string in the code above it will become crazy and keep printing the same message "Too high!" or "Too low!" (be ready to restart the kernel...). The following code fixes this:

```c++
#include <random>
#include <iostream>


{
    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(0, 100);
    
    auto hidden = dis(gen);
    
    int guess = -1;
    
    while (guess != hidden)
    {
        do
        {
            if (!std::cin)
            {
                std::cin.clear(); // clear the states of std::cin, putting it back to `goodbit`.
                std::cin.ignore(10000, '\n'); // clean-up what might remain in std::cin before using it again.
            }
            
            std::cout << "Find the value between 0 and 100: ";            
            std::cin >> guess;            

        } while (!std::cin);
        
        
        if (guess > hidden)
            std::cout << " Too high!" << std::endl;
        
        else if (guess < hidden)
            std::cout << " Too low!" << std::endl;
        
    }
    
    std::cout << "Congratulations! You have found the hidden number!" << std::endl;
}
```

If you want to learn more about `std::cin`, you might want to look at [this post](https://stackoverflow.com/questions/5131647/why-would-we-call-cin-clear-and-cin-ignore-after-reading-input) on StackOverflow.

If you need to use it extensively, you should look more deeply the behaviour of the bit flags (`goodbit`, `badbit`, `failbit`, `eofbit`).



## Input/output with files

The same syntax with operators `<<` and `>>` may be used to interact with files; the streams are built with `std::ofstream` for an output stream and `std::ifstream` for an input stream.




```c++
#include <fstream> // for std::ifstream and std::ofstream
#include <iostream>

{
    std::ofstream out("File.tmp");
    
    out << 5 << std::endl;
    out << -7 << std::endl;
    out << 9 << std::endl;    
    
    out.close(); // file is written on disk when closed; automatically done when `out` gets out of scope otherwise
    
    std::ifstream in("File.tmp");
    
    int value;
    
    while (in >> value)
        std::cout << value << std::endl;
}
```

### `getline()`

When reading a file, if you want to interpret it line by line you should also consider `getline()`; this function may get a third argument to choose which separator to use (`\n` by default).

```c++
#include <iostream>
#include <fstream>
#include <string>

{ 
    std::ifstream in("File.tmp"); // assumes previous cell has been played recently!
    
    std::string line;
    
    while (getline(in, line))
        std::cout << line << std::endl;
}
```

## `ostream` and `istream`

If you want to devise a function that may take as argument either a `std::cout` or a `std::ofstream`, you should use a `std::ostream` (we'll study [later](../2-ObjectProgramming/6-inheritance.ipynb) why this works but just take my word for now):

```c++
#include <iostream>

void PrintOnStream(std::ostream& out)
{
    out << "Printed on the chosen stream!" << std::endl;
}
```

```c++
PrintOnStream(std::cout);
```

```c++
#include <string>
#include <fstream> 

{
    std::ofstream out("test_stream.txt");
    PrintOnStream(out);
}
```

```c++
{
    // Read the content of the line previously written.
    std::ifstream in("test_stream.txt");
    std::string line;
    getline(in, line);
    std::cout << line << std::endl;
}
```

## Conversion

Stream syntax was until C++ 11 the only way to convert:

- A string into a number with `std::istringstream`
- A number into a string with `std::ostringstream`; the `str()` method returns the content as a `std::string`.

```c++
#include <sstream> // for std::ostringstream and std::istringstream
#include <string>

{
    std::string number_as_string = "657";
    
    int number;
    
    std::istringstream iconv(number_as_string);
    iconv >> number;
    
    std::cout << "Number + 1 = " << number + 1 << std::endl;
}
```

```c++
#include <sstream> // for std::ostringstream and std::istringstream
#include <string>

{
    int number = 657;
    
    std::ostringstream oconv;    
    oconv << "The number is " << number;    
    std::cout << oconv.str() << std::endl;
}
```

To reuse a `std::ostringstream`, you must set its content to an empty string with an overloaded `str()`:

```c++
#include <sstream> // for std::ostringstream and std::istringstream
#include <string>

{
    int number = 657;
    
    std::ostringstream oconv;    
    oconv << "The number is " << number;
    std::cout << oconv.str() << std::endl;
    
    oconv.str(""); // reset oconv
    oconv << "My new content is now there!";
    std::cout << oconv.str() << std::endl;    
}
```

Of course as for `std::cin` you may check the state of the object is still valid - if conversion is incorrect it won't!:

```c++
#include <sstream> // for std::ostringstream and std::istringstream
#include <string>

{
    std::string number_as_string = "abd";
    
    int number;
    
    std::istringstream iconv(number_as_string);
    iconv >> number; // invalid conversion!

    if (!iconv)
        std::cerr << "Invalid string!" << std::endl;
    else
        std::cout << "Number + 1 = " << number + 1 << std::endl;
}
```

In C++ 11, `std::to_string()` and the [`stoi` (and similar functions for long)](https://en.cppreference.com/w/cpp/string/basic_string/stol) were introduced to provide similar functionality with a more direct syntax:

```c++
#include <iostream>
#include <string>

{
    int number = 657;
    
    std::string int_to_string("Number is ");
    int_to_string += std::to_string(number);
    std::cout << int_to_string << std::endl;
}
 
```

```c++
#include <iostream>
#include <string>

{
    std::string number_as_string = "4557";
    
    int number = std::stoi(number_as_string);
    
    std::cout << "Number is " << number << std::endl;
}
```

It is however useful to be aware of the pre-C++ 11 syntax, especially for the number to string conversion: 'arithmetic' operations between strings (as `+`) incur copies that are avoided with the `std::ostringstream` syntax... but the construction of such a `std::ostringstream` object is costly as well...

C++ 20 should provide a better looking and more efficient syntax with `std::format` (see [this page](https://en.cppreference.com/w/cpp/utility/format/format) for more details)... but unfortunately current support by compilers is [not great](https://en.cppreference.com/w/cpp/compiler_support) (still true in 2024...)



## Formatting and manipulators

You may act upon the exact formatting of the output.

I'll be honest: it's not what is the most refined tool in the C++ library, and you may long for the simplicity and power of something like Python (or even C `printf`, which is much simpler to use while being a mess under the hood...).

Once again `std::format` from C++ 20 should be a game changer here!

The difficulty is that some settings apply only to the next entry onto the stream (`width` here), while others change the behaviour permanently (until told otherwise of course, e.g. `precision` here). Here are few examples of these syntaxes:




```c++
#include <iostream>

{
    std::cout.setf(std::ios::showpos); // Add the `+` sign explicitly
    std::cout.setf(std::ios::fixed, std::ios::floatfield); // use decimal notation only
    std::cout.precision(2); // number of decimal digits

    std::cout.width(8) ;
    std::cout << 1.237 ;
    std::cout.width(8) ;
    std::cout << 100.1245 ;

    std::cout.width(8) ;
    std::cout << '\n' ;

    std::cout.width(8) ;
    std::cout << 1.5774e-2 ;
    std::cout.width(8) ;
    std::cout << 12. << '\n' ;
}
```

**Manipulators** provide a shorter syntax to add some of the properties as the `width` or the `precision`:

```c++
#include <iostream>
#include <iomanip> // for std::setprecision

{
    std::cout.setf(std::ios::showpos); // Add the `+` sign explicitly
    std::cout.setf(std::ios::fixed, std::ios::floatfield); // use decimal notation only

    std::cout << std::setprecision(2) << std::setw(8) << 1.237;
    std::cout << std::setw(8) << 100.1245;
    std::cout << '\n';

    std::cout << std::setw(8) << 1.5774e-2;
    std::cout << std::setw(8) << 12. << '\n';
}
```


[© Copyright](../COPYRIGHT.md)   


