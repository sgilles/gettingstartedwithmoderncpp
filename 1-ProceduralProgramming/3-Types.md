---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Procedural programming](./0-main.ipynb) - [Predefined types](./3-Types.ipynb)


## Boolean

Variables with type `bool` may be set to `true` or `false`.

It should be noted that this type did not originally exist, and that C++ instructions with conditions do not necessarily expect boolean values, but rather integers.

There is a form of equivalence between booleans and integers: any null integer is equivalent to `false`, and any other value is equivalent to `true`.

```c++
#include <iostream>

bool undefined;  // UNDEFINED !! 
if (undefined)
    std::cout << "This text might appear or not - it's truly undefined and may vary from "
                "one run/compiler/architecture/etc... to another!" << std::endl;
```

```c++
bool defined { true };

if (defined)
    std::cout << "Defined!" << std::endl;
```

```c++
int n = -5;

if (n) 
    std::cout << "Boolean value of " << n << " is true." << std::endl;
```

```c++
int n = 0;

if (!n) // ! is the not operator: the condition is true if n is false.
    std::cout << "Boolean value of " << n << " is false." << std::endl;
```

## Enumerations

### Historical enumerations

The historical enumerations `enum` of C++ allow to define constants that are treated as integers, and that can be initialized from integers. By default the first value is 0 and the `enum` is incremented for each value, but it is possible to bypass these default values and provide the desired numerical value yourself.

```c++
#include <iostream>

{
    enum color { red, green, blue } ;
    std::cout << red << " " << green << " " << blue << " (expected: 0, 1, 2)" << std::endl;

    enum shape { circle=10, square, triangle=20 };
    std::cout << circle << " " << square << " " << triangle << " (expected: 10, 11, 20)"<< std::endl;  //  10 11 20    
}
```

These `enum` are placeholders for integers and might be used as such:

```c++
#include <iostream>

{
    enum color { red, green, blue } ;
    int a { 5 };
    color c = green;
    int b = a + c;
    std::cout << "b = " << b << " (expected: 6)" << std::endl;
    
    enum shape { circle=10, square, triangle=20 };
    shape s = triangle;
    int d = s + c;
    std::cout << "d = " << d << " (expected: 21... but we've just added a shape to a color without ado!)" << std::endl;
}
```

A (huge) shortcoming of historical `enum ` is that the same word can't be used in two different `enum`:

```c++
{
    enum is_positive { yes, no };
    
    enum is_colored { yes, no }; // COMPILATION ERROR!
}
```

### New enumerations

To overcome the two limitations we have just mentioned, C++11 makes it possible to declare new `enum class` enumerations, each constituting a separate type, not implicitly convertible into an integer. This type protects against previous errors at the cost of a little more writing work.

```c++
enum class is_positive { yes, no };    
enum class is_colored { yes, no };  // OK    
```

```c++
yes; // COMPILATION ERROR: `enum class ` value must be prefixed! (see below)
```

```c++
is_positive p = is_positive::yes; // OK
```

```c++
int a = is_positive::no; // COMPILATION ERROR: not implicitly convertible into an integer
```

```c++
is_positive::yes + is_colored::no; // COMPILATION ERROR: addition of two unrelated types
```

```c++
{
    enum class color { red, green, blue } ;
    color c = color::green;
    bool is_more_than_red = (c > color::red); // Both belong to the same type and therefore might be compared
}
```

<!-- #region -->
These enum types are really handy to make code more expressive, especially in function calls:

```c++
f(print::yes, perform_checks::no);
```

is much more expressive (and less error-prone) than:

```c++
f(true, false);
```

for which you will probably need to go check the prototype to figure out what each argument stands for.

As we shall see [shortly](#Explicit-conversions-by-static_cast), you may perform arithmetic with the underlying integer through _explicit cast_ of the enum into an integer.
<!-- #endregion -->

## Numerical types

#### List of numerical types

The FORTRAN correspondences below are given as examples. The
size of the C++ digital types can vary depending on the processor used. The
standard C++ only imposes `short <= int <= long` and `float <= double <= long double`. This makes these predefined types unportable. Like many things
in C, and therefore in C++, performance is given priority over any other consideration.
The default integer and real types, `int` and `double`, are assumed
to match the size of the processor registers and be the fastest (for more details see [the article on cppreference](http://en.cppreference.com/w/cpp/language/types))

| C++           | Fortran   | Observations        | 0 notation |
|:------------- |:---------:|:-------------------:|:----------:|
| `short`       | INTEGER*2 | At least on 16 bits | None       |
| `int`         | INTEGER*4 | At least on 16 bits | 0          | 
| `long`        | INTEGER*8 | At least on 32 bits | 0l         |
| `long long`   | INTEGER*16| At least on 64 bits | 0ll        |
| `float`       | REAL*4    | -                   | 0.f        |
| `double`      | REAL*8    | -                   | 0.         |
| `long double` | REAL*16   | -                   | 0.l        |

All integer types (`short`, `int` and `long`) also have an unsigned variant, for example
`unsigned int`, which only takes positive values.

It should also be noted that the type `char` is the equivalent of one byte,
and depending on the context will be interpreted as a number or as a
character.

If you need an integer type of a defined size, regardless of the type of processor or platform used, you should use those already defined in `<cstdint>` for C++11 (for more details click [here](http://en.cppreference.com/w/cpp/types/integer)).

The _0 notation column_ is the way to notice explicitly the type in an expression; of course any value might be used instead of 0. A `u` might be used to signal the unsigned status for integer types; for instance `3ul` means 3 as an _unsigned long_. `auto` notation below will illustrate a case in which such a notation is useful.

The STL features rather heavily a type named `std::size_t`, which by design is able to store the maximum size of a theoretically possible object of any type (including array). On most (all?) systems `std::size_t` is an alias to an `unsigned long`. More may be found about this type on [CppReference](https://en.cppreference.com/w/cpp/types/size_t). The equivalent counterpart for *signed* integers is the [`std::ptrdiff_t`](https://en.cppreference.com/w/cpp/types/ptrdiff_t), which is the signed integer type of the result of subtracting two pointers.

#### Numeric limits

Always keep in mind the types of the computer don't match the abstract concept you may use in mathematics... The types stored especially don't go from minus infinity to infinity:

```c++
#include <iostream>
#include <limits>  // for std::numeric_limits


{
    std::cout << "int [min, max] = [" << std::numeric_limits<int>::lowest() << ", "
            << std::numeric_limits<int>::max() << "]" << std::endl;

    std::cout << "unsigned int [min, max] = [" << std::numeric_limits<unsigned int>::lowest() << ", "
            << std::numeric_limits<unsigned int>::max() << "]" << std::endl;    
    
    std::cout << "short [min, max] = [" << std::numeric_limits<short>::lowest() << ", "
            << std::numeric_limits<short>::max() << "]" << std::endl;
    
    std::cout << "long [min, max] = [" << std::numeric_limits<long>::lowest() << ", "
            << std::numeric_limits<long>::max() << "]" << std::endl;
    
    std::cout << "float [min, max] = [" << std::numeric_limits<float>::lowest() << ", "
            << std::numeric_limits<float>::max() << "]" << std::endl;

    std::cout << "double [min, max] = [" << std::numeric_limits<double>::lowest() << ", "
            << std::numeric_limits<double>::max() << "]" << std::endl;

    std::cout << "long double [min, max] = [" << std::numeric_limits<long double>::lowest() << ", "
        << std::numeric_limits<long double>::max() << "]" << std::endl;
}
```

(see the [CppReference dedicated page](https://en.cppreference.com/w/cpp/types/numeric_limits) for more details about `std::numeric_limits`).

##### Integral types

If an initial value is not in the range, the compiler will yell:

```c++
#include <iostream>

{
    short s = -33010; // triggers a warning: outside the range
    std::cout << s << std::endl;
}
```

However, if you go beyond the numeric limit during a computation you're on your own:

```c++
#include <iostream>
#include <limits>  // for std::numeric_limits

{
    unsigned int max = std::numeric_limits<unsigned int>::max();
    
    std::cout << "Max = " << max << std::endl;
    std::cout << "Max + 1 = " << max + 1 << " // !" << std::endl;
}
```

When you reach the end of a type, a modulo is actually applied to make put it back into the range! 

Don't worry, for most computations you shouldn't run into this kind of trouble, but if you are dealing with important values it is important to keep in mind this kind of issues.

The most obvious way to avoid this is to choose appropriate types: if your integer might be huge a `long` is more appropriate than an `int`.

Other languages such as Python (but not its numeric modules such as _numpy_ which are using C or C++ under the hood) gets a underlying integer model that is resilient to this kind of issue but there is a performance cost behind it; types such as those used in C++ are tailored to favor optimization on your hardware.


##### Floating-point types

C++ provides special values to represent infinite or not-a-number values for floating-point types (the facilities below appeared after C++ 11; there were others prior to that inherited directly from C).

```c++
#include <cmath>
#include <iostream>

float max_float {std::numeric_limits<float>::max()};
max_float += 1.e+32; // Add something significant enough to max value
std::cout << "Is " << max_float << " infinite ? " << std::isinf(max_float) << std::endl;
max_float -= 1.e+32;
std::cout << "Is " << max_float << " infinite ? " << std::isinf(max_float) << std::endl;

double nan = 0. / 0.;

std::cout << "Is " << nan << " infinite ? " << std::isinf(nan) << std::endl;
std::cout << "Is " << nan << " not-a-number ? " << std::isnan(nan) << std::endl;
nan = nan + 1.e5;
std::cout << "Is " << nan << " not-a-number ? " << std::isnan(nan) << std::endl;

std::cout << "The unnatural property of nan is that the expression 'nan  == nan' is " << std::boolalpha << (nan == nan) << "!" << std::endl;

```

There are subtleties about NaN (see [Cppreference](https://en.cppreference.com/w/cpp/numeric/math/nan)) but in most cases you don't need to bother much with either `inf` or `nan`, except if you have reasons to think your computation may produce either of them. In that case, you may want to check your value is correct with `std::isfinite` ([Cppreference](https://en.cppreference.com/w/cpp/numeric/math/isfinite)).


#### Conversions between digital types

[Earlier](/notebooks/1-ProceduralProgramming/1-Variables.ipynb#Initialisation) I indicated there were small differences between the three initialization methods, that could be ignored most of the time. 

The difference is related to implicit conversion: both historical initialization methods are ok with implicit conversion __with accuracy loss__:

```c++
#include <iostream>
#include <iomanip>
{
    float f = 1.1234567890123;
    double d = 2.1234567890123;
    float f_d(d);
    float f_dd = d;

    std::cout << "A double may print around 15 significant digits, d = " << std::setprecision(16) << d << std::endl;
    std::cout << "A float may print around 7 significant digits, f_d = " << std::setprecision(16) << f_d << " so we see here the value was altered from the initial double value." <<  std::endl;

    std::cout << "Even without conversion involved there is an accuracy loss: f = " << std::setprecision(16) << f << std::endl;
    
}
```

whereas C++ 11 introduced initialization with braces isn't:

```c++
{
    double d = 2.12345678901234567890;
    float f_d{d}; // COMPILATION ERROR
}
```

This is really related to **accuracy loss**: initialization with braces is ok if there are none:

```c++
{
    float f = 1.12345678901234567890;
    double d_f { f }; // OK
}
```

Accuracy losses are detected during conversion:
* from a floating point type (`long double`, `double` and `float`) into an integer type.
* from a `long double` into a `double` or a `float`, unless the source is constant and its value fits into the type of the destination.
* from a `double` into a `float`, unless the source is constant and its value fits in the type of the destination.
* from an integer type to an enumerated or floating point type, unless the source is constant and its value fits into the type of the destination.
* from an integer type to an enumerated type or another integer type, unless the source is constant and its value fits into the type of the destination.


### Explicit conversions inherited from C

In the case of an explicit conversion, the programmer explicitly says which conversion to use.
C++ inherits the forcing mechanism of the C type:

```c++
{
    unsigned short i = 42000 ;
    short j = short(i) ;
    unsigned short k = (unsigned short)(j) ;
}
```

It is **not recommended** to use this type of conversion: even if it is clearly faster to type, it can perform a different C++ conversion under the hood and does not stand out clearly when reading a code; it is preferable to use the other conversion modes mentioned below.


### Explicit conversions by static_cast

C++ has also redefined a family of type forcing,
more verbose but more precise. The most common type of explicit conversion is the `static_cast`:

```c++
{
    unsigned short i = 42000;
    short j = static_cast<short>(i);
    unsigned short k = static_cast<unsigned short>(j);
}
```

Another advantage of this more verbosy syntax is that you may find it more easily in your code with your editor search functionality.


### Other explicit conversions

There are 3 other types of C++ conversions:
* `const_cast`, to add or remove constness to a reference or a pointer (obviously to be used with great caution!)
* `dynamic_cast`, which will be introduced when we'll deal with [polymorphism](../2-ObjectProgramming/7-polymorphism.ipynb#dynamic_cast).
* `reinterpret_cast`, which is a very brutal cast which changes the type into any other type, regardless of the compatibility of the two types considered. It is a dangerous one that should be considered only in very last resort (usually when interacting with a C library).


### Be wary of mathematical operators and types

It was already covered in the Hands-On but please bear in mind that this operation: `2/3` is an integer division, so the result will be an `int`, and might not be what you want.


```c++ vscode={"languageId": "cpp"}
{
    double a = 2 / 3;
    std::cout << a << std::endl;    // a == 0
}
```

If you really want a `float` division, you have to cast at least one of the value:

```c++ vscode={"languageId": "cpp"}
{
  double a = static_cast<float>(2) / 3;
  std::cout << a << std::endl;
  // this is also valid
  double b = 2. / 3;
  std::cout << b << std::endl;
}
```

Please note that the best would be to explicitly cast both values, to avoid useless implicit conversion.


## Characters and strings

### Historical strings

In C, a character string is literally an array of `char` variables, the last character of which is by convention the symbol `\0`.

The `strlen` function returns the length of a string, which is the number of characters between the very first character and the first occurrence of `\0`.

The `strcpy` function copies a character string to a new memory location; care must be taken to ensure that the destination is large enough to avoid any undefined behavior.

The `strncpy` function allows you to copy only the first <b>n</b> first characters, where <b>n</b> is the third parameter of the function. Same remark about the need to foresee a large enough destination.


```c++
#include <iostream>
#include <cstring>  // For strlen, strcpy, strncpy

char hello[] = {'h','e','l','l','o', '\0'};

char copy[6] = {}; // = {'\0','\0','\0','\0','\0','\0' };

strcpy(copy, hello);
std::cout << "String '" << copy << "' is " << strlen(copy) << " characters long." << std::endl;
```

```c++
const char* hi = "hi";  // Not putting the const here triggers a warning.
strncpy(copy, hi, strlen(hi));
copy[strlen(hi)] = '\0';  // Don't forget to terminate the string!
std::cout << "String '" << copy << "' is " << strlen(copy) << " characters long." << std::endl;

```

There are several other functions related to historical strings; for more information, do not hesitate to consult [this reference page](http://www.cplusplus.com/reference/cstring/).


### std::string

In modern C++, rather than bothering with character tables
which come from the C language, it's easier to use the type `std::string`, provided
through the standard language library, that provides a much simpler syntax:

```c++
#include <iostream>
#include <cstring>  // For strlen
#include <string>   // For std::string

const char* hello_str = "hello";
std::string hello = hello_str;
std::string hi("hi");
std::string copy {};
```

```c++
copy = hello; // please notice assignment is much more straightforward
std::cout << "String '" << copy << "' is " << copy.length() << " characters long." << std::endl;
```

```c++
const char* copy_str = copy.data();  // Returns a classic C-string (from C++11 onward)
std::cout << "String '" << copy_str << "' is " << strlen(copy_str) << " characters long." << std::endl;
```

```c++
const char* old_copy_str = &copy[0];  // Same before C++11... 
std::cout << "String '" << old_copy_str << "' is " << strlen(old_copy_str) << " characters long." << std::endl;
```

```c++
std::string dynamic {"dynamic std::string"};
std::cout << "String '" << dynamic << "' is " << dynamic.length() << " characters long." << std::endl;
```

```c++
dynamic = "std::string is dynamical and flexible";
std::cout << "String '" << dynamic << "' is " << dynamic.length() << " characters long." << std::endl;
```

If needed (for instance to interact with a C library) you may access to the underlying table with `c_str()` or `data()` (both are interchangeable):

```c++
#include <string>
{
    std::string cplusplus_string("C++ string!");
    
    const char* c_string = cplusplus_string.c_str();
    const char* c_string_2 = cplusplus_string.data();
}
```

The `const` here is important: you may access the content but should not modify it; this functionality is provided for read-only access.


FYI, C++17 introduced [std::string_view](https://en.cppreference.com/w/cpp/header/string_view) which is more efficient than `std::string` for some operations (it is presented [in appendix](../7-Appendix/StringView.ipynb) but if it's your first reading it's a bit early to tackle it now).


## Renaming types

Sometimes it may be handy to rename a type, for instance if you want to be able to change easily throughout the code the numeric precision to use. Historical syntax (up to C++ 11 and still valid) was `typedef`:

```c++
#include <iostream>
#include <iomanip>  // For std::setprecision

{
    typedef double real; // notice the ordering: new typename comes after its value
    
    real radius {1.};
    real area = 3.1415926535897932385 * radius * radius;
    std::cout <<"Area = " << std::setprecision(15) << area << std::endl;
}
```

In more modern C++ (C++11 and above), another syntax relying on `using` keyword was introduced; it is advised to use it as this syntax is more powerful in some contexts (see later with templates...):

```c++
#include <iostream>
#include <iomanip>  // For std::setprecision

{
    using real = float; // notice the ordering: more in line with was we're accustomed to when 
                        // initialising variables.

    real radius {1.};
    real area = 3.1415926535897932385 * radius * radius;
    std::cout <<"Area = " << std::setprecision(15) << area << std::endl;
}
```

## `decltype` and `auto`

C++ 11 introduced new keywords that are very handy to deal with types:

* `decltype` which is able to determine **at compile time** the underlying type of a variable.
* `auto` which determines automatically **at compile time** the type of an expression.

```c++
#include <vector>

{
    auto i = 5; // i is here an int.    
    auto j = 5u; // j is an unsigned int
    
    decltype(j) k; // decltype(j) is interpreted by the compiler as an unsigned int.
}
```

On such trivial examples it might not seem much, but in practice it might prove incredibly useful. Consider for instance the following versions of the code for iterating over a vector with an 'historical' `for` loop (the details don't matter: we'll deal with `std::vector` in a [later notebook](../5-UsefulConceptsAndSTL/3-Containers.ipynb)):

```c++
#include <vector>
#include <iostream>

{
    std::vector<unsigned int> primes { 2, 3, 5 };

    // C++ 03 way of iterating over the content of a vector.
    for (std::vector<unsigned int>::const_iterator it = primes.cbegin();
          it != primes.cend();
          ++it)
     {
         std::cout << *it << " is prime." << std::endl;
     }
}

```

It's very verbose; we could of course use an alias:

```c++
#include <vector>
#include <iostream>

{
    std::vector<unsigned int> primes { 2, 3, 5 };
    using iterator = std::vector<unsigned int>::const_iterator;
    
    // C++ 03 way of iterating over the content of a vector - with an alias
    for (iterator it = primes.cbegin();
         it != primes.cend();
         ++it)
    {
        std::cout << *it << " is prime." << std::endl;
    }
}

```

But with `decltype` we may write instead:

```c++
#include <vector>
#include <iostream>

{
    std::vector<unsigned int> primes { 2, 3, 5 };
    
    // C++ 11 decltype
    for (decltype(primes.cbegin()) it = primes.cbegin();
         it != primes.cend();
         ++it)
    {
        std::cout << *it << " is prime." << std::endl;
    }
}
```

or even better:

```c++
#include <vector>
#include <iostream>

{
     std::vector<unsigned int> primes { 2, 3, 5 };
    
     // C++ 11 auto
     for (auto it = primes.cbegin();
          it != primes.cend();
          ++it)
     {
         std::cout << *it << " is prime." << std::endl;
     }
}
```

That is not to say `decltype` is always inferior to `auto`: there are some cases in which decltype is invaluable (especially in metaprogramming, but it's mostly out of the scope of this lecture - we'll skim briefly over it in a later [notebook](../4-Templates/4-Metaprogramming.ipynb)). 

C++ 14 introduced a new one (poorly) called `decltype(auto)` which usefulness will be explained below:

```c++
#include <algorithm>
#include <iostream>

int i = 5;
int& j = i;

auto k = j;

if (std::is_same<decltype(j), decltype(k)>())
    std::cout << "j and k are of the same type." << std::endl;
else
    std::cout << "j and k are of different type." << std::endl;
```

```c++
if (std::is_same<decltype(i), decltype(k)>())
    std::cout << "i and k are of the same type." << std::endl;
else
    std::cout << "i and k are of different type." << std::endl;
```

Despite the `auto k = j`, j and k don't share the same type! The reason for this is that `auto` loses information about pointers, reference or constness in the process...

A way to circumvent this is `auto& k = j`.

`decltype(auto)` was introduced to fill this hole: contrary to `auto` it retains all these information:

```c++
#include <algorithm>
#include <iostream>

{
    int i = 5;
    int& j = i;
    
    decltype(auto) k = j;
    
    if (std::is_same<decltype(j), decltype(k)>())
        std::cout << "j and k are of the same type." << std::endl;
    else
        std::cout << "j and k are of different type." << std::endl;
}
```

Fore more details about `auto`'s type deduction, you can check the C++ weekly videos at [this link](https://www.youtube.com/watch?v=tn69TCMdYbQ) and also [this one](https://www.youtube.com/watch?v=E5L66fkNlpE) regarding `decltype(auto)`'s usage. 


### `auto` and string literals

**Beware:** when you declare a string literals with `auto`, the type deduction makes it a `const char*`, not a `std::string`:

```c++
#include <algorithm>
#include <string>
#include <iostream>

auto hello_str = "Hello world"; // declares a char*

std::cout << "Is 'hello_str' a const char*? " << std::boolalpha << std::is_same<decltype(hello_str), const char*>() << std::endl;
std::cout << "Is 'hello_str' a std::string? " << std::boolalpha << std::is_same<decltype(hello_str), std::string>() << std::endl;
```

C++ 14 introduced a suffix to facilitate declaration of a `std::string` from a string literals... but which requires to add a specific `using namespace` first (we will see that those are in a [much later notebook](../6-InRealEnvironment/5-Namespace.ipynb)). 

```c++
#include <string>

using namespace std::string_literals;

std::string hello_string("Hello world"); // the 'classic' way to define a std::string
auto hello_str = "Hello world"s;  // declares a std::string - requires first the using namespace directive

std::cout << "Is 'hello_string' a const char*? " << std::boolalpha << std::is_same<decltype(hello_string), const char*>() << std::endl;
std::cout << "Is 'hello_string' a std::string? " << std::boolalpha << std::is_same<decltype(hello_string), std::string>() << std::endl;

std::cout << "Is 'hello_str' a const char*? " << std::boolalpha << std::is_same<decltype(hello_str), const char*>() << std::endl;
std::cout << "Is 'hello_str' a std::string? " << std::boolalpha << std::is_same<decltype(hello_str), std::string>() << std::endl;
```

Not sure it it is entirely worth it (maybe when you define loads of `std::string` is a same file?) but you may see that syntax in an existing program.



[© Copyright](../COPYRIGHT.md)   




