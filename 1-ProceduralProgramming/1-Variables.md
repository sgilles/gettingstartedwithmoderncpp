---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Procedural programming](./0-main.ipynb) - [Predefined types](./1-Variables.ipynb)


## Ordinary variables


### Declaration


To be usable in a C++ program, a variable must be declared. This declaration shall include at least the type of
the variable, followed by its name and a semicolon.

```c++
#include <iostream>

{
    int number; // integer variable
    double real;  // floating-point variable

    std::cout << number << std::endl; 
    std::cout << real << std::endl;
}
```

### Initialisation

<!-- #region -->
Although not mandatory, it is **strongly** recommended to give an
initial value to your variables, as an expression between brackets. 

Not providing an initial value may lead to unexpected behaviour. For instance you can't make hypothesis upon the values of `number` and `real` in the cell above: you might end-up with any value... and someone else might get other values on their computer!


If you give braces without values, a predefined and associated value
to the type is used (usually a form of 0).
<!-- #endregion -->

```c++
{
    int nb1 { 1 }; // integer variable set with the value 1
    int nb2 {}; // same as int nb2{0};
    double pi { 3.14 };  // real variable
}
```

C++ actually supports many other historical forms
of initialization, which you will encounter everywhere, including in this tutorial,
with brackets and/or an equal sign. There are some subtle differences
between each other... that you can ignore most of the time (still, if you're beginning our advice is to take the habit to use braces which are both less ambiguous and a bit more secure than the others syntaxes).

```c++
{
    int a = 5;
    int b(5);
    int c { 5 }; // From C++ 11 onward; we advise you to use this syntax
}
```

In all cases, even if there is an equal sign, it is important to remember
that it is an initialization, not an assignment (this will be
important when we will define our own types).


### Assignment

A new value is stored in an existing variable using the assignment
operator `=`. The name of the variable is on the left; the expression
on the right of the `=` sign is evaluated, and its result is assigned to the variable.

```c++
#include <iostream> // for std::cout and std::endl

{
    int a {}, b {}, c {} ;  // default initialization; set the values to 0
    
    std::cout << "Default initialization: a = " << a << ", b = " << b << " and c = " << c << std::endl;

    a = 4;
    b = 7;
    c = a + b;
    
    std::cout << "After assignments: a = " << a << ", b = " << b << " and c = " << c << std::endl;
    
}   
```

Assignments may be chained:

```c++
{
    int a {}, b {}, c {};
    
    a = b = c = 5;    
    
    std::cout << "a = " << a << ", b = " << b << " and c = " << c << std::endl;
}
```

It is also possible to define (slightly) more advanced operators of assignments that modify the value currently stored by a simple operation:

```c++
#include <iostream>

{
    int a {}, b {}, c {} ;  // default initialization; set the values to 0
    
    a += 4; // add 4 to the current value of 'a'
    std::cout << "a = " << a << std::endl;
    a *= 7; // multiply current value of 'a' by 7
    std::cout << "a = " << a << std::endl;
    a /= 9; // divide 'a' by 9 and assign the quotient to 'a'
    std::cout << "a = " << a << std::endl;
    
}   
```

### Scope and blocks

A variable is destroyed when it becomes **out of scope**. 

The **scope** of a variable is its lifetime: it begins when the variable is declared and ends when it is destroyed, usually when we reach the end of the block where it is defined.

A **block** is essentially what is between braces `{}`.




```c++
#include <iostream>

int a = 5;

{
    std::cout << "a is available at this level: " << a << std::endl; 

    {
        int b = 10;
        std::cout << "b is available at this level: " << b << std::endl; 
        std::cout << "and a is also still available: " << a << std::endl;        
    } // b becomes out of scope
    
    std::cout << "a is available at this level: " << a << "... but b is not!" << std::endl; 
}
```

Within a same block, a variable name may be only used once:

```c++
{
    int a = 5;
    std::cout << "Value = " << a << std::endl;
    int a = 1; // COMPILATION ERROR
}
```

**Beware:** it is however entirely possible to reuse in an inner block a variable name... but it is to be avoided because it really clutters the understanding of the code for a reader! (and compilers usually warn against this, even if here Xeus-cling does not):

```c++
#include <iostream>

int a = 5;

{
    int a = 10;
    std::cout << "a is available at this level; it is the most inner scope one: " << a << std::endl; 

    {
        std::cout << "Of course same here: " << a << std::endl;
    }
    
    a = a + 5; // this is the inner most 'a' that is modified
} // inner most a becomes out if scope

std::cout << "Here the innermost 'a' got out of scope and therefore the best choice for 'a' "
     "is the initial one: " << a << std::endl;

```

As seen above, you can declare a block very easily by firing up opening and closing braces. This is an incredibly useful feature: you may thus **fine tune the lifetime of your variable** to ensure a variable is not used past its prime.


### Increment and decrement operators

Finally, C++ also provides a shortcut when value is either incremented or decremented by adding `++` or `--` before or after the name of the variable. 

* If the sign is placed before the variable, it is a **pre-increment**. 
* If the sign is placed after the variable, it is a **post-increment**. 

An example is the better way to explain the difference between both:


```c++
#include <iostream>

int a = 5;
int b = 3;

a++; // increment a by 1.
++a; // same, both are actually equivalents here.

int c = a + b;    
std::cout << "a = " << a << ", b = " << b << " and c = " << c << std::endl;
```

```c++
c = a-- + b; // first a + b is evaluated, and only then a is decremented.    
std::cout << "a = " << a << ", b = " << b << " and c = " << c << std::endl;
```

```c++
c = a + ++b; // first b is incremented, and only then a + b is evaluated.
std::cout << "a = " << a << ", b = " << b << " and c = " << c << std::endl;
```

Honestly it's usually better to remove any ambiguity by separating explicitly both operations:

```c++
#include <iostream>

{
    int a = 7;
    int b = 3;

    int c = a + b;
    std::cout << "a = " << a << ", b = " << b << " and c = " << c << std::endl;
        
    c = a + b;
    --a; // equivalent to a-- but for reasons related to the standard library I advise you 
         // to rather use the pre-increment form when both are equivalent.
    std::cout << "a = " << a << ", b = " << b << " and c = " << c << std::endl;
       
    ++b; // same: equivalent to b++;    
    c = a + b;
    std::cout << "a = " << a << ", b = " << b << " and c = " << c << std::endl;
}
```

<!-- #region -->
### Comparing values

As shown above, `=` is the assignment operator. To compare two values, the symbol to use is `==`.

Other comparison operators are:

| Operator      | Effect                                      |
|:------------- |:-------------------------------------------:|
| `a == b`      | `true` if a and b are equals                |
| `a != b`      | `true` if a and b are different             |
| `a < b`       | `true` if a is less than b                  |
| `a > b`       | `true` if a is greater than b               |
| `a >= b`       | `true` if a is greater than or equal to b  |
| `a <= b`       | `true` if a is less than or equal to b     |


These operators are defined for most ordinary types and may be defined for your own types (we'll see that [later](../3-Operators/2-Comparison.ipynb)).

<!-- #endregion -->

## References

A reference is a variable that acts as a kind of alias, and provides another name for the same variable.

When defining a reference, it must be **immediately** initialized by
indicating to which variable it should point; it cannot be changed after that.

The syntax is to add a `&` character just after the type:

```c++
#include <iostream> 

int a { 2 };
int b { a };
int& c { a }; // c is a reference to a
std::cout << "Initial values     : a = " << a << ", b = " << b << " and c = " << c << std::endl;
```

```c++
a = -7;
std::cout << "Modify a     : a = " << a << ", b = " << b << " and c = " << c << std::endl;
```

```c++
b = 42;
std::cout << "Modify b     : a = " << a << ", b = " << b << " and c = " << c << std::endl;
```

```c++
c = 0;
std::cout << "Modify c     : a = " << a << ", b = " << b << " and c = " << c << std::endl;
```

Reference is a purely C++ concept that doesn't exist in C.


## Pointers

A pointer contains the address in memory of another variable. It declares itself by slipping
a `*` character before the name. It can be initialized or not with the address
of another variable. To explicitly extract the address of this other variable,
we use the symbol `&`.



```c++
#include <iostream>

int a { 2 };
int* p {&a}; // define a pointer p which is initialized with the address of a
    
std::cout << "a = " << a << std::endl;
std::cout << "p = " << p << std::endl;
```

You may ask the underlying value at a given address with `*` (I reckon: this syntax may be _very_ confusing at first...)

```c++
std::cout << "Value stored at p = " << *p << std::endl;
```

The `*` syntax may be used to modify the underlying value:

```c++
*p = *p + 5;
std::cout << "After the operation: pointer " << p << " stores the value " << *p << std::endl;
```

Pointers may be used as variables by themselves and see their actual content changed during execution of a program:

```c++
int b { 3 };
p = &b;
std::cout << "After the pointer assignation: pointer " << p << " stores the value " << *p << std::endl;
std::cout << "(and value of a remains unchanged: " << a << ')' << std::endl;
```

We can therefore see the pointer as a kind of redefinable reference (pointers are a feature from C language whereas references are a C++ specific feature).


### Cheatsheet: pointers and reference syntax



| Applied to:      |            A type `T`                        | A variable `x`
|:------------- |:-------------------------------------------:|:-------------------------------------------:|
|  *   | Pointer to an object of type `T`             | Variable under `x` if `x` is a pointer, invalid code otherwise |
|  & | Reference to an object of type `T` | Address of the variable (i.e. a pointer) |



### Chaining pointers

It is possible to chain pointers:

```c++
#include <iostream>
{
    int n { 5 };
    int* p { &n };
    int** q { &p };
    
    std::cout << "*q is a pointer to an int (i.e. an address): " << *q << " - it is the same as what is stored in p: " << p << std::endl;
    std::cout << "**q gives the original `n` value: " << **q << std::endl;
}
```

This was something that was very common in C, but that I do not recommend in C++ as there are much better ways to achieve the same goals (that we do not have seen yet, so don't panic if you do not understand why we would need this).


### `nullptr`

A pointer can also designate no variables if initialized with the special value
 `nullptr`. It is then a mistake to try to access its pointed value (as we shall see later, an [`assert`](../5-UsefulConceptsAndSTL/1-ErrorHandling.ipynb#Assert) is a good idea to ensure we do not try to dereference a `nullptr` value).

It is strongly recommended to initialize a pointer when creating it: if you define an uninitialized pointer, it points to an arbitrary area of memory, which can create undefined behaviors that are not necessarily reproducible.

If the pointed area is known at initialization and never changes throughout the program, you should consider a reference rather than a pointer.

```c++
#include <iostream>

{    
    int* p { nullptr }; // define a null pointer p

    std::cout << "\t p: address = " << p << ", value = " << *p << std::endl; // Dereferencing p is misguided!
}
```

`nullptr` was introduced in C++ 11; if you're working with a legacy codebase that doesn't use this standard use `NULL` (but stick to `nullptr` for modern code!)


## Constant variables and pointers

When declaring a variable of a predefined type, it is possible to specify its value can't be changed afterward by using the word `const` which may be placed before or after the type:

```c++
const double pi { 3.1415927 };
double const pi_2 { 3.1415927 }; // equally valid; it is just a matter of taste. Mine is to put it before,
                                 // so that is what you will see in the remaining of the lecture.
                                 // There are however very persuasive arguments for the other convention: 
                                 // see http://slashslash.info/eastconst/
```

```c++
pi = 5.; // COMPILATION ERROR!
```

```c++
pi_2 = 7.; // COMPILATION ERROR!
```

In the case of a pointer, we can declare the pointer itself constant, and/or the value pointed to depending on whether we place the keyword `const` before or after the type name (in this case it applies to the pointed value) or after the character `*` (in this case it applies to the pointer itself):

```c++
{
    int a { 2 }, b { 3 };
    int* p { &a }; // Both pointer and pointed values are modifiable.
    
    p = &b; // OK
    *p = 5; // OK     
}
```

```c++
int a { 2 }, b { 3 };
int* const p { &a }; // Value is modifiable, but not the address pointed to.

*p = 5; // OK     
```

```c++
p = &b; // COMPILATION ERROR 
```

```c++
const int* p { &a }; // Address pointed to is modifiable, but not the underlying value.
p = &b; // OK
```

```c++
*p = 5; // COMPILATION ERROR    
```

```c++
int a { 2 }, b { 3 };
const int* const p { &a }; // Nothing is modifiable
    
p = &b; // COMPILATION ERROR
```

```c++
*p = 5; // COMPILATION ERROR    

```


**IMPORTANT**: Even if declared `const`, the pointed value is
not intrinsically constant. It just can't be
modified through this precise pointer.
If other variables reference the same memory area and 
are not constant, they are able to modify the value:


```c++
#include <iostream>

{
    int a { 2 }, b { 3 };
    const int* p { &a }; // Address pointed to is modifiable, but not the underlying value.
    
    std::cout << "Value pointed by pointer p (which doesn't allow value modification) is: " << *p << std::endl;
    
    int* p2 {&a}; // Pointer to the same memory area, but no constness here.
    *p2 = 10;
    
    std::cout << "Value pointed by pointer p (and modified through p2) is: " << *p << std::endl;    
    
    a = -3;    
    std::cout << "Value pointed by pointer p (and modified through variable directly) is: " << *p << std::endl;    

}
```

On the other hand, pointers can't be used as a work-around to modify a constant value:

```c++
const int n { 3 };
```

```c++
int* p { &n }; // COMPILATION ERROR
```

```c++
const int* p_n2 { &n }; // OK
```

## Arrays

The operator `[]` enables the creation of an array.

**Beware:** In C++, the indexes of an array start at 0.

```c++
#include <iostream>

{
    int i[10] ; // Array of 10 integers - not initialised properly!
    double x[3] = { 1., 2., 3. }; // Array of 3 reals, C++ 11 syntax
    
    std::cout << "i[2] = " << i[2] << " (may be gibberish: undefined behaviour due to lack of initialization!)" << std::endl;
    std::cout << "i[10] = " << i[10] << " (undefined behaviour: out of range. Warning identifies the issue)" << std::endl ;
    std::cout << "x[1] = " << x[1] << " (expected: 2.)" << std::endl ;
}
```

Multi-dimensional arrays are also possible:

```c++
#include <iostream>

{
    int k[2][3] = { { 5, 7, 0 }, { 3, 8, 9 } };
    
    std::cout << "k[0][0] = " << k[0][0] << " (expected: 5)" << std::endl;
    std::cout << "k[1][2] = " << k[1][2] << " (expected: 9)" << std::endl;
    
    int l[2][3] = {};  // default initialization of all elements of the array.
    std::cout << "l[0][0] = " << l[0][0] << " (expected: 0)" << std::endl;
    std::cout << "l[1][2] = " << l[1][2] << " (expected: 0)" << std::endl;
    
}
```

**IMPORTANT**: so far we have considered only the case of _static_ arrays, for which the size is already known at compilation. We will deal with dynamic ones [later in this tutorial](./5-DynamicAllocation.ipynb#Arrays-on-heap) (and also with the standard libraries [alternatives](../5-UsefulConceptsAndSTL/3-Containers.ipynb) such as `std::vector` or `std::array` which are actually much more compelling).


### Arrays and pointers

The variable designating an array is similar to a constant pointer pointing to the beginning of the array. Increasing this pointer is like moving around in the array.


```c++
#include <iostream>

int i[2] = { 10, 20 } ;
```

```c++
std::cout << *i << " (expected: 10)" << std::endl;
```

```c++
std::cout << *i + 1 << " (expected: 11)" << std::endl;
```

```c++
std::cout << *(i + 1) << " (expected: 20)" << std::endl;
```

```c++
int* j = i; // OK!
++j; // OK!
std::cout << "Value pointed by j is " << *j << " (expected: 20)" << std::endl;

```


[© Copyright](../COPYRIGHT.md)   




