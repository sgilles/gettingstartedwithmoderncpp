#include <cmath>
#include <iostream>
#include <sstream>
#include <string>


/************************************/
// Declarations
/************************************/

//! Structure in charge of holding numerator and exponent data together.
class PowerOfTwoApprox
{

public:

    PowerOfTwoApprox(int Nbits, double value);

    //! Compute the best possible approximation of `value` with `Nbits`
    //! \return The approximation as a floating point.    
    double Compute(int Nbits, double value);

    //! Operator for explicit cast to double.
    explicit operator double() const;

    //! Accessor to numerator.
    int GetNumerator() const;

    //! Accessor to exponent.
    int GetExponent() const;

private:

    int numerator_ {};
    int exponent_ {};
};

/*! 
* \brief Multiply the approximate representation by an integer. 
* 
* \param[in] coefficient Integer coefficient by which the object is multiplied.
* 
* \return An approximate integer result of the multiplication.
*/
int operator*(int coefficient, const PowerOfTwoApprox& approx);

//! To enable commutation for operator*
int operator*(const PowerOfTwoApprox& approx, int coefficient);


//! Abstract class to display
class TestDisplay
{
public:
    
    //! Constructor which sets the only the resolution (i.e. the maximum index upon which error is defined).
    TestDisplay(int resolution);
    
    //! To make TestDisplay an abstract class.
    virtual ~TestDisplay() = 0;

    //! Get the resolution.
    int GetResolution() const;

    //! Pure virtual method Do().
    virtual void Do(int Nbits) const = 0;

    //! Stupid method to silence the -Wweak-vtables warning - at least one virtual method must be defined outside the class declaration, and virtual method definition unfortunately doesn't count.
    virtual void Unused() const;

protected:

    //! Convenient enum used in \a PrintLine().
    enum class RoundToInteger { no, yes };

    
   /*!
    * \brief  Print a line with information about error.
    *
    * \param[in] optional_string1 String that might appear just after the "[With N bits]:".
    * \param[in] optional_string2 String that might appear just before the "[error = ...]".
    * \param[in] do_round_to_integer If yes, the exact result is approximated as an integer.
    */
   void PrintLine(int Nbits, double exact, double approx,
                  RoundToInteger do_round_to_integer = RoundToInteger::no,
                  std::string optional_string1 = "", std::string optional_string2 = "") const;
                  
private:
    
    //! Resolution.
    const int resolution_; // `const` ensures here that it is defined in the constructor!
  
};



//! Class in charge of the display of a given `PowerOfTwoApprox`.
class TestDisplayPowerOfTwoApprox : public TestDisplay
{
public:

    /*!
    * \brief Constructor.
    *
    * Default constructor wouldn't have work, as base class doesn't provide a default constructor
    * and current class needs to construct base class first.
    */
    TestDisplayPowerOfTwoApprox(int resolution);

    //! Destructor. 
    // `virtual` keyword is optional; it doesn't matter much (method would be virtual anyway)
    // `override` keyword is optional but you really should put all the time (if you're using C++ 11 or above).
    virtual ~TestDisplayPowerOfTwoApprox() override;
    
    //! Display the output for the chosen `Nbits`.
    virtual void Do(int Nbits) const override;
        
protected: // as we want inherited class to access it!
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value) const;
    
};


//! Class in charge of the display of the sum of 2 `PowerOfTwoApprox` with real coefficients.
class TestDisplaySumOfMultiply : public TestDisplay
{
public:

    //! Constructor.
    TestDisplaySumOfMultiply(int resolution);

    //! Destructor. 
    virtual ~TestDisplaySumOfMultiply() override;
    
    //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const override;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const;
    
};


//! Class in charge of displaying information regarding 0.65
class TestDisplayPowerOfTwoApprox065 : public TestDisplayPowerOfTwoApprox
{
public:

    //! Constructor
    TestDisplayPowerOfTwoApprox065(int resolution);

    //! Destructor
    virtual ~TestDisplayPowerOfTwoApprox065() override;

    //! Override of the method that does the work.
    void Do(int Nbits) const override;

};


//! Class in charge of displaying information regarding 0.35
class TestDisplayPowerOfTwoApprox035 : public TestDisplayPowerOfTwoApprox
{
public:

    TestDisplayPowerOfTwoApprox035(int resolution);

    virtual ~TestDisplayPowerOfTwoApprox035() override;

    void Do(int Nbits) const override;

};


//! Class intended to store several `TestDisplay` objects.
class TestDisplayContainer
{
public:

    //! Constructor
    //! \param[in] capacity Maximum number of items that might be stored in the container.
    TestDisplayContainer(std::size_t capacity);

    //! Destructor - as we will need to free properly the memory allocated on destruction.
    ~TestDisplayContainer();

    //! Add a new test_display_register.
    //! At each call, the item to be registered is put at the first available position and internal current_position_
    //! is incremented. If the end-user attempts to register more than three items, the Error() function is called.
    void Register(TestDisplay* test_display);

    //! Return the actual number of elements in the container.
    std::size_t GetSize() const;

    //! Returns the i-th element. If element is invalid, call `Error()`.     
    const TestDisplay& operator[](std::size_t i) const;
    
private:

    std::size_t GetCapacity() const;
    
private:
    
    //! List of all known `TestDisplay` objects.
    TestDisplay** list_;
    
    //! Index to place the next register object. If '3', no more object may be registered.
    std::size_t current_position_ {};

    //! Capacity.
    const std::size_t capacity_;
};

    
//! Returns `number` * (2 ^ `exponent`) 
int TimesPowerOf2(int number, int exponent);

//! Round `x` to the nearest integer.
int RoundAsInt(double x);

//! Function for error handling. We will see later how to fulfill the same functionality more properly.
//! Don't bother here about [[noreturn]] - it's just a keyword to silence a possible warning telling
//! the program may not return at the calling site (which is definitely the case here as there is a 
//! std::exit() called in the function).
[[noreturn]] void Error(std::string explanation);

//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! We'll see much later that it is typically the kind of function that could be put in an anonymous namespace.
void HelperComputePowerOf2Approx(double value, int exponent, int& numerator, int& denominator);

//! Maximum integer that might be represented with `nbits` bits.  
int MaxInt(int nbits);

//! Function which loops over the number of bits and call the displays.
void Loop(int initial_Nbit, int final_Nbit, int increment_Nbit, const TestDisplayContainer& container);


/************************************/
// Definitions
/************************************/

int TimesPowerOf2(int number, int exponent)
{
    // Very crude implementation that is not safe enough - we'll remedy this later...
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    

int RoundAsInt(double x)
{
    // Very crude implementation that is not safe enough - we'll remedy this later...
    return static_cast<int>(std::round(x));
}


[[noreturn]] void Error(std::string explanation)
{
    std::cout << "ERROR: " << explanation << std::endl;
    exit(EXIT_FAILURE);
}


void HelperComputePowerOf2Approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = TimesPowerOf2(1, exponent);   
    numerator = RoundAsInt(value * denominator);
}


TestDisplayPowerOfTwoApprox::TestDisplayPowerOfTwoApprox(int resolution)
: TestDisplay(resolution)
{ }


TestDisplayPowerOfTwoApprox::~TestDisplayPowerOfTwoApprox()  = default;


void TestDisplayPowerOfTwoApprox::Do(int Nbits) const
{
    Display(Nbits, 0.65);
    Display(Nbits, 0.35);    
}


void TestDisplayPowerOfTwoApprox::Display(int Nbits, double value) const
{
    PowerOfTwoApprox approximation(Nbits, value);

    const double double_quotient = static_cast<double>(approximation);
    
    std::ostringstream oconv;
    oconv << " (" << approximation.GetNumerator() << " / 2^" << approximation.GetExponent() << ")";

    PrintLine(Nbits, value, double_quotient, RoundToInteger::no, "", oconv.str());
    // < here you can't use the default argument for `optional_string1` in `PrintLine` declaration,
    // < as there are non default arguments after it.
}


int MaxInt(int nbits)
{ 
    return (TimesPowerOf2(1, nbits) - 1);
}


PowerOfTwoApprox::PowerOfTwoApprox(int Nbits, double value)
{
    int max_numerator = MaxInt(Nbits);
    
    auto& numerator = numerator_; // alias!
    auto& exponent = exponent_; // alias!

    int denominator {};
    
    do
    {
        // I used here the prefix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperComputePowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    // After the while loop we have numerator > max_numerator,  
    // hence we need to update the fraction using the previous exponent with --exponent.
    HelperComputePowerOf2Approx(value, --exponent, numerator, denominator);
}


PowerOfTwoApprox::operator double() const
{
   int denominator = TimesPowerOf2(1, exponent_);
   return static_cast<double>(GetNumerator()) / denominator;
}


int PowerOfTwoApprox::GetNumerator() const
{
    return numerator_;
}


int PowerOfTwoApprox::GetExponent() const
{
    return exponent_;
}


int operator*(int coefficient, const PowerOfTwoApprox& approx)
{
    return TimesPowerOf2(approx.GetNumerator() * coefficient, -approx.GetExponent());
}


int operator*(const PowerOfTwoApprox& approx, int coefficient)
{
    return coefficient * approx;
}


TestDisplaySumOfMultiply::TestDisplaySumOfMultiply(int resolution)
: TestDisplay(resolution)
{ }

TestDisplaySumOfMultiply::~TestDisplaySumOfMultiply() = default;


void TestDisplaySumOfMultiply::Do(int Nbits) const
{
    Display(Nbits, 0.65, 3515, 0.35, 4832);    
}


void TestDisplaySumOfMultiply::Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const
{
    double exact = value1 * coefficient1 + value2 * coefficient2;

    auto approximation1 = PowerOfTwoApprox(Nbits, value1); // auto-to-stick syntax for constructor
    auto approximation2 = PowerOfTwoApprox(Nbits, value2);

    int computed_approx = approximation1 * coefficient1 + coefficient2 * approximation2;

    std::ostringstream oconv;
    oconv << value1 << " * " << coefficient1 
        << " + " << value2 << " * " << coefficient2 << " = ";

    PrintLine(Nbits, exact, computed_approx, RoundToInteger::yes, oconv.str());
    // < here we use the default value for the 6-th argument `optional_string2`
}


TestDisplay::TestDisplay(int resolution)
: resolution_(resolution)
{ }


TestDisplay::~TestDisplay() = default;


void TestDisplay::PrintLine(int Nbits, double exact, double approx,
                            RoundToInteger do_round_to_integer,
                            std::string optional_string1, std::string optional_string2) const
{
    int error = RoundAsInt(GetResolution() * std::fabs(exact - approx) / exact);
    
    std::cout << "[With " << Nbits << " bits]: " << optional_string1
        << (do_round_to_integer == RoundToInteger::yes ? RoundAsInt(exact) : exact) << " ~ " << approx
        << optional_string2
        << "  [error = " << error << "/" << GetResolution() << "]" 
        << std::endl;    
}


int TestDisplay::GetResolution() const
{
    return resolution_;
}


void TestDisplay::Unused() const
{
    // We could have left this method empty... The error handling is just to tell user that might think this method
    // calls does anything that it doesn't.
    // In production code I would have used `assert(false && "This method is defined to address the Wweak-vtables 
    // warning; it is not intended to be called!");`; we'll see that later in ErrorHandling notebook.
    Error("This method is defined to address the Wweak-vtables warning; it is not intended to be called!");
}


TestDisplayPowerOfTwoApprox065::TestDisplayPowerOfTwoApprox065(int resolution)
: TestDisplayPowerOfTwoApprox(resolution)
{ }

TestDisplayPowerOfTwoApprox065::~TestDisplayPowerOfTwoApprox065() = default;

TestDisplayPowerOfTwoApprox035::TestDisplayPowerOfTwoApprox035(int resolution)
: TestDisplayPowerOfTwoApprox(resolution)
{ }

TestDisplayPowerOfTwoApprox035::~TestDisplayPowerOfTwoApprox035() = default;


void TestDisplayPowerOfTwoApprox065::Do(int Nbits) const
{
    Display(Nbits, 0.65);
}

void TestDisplayPowerOfTwoApprox035::Do(int Nbits) const
{
    Display(Nbits, 0.35);
}


TestDisplayContainer::TestDisplayContainer(std::size_t capacity)
: capacity_ { capacity }
{ 
    list_ = new TestDisplay*[capacity];

    // Allocate nullptr by default is better than undefined behaviour.
    for (auto index = 0ul; index < capacity; ++index)
        list_[index] = nullptr;
}


TestDisplayContainer::~TestDisplayContainer()
{
    for (auto index = 0ul; index < capacity_; ++index)
        delete list_[index];

    delete[] list_; // don't forget the [] to delete an array!
}


void TestDisplayContainer::Register(TestDisplay* test_display)
{
    // Same remark as previously about the sanity check, which would be now:
    // assert(current_position_ <= GetCapacity());

    if (current_position_ == GetCapacity())
        Error("There are already three elements stored in the contained; can't take more!");

    list_[current_position_] = test_display;    
    ++current_position_;
}


void Loop(int initial_Nbit, int final_Nbit, int increment_Nbit, const TestDisplayContainer& container)
{
    // By initializing a local variable `size` in the init part of the loop, we enable more optimizations
    // by the compiler than if we just use `i < GetSize()` as second argument of the for loop.
    for (auto i = 0ul, size = container.GetSize(); i < size; ++i)
    {
        const auto& current_element = container[i];

        for (int nbits = initial_Nbit; nbits <= final_Nbit; nbits += increment_Nbit)
            current_element.Do(nbits);

        std::cout << std::endl;
    }
}


std::size_t TestDisplayContainer::GetCapacity() const
{
    return capacity_;
}


std::size_t TestDisplayContainer::GetSize() const
{
    return current_position_;
}


const TestDisplay& TestDisplayContainer::operator[](std::size_t i) const
{
    if (i >= GetSize())
        Error("You requested an element out of bounds!");

    // In real code I would also put sanity check
    // assert(list_[i] != nullptr); 
    // which should be true if first condition is right!

    return *(list_[i]);
}


/************************************/
// Main function
/************************************/

// [[maybe_unused]] is a C++ 17 keyword to indicate we're fully aware the variable may not be used.
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)     
{
    TestDisplayContainer container(3ul);

    container.Register(new TestDisplayPowerOfTwoApprox065(100000000));
    container.Register(new TestDisplayPowerOfTwoApprox035(100000000));
    container.Register(new TestDisplaySumOfMultiply(1000000));
    
    Loop(4, 16, 4, container);
    
    return EXIT_SUCCESS;
}

