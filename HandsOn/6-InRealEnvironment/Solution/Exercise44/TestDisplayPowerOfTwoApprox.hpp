#pragma once

#include "TestDisplay.hpp"


//! Class in charge of the display of a given `PowerOfTwoApprox`.
template<typename IntT>
class TestDisplayPowerOfTwoApprox : public TestDisplay
{
public:

    /*!
    * \brief Constructor.
    *
    * Default constructor wouldn't have work, as base class doesn't provide a default constructor
    * and current class needs to construct base class first.
    */
    TestDisplayPowerOfTwoApprox(int resolution);

    //! Destructor. 
    // `virtual` keyword is optional; it doesn't matter much (method would be virtual anyway)
    // `override` keyword is optional but you really should put all the time (if you're using C++ 11 or above).
    virtual ~TestDisplayPowerOfTwoApprox() override;
    
    //! Display the output for the chosen `Nbits`.
    virtual void operator()(int Nbits) const override;
        
protected: // as we want inherited class to access it!
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value) const;
    
};




//! Class in charge of displaying information regarding 0.65
template<typename IntT>
class TestDisplayPowerOfTwoApprox065 : public TestDisplayPowerOfTwoApprox<IntT>
{
public:

    //! Convenient alias to the parent.
    //! We could have avoided it by repeating `TestDisplayPowerOfTwoApprox<IntT>` 
    //! but using such an alias (called a 'trait' - we'll see that shortly)
    //! is incredibly more convenient the day you have to refactor and for instance add
    //! another template parameter.
    using parent = TestDisplayPowerOfTwoApprox<IntT>;

    //! Constructor
    TestDisplayPowerOfTwoApprox065(int resolution);

    //! Destructor
    virtual ~TestDisplayPowerOfTwoApprox065() override;

    //! Override of the method that does the work.
    void operator()(int Nbits) const override;

};


//! Class in charge of displaying information regarding 0.35
template<typename IntT>
class TestDisplayPowerOfTwoApprox035 : public TestDisplayPowerOfTwoApprox<IntT>
{
public:

    using parent = TestDisplayPowerOfTwoApprox<IntT>;

    TestDisplayPowerOfTwoApprox035(int resolution);

    virtual ~TestDisplayPowerOfTwoApprox035() override;

    void operator()(int Nbits) const override;

};

#include "TestDisplayPowerOfTwoApprox.hxx"
