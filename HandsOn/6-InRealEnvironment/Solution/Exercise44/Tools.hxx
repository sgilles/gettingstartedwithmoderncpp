#pragma once

#include <sstream>

#include "Exception.hpp"


template<class IntT>
IntT TimesPowerOf2(IntT number, int exponent)
{
    constexpr IntT two = static_cast<IntT>(2);

    while (exponent > 0)
    { 
        IntT product;
        if (__builtin_mul_overflow(number, two, &product))
            throw Exception("Overflow! (in TimesPowerOf2())");

        number = product;
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= two;
        exponent += 1 ; 
    }
    
    return number;
}
    

template<class IntT>
IntT RoundAsInt(double x)
{
    constexpr auto min = static_cast<double>(std::numeric_limits<IntT>::lowest());
    constexpr auto max = static_cast<double>(std::numeric_limits<IntT>::max());

    if (x <= min || x >= max)
    {
        std::ostringstream oconv;
        oconv << "Double '" << x << "' can't be rounded as an integer!";
        throw Exception(oconv.str());
    }

    return static_cast<IntT>(std::round(x));
}



template<typename IntT>
IntT MaxInt(int nbits)
{ 
    return (TimesPowerOf2(static_cast<IntT>(1), nbits) - static_cast<IntT>(1));
}


template<typename IntT>
void DisplayInteger(std::ostream& out, IntT integer)
{
    if constexpr(std::is_same<IntT, char>() || std::is_same<IntT, unsigned char>())
        out << static_cast<int>(integer);
    else
        out << integer;
}


