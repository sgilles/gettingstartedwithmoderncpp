#pragma once

#include <string>

// Forward declaration.
class Exception;


//! Abstract class to display
class TestDisplay
{
public:
    
    //! Constructor which sets the only the resolution (i.e. the maximum index upon which error is defined).
    TestDisplay(int resolution);
    
    //! To make TestDisplay an abstract class.
    virtual ~TestDisplay() = 0;

    //! Get the resolution.
    int GetResolution() const;

    //! Pure virtual method Do().
    virtual void operator()(int Nbits) const = 0;

    //! Stupid method to silence the -Wweak-vtables warning - at least one virtual method must be defined outside the class declaration, and virtual method definition unfortunately doesn't count.
    virtual void Unused() const;

protected:

    //! Convenient enum used in \a PrintLine().
    enum class RoundToInteger { no, yes };

    
   /*!
    * \brief  Print a line with information about error.
    *
    * \param[in] optional_string1 String that might appear just after the "[With N bits]:".
    * \param[in] optional_string2 String that might appear just before the "[error = ...]".
    * \param[in] do_round_to_integer If yes, the exact result is approximated as an integer.
    */
   template<typename IntT>
   void PrintLine(int Nbits, double exact, double approx,
                  RoundToInteger do_round_to_integer = RoundToInteger::no,
                  std::string optional_string1 = "", std::string optional_string2 = "") const;

    //! Method to call when an overflow was found.
    //! We didn't go into such precision here, but it might have idea to define a specific 
    //! `Overflow` exception that derives from `Exception` - here we assume all `Exception` 
    //! are overflows.
    void PrintOverflow(int Nbits, const Exception& e) const;                  

private:
    
    //! Resolution.
    const int resolution_; // `const` ensures here that it is defined in the constructor!
  
};

#include "TestDisplay.hxx"
