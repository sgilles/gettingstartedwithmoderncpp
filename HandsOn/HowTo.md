---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [How to do the hands-on](./HowTo.ipynb)

<!-- #region -->
## How to do the exercises?

Hands-on are interspersed throughout the lecture, and are really recommended as it is a way to check you truly assimilated what was explained in the notebooks (in programming rather often things seem easy... until you try to do them by yourself!)

There are basically three ways to do the exercises, but I will start with the one to avoid:

### Don't do the hands-on in Jupyter notebook!

Jupyter notebooks are a great tool to provide a decent and interactive support for lectures, and we can really thank the [guys behind Xeus-cling](https://github.com/QuantStack/xeus-cling/graphs/contributors) for their amazing work to port C++ into an interpreter-like environment.

However, it is clearly not an environment for direct development: the fact you need to restart the kernel and rerun all relevant cells each time you modify a function is extremely cumbersome during the trial-and-error that is often the writing of code.

### Online compilers

[Coliru](https://coliru.stacked-crooked.com/) or [Wandbox](https://wandbox.org/) provides online compilers in which you may paste code, compile it and then run it.

They are clearly the easiest way to go (except for the [hands-on related to file structure](../6-InRealEnvironment/2b-hands-on.ipynb) for which we will need to work locally). 

### On your local machine

This implies a _recent_ compiler is already installed on your machine and ready to use.

I provide in the folder in which hands-ons are set a very basic and crude `CMakeLists.txt` that should provide compilation (at least on Unix systems).

#### How to compile and run the program?

First create a folder in which the executables will be built and initialize CMake here:

```
mkdir -p build
cd build
cmake ..
```

Now in this folder you may run `make` to build the executables.

At first only `initial_file.cpp` is compiled into a `initial` executable.

You may run it through:

`./initial`

and if everything is alright you should see (in the case of the [first hands-on](../1-ProceduralProgramming/2b-hands-on.ipynb#EXERCISE-1:-Adding-a-loop)):

```
0.65 ~ 1 / 2^1
0.65 ~ 3 / 2^2
0.65 ~ 5 / 2^3
```

Each time a change is made or a new file is added in `CMakeLists.txt`, you just need to run again `make` command.

#### For each exercise

The idea is to start from `initial_file.cpp` and modify it slightly in each exercise.

You need therefore to copy this file and create a new executable; you have basically two options:

* Using one file for all exercises in a given thematic. The pro is that you won't need to edit the CMakeLists.txt more than once, the con is that you don't keep the intermediate states of your work.
* Use a file per exercise; when beginning a new one you need to:
    * Copy the former one into new cpp file, e.g. `cp exercise1.cpp exercise2.cpp`.
    * Edit the CMakeLists.txt and uncomment the relevant line, e.g. `add_executable(exercise2 exercise2.cpp)`

I have done the latter but feel free to do the former if you wish.


### With Docker

I also provide a [Docker](https://www.docker.com/) image in which a very basic Fedora environment is displayed.

You may compile / run the code in a Docker container and edit your file in your computer environment with you favorite editor / IDE.

The steps to compile are the same as presented in the previous section.

For more information about the Docker way please look the dedicated [README](README.md) (the one within the _HandsOn_ folder of the root directory).
<!-- #endregion -->


[© Copyright](../COPYRIGHT.md)   


