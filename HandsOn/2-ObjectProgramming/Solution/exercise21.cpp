#include <cmath>
#include <iostream>
#include <sstream>
#include <string>


/************************************/
// Declarations
/************************************/

//! Structure in charge of holding numerator and exponent data together.
class PowerOfTwoApprox
{

public:

    PowerOfTwoApprox(int Nbits, double value);

    //! Compute the best possible approximation of `value` with `Nbits`
    //! \return The approximation as a floating point.    
    double Compute(int Nbits, double value);

    //! \return The approximation as a floating point.    
    double AsDouble() const;

    //! Accessor to numerator.
    int GetNumerator() const;

    //! Accessor to exponent.
    int GetExponent() const;

    /*! 
     * \brief Multiply the approximate representation by an integer. 
     * 
     * \param[in] coefficient Integer coefficient by which the object is multiplied.
     * 
     * \return An approximate integer result of the multiplication.
     */
    int Multiply(int coefficient) const;

private:

    int numerator_ {};
    int exponent_ {};
};


//! Abstract class to display
class TestDisplay
{
public:
    
    //! Constructor which sets the only the resolution (i.e. the maximum index upon which error is defined).
    TestDisplay(int resolution);
    
    //! To make TestDisplay an abstract class.
    virtual ~TestDisplay() = 0;

    //! Get the resolution.
    int GetResolution() const;

    //! Stupid method to silence the -Wweak-vtables warning - at least one virtual method must be defined outside the class declaration, and virtual method definition unfortunately doesn't count.
    virtual void Unused() const;

protected:

    //! Convenient enum used in \a PrintLine().
    enum class RoundToInteger { no, yes };

    
   /*!
    * \brief  Print a line with information about error.
    *
    * \param[in] optional_string1 String that might appear just after the "[With N bits]:".
    * \param[in] optional_string2 String that might appear just before the "[error = ...]".
    * \param[in] do_round_to_integer If yes, the exact result is approximated as an integer.
    */
   void PrintLine(int Nbits, double exact, double approx,
                  RoundToInteger do_round_to_integer = RoundToInteger::no,
                  std::string optional_string1 = "", std::string optional_string2 = "") const;
                  
private:
    
    //! Resolution.
    const int resolution_; // `const` ensures here that it is defined in the constructor!
  
};



//! Class in charge of the display of a given `PowerOfTwoApprox`.
class TestDisplayPowerOfTwoApprox : public TestDisplay
{
public:

    /*!
    * \brief Constructor.
    *
    * Default constructor wouldn't have work, as base class doesn't provide a default constructor
    * and current class needs to construct base class first.
    */
    TestDisplayPowerOfTwoApprox(int resolution);

    //! Destructor. 
    // `virtual` keyword is optional; it doesn't matter much (method would be virtual anyway)
    // `override` keyword is optional but you really should put all the time (if you're using C++ 11 or above).
    virtual ~TestDisplayPowerOfTwoApprox() override;
    
    //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value) const;
    
};


//! Class in charge of the display of the sum of 2 `PowerOfTwoApprox` with real coefficients.
class TestDisplaySumOfMultiply : public TestDisplay
{
public:

    //! Constructor.
    TestDisplaySumOfMultiply(int resolution);

    //! Destructor. 
    virtual ~TestDisplaySumOfMultiply() override;
    
    //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const;
    
};

    
//! Returns `number` * (2 ^ `exponent`) 
int TimesPowerOf2(int number, int exponent);

//! Round `x` to the nearest integer.
int RoundAsInt(double x);

//! Function for error handling. We will see later how to fulfill the same functionality more properly.
//! Don't bother here about [[noreturn]] - it's just a keyword to silence a possible warning telling
//! the program may not return at the calling site (which is definitely the case here as there is a 
//! std::exit() called in the function).
[[noreturn]] void Error(std::string explanation);

//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! We'll see much later that it is typically the kind of function that could be put in an anonymous namespace.
void HelperComputePowerOf2Approx(double value, int exponent, int& numerator, int& denominator);

//! Maximum integer that might be represented with `nbits` bits.  
int MaxInt(int nbits);




/************************************/
// Definitions
/************************************/

int TimesPowerOf2(int number, int exponent)
{
    // Very crude implementation that is not safe enough - we'll remedy this later...
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    

int RoundAsInt(double x)
{
    // Very crude implementation that is not safe enough - we'll remedy this later...
    return static_cast<int>(std::round(x));
}


[[noreturn]] void Error(std::string explanation)
{
    std::cout << "ERROR: " << explanation << std::endl;
    exit(EXIT_FAILURE);
}


void HelperComputePowerOf2Approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = TimesPowerOf2(1, exponent);   
    numerator = RoundAsInt(value * denominator);
}


TestDisplayPowerOfTwoApprox::TestDisplayPowerOfTwoApprox(int resolution)
: TestDisplay(resolution)
{ }


TestDisplayPowerOfTwoApprox::~TestDisplayPowerOfTwoApprox()  = default;


void TestDisplayPowerOfTwoApprox::Do(int Nbits) const
{
    Display(Nbits, 0.65);
    Display(Nbits, 0.35);    
}


void TestDisplayPowerOfTwoApprox::Display(int Nbits, double value) const
{
    PowerOfTwoApprox approximation(Nbits, value);

    double double_quotient = approximation.AsDouble();
    
    std::ostringstream oconv;
    oconv << " (" << approximation.GetNumerator() << " / 2^" << approximation.GetExponent() << ")";

    PrintLine(Nbits, value, double_quotient, RoundToInteger::no, "", oconv.str());
    // < here you can't use the default argument for `optional_string1` in `PrintLine` declaration,
    // < as there are non default arguments after it.
}


int MaxInt(int nbits)
{ 
    return (TimesPowerOf2(1, nbits) - 1);
}


PowerOfTwoApprox::PowerOfTwoApprox(int Nbits, double value)
{
    int max_numerator = MaxInt(Nbits);
    
    auto& numerator = numerator_; // alias!
    auto& exponent = exponent_; // alias!

    int denominator {};
    
    do
    {
        // I used here the prefix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperComputePowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    // After the while loop we have numerator > max_numerator,  
    // hence we need to update the fraction using the previous exponent with --exponent.
    HelperComputePowerOf2Approx(value, --exponent, numerator, denominator);
}


double PowerOfTwoApprox::AsDouble() const
{
   int denominator = TimesPowerOf2(1, exponent_);
   return static_cast<double>(GetNumerator()) / denominator;
}


int PowerOfTwoApprox::GetNumerator() const
{
    return numerator_;
}


int PowerOfTwoApprox::GetExponent() const
{
    return exponent_;
}


int PowerOfTwoApprox::Multiply(int coefficient) const
{
    return TimesPowerOf2(GetNumerator() * coefficient, -GetExponent());
}



TestDisplaySumOfMultiply::TestDisplaySumOfMultiply(int resolution)
: TestDisplay(resolution)
{ }

TestDisplaySumOfMultiply::~TestDisplaySumOfMultiply() = default;


void TestDisplaySumOfMultiply::Do(int Nbits) const
{
    Display(Nbits, 0.65, 3515, 0.35, 4832);    
}


void TestDisplaySumOfMultiply::Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const
{
    double exact = value1 * coefficient1 + value2 * coefficient2;

    auto approximation1 = PowerOfTwoApprox(Nbits, value1); // auto-to-stick syntax for constructor
    auto approximation2 = PowerOfTwoApprox(Nbits, value2);

    int computed_approx = approximation1.Multiply(coefficient1) + approximation2.Multiply(coefficient2);

    std::ostringstream oconv;
    oconv << value1 << " * " << coefficient1 
        << " + " << value2 << " * " << coefficient2 << " = ";

    PrintLine(Nbits, exact, computed_approx, RoundToInteger::yes, oconv.str());
    // < here we use the default value for the 6-th argument `optional_string2`
}


TestDisplay::TestDisplay(int resolution)
: resolution_(resolution)
{ }


TestDisplay::~TestDisplay() = default;


void TestDisplay::PrintLine(int Nbits, double exact, double approx,
                            RoundToInteger do_round_to_integer,
                            std::string optional_string1, std::string optional_string2) const
{
    int error = RoundAsInt(GetResolution() * std::fabs(exact - approx) / exact);
    
    std::cout << "[With " << Nbits << " bits]: " << optional_string1
        << (do_round_to_integer == RoundToInteger::yes ? RoundAsInt(exact) : exact) << " ~ " << approx
        << optional_string2
        << "  [error = " << error << "/" << GetResolution() << "]" 
        << std::endl;    
}


int TestDisplay::GetResolution() const
{
    return resolution_;
}


void TestDisplay::Unused() const
{
    // We could have left this method empty... The error handling is just to tell user that might think this method
    // calls does anything that it doesn't.
    // In production code I would have used `assert(false && "This method is defined to address the Wweak-vtables 
    // warning; it is not intended to be called!");`; we'll see that later in ErrorHandling notebook.
    Error("This method is defined to address the Wweak-vtables warning; it is not intended to be called!");
}


/************************************/
// Main function
/************************************/

// [[maybe_unused]] is a C++ 17 keyword to indicate we're fully aware the variable may not be used.
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{     
    TestDisplayPowerOfTwoApprox test_display_approx(100);
     
    for (int nbits = 2; nbits <= 8; nbits += 2)
         test_display_approx.Do(nbits); 

    std::cout << std::endl;

    TestDisplaySumOfMultiply test_display_sum_of_multiply(1000);

    for (int nbits = 1; nbits <= 8; ++nbits)
        test_display_sum_of_multiply.Do(nbits);

    return EXIT_SUCCESS;
}

