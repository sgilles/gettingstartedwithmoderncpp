cmake_minimum_required(VERSION 3.9)

include(../Config/cmake/Settings.cmake)

project(GettingStartedWithModernCpp_HandsOn_Procedural)

include(../Config/cmake/AfterProjectSettings.cmake)    

add_executable(initial initial_file.cpp)

# add_executable(exercise1 exercise1.cpp)
# add_executable(exercise2 exercise2.cpp)
# add_executable(exercise3 exercise3.cpp)
# add_executable(exercise4 exercise4.cpp)
# add_executable(exercise5 exercise5.cpp)
# add_executable(exercise6 exercise6.cpp)
# add_executable(exercise7 exercise7.cpp)
# add_executable(exercise8 exercise8.cpp)
# add_executable(exercise9 exercise9.cpp)
# add_executable(exercise10 exercise10.cpp)
# add_executable(exercise11 exercise11.cpp)
# add_executable(exercise12 exercise12.cpp)