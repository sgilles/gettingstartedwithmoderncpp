---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Appendix](./0-main.ipynb) - [Weak pointers](./WeakPtr.ipynb)


## How to handle properly circular relationship with `shared_ptr`

### Illustration of circular definition problem

The danger with `shared_ptr` is circular definition: you might end up not releasing properly the memory:



```c++
#include <iostream>
#include <vector>
#include <memory>
#include <string>

// Forward declaration; see dedicated notebook!
class Child;    
```

(If  a refresher is needed, the notebook for [forward declaration](../6-InRealEnvironment/2-FileStructure.ipynb#Forward-declaration)).

```c++
class Father
{
    public:
        
        Father(std::string name);
    
        ~Father();
    
        void AddChild(const std::shared_ptr<Child>& child);        
        
        void Print() const;
    
    private:
        
        std::string name_;

        std::vector<std::shared_ptr<Child>> children_;
};
```

```c++
class Child
{
    public:
        Child(std::string name, const std::shared_ptr<Father>& father);
    
        ~Child();
        
        const std::string& GetName() const; // noexcept unfortunately not supported by Xeus-cling
    
    private:
        
        std::string name_;
        
        std::shared_ptr<Father> father_ = nullptr;
};
```

```c++
void Father::AddChild(const std::shared_ptr<Child>& child)
{
    children_.push_back(child);
}

```

```c++
Father::Father(std::string name)
: name_(name)
{
    std::cout << "Allocating father " << name_ << std::endl;
}
```

```c++
Child::Child(std::string name, const std::shared_ptr<Father>& father)
: name_(name), 
father_(father)
{ 
    std::cout << "Allocating child " << name_ << std::endl;
}
```

```c++
void Father::Print() const
{
    std::cout << name_ << " is the father of " << children_.size() << " children: " << std::endl;
    
    for (const auto& child : children_)
        std::cout<< "\t" << child->GetName() << std::endl;
    
}
```

```c++
Father::~Father()
{
    std::cout << "Release Father " << name_ << std::endl;
}
```

```c++
Child::~Child()
{
    std::cout << "Release Child " << name_ << std::endl;
}
```

```c++
const std::string& Child::GetName() const // noexcept
{
    return name_;
}
```

```c++
{
    auto father = std::make_shared<Father>("Darth Vader");    
    auto boy = std::make_shared<Child>("Luke", father);
    auto girl = std::make_shared<Child>("Leia", father);    
    
    father->AddChild(boy);
    father->AddChild(girl);    
    
    father->Print();
}
```

There are two strong issues with this code:

* We see destructors aren't properly called: due to circular reference neither counter ever reaches 0, hence preventing the deletion of the underlying objects `Father` and `Child`...
* Moreover, this code is very unsatisfactorily on the design side: if the end-user forgets or does not know `AddChild` must be called, we would have ill-defined objects with a father that doesn't know its child...
Ideally we would like to add the `Father::AddChild()` call to the `Child` constructor, but we need a way to be able to provide the `shared_ptr` related to the main class in `Child` constructor.

### `weak_ptr`

The first issue is resolved with a `weak_ptr`: it is very similar to a `shared_ptr` except that is doesn't increase the reference count; so the underlying object may be destroyed even if the weak pointer still exists. 

It is only used for storage; you can't use the object underneath directly and have to build a `shared_ptr` from it before use with the method `lock()`.  

There are just two modifications to do (used to be done here but since 2021 Xeus emit an error (always there in 2024...) so the code will be put instead [@Coliru](http://coliru.stacked-crooked.com/a/6e32de57ca65b6fb)):


- In `Father` class, use this to store children:

```c++
std::vector<std::weak_ptr<Child>> children_;
```

- `Print()` becomes (slightly) more complicated:

```c++
void Father::Print() const
{
    std::cout << name_ << " is the father of " << children_.size() << " children: " << std::endl;
    
    for (const auto& child : children_)
    {
        auto shared = child.lock();
        std::cout<< "\t" << shared->GetName() << std::endl;
    }
    
}
```

And the memory is now properly released!

### Proper usage of `lock()`

However our code is currently not robust: we forget to check whether the underlying objects are still valid. Let's consider the following main ([@Coliru](http://coliru.stacked-crooked.com/a/aa96edd87fbd1f9b) - don't be surprised segmentation fault is fully expected!):

```c++
// Doesn't work in Xeus-cling!

int main(int argc, char** argv)
{
    auto father = std::make_shared<Father>("Darth Vader");    
    auto boy = std::make_shared<Child>("Luke", father);
    auto girl = std::make_shared<Child>("Leia", father);    
    
    father->AddChild(boy);
    father->AddChild(girl);    
    
    {
        auto mistake = std::make_shared<Child>("Yoda", father); // ages don't even match!
        father->AddChild(mistake);
    } // mistake is released here: it becomes out of scope, and children_ data attribute
      // is made of weak pointers.
    
    father->Print();
    
    return EXIT_SUCCESS;
}
```

This code causes an issue (which might be a crash or an error depending on your platform): the vector still includes `Yoda` as a child of `Darth Vader`, whereas the object no longer exists (we're once again in undefined behavior territory).

There is a method `expired()` that returns `true` if the underlying object no longer exists; but in fact `lock()` gets a return value that may be used to the same purpose.

We just change the `Print()` function (see [@Coliru](http://coliru.stacked-crooked.com/a/c9deaf942e703f4c)):

```c++
// Doesn't work in Xeus-cling!

void Father::Print() const
{
    std::cout << name_ << " is the father of " << children_.size() << " children: " << std::endl;
    
    for (const auto& child : children_)
    {
        if (auto shared = child.lock()) // we just added the condition here!
            std::cout<< "\t" << shared->GetName() << std::endl;
    }    
}
```

With this proper use of `lock()`, we at least avoid the undefined behaviour about the objects behind the weak pointers...

But the code is not satisfactorily: we get as result:


```
Allocating father Darth Vader
Allocating child Luke
Allocating child Leia
Allocating child Yoda
Release Child Yoda
Darth Vader is the father of 3 children: 
	Luke
	Leia
Release Child Leia
Release Child Luke
Release Father Darth Vader
```


### Design issue: make the code hard to misuse!


Now we will tackle the remaining design issues:

- Making the end-user call `AddChild()` himself is very ill-advised, as you put on him the burden of making sure the objects are consistent. 
- If a `Child` is removed at some point, it remains present in the `Father` dedicated data attribute `children_`; taking the size of this vector thus yields a wrong result. This second issue is not as bad as the first one: `children_` is a private data attribute that is not exposed publicly at all; it might be acceptable to keep this behaviour and provide a method that yields the number of children (and in this case documenting it properly in Doxygen or equivalent: if someone fiddles with the class implementation in the future he must be fully aware of it... and this someone could be yourself in few months!).

What would be the neater way to solve the first issue would be to do this step during the `Child` constructor: we set the relationship both ways at the same time. What we lacked is a way to derive a `shared_ptr` from the `this` pointer; STL provides a CRTP to do exactly that: `std::enable_shared_from_this`. With this CRTP, we gain a `shared_from_this()` method that provides exactly what we need (you should have a look at [cppreference](https://en.cppreference.com/w/cpp/memory/enable_shared_from_this) for more details about why you shouldn't try to create the shared pointer from this yourself).

Unfortunately, due to this being a CRTP, we can't call it from the constructor (see [the notebook dedicated to CRTP](./Crtp.ipynb#Never-call-a-CRTP-method-in-base-constructor!"); we therefore need to add an `Init()` method. To ensure the end-user doesn't forget it, we may add a variable that checks initialisation has been properly done, at least in debug mode.



```c++
#include <iostream>
#include <vector>
#include <memory>
#include <string>
#include <cassert>

// Forward declaration
class Child3;
```

```c++
class Father3
{
    public:
        
        // Friendship: `Child3` will need access to `AddChild` and `UpdateChildren`,
        // but those methods have no reason otherwise to be publicly available.
        friend Child3;
        
        Father3(std::string name);
    
        ~Father3();
    
        void Print() const;
    
    private:

        void AddChild(const std::shared_ptr<Child3>& child);              
        
        void UpdateChildren();
        
        std::string name_;

        std::vector<std::weak_ptr<Child3>> children_;
};
```

```c++
class Child3 : public std::enable_shared_from_this<Child3> // CRTP!
{
    public:
        Child3(std::string name, const std::shared_ptr<Father3>& father);
    
        ~Child3();
        
        void Init();
        
        const std::string& GetName() const; // noexcept would be even better!
        
//        #ifndef NDEBUG - commented for Xeus-cling only...
        bool IsInitialised() const; // noexcept would be even better!
//        #endif // NDEBUG - commented for Xeus-cling only...

    private:
        
//        #ifndef NDEBUG - commented for Xeus-cling only...
        bool is_initialised_ = false;
//        #endif // NDEBUG - commented for Xeus-cling only...
        
        std::string name_;
        
        std::shared_ptr<Father3> father_ = nullptr;
};
```

```c++
void Father3::AddChild(const std::shared_ptr<Child3>& child)
{
    children_.push_back(child);
}
```

```c++
Father3::Father3(std::string name)
: name_(name)
{
    std::cout << "Allocating father " << name_ << std::endl;
}
```

```c++
Child3::Child3(std::string name, const std::shared_ptr<Father3>& father)
: name_(name), 
father_(father)
{ 
    std::cout << "Allocating child " << name_ << std::endl;
}
```

```c++
void Child3::Init()
{
    #ifndef NDEBUG
    is_initialised_ = true;
    #endif // NDEBUG
    father_->AddChild(shared_from_this());
}
```

```c++
//#ifndef NDEBUG - commented for Xeus-cling only...
bool Child3::IsInitialised() const // noexcept
{
    return is_initialised_;
}
//#endif // NDEBUG - commented for Xeus-cling only...
```

```c++
void Father3::Print() const
{
    std::cout << name_ << " is the father of " << children_.size() << " children: " << std::endl;
    
    for (const auto& child : children_)
    {
        if (auto shared = child.lock())
        {
            assert(shared->IsInitialised()); // remember: assert is defined only if NDEBUG macro is not! (see notebook about error handling)
            std::cout<< "\t" << shared->GetName() << std::endl;
        }
    }    
}
```

```c++
#include <algorithm>

void Father3::UpdateChildren()
{
    auto logical_end = std::remove_if(children_.begin(),
                                      children_.end(),
                                      [](auto& weak_ptr)
                                      {
                                         return weak_ptr.expired(); 
                                      });
                                      
    children_.erase(logical_end, children_.end());
}
```

If you're a bit lost by the implementation above, please check a former [notebook](../5-UsefulConceptsAndSTL/7-Algorithms.ipynb#std::remove).

```c++
Father3::~Father3()
{
    std::cout << "Release Father " << name_ << std::endl;
}
```

```c++
Child3::~Child3()
{
    std::cout << "Release Child " << name_ << std::endl;
    
    try // Remember: no exception should be thrown from destructors!
        // And to be honest even the std::cout should be put inside:
        // exception could be thrown during operator<< calls.
    {
        father_->UpdateChildren();
    }
    catch(const std::exception& e)
    {
        std::cerr << "An exception occurred in Child3 destructor; it is neutralized and "
        "the program will abort." << std::endl;
        abort();
    }
}
```

```c++
const std::string& Child3::GetName() const // noexcept
{
    return name_;
}
```

```c++
{
    auto father = std::make_shared<Father3>("Darth Vader");    
    auto boy = std::make_shared<Child3>("Luke", father);
    auto girl = std::make_shared<Child3>("Leia", father);    
    boy->Init();
    girl->Init();
    
    {
        auto mistake = std::make_shared<Child3>("Yoda", father);
        mistake->Init();
    }
    
    father->Print();
}
```

With this, we have a perfectly valid and safe circular loop between two classes. 

As you can surmise, you shouldn't rely too much on such circular dependencies that are clearly a lot of work to set-up to be full-proof; it is best to think from the beginning in which way your relationship is defined and stick to it as much as possible. 



[© Copyright](../COPYRIGHT.md)   

