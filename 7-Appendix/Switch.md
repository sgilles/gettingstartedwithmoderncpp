---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Appendix](./0-main.ipynb) - [Switch statement](./Switch.ipynb)


## Introduction

As mentioned in the [notebook](../1-ProceduralProgramming/2-Conditions-and-loops.ipynb) about conditions, `switch` statement is an alternative to `if..else if..else`.

It is however rather limited, especially considered compared to more recent languages such as [Swift](https://developer.apple.com/swift/):

* The variable is an integer, an enum or might be convertible into one of those.
* The relationship considered is an equality.

```c++
enum class Color { blue, red, green };
```

```c++
#include <iostream>

{
    Color my_color = Color::blue;
    switch(my_color)
    {
        case Color::blue:
            std::cout << "Blue!" << std::endl;
            break;
        case Color::red:
            std::cout << "Red!" << std::endl;
            break;
        case Color::green:
            std::cout << "Green!" << std::endl;
            break;
            
    }
}
```

## `break` statement

`case` describes one possible case, and ends with the `break` statement. If `break` is not present, the next case is also considered:

```c++
#include <iostream>

{
    Color my_color = Color::blue;
    switch(my_color)
    {
        case Color::blue:
            std::cout << "Blue!" << std::endl;
        case Color::red:
            std::cout << "Red!" << std::endl;
        case Color::green:
            std::cout << "Green!" << std::endl;
    }
}
```

However, unlike the rather primitive cling here most compilers will tell you if you forget about the break (to the point C++ 17 introduced `[[fallthrough]];` to silence the warning when you really want to enable several cases until a break is found).

```c++
#include <iostream>

{
    Color my_color = Color::blue;
    switch(my_color)
    {
        case Color::blue:
            std::cout << "Blue!" << std::endl;
            [[fallthrough]]; // will silence compiler warning
        case Color::red:
            std::cout << "Red!" << std::endl;
            [[fallthrough]]; // will silence compiler warning
        case Color::green:
            std::cout << "Green!" << std::endl;       
    }
}
```

## `default` statement

You may add a catch-all case that will handle the cases not considered previously; just use `default` after all cases:

```c++
#include <iostream>

{
    Color my_color = Color::green;
    switch(my_color)
    {
        case Color::blue:
            std::cout << "Blue!" << std::endl;
            break;
        case Color::red:
            std::cout << "Red!" << std::endl;
            break;
        default:
            std::cout << "Neither blue nor red!" << std::endl;
    }
}
```

I do not advise using it: it might hide the fact one case wasn't properly considered which is to my mind the main reason to use a `switch`:

```c++
#include <iostream>

{
    Color my_color = Color::green;
    switch(my_color)
    {
        case Color::blue:
            std::cout << "Blue!" << std::endl;
            break;
        case Color::red:
            std::cout << "Red!" << std::endl;
            break;
    }
}
```

## Declaration in case

If you need to declare a variable in a case, you need to use a block:

```c++
#include <iostream>

{
    Color my_color = Color::green;
    switch(my_color)
    {
        case Color::blue:
            std::cout << "Blue!" << std::endl;
            int i = 5; // COMPILATION ERROR!
            break;
        default:
            std::cout << "Not blue!" << std::endl;
    }
}
```

```c++
// Fails under Xeus-cling in 2024 but it is srtrictly valid code!

#include <iostream>

{
    Color my_color = Color::green;
    switch(my_color)
    {
        case Color::blue:
        { // Just adding these brackets will solve the issue...
            std::cout << "Blue!" << std::endl;
            int i = 5;
            break;
        }
        default:
            std::cout << "Not blue!" << std::endl;
    }
}
```

[@Coliru link](https://coliru.stacked-crooked.com/a/b860bc760d9c099a) for the snippet above.



[© Copyright](../COPYRIGHT.md)   

