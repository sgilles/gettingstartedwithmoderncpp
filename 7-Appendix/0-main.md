---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Appendix](./0-main.ipynb)


This appendix groups some stuff just mentioned in earlier tutorials but that I have chosen not to speak longly about because the lecture is already long enough as it is and they might be a tiny bit more complex for some of them.

* [CRTP](./Crtp.ipynb) is one of my favourite idiom to provide a same functionality to utterly different classes; it was teased in the [notebook](../4-Templates/5-MoreAdvanced.ipynb) about advanced features with templates.

* [Breaking circular definition of shared pointers with weak pointers](./WeakPtr.ipynb) explains how to set up properly a circular relationship shared/weak pointers (unfortunately the way to set it up properly is often coyly mentioned but not explained); this is a follow-up of the notebook about [smart pointers](../5-UsefulConceptsAndSTL/6-SmartPointers.ipynb). An existing CRTP from STL is used there at some point so you should probably consider reading it after the one explaining CRTP, but it is by no means a hard prerequisite.

* [Homemade exceptions](./HomemadeException.ipynb) just provides the instantiation of the class I personally use when I want to raise an exception; it's a direct follow-up of the section that [mentioned it](../5-UsefulConceptsAndSTL/1-ErrorHandling.ipynb#The-exception-class-I-use).

* [Switch](./Switch.ipynb) is the less important one: it just explains `switch` statement and the syntax caveats you might encounter with them. It was mentioned in the [early notebook](../1-ProceduralProgramming/2-Conditions-and-loops.ipynb#switch-statement) about conditions.

* [StringView](./StringView.ipynb) explains briefly the whereabouts of `std::string_view` which was introduced in C++ 17 - and which will be expanded to other types in C++ 20.

* [Some guidance about how to deal with compilation errors](./CompilationErrors.ipynb).





[© Copyright](../COPYRIGHT.md)   

