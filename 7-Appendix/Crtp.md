---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Appendix](./0-main.ipynb) - [Curiously recurrent template pattern](./Crtp.ipynb)

<!-- #region -->
## Introduction

The **curiously recurrent template pattern** (often shortened as **CRTP**) is a idiom that might put you off guard the first time you meet it: it's not exactly obvious it should be allowed to compile...

The syntax is:

```c++
class Derived : public Base<Derived>
```

i.e. the class derives from a template specialization of a base class... which template argument is the class itself!

The purpose of this idiom is to provide a specific behaviour to several classes that might be unrelated otherwise.

Throughout this tutorial, we will consider the functionality of giving to an object a unique identifier: each time a new object is created, we want to provide to it a unique index and the accessor to get it. We'll try first what we already have in store and see why the CRTP is very handy to get.

Of course, some of the limitations we shall see stem from the use of a static method; however this is an example among others of cases that aren't properly supported by the basic inheritance or composition.


## Attempt without CRTP

### First attempt: public inheritance

The first idea should be for most of you to use inheritance, probably in its public form.


<!-- #endregion -->

```c++
#include <iostream>

using uint = unsigned int; // just to bypass a Xeus-cling limitation...
```

```c++
class UniqueId
{
public:
    UniqueId();
    
    // Allowing copy would defeat the purpose of such a class...
    UniqueId(const UniqueId&) = delete;
    UniqueId& operator=(const UniqueId&) = delete;
    
    uint GetUniqueId() const; // noexcept would be better, but cling disagree...
    
private:   
    
    const uint unique_id_;
    
    static uint Generate();

};
```

```c++
uint UniqueId::Generate()
{
    static auto ret = 0u;
    return ret++;
}
```

```c++
UniqueId::UniqueId()
: unique_id_(Generate())
{ }
```

```c++
uint UniqueId::GetUniqueId() const
{
    return unique_id_;
}
```

```c++
struct Carrot : public UniqueId
{ };
```

```c++
struct Cabbage : public UniqueId
{ };
```

```c++
{
    Carrot carrot[2];
    Cabbage cabbage[2];    
    
    for (auto i = 0ul; i < 2; ++i)
    {
        std::cout << "Carrot " << carrot[i].GetUniqueId() << std::endl;
        std::cout << "Cabbage " << cabbage[i].GetUniqueId() << std::endl;        
    }
}
```

That is not exactly what we had in mind: the idea of providing the same functionality for `Carrot` and `Cabbage` is appealing, but it would have been better not to share the same index (and we can imagine more complex CRTP for which we absolutely do not want to intertwine the derived classes).

Besides that, there is another drawback: doing so enables a mischiever developer to store in a same container otherwise unrelated classes as pointers:




```c++
#include <vector>

{
    Carrot* carrot = new Carrot;
    Cabbage* cabbage = new Cabbage;
    
    std::vector<UniqueId*> list { carrot, cabbage };
}
```

Of course, at some point you can't protect a developer against his/her own stupidity, but ideally it's better if you can protect it as much a possible, and closing this possibility would be nice.

In case you're wondering, the list above would not be terribly useful: if you want to use the list for something other that the unique id functionality, you would have to use **dynamic_cast** - which I do not recommend) 


### Second attempt: private inheritance

```c++
struct Carrot2 : private UniqueId
{ 
    using UniqueId::GetUniqueId;
};
```

```c++
struct Cabbage2 : private UniqueId
{ 
    using UniqueId::GetUniqueId;
};
```

```c++
{
    Carrot2 carrot[2];
    Cabbage2 cabbage[2];    
    
    for (auto i = 0ul; i < 2; ++i)
    {
        std::cout << "Carrot " << carrot[i].GetUniqueId() << std::endl;
        std::cout << "Cabbage " << cabbage[i].GetUniqueId() << std::endl;        
    }
}
```

We haven't gained much with private inheritance, and even lost a bit:

* Underlying numeration is still the same for unrelated classes.
* We now additionally need to explicitly allow in derived classes the base class method with `using` statement. This may not seem much, but in a complex CRTP with several methods it adds boilerplate to provide in each derived class.
* It is up to the user to proceed to private inheritance: he may use public one as easily as private one.

The only substantive gain is that we block the possibility to list together unrelated objects:

```c++
#include <vector>

{
    Carrot2* carrot = new Carrot2;
    Cabbage2* cabbage = new Cabbage2;
    
    std::vector<UniqueId*> list { carrot, cabbage };
}
```

### Third attempt: composition

Composition actually displays most of the same weaknesses as the private inheritance, and is also more wordy to use:

```c++
#include <memory>

class Carrot3
{ 
    
public:
    
    Carrot3();    
    
    uint GetUniqueId() const;
    
private:
    
    const std::unique_ptr<UniqueId> unique_id_ = nullptr;
    
};
```

```c++
Carrot3::Carrot3()
: unique_id_(std::make_unique<UniqueId>())
{ }

```

```c++
uint Carrot3::GetUniqueId() const
{
    assert(!(!unique_id_));
    return unique_id_->GetUniqueId();
}
```

```c++
class Cabbage3
{ 
    
public:
    
    Cabbage3();    
    
    uint GetUniqueId() const;
    
private:
    
    const std::unique_ptr<UniqueId> unique_id_ = nullptr;
    
};
```

```c++
Cabbage3::Cabbage3()
: unique_id_(std::make_unique<UniqueId>())
{ }
```

```c++
uint Cabbage3::GetUniqueId() const
{
    assert(!(!unique_id_));
    return unique_id_->GetUniqueId();
}

```

```c++
{
    Carrot3 carrot[2];
    Cabbage3 cabbage[2];    
    
    for (auto i = 0ul; i < 2; ++i)
    {
        std::cout << "Carrot " << carrot[i].GetUniqueId() << std::endl;
        std::cout << "Cabbage " << cabbage[i].GetUniqueId() << std::endl;        
    }
}
```

So it's mostly more of the same; it is pretty wordy but at least unrelated objects can't be put in the same container.


## CRTP

Now let's write a CRTP:

```c++
template<class DerivedT> 
class CrtpUniqueId
{
public:
    CrtpUniqueId();
    
    // Allowing copy would defeat the purpose of such a class...
    CrtpUniqueId(const CrtpUniqueId&) = delete;
    CrtpUniqueId& operator=(const CrtpUniqueId&) = delete;
    
    uint GetUniqueId() const; // noexcept would be better, but cling disagree...
    
private:   
    
    const uint unique_id_;
    
    static uint Generate();

};
```

```c++
template<class DerivedT>
uint CrtpUniqueId<DerivedT>::Generate()
{
    static auto ret = 0u;
    return ret++;
}
```

```c++
template<class DerivedT>
CrtpUniqueId<DerivedT>::CrtpUniqueId()
: unique_id_(Generate())
{ }
```

```c++
template<class DerivedT>
uint CrtpUniqueId<DerivedT>::GetUniqueId() const
{
    return unique_id_;
}
```

And now define the derived class with this curious syntax:

```c++
struct Carrot4 : public CrtpUniqueId<Carrot4>
{ };
```

```c++
struct Cabbage4 : public CrtpUniqueId<Cabbage4>
{ };
```

```c++
{
    Carrot4 carrot[2];
    Cabbage4 cabbage[2];    
    
    for (auto i = 0ul; i < 2; ++i)
    {
        std::cout << "Carrot " << carrot[i].GetUniqueId() << std::endl;
        std::cout << "Cabbage " << cabbage[i].GetUniqueId() << std::endl;        
    }
}
```

We see with this syntax that:

* Each class properly use its own internal numbering.
* There are no relationship at all between derived classes: `CrtpUniqueId<Carrot4>` is an entirely different object than `CrtpUniqueId<Cabbage4>`.
* There are no boilerplate in derived classes implementation: nothing to add besides the CRTP syntax.

Moreover, this occurs at compilation and there are no runtime cost whatsoever.

The inheritance might be public, protected or private, depending on your needs.



### Referring to the base class

Sometimes however, you need to access something from the derived class in the CRTP, and I must admit in this case the syntax could have been more sweet...

```c++
template<class DerivedT>
struct CrtpPrint
{
    
    void Print() const;
    
};
```

```c++
#include <iostream>

template<class DerivedT>
void CrtpPrint<DerivedT>::Print() const
{
    std::cout << "The name of the class is " << static_cast<const DerivedT&>(*this).ClassName() << std::endl;
}
```

```c++
#include <string>

struct Tomato : public CrtpPrint<Tomato>
{
    static std::string ClassName();
};
```

```c++
std::string Tomato::ClassName()
{
    return std::string("Tomato");
}
```

```c++
{
    Tomato tomato;
    tomato.Print();
}
```

As we saw, the way to access something in the base class is to use:

```
static_cast<DerivedT&>(*this)
```

In the case above, I added a `const` as we wanted to access a constant method of the base class.




### Never call a CRTP method in base constructor!

CRTP is sometimes dubbed **static polymorphism** due to this functionality. This name is helpful in the sense a warning true for usual dynamic polymorphism is also true here: **never** call a derived method in the base constructor: it would lead to undefined behaviour.



[© Copyright](../COPYRIGHT.md)   

