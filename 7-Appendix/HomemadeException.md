---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Appendix](./0-main.ipynb) - [Home-made exception class](./HomemadeException.ipynb)

<!-- #region -->
## Why an home-made exception class?

### Clunky constructors provided in `std::exception`

`std::exception` provides two constructors: a copy one and a default one without arguments. The idea is that you should provide your own derived exception class for each exception in your program, for which you would override properly the `what()` virtual method to display the adequate message.

I prefer to provide a simple way to provide on spot exception with a mere constructor, to be able to write something like:

```c++
    double x = -3.;
    
    if (x < -2. || x > 5.)
        throw HomemadeException("x should be in [-2., 5.]");
```

rather than much more verbosy:
<!-- #endregion -->

```c++
#include <exception>

class RangeMinus2PlusFiveException : public std::exception
{
public:
    RangeMinus2PlusFiveException() = default;
    
    virtual const char* what() const noexcept override
    {
        return "x should be in [-2., 5.]";
    } 

};
```

```c++
{
    double x = -3.;
    
    if (x < -2. || x > 5.)
        throw RangeMinus2PlusFiveException();
}
```

<!-- #region -->
### `std::string` for storage

I'm not comfortable either with the `char*` type for the `what` message, especially if you want to tailor it to provide as many information as possible to the end-user (here for instance providing the value of `x` that triggered the exception is much better).

With `char*`, you might encounter rather easily two opposites issues:

- The memory could leak if not properly deallocated.
- On the contrary, if it was somehow released and the exception `what()` is asked later along the line you might end-up with garbage characters in part of your string, as the memory freed might have been used for something else.

We already saw the patch to avoid this kind of issues: using a container with proper RAII ensured. `std::string`is the most obvious choice for a text message.

So in my code I derive my exceptions from a homemade class which is itself derived from `std::exception`; the main difference is that the storage is done with a `std::string` and the override of the `what()` method reads this `std::string`.

### Indicating the file and line

If your code is huge, knowing the exception itself is not enough, if this exception may be thrown from several locations in your code. To identify where the exception was thrown, I used for a long time the `__FILE__` and `__LINE__` macro which gives the file and line where they were found in the code.

So my constructor looked like:

```c++
HomemadeException(const std::string& msg, const char* invoking_file, int invoking_line);
```

It might be a bit wordy but:
* It's safe.
* Print is correct.
* It is easy to figure out where it happened.

A better alternative named `std::source_location` was introduced in C++ 20, but it took some time (circa 2023) to be supported properly by both libc++ and libstdc++ (and even currently using libc++ some features don't exactly work [as printed on the can](https://en.cppreference.com/w/cpp/utility/source_location)...)

So current constructor is now:

```c++
HomemadeException(const std::string& msg, const std::source_location location = std::source_location::current());
```


## Implementation of my home-made class

Here is a transcript of my own exception class, which may be found in [my main project](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/tree/master/Sources/Utilities/Exceptions). Namespace has ben removed (you should add one if you intend to use it in practice):

### Exception.hpp
<!-- #endregion -->

```c++
// Doesn't work on Xeus-cling: C++ 20 on board!

#include <exception>  
#include <source_location>
#include <string>         


    /*!
     * \brief Generic class for exceptions used in MoReFEM library 
     *
     * (https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM)
     */
    class Exception : public std::exception
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor with simple message.
         *
         * \param[in] msg Message.
         * \param[in] location STL object with relevant information about the calling site.         
         */

        //@}
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Exception() noexcept override;

        //! Enable default copy constructor.
        Exception(const Exception& rhs) = default;

        //! Enable default move constructor.
        Exception(Exception&& rhs) = default;

        //! Enable default assignment operator.
        Exception& operator=(const Exception& rhs) = default;

        //! Enable default move assignment operator.
        Exception& operator=(Exception&& rhs) = default;

        ///@}

        /*!
         * \brief Display the what message from std::exception.
         *
         * \return The what() message as a char* (which reads the internal std::string so no risk of deallocation
         * issue).
         */
        virtual const char* what() const noexcept override final;

        /*!
         * \brief Display the raw message (Without file and line).
         *
         * Might be useful if exception is caught to rewrite a more refined message.
         *
         * Before introducing this, we could end up in some cases with something like:
         * \verbatim
         * Exception caught: Exception found at Sources/Model/Internal/InitializeHelper.hxx, line 114: Ill-defined
         * finite element space 1: Exception found at Sources/Model/Internal/InitializeHelper.hxx, line 101:
         * Domain 1 is not defined!
         * \endverbatim
         *
         * Clearly it is nicer to provide:
         * \verbatim
         * Exception caught: Exception found at Sources/Model/Internal/InitializeHelper.hxx, line 114: Ill-defined
         * finite element space 1: Domain 1 is not defined!
         * \endverbatim
         *
         * \return Exception error message without information about file and line in which the exception was invoked.
         */
        const std::string& GetRawMessage() const noexcept;


      private:
        //! The complete what() message (with the location part)
        std::string what_message_;

        //! Incomplete message (might be useful if we catch an exception to tailor a more specific message).
        std::string raw_message_;
    };
```

### Exception.cpp

```c++
// Doesn't work on Xeus-cling: C++ 20 on board!

#include <cstring>
#include <iostream>
#include <sstream>
#include <type_traits>
#include <utility>

#include "Exception.hpp"


namespace // anonymous
{


    //! Call this function to set the message displayed by the exception.
    void SetWhatMessage(const std::string& msg, std::string& what_message, const std::source_location location)
    {
        std::ostringstream stream;
        stream << "Exception found in file " << location.file_name() << " line " << location.line();

        if (strcmp(location.function_name(), "") == 0) // currently Apple clang returns empty!
            stream << " in function " << location.function_name();

        stream << ": " << msg;
        what_message = stream.str();
    }


} // namespace


Exception::Exception(const std::string& msg, const std::source_location location)
: std::exception(), raw_message_(msg)
{
    SetWhatMessage(msg, what_message_, location);
}


Exception::~Exception() noexcept = default;


const char* Exception::what() const noexcept
{
    return what_message_.c_str();
}


const std::string& Exception::GetRawMessage() const noexcept
{
    return raw_message_;
}

```

### Call to use the exception class

```c++
{
    throw Exception("Example of exception with home-made class", std::source_location::current());
}
```

```c++
{
    int i = -5;
    std::ostringstream oconv;
    oconv << i << " should have been even!";
    
    throw Exception(oconv.str(), std::source_location::current());
}
```

If you read [the documentation](https://en.cppreference.com/w/cpp/utility/source_location), we should be able to completely avoid the explicit `std::source_location::current()` call at calling site, according to [CppReference](https://en.cppreference.com/w/cpp/utility/source_location/current):

> If current() is used in a default argument, the return value corresponds to the location of the call to current() at the call site.

That is unfortunately not the behaviour I have observed with libc++ on macOS: the file and line returned is the one from the header file, hence the explicit argument at calling site. Likewise, `function_name()` is empty, hence the test in my `SetWhat()` message.

Nonetheless, we may hope these glitches will be solved at some point, and even with them `std::source_location` is more comfy to use than `__FILE__` and `__LINE__` C macros.



[© Copyright](../COPYRIGHT.md)   


