---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Appendix](./0-main.ipynb) - [Stringview](./StringView.ipynb)


C++ 17 introduced `std::string_view`, which is basically a sort of viewer over a string (not especially a `std::string`: it works basically as long as it's a chain of contiguous characters, where what is a *character* is defined as first template argument).

Let's roll an example to illustrate how it works:

```c++
#include <string>
#include <string_view>
#include <iostream>

void Print(std::string_view content)
{
    std::cout << "Content is '" << content << "'" << std::endl;
}


std::string hello("Hello world from std::string!");

Print(hello);

Print("Hello world!");
```

Prior to C++ 17, the usual way was to use `const std::string&` as parameter (passing a `std::string` directly would incur copies):

```c++
#include <string>
#include <iostream>

void PrintWithConstRef(const std::string& content)
{
    std::cout << "Content is '" << content << "'" << std::endl;
}


std::string hello("Hello world from std::string!");

PrintWithConstRef(hello);
PrintWithConstRef("Hello world!");
```

So what did we gain exactly in the bargain?

If you remember the discussion about *l-values* and *r-values* in the [notebook about move semantics](../5-UsefulConceptsAndSTL/5-MoveSemantics.ipynb), the construct used prior to C++ 17 doesn't necessarily make much sense: in our second call the argument `Hello world!` is clearly a r-value whereas a l-value would be expected given the prototype!


Let's write the same *without* the `const` to convince ourselves:

```c++
#include <string>
#include <iostream>

void PrintWithRef(std::string& content)
{
    std::cout << "Content is '" << content << "'" << std::endl;
}

PrintWithRef("Hello world!"); // COMPILATION ERROR!
```

This time it doesn't work at all...

So when we provide a r-value argument to a `const std::string&` parameter, the compiler does some magic to interpret it. This magic is not without cost: a `std::string` is allocated on the fly to store the content of the r-value.

That is what `std::string_view` strives to correct: when it is used there are no such allocation involved.

So whenever possible, using `std::string_view` instead of `const std::string&` is advised - provided of course your project is firmly grounded in C++ 17 or later.

That doesn't mean there are no costs involved: when you are using `std::string_view`, you are essentially taking the responsibility to ensure the memory area still exists (very similarly to the kind of contract you pass when you define a reference). [CppReference](https://en.cppreference.com/w/cpp/string/basic_string_view) illustrates this on a very basic case:



```c++
#include <iostream>
#include <string>
#include <string_view>

std::string_view bad(std::string("a temporary string")); // "bad" holds a dangling pointer

// Any use of bad here will result in undefined behaviour as the `std::string` was destroyed in 
// previous line when it went out of scope!

```

C++ 20 will expand on this possibility for other contiguous containers with [`std::span`](https://en.cppreference.com/w/cpp/container/span).


[© Copyright](../COPYRIGHT.md)
