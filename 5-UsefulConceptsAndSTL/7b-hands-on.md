---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Useful concepts and STL](/notebooks/5-UsefulConceptsAndSTL/0-main.ipynb) -  [Hands-on 17](/notebooks/5-UsefulConceptsAndSTL/7b-hands-on.ipynb)

<!-- #region -->
### **EXERCISE 43: `unsigned char` case**

In exercise 34 dealing with `char` case, we overlooked the case of its `unsigned char` counterpart.

We could of course add a new specialization to `DisplayInteger()`, but now we've got all the tools to better implement it - we may use a combination of:
    - `if constexpr` (assuming C++ 17 of course - even if in this specific case a simple `if` would do the work)
    - [`std::is_same`](https://en.cppreference.com/w/cpp/types/is_same), a very handly algorithm to check if two types are equal.

Remove `DisplayInteger` specialization, and rewrite `DisplayInteger()` implementation to deal properly with both `char` and `unsigned char` cases.

Following `main`:

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    try
    {
        TestDisplayContainer container;

        using type = unsigned char;

        container.emplace_back(std::make_unique<TestDisplayPowerOfTwoApprox065<type>>(100));
        container.emplace_back(std::make_unique<TestDisplayPowerOfTwoApprox035<type>>(100));
        
        Loop(1, 8, 1, container);
    }
    catch(const std::exception& e) // we could trap only `Exception`, but it's not a bad idea to also
                                   // catch exceptions from STL...
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}
```

should give the following results:

```
[With 1 bits]: 0.65 ~ 0.5 (1 / 2^1)  [error = 23/100]
[With 2 bits]: 0.65 ~ 0.75 (3 / 2^2)  [error = 15/100]
[With 3 bits]: 0.65 ~ 0.625 (5 / 2^3)  [error = 4/100]
[With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 4/100]
[With 5 bits]: 0.65 ~ 0.65625 (21 / 2^5)  [error = 1/100]
[With 6 bits]: 0.65 ~ 0.65625 (42 / 2^6)  [error = 1/100]
[With 7 bits]: Overflow! (in TimesPowerOf2())
[With 8 bits]: Overflow! (in TimesPowerOf2())

[With 1 bits]: 0.35 ~ 0.25 (1 / 2^2)  [error = 29/100]
[With 2 bits]: 0.35 ~ 0.375 (3 / 2^3)  [error = 7/100]
[With 3 bits]: 0.35 ~ 0.375 (6 / 2^4)  [error = 7/100]
[With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 2/100]
[With 5 bits]: 0.35 ~ 0.34375 (22 / 2^6)  [error = 2/100]
[With 6 bits]: Overflow! (in TimesPowerOf2())
[With 7 bits]: Overflow! (in TimesPowerOf2())
[With 8 bits]: Overflow! (in TimesPowerOf2())
```


<!-- #endregion -->

[© Copyright](../COPYRIGHT.md)   

