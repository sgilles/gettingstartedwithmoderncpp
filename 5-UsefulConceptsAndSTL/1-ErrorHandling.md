---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Useful concepts and STL](./0-main.ipynb) - [Error handling](./1-ErrorHandling.ipynb)


## Introduction

It is very important of course to be able to track and manage as nicely as possible when something goes south in your code. We will see in this chapter the main ways to provide such insurance.

## Compiler warnings and errors

The first way to find out possible errors are during compilation time: you may ensure your code is correct by making its compilation fails if not (that's exactly the spirit of the example we provided for [template template parameter](../4-Templates/5-MoreAdvanced.ipynb#Template-template-parameters-(not-a-mistake...))). There are many ways to do so, even more so if templates are involved; here are few of them we have already seen:

* `static_assert` we saw in [template introduction](../4-Templates/1-Intro.ipynb#static_assert)
* Duck typing failure: if a template argument used somewhere doesn't comply with the expected API. If you're using C++ 20, consider using `concept` to restrain what is provided as template argument.
* Locality of reference: use heavily blocks so that a variable is freed as soon as possible. This way, you will avoid mistakes of using a variable that is in fact no longer up-to-date.
* Strive to make your code without any compiler warning: if there are even as less as 10 warnings, you might not see an eleventh that might sneak its way at some point. Activate as many types of warnings as possible for your compiler, and deactivate those unwanted with care (see [this notebook](../6-InRealEnvironment/4-ThirdParty.ipynb) to see how to manage third party warnings.).

## Assert

`assert` is a very handy tool that your code behaves exactly as expected. `assert` takes one argument; if this argument is resolved to `false` the code aborts with an error message:



```c++
#undef NDEBUG // Don't bother with this outside of Xeus-cling!
#include <cassert>
#include <iostream>

// THIS CODE WILL KILL Xeus-cling kernel!
{
    double* ptr = nullptr;
    assert(ptr != nullptr && "Pointer should be initialized first!"); 
    //< See the `&&`trick above: you can't provide a message like in `static_assert`,
    // but you may use a AND condition and a string to detail the issue
    // (it works because the string is evaluated as `true`).
    std::cout << *ptr << std::endl;
}
```

(here in Xeus-cling it breaks the kernel; you may check on [Coliru](https://coliru.stacked-crooked.com/a/5fb74d5ae0118cb2))

The perk of `assert` is that it checks the condition is `true` *only in debug mode*! 

So you may get extensive tests in debug mode that are ignored once your code has been thoroughly checked and is ready for production use (_debug_ and _release_ mode will be explained in a [later notebook](../6-InRealEnvironment/3-Compilers.ipynb#Debug-and-release-flags)).

The example above is a very useful use: before dereferencing a pointer checks it is not `nullptr` (hence the good practice to always initialize a pointer to `nullptr`...)

In **release mode**, the macro `NDEBUG` should be defined and all the `assert` declarations will be ignored by the compiler.

I recommend to use `assert` extensively in your code:

* You're using a pointer? Check it is not `nullptr`.
* You get in a function a `std::vector` and you know it should be exactly 3 elements long? Fire up an assert to check this...
* A `std::vector` is expected to be sorted a given way? Check it through an `assert`... (yes it's a O(n) operation, but if your contract is broken you need to know it!)

Of course, your debug mode will be much slower; but its role is anyway to make sure your code is correct, not to be the fastest possible (release mode is there for that!)



## Exceptions


Asserts are clearly a **developer** tool: they are there to signal something does not behave as intended and therefore that there is a bug somewhere...

However, they are clearly not appropriate to handle an error of your program end-user: for instance if he specifies an invalid input file, you do not want an `abort` which is moreover handled only in debug mode!

### `throw`

There is an **exception** mechanism that is appropriate to deal with this; this mechanism is activated with the keyword `throw`.

```c++
#include <iostream>

void FunctionThatExpectsSingleDigitNumber(int n)
{
    if (n < -9)
        throw -1;
    
    if (n > 9)
        throw 1;
    
    std::cout << "Valid digit is " << n << std::endl;
}
```

```c++
{
    FunctionThatExpectsSingleDigitNumber(5);
    std::cout << "End" << std::endl;
}
```

```c++
{
    FunctionThatExpectsSingleDigitNumber(15);
    std::cout << "End" << std::endl;
}
```

As you can see, an exception provokes an early exit of the function: the lines after the exception is thrown are not run, and unless it is caught it will stop at the abortion of the program.


### `try`/`catch`

`throw` expects an object which might be intercepted by the `catch` command if the exception occurred in a `try` block; `catch` is followed by a block in which something may be attempted (or not!)

```c++
{
    try
    { 
        FunctionThatExpectsSingleDigitNumber(15);
    }
    catch(int n)
    {
        if (n == 1)
            std::cerr << "Error: value is bigger than 9!" << std::endl;
        if (n == -1)
            std::cerr << "Error: value is less than -9!" << std::endl;
    }
    std::cout << "End" << std::endl;
}
```

If the type doesn't match, the exception is not caught; if you want to be sure to catch everything you may use the `...` syntax. The drawback with this syntax is you can't use the thrown object information:

```c++
{
    try
    { 
        FunctionThatExpectsSingleDigitNumber(15);
    }
    catch(float n) // doesn't catch your `int` exception!
    {
        std::cerr << "Float case: " << n << " was provided and is not an integer" << std::endl;
    }
    catch(...)
    {
        std::cerr << "Gluttony case... but no object to manipulate to extract more information!" << std::endl;
    }
}
```

### Re-throw

Once an exception has been caught by a `catch` block, it is considered to be handled; the code will therefore go on to what is immediately after the block. If you want to throw the exception again (for instance after logging a message) you may just type `throw`:

```c++
// No re-throw
{
    try
    { 
        FunctionThatExpectsSingleDigitNumber(15);
    }
    catch(int n)
    {
        std::cerr << "Int case: " << n << " not a single digit number" << std::endl;
    }
    
    std::cout << "After catch" << std::endl;
}
```

```c++
// Rethrow
{
    try
    { 
        FunctionThatExpectsSingleDigitNumber(15);
    }
    catch(int n)
    {
        std::cerr << "Int case: " << n << " not a single digit number" << std::endl;
        throw; // `throw n` would have been correct as well but is not necessary
    }
    
    std::cout << "After catch" << std::endl;
}
```

### Good practice: use as much as possible exceptions that derive from `std::exception`

Using the _catch all_ case is not recommended in most cases... In fact even the `int`/`float` case is not that smart: it is better to use an object with information about why the exception was raised in the first place.

It is advised to use exception classes derived from the `std::exception` one; this way you provide a catch all without the drawback mentioned earlier. This class provides a virtual `what()` method which gives away more intel about the issue:

```c++
#include <exception>

struct TooSmallError : public std::exception
{
    virtual const char* what() const noexcept override // we'll go back to `noexcept` later...
    {
        return "Value is less than -9!";
    }
};
```

```c++
struct TooBigError : public std::exception
{
    virtual const char* what() const noexcept override
    {
        return "Value is more than 9!";
    }
};
```

```c++
#include <iostream>

void FunctionThatExpectsSingleDigitNumber2(int n)
{
    if (n < -9)
        throw TooSmallError();
    
    if (n > 9)
        throw TooBigError();
    
    std::cout << "Valid digit is " << n << std::endl;
}
```

```c++
{
    try
    { 
        FunctionThatExpectsSingleDigitNumber2(15);
    }
    catch(const std::exception& e)
    {
        std::cerr << "Properly caught: " << e.what() << std::endl;
    }
}
```

The information comes now with the exception object, which is much better... 


Unfortunately, you are not always privy to the choice of deriving from `std::exception`: if for instance you're using [Boost library](https://www.boost.org) the exception class they use don't inherit from `std::exception` (but some derived ones such as `boost::filesystem::error` do...). In this case, make sure to foresee to catch them with a dedicated block:

```c++
// Pseudo-code - Do not run in Xeus-cling!

try
{
    ...    
}
catch(const std::exception& e)
{
    ...
}
catch(const boost::exception& e)
{
    ...
}


```

### Storing more information in the class... and avoiding the `char*` pitfall!

In fact we could have gone even further and personnalize the exception message, for instance by printing for which value of `n` the issue arose:

```c++
struct TooBigErrorWithMessage : public std::exception
{
    TooBigErrorWithMessage(int n);

    virtual const char* what() const noexcept override
    {
        return msg_.c_str(); // c_str() as we need a const char*, not a std::string!
    }

private:

    std::string msg_;
};
```

```c++
#include <sstream>

TooBigErrorWithMessage::TooBigErrorWithMessage(int n)
{
    std::ostringstream oconv;
    oconv << "Value '" << n << "' is more than 9!";
    msg_ = oconv.str();
}
```

```c++
#include <iostream>

void FunctionThatExpectsSingleDigitNumber3(int n)
{
    if (n < -9)
        throw TooSmallError();
    
    if (n > 9)
        throw TooBigErrorWithMessage(n);
    
    std::cout << "Valid digit is " << n << std::endl;
}
```

```c++
{
    try
    { 
        FunctionThatExpectsSingleDigitNumber3(15);
    }
    catch(const std::exception& e)
    {
        std::cerr << "Properly caught: " << e.what() << std::endl;
    }
}
```

This might seem trivial here, but in real code it is really handy to get all relevant information from your exception.

However, I avoided silently a common pitfall when dabbling with `std::exception`: one of its cardinal sin for me at least is to use C string as return type for its `what()` method. It might seem innocuous enough, but is absolutely not if you do not use a `std::string` as a data attribute to encapsulate the message.

Let's write it without the `msg_` data attribute:







```c++
#include <exception>
#include <sstream>
#include <iostream>
#include <string>

struct TooBigErrorWithMessagePoorlyImplemented : public std::exception
{
    TooBigErrorWithMessagePoorlyImplemented(int n)
    : n_(n)
    { }

    virtual const char* what() const noexcept override
    {        
        std::ostringstream oconv;
        oconv << "Value '" << n_ << "' is more than 9!";
        std::string msg = oconv.str();
        std::cout << "\nCheck: message is |" << msg << "|" << std::endl;
        return msg.c_str();
    }

private:

    int n_;
};
```

```c++
#include <iostream>

void FunctionThatExpectsSingleDigitNumberWithPoorlyImplementedException(int n)
{
    // if (n < -9)
    //     throw TooSmallError(); // skip it - we will make the kernel crash and it's better not to bother reloading this one each time
    
    if (n > 9)
        throw TooBigErrorWithMessagePoorlyImplemented(n);
    
    std::cout << "Valid digit is " << n << std::endl;
}
```

```c++
{
    try
    { 
        FunctionThatExpectsSingleDigitNumberWithPoorlyImplementedException(15);
    }
    catch(const std::exception& e)
    {
        std::cerr << "Properly caught: " << e.what() << std::endl;
    }
}
```

So what happens here?

The deal is that `c_str()` returns the pointer to the underlying data used in the `std::string` object... which got destroyed at the end of `what()` function...

So we're directly in the realm of undefined behaviour (sometimes it might be kernel crash, sometimes it might be partial or complete gibberish printed instead of the expected string).

The ordeal would of course be the same with another type (for instance if instead of using `std::string` you allocate manually a `char*` variable): as soon as you get out of scope the variable is destroyed and behaviour is erratic.

So when you define an exception class you can't define the return of `what()` method inside the implementation of the method itself; you **must** use a data attribute to store it. The most common choice is to use a `std::string`.





<!-- #region -->
### Good practice: be wary of a forest of exception classes

At first sight, it might be tempting to provide a specific exception whenever you want to throw one: this way, you are able to catch only this one later on.

In practice, it's not necessarily such a good idea:

* When the code becomes huge, you (and even more importantly a new developer) may be lost in all the possible exceptions.
* It is rather time consuming: defining a specific exception means a bit of boilerplate to write, and those minutes might have been spent more efficiently, as...
* Most of the time, you don't even need the filtering capacity; in my code for instance if an exception is thrown it is 99 % of the time to be caught in the `main()` function to terminate properly the execution.

The only case in which it might be very valuable to use a tailored exception is for your integration tests: if you are writing a test in which an exception is expected, it is better to check the exception you caught is exactly the one that was expected and not a completely unrelated exception which was thrown for another reason.

STL provides many derived class from `std::exception` which you might use directly or as base of your own class; see [cppreference](https://en.cppreference.com/w/cpp/error/exception) for more details. [OpenClassrooms](https://openclassrooms.com/fr/courses/7137751-programmez-en-oriente-objet-avec-c/7532931-gerez-des-erreurs-avec-les-exceptions) (in french) sorted out the go-to exceptions for lazy developers which cover most of the cases (don't get me wrong: laziness is often an asset for a software developer!):

* `std::domain_error`
* `std::invalid_argument`
* `std::length_error`
* `std::out_of_range`
* `std::logic_error`
* `std::range_error`
* `std::overflow_error`
* `std::underflow_error`
* `std::runtime_error`
	
with the latter being the default choice if no other fit your issue. Most of those classes provide a `std::string` argument in its constructor so that you may explain exactly what went wrong.


### `noexcept`

Exceptions are in fact very subtle to use; see for instance [Herb Sutter's books](../bibliography.ipynb#Exceptional-C++-/-More-Exceptional-C++) that deal with them extensively (hence their title!).

In C++03, it was possible to specify a method or a function wouldn't throw, but the underlying mechanism with keywords `throw` and `nothrow` was such a mess many C++ gurus warned against using them.

In C++11, they tried to rationalize it and a new keyword to replace them was introduced: `noexcept`. 

In short, if you have a method you're 100 % percent sure can't throw an exception, add this suffix and the compiler may optimize even further. However, do not put it if an exception can be thrown: it would result in a ugly runtime crash should an exception be raised there... (and up to now compilers are completely oblivious to that: no associated warning is displayed).

As you saw, in recent C++ `what()` is to be a `noexcept` method. It is therefore a bad idea to try to allocate there the string to be returned: allocation and string manipulation could lead to an exception from the STL functions used.

FYI, currently the error messages provided by compilers when your runtime crash due to poorly placed `noexcept` may look like:
<!-- #endregion -->

clang++: 
```shell
libc++abi: terminating due to uncaught exception of type **your exception** 
```

g++:
```shell
terminate called after throwing an instance of **your exception** 
154:   what():  Exception found
```


They're not great: it's not obvious the issue stems from a call happening where it shouldn't, and they do not give a lot of information to where you should look to fix it. The best is therefore to be extremely cautious before marking a function as `noexcept`. However, use it when you can (see item 14 of [Effective modern C++](../bibliography.ipynb#Effective-Modern-C++) for incentives to use it).

<!-- #region -->
### Good practice: never throw an exception from a destructor

The explanation is quite subtle and explained in detail in item 8 of [Effective C++](../bibliography.ipynb#Effective-C++-/-More-Effective-C++); however just know you should never throw an exception there. If you need to deal with an error there, use something else (`std::abort` for instance).


### The exception class I use

I (Sébastien) provide in [appendix](../7-Appendix/HomemadeException.ipynb) my own exception class (which of course derives from `std::exception`) which provides additionally:

* A constructor with a string, to avoid defining a verbosy dedicated exception class for each case.
* Better management of the string display, with an underlying `std::string` object.
* Information about the location from where the exception was thrown.

Vincent uses the STL exceptions described in [the previous section](#Good-practice:-be-wary-of-a-forest-of-exception-classes).

## Error codes

A quick word about C-style error management which you might find in use in some libraries: **error codes**.

The principe of the error codes is that your functions and methods should return an `int` which provides an indication of the success or not of the call; the eventual values sought are returned from reference. For instance:

<!-- #endregion -->

```c++
#include <type_traits>

constexpr auto INVALID_TYPE = -1;

```

```c++

template<class T>
int AbsoluteValue(T value, T& result)
{
    if constexpr (!std::is_arithmetic<T>())
        return INVALID_TYPE;
    else
    {
        if (value < 0)
            result = -value;
        else
            result = value;

        return EXIT_SUCCESS;
    }
}
```

I don't like these error codes much, because:

* The result can't be naturally given in return value and must be provided in argument.
* You have to bookkeep the possible error codes somewhere, and a user must know this somewhere to go consult them if something happens (usually a header file: see for instance one for [PETSc library](https://www.mcs.anl.gov/petsc/petsc-master/include/petscerror.h.html)).
* In the libraries that use them, more often than not some are not self descriptive and you have to figure out what the hell the issue is.
* And more importantly, this relies on the end-user thinking to check the error value:

```c++
#include <string>
#include <iostream>

{
    std::string hello { "Hello world" };
    std::string absolute_str { "not modified at all by function call..." };

    int negative { -5 };
    int absolute_int { };

    AbsoluteValue(negative, absolute_int);
    std::cout << "Absolute value for integer is " <<  absolute_int << std::endl;
    
    AbsoluteValue(hello, absolute_str); // No compilation or runtime error (or even warning)!
    std::cout << "Absolute value for string is " << absolute_str << std::endl;
}
```

It should be noticed C++ 11 introduced a dedicated class to handle more gracefully error codes: [`std::error_code`](https://en.cppreference.com/w/cpp/error/error_code). I have no direct experience with it but it looks promising as illustrated by this [blog post](https://akrzemi1.wordpress.com/2017/07/12/your-own-error-code/).


### nodiscard

The point about forgetting to check the value may however be mitigated since C++17 with the attribute [``nodiscard``](https://en.cppreference.com/w/cpp/language/attributes/nodiscard), which helps your compiler figure out the return value should have been checked.

```c++

template<class T>
[[nodiscard]] int AbsoluteValueNoDiscard(T value, T& result)
{
    if constexpr (!std::is_arithmetic<T>())
        return INVALID_TYPE;
    else
    {
        if (value < 0)
            result = -value;
        else
            result = value;

        return EXIT_SUCCESS;
    }
}
```

```c++
#include <string>

{
    std::string hello("Hello world");
    std::string absolute_value = "";
    
    AbsoluteValueNoDiscard(hello, absolute_value); // Now there is a warning! But only available after C++ 17...
}
```


[© Copyright](../COPYRIGHT.md)   

