---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Useful concepts and STL](./0-main.ipynb)


* [Error handling](./1-ErrorHandling.ipynb)
    * [Hands-on 14](./1b-hands-on.ipynb)
* [RAII idiom](./2-RAII.ipynb)
* [Containers](./3-Containers.ipynb)
    * [Hands-on 15](./3b-hands-on.ipynb)
* [Associative containers](./4-AssociativeContainers.ipynb)
* [Move semantics](./5-MoveSemantics.ipynb)
* [Smart pointers](./6-SmartPointers.ipynb)
    * [Hands-on 16](./6b-hands-on.ipynb)
* [Algorithms](./7-Algorithms.ipynb)
    * [Hands-on 17](./7b-hands-on.ipynb) 



[© Copyright](../COPYRIGHT.md)   

