---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Useful concepts and STL](/notebooks/5-UsefulConceptsAndSTL/0-main.ipynb) -  [Hands-on 16](/notebooks/5-UsefulConceptsAndSTL/6b-hands-on.ipynb)


### **EXERCISE 40: Use a shared pointer in the `TestDisplayContainer::test_display_list_` container**

`Register` and `main` function will of course have to be adapted accordingly.

_Expected results: (unmodified)_ 

```
    [With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 3846154/100000000]
    [With 8 bits]: 0.65 ~ 0.648438 (166 / 2^8)  [error = 240385/100000000]
    [With 12 bits]: 0.65 ~ 0.649902 (2662 / 2^12)  [error = 15024/100000000]
    [With 16 bits]: 0.65 ~ 0.649994 (42598 / 2^16)  [error = 939/100000000]
    [With 20 bits]: 0.65 ~ 0.65 (681574 / 2^20)  [error = 59/100000000]
    [With 24 bits]: 0.65 ~ 0.65 (10905190 / 2^24)  [error = 4/100000000]
    [With 28 bits]: 0.65 ~ 0.65 (174483046 / 2^28)  [error = 0/100000000]
    [With 32 bits]: 0.65 ~ 0.65 (2791728742 / 2^32)  [error = 0/100000000]
    
    [With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 1785714/100000000]
    [With 8 bits]: 0.35 ~ 0.349609 (179 / 2^9)  [error = 111607/100000000]
    [With 12 bits]: 0.35 ~ 0.349976 (2867 / 2^13)  [error = 6975/100000000]
    [With 16 bits]: 0.35 ~ 0.349998 (45875 / 2^17)  [error = 436/100000000]
    [With 20 bits]: 0.35 ~ 0.35 (734003 / 2^21)  [error = 27/100000000]
    [With 24 bits]: 0.35 ~ 0.35 (11744051 / 2^25)  [error = 2/100000000]
    [With 28 bits]: 0.35 ~ 0.35 (187904819 / 2^29)  [error = 0/100000000]
    [With 32 bits]: 0.35 ~ 0.35 (3006477107 / 2^33)  [error = 0/100000000]
    
    [With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 29917/1000000]
    [With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2000/1000000]
    [With 12 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 16 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 20 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 24 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 28 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 32 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
```


### **EXERCISE 41: Replace `shared_ptr` by `unique_ptr` for  `TestDisplayContainer::test_display_list_`**

`shared_ptr` was easier to introduce first due to its easier copy mechanism, but is actually unnecessary: `unique_ptr` may provide the same service with more efficiency.

Replace `shared_ptr` by `unique_ptr`; you will need to play a bit with move semantics for that.

To add a _rvalue_ to a `std::vector`, you should use `emplace_back()` method which does almost the same as `push_back()` but is more optimized for move semantics.


### **EXERCISE 42: remove `TestDisplayContainer` class**

If you look closely at your current `TestDisplayContainer`, you should realize it is nothing more than a thin wrapper over its internal `std::vector`. Remove the class entirely and replace it by a `std::vector<str::unique_ptr<TestDisplay>>` (you may if you wish create an alias to this type), that might even be named... `TestDisplayContainer`).

Consider optimizing `Loop()` function with new `for` syntax.


[© Copyright](../COPYRIGHT.md)   

