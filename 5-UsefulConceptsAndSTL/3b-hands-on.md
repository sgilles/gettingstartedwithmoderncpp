---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Useful concepts and STL](/notebooks/5-UsefulConceptsAndSTL/0-main.ipynb) -  [Hands-on 14](/notebooks/5-UsefulConceptsAndSTL/3b-hands-on.ipynb)

<!-- #region -->
### **EXERCISE 39: Use `std::vector` to store the `TestDisplayContainer::test_display_list_` data**

Replace the tedious double pointer by a `std::vector<TestDisplay*>`.

Remove the template argument of `TestDisplayContainer`: we will now use `push_back()` to add any new element registered.

The interface of the class will be lightened:

* `GetCapacity()` (if you kept it) and `current_position_` are no longer required.
* `GetSize()` implementation should just call the `size_` method of the underlying `std::vector` object.

We'll use the following main (using back `int`):

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    try
    {
        TestDisplayContainer container;

        using type = int;

        container.Register(new TestDisplayPowerOfTwoApprox065<type>(100000000));
        container.Register(new TestDisplayPowerOfTwoApprox035<type>(100000000));
        container.Register(new TestDisplaySumOfMultiply<type>(1000000));
        
        Loop(4, 32, 4, container);
    }
    catch(const std::exception& e) // we could trap only `Exception`, but it's not a bad idea to also
                                   // catch exceptions from STL...
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}
```


_Expected results:_ 

```
    [With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 3846154/100000000]
    [With 8 bits]: 0.65 ~ 0.648438 (166 / 2^8)  [error = 240385/100000000]
    [With 12 bits]: 0.65 ~ 0.649902 (2662 / 2^12)  [error = 15024/100000000]
    [With 16 bits]: 0.65 ~ 0.649994 (42598 / 2^16)  [error = 939/100000000]
    [With 20 bits]: 0.65 ~ 0.65 (681574 / 2^20)  [error = 59/100000000]
    [With 24 bits]: 0.65 ~ 0.65 (10905190 / 2^24)  [error = 4/100000000]
    [With 28 bits]: 0.65 ~ 0.65 (174483046 / 2^28)  [error = 0/100000000]
    [With 32 bits]: 0.65 ~ 0.65 (2791728742 / 2^32)  [error = 0/100000000]
    
    [With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 1785714/100000000]
    [With 8 bits]: 0.35 ~ 0.349609 (179 / 2^9)  [error = 111607/100000000]
    [With 12 bits]: 0.35 ~ 0.349976 (2867 / 2^13)  [error = 6975/100000000]
    [With 16 bits]: 0.35 ~ 0.349998 (45875 / 2^17)  [error = 436/100000000]
    [With 20 bits]: 0.35 ~ 0.35 (734003 / 2^21)  [error = 27/100000000]
    [With 24 bits]: 0.35 ~ 0.35 (11744051 / 2^25)  [error = 2/100000000]
    [With 28 bits]: 0.35 ~ 0.35 (187904819 / 2^29)  [error = 0/100000000]
    [With 32 bits]: 0.35 ~ 0.35 (3006477107 / 2^33)  [error = 0/100000000]
    
    [With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 29917/1000000]
    [With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2000/1000000]
    [With 12 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 16 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 20 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 24 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 28 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 32 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
```

<!-- #endregion -->

[© Copyright](../COPYRIGHT.md)   

