---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Useful concepts and STL](./0-main.ipynb) - [RAII idiom](./2-RAII.ipynb)


## Introduction

This chapter is one of the most important in this tutorial: it is an idiom without which the most common critic against C++ is totally justified!

Often, people who criticizes the language say C++ is really tricky and that is extremely easy to leak memory all over the place, and that it sorely misses a [**garbage collector**](https://en.wikipedia.org/wiki/Garbage_collection_(computer_science)) which does the job of cleaning-up and freeing the memory when the data are no longer used.

However, garbage collection, used for instance in Python and Java, is not without issues itself: the memory is not always freed as swiftly as possible, and the bookkeeping of references is not free performance-wise.

C++ provides in fact the best of both worlds: a way to provide safe freeing of memory as soon as possible... provided you know how to adequately use it.

The **Resource Acquisition Is Initialization** or **RAII** idiom is the key mechanism for this; the idea is just to use an object with:
* The constructor in charge of allocating the resources (memory, mutexes, etc...)
* The destructor in charge of freeing all that as soon as the object becomes out-of-scope.

And that's it!


## Example: dynamic array

```c++
#include <string>
#include <iostream>

class Array
{
    public:

        Array(std::string name, std::size_t dimension);
    
        ~Array();
    
    private:
    
        std::string name_;
    
        double* underlying_array_ = nullptr;    
};
```

```c++
Array::Array(std::string name, std::size_t dimension)
: name_(name)
{
    std::cout << "Acquire resources for " << name_ << std::endl;
    underlying_array_ = new double[dimension];
    for (auto i = 0ul; i < dimension; ++i)
        underlying_array_[i] = 0.;
}
```

```c++
Array::~Array()
{
    std::cout << "Release resources for " << name_ << std::endl;
    delete[] underlying_array_;
}
```

```c++
{
    Array array1("Array 1", 5);
    {
        Array array2("Array 2", 2);
        
        {
            Array array3("Array 3", 2);        
        }
        
        Array array4("Array 4", 4);
    }
    Array array5("Array 5", 19);

}
```

Of course, don't use such a class: STL `std::vector` and `std::array` are already there for that (and use up RAII principle under the hood!) and provide also more complicated mechanisms such as the copy.

The resource itself needs not be memory; for instance `std::ofstream` also use up RAII: its destructor calls `close()` if not done manually before, ensuring the file on disk features properly the changes you might have done on it during the run of your program.


[© Copyright](../COPYRIGHT.md)   

