---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Useful concepts and STL](./0-main.ipynb) -  [Hands-on 14](./1b-hands-on.ipynb)


### Introduction

[This notebook](../HandsOn/HowTo.ipynb) explains very briefly your options to run the hands-ons.


### **EXERCISE 36: replace `error` function by an `Exception` class**

This exception class will:

- Take as argument a `std::string` explaining the exception (to be translated into the `what()` virtual method).
- Derives from `std::exception`.

Add a `try/catch` block in the main to properly catch the exceptions.

Try it works by introducing a behaviour that should throw (for instance reduce the `TestDisplayContainer` template parameter).

_Expected results (unmodified):_

     [With 4 bits]: 0.65 ~ 0.625  (10/2^4)  [error = 38462/1000000]
     [With 8 bits]: 0.65 ~ 0.648438  (166/2^8)  [error = 2404/1000000]
     [With 12 bits]: 0.65 ~ 0.649902  (2662/2^12)  [error = 150/1000000]
     [With 16 bits]: 0.65 ~ 0.649994  (42598/2^16)  [error = 9/1000000]
     [With 20 bits]: 0.65 ~ 0.65  (681574/2^20)  [error = 1/1000000]
     [With 24 bits]: 0.65 ~ 0.65  (10905190/2^24)  [error = 0/1000000]
     [With 28 bits]: 0.65 ~ 0.65  (174483046/2^28)  [error = 0/1000000]
     [With 32 bits]: 0.65 ~ 0.65  (2791728742/2^32)  [error = 0/1000000]

     [With 4 bits]: 0.35 ~ 0.34375  (11/2^5)  [error = 17857/1000000]
     [With 8 bits]: 0.35 ~ 0.349609  (179/2^9)  [error = 1116/1000000]
     [With 12 bits]: 0.35 ~ 0.349976  (2867/2^13)  [error = 70/1000000]
     [With 16 bits]: 0.35 ~ 0.349998  (45875/2^17)  [error = 4/1000000]
     [With 20 bits]: 0.35 ~ 0.35  (734003/2^21)  [error = 0/1000000]
     [With 24 bits]: 0.35 ~ 0.35  (11744051/2^25)  [error = 0/1000000]
     [With 28 bits]: 0.35 ~ 0.35  (187904819/2^29)  [error = 0/1000000]
     [With 32 bits]: 0.35 ~ 0.35  (3006477107/2^33)  [error = 0/1000000]

     [With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 299/10000]
     [With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 20/10000]
     [With 12 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 2/10000]
     [With 16 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 2/10000]
     [With 20 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 2/10000]
     [With 24 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 2/10000]
     [With 28 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 2/10000]
     [With 32 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 2/10000]


### **EXERCISE 37: proper handling of overflow**

We are now well equipped to deal with the overflow issue we saw earlier. 

We would like to print _Overflow!_ in the lines when the chosen number of bits is not compatible with the chosen integer type. Contrary to what we did in previous exercise, we want to recover after such an exception, not to terminate immediately the program.

This [StackOverflow post](https://stackoverflow.com/questions/199333/how-to-detect-unsigned-integer-Multiply-overflow) provides an interesting non-standard feature introduced in clang and gcc to deal with this: special functions that tell if an operation will overflow (other replies give hints to do so manually). The functions to use are:

```
__builtin_add_overflow(T a, T b, T* sum)
__builtin_mul_overflow(T a, T b, T* product)
```

where `T` is the type considered; these functions return 0 if no overflow occurs (and result is in this case given in third argument).

With this:

* Replace `long` by `int` to make the issues reappear.
* Use the two functions above to identify when an overflow occur. There are three such checks to add (even if only two plays out with the `int` case):
    - In `TimesPowerOf2()`
    - In `operator*(IntT, const PowerOfTwoApprox<IntT>&)`
    - In `TestDisplaySumOfMultiply::Display()`
* Define a `void TestDisplay::PrintOverflow(int nbits, const Exception& e) const` method that will provide the print when an overflow occurs. This method will be used in the `catch` blocks of `Display` methods.

_Expected results:_

```
    [Width 4 bits]: 0.65 ~ 0.625  (10/2^4)  [error = 3846154/100000000]
    [Width 8 bits]: 0.65 ~ 0.648438  (166/2^8)  [error = 240385/100000000]
    [Width 12 bits]: 0.65 ~ 0.649902  (2662/2^12)  [error = 15024/100000000]
    [Width 16 bits]: 0.65 ~ 0.649994  (42598/2^16)  [error = 939/100000000]
    [Width 20 bits]: 0.65 ~ 0.65  (681574/2^20)  [error = 59/100000000]
    [Width 24 bits]: 0.65 ~ 0.65  (10905190/2^24)  [error = 4/100000000]
    [Width 28 bits]: 0.65 ~ 0.65  (174483046/2^28)  [error = 0/100000000]
    [Width 32 bits]: Overflow! (in TimesPowerOf2())

    [Width 4 bits]: 0.35 ~ 0.34375  (11/2^5)  [error = 1785714/100000000]
    [Width 8 bits]: 0.35 ~ 0.349609  (179/2^9)  [error = 111607/100000000]
    [Width 12 bits]: 0.35 ~ 0.349976  (2867/2^13)  [error = 6975/100000000]
    [Width 16 bits]: 0.35 ~ 0.349998  (45875/2^17)  [error = 436/100000000]
    [Width 20 bits]: 0.35 ~ 0.35  (734003/2^21)  [error = 27/100000000]
    [Width 24 bits]: 0.35 ~ 0.35  (11744051/2^25)  [error = 2/100000000]
    [Width 28 bits]: 0.35 ~ 0.35  (187904819/2^29)  [error = 0/100000000]
    [Width 32 bits]: Overflow! (in TimesPowerOf2())

    [Width 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 29917/1000000]
    [Width 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2000/1000000]
    [Width 12 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [Width 16 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [Width 20 bits]: Overflow! (in operator*(IntT, const PowerOfTwoApprox<IntT>&))
    [Width 24 bits]: Overflow! (in operator*(IntT, const PowerOfTwoApprox<IntT>&))
    [Width 28 bits]: Overflow! (in operator*(IntT, const PowerOfTwoApprox<IntT>&))
    [Width 32 bits]: Overflow! (in TimesPowerOf2())
```

<!-- #region -->
### **EXERCISE 38: char case return**

Let's consider again the `char` case first handled in exercise 34:

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    try
    {
        TestDisplayContainer<2ul> container;

        using type = char;

        container.Register(new TestDisplayPowerOfTwoApprox065<type>(100000000));
        container.Register(new TestDisplayPowerOfTwoApprox035<type>(100000000));
        //container.Register(new TestDisplaySumOfMultiply<type>(1000000));
        
        Loop(1, 4, 1, container);
    }
    catch(const std::exception& e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}
```

If you run it directly, you will get something like:

    [With 1 bits]: 0.65 ~ 0.5 (1 / 2^1)  [error = 59/100000000]
    [With 2 bits]: 0.65 ~ 0.75 (3 / 2^2)  [error = 39/100000000]
    [With 3 bits]: 0.65 ~ 0.625 (5 / 2^3)  [error = 10/100000000]
    [With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 10/100000000]
    
    [With 1 bits]: 0.35 ~ 0.25 (1 / 2^2)  [error = 37/100000000]
    [With 2 bits]: 0.35 ~ 0.375 (3 / 2^3)  [error = -55/100000000]
    [With 3 bits]: 0.35 ~ 0.375 (6 / 2^4)  [error = -55/100000000]
    [With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 114/100000000]

which is deeply suspicious... (negative errors, and if you compare to output of exercise 34 positive errors aren't consistent either). In exercise 34 we cheated to avoid that by lowering the resolution (to 100).

The reason is that one of our function is not secure at all: `RoundAsInt()` converts a double into an integer, but doesn't check if the double is within the acceptable range of the integer type used... With the lower resolution the error computation remained under the correct range.

So add protections inside `RoundAsInt` to throw an exception when the double is out of proper range.

You will need [numeric limits](https://en.cppreference.com/w/cpp/types/numeric_limits) to determine the boundaries of `IntT`; beware that the interface is a bit weird and `min()` is probably not what you're looking for...

Expected result should be:

    [With 1 bits]: Double '2.30769e+07' can't be rounded as an integer!
    [With 2 bits]: Double '1.53846e+07' can't be rounded as an integer!
    [With 3 bits]: Double '3.84615e+06' can't be rounded as an integer!
    [With 4 bits]: Double '3.84615e+06' can't be rounded as an integer!
    
    [With 1 bits]: Double '2.85714e+07' can't be rounded as an integer!
    [With 2 bits]: Double '7.14286e+06' can't be rounded as an integer!
    [With 3 bits]: Double '7.14286e+06' can't be rounded as an integer!
    [With 4 bits]: Double '1.78571e+06' can't be rounded as an integer!

and it you lower resolution to 1000:

    [With 1 bits]: Double '230.769' can't be rounded as an integer!
    [With 2 bits]: Double '153.846' can't be rounded as an integer!
    [With 3 bits]: 0.65 ~ 0.625 (5 / 2^3)  [error = 38/1000]
    [With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 38/1000]
    
    [With 1 bits]: Double '285.714' can't be rounded as an integer!
    [With 2 bits]: 0.35 ~ 0.375 (3 / 2^3)  [error = 71/1000]
    [With 3 bits]: 0.35 ~ 0.375 (6 / 2^4)  [error = 71/1000]
    [With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 18/1000]

(a bit counterintuitive, but it's really the error computation that goes out of bounds!)

<!-- #endregion -->

[© Copyright](../COPYRIGHT.md)   

