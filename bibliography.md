---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Bibliography](./bibliography.ipynb)


## Books

### Effective C++ / More Effective C++

Albeit being now quite old (and therefore pre-C++ 11), these books are still to my mind highly recommended reading: Scott Meyers explain several items in few pages each with a very precious insight on why some idioms should be employed. The style is rather enjoyable: the reading is easy, which is not a small feat for a programming book!

Scott Meyers present _Effective C++_ as the second book you should read about C++, the first one being the one explaing the basics to you. Hopefully this formation will play the role of the first book for you, allowing you to jump on the bandwagon and reading these directly.

- [Effective C++: 55 Specific Ways to Improve Your Programs and Designs (3rd Edition)](https://search.library.ucla.edu/discovery/fulldisplay?docid=alma99587823606533&context=L&vid=01UCS_LAL:UCLA&lang=en&search_scope=ArticlesBooksMore&adaptor=Local%20Search%20Engine&tab=Articles_books_more_slot&query=any,contains,scott%20meyers&offset=0)  by Scott Meyers.
- [More Effective C++: 35 New Ways to Improve Your Programs and Designs](https://search.library.ucla.edu/discovery/fulldisplay?vid=01UCS_LAL:UCLA&search_scope=ArticlesBooksMore&tab=Articles_books_more_slot&docid=alma99587833606533&lang=en&context=L&adaptor=Local%20Search%20Engine&query=any,contains,scott%20meyers&offset=0&virtualBrowse=true) by Scott Meyers.


### Effective Modern C++

Scott Meyers's most recent C++ book, and probably his last (he announced his retirement from C++ few years ago). Very helpful to grasp many new C++ 11/14 concepts (its explanation of move semantics is really clear and corrected for me many unexact stuff read on the Web on the topic) but a bit steeper for a beginner. So also a very recommended reading, but probably once you've read the two precedent entries before.

- [Effective modern C++ : 42 specific ways to improve your use of C++11 and C++14](https://search.library.ucla.edu/discovery/fulldisplay?docid=alma9914814323506531&context=L&vid=01UCS_LAL:UCLA&lang=en&search_scope=ArticlesBooksMore&adaptor=Local%20Search%20Engine&tab=Articles_books_more_slot&query=any,contains,effective%20modern%20C%2B%2B&offset=0) by Scott Meyers.


### The C++ Standard Library: A Tutorial and Reference

A very helpful reference guide to know what is offered in the C++ standard library. The second edition has been updated to add many new features introduced in C++ 11.

- [The C++ Standard Library: A Tutorial and Reference, Second Edition](https://search.library.ucla.edu/discovery/fulldisplay?docid=alma9914810872606531&context=L&vid=01UCS_LAL:UCLA&lang=en&search_scope=ArticlesBooksMore&adaptor=Local%20Search%20Engine&tab=Articles_books_more_slot&query=any,contains,nicolai%20josuttis&offset=0) by Nicolai M. Josuttis.


### Modern C++ Design

A very brilliant book to understand the reasoning behind metaprogramming, by one of the father of this branch of software development. Clearly not for the faint of heart, but a really enlightening reading; I especially recommend the chapter about policies (the first one is also very interesting but some of the stuff there is now directly available in the standard library).

- [ Modern C++ design : generic programming and design patterns applied](https://search.library.ucla.edu/discovery/fulldisplay?docid=alma9943936983606533&context=L&vid=01UCS_LAL:UCLA&lang=en&search_scope=ArticlesBooksMore&adaptor=Local%20Search%20Engine&tab=Articles_books_more_slot&query=any,contains,andrei%20alexandrescu&offset=0) by Andrei Alexandrescu.


### Exceptional C++ / More Exceptional C++

These books are questions and answers that provide very helpful insight; the reading is however a bit terser than Scott Meyers' books. The content is a bit more advanced - in introduction Herb Sutter tells his books should be the third reading after Meyers' Effective C++ one... 

- [Exceptional C++ : 47 engineering puzzles, programming problems, and solutions](https://search.library.ucla.edu/discovery/fulldisplay?docid=alma99588013606533&context=L&vid=01UCS_LAL:UCLA&lang=en&search_scope=ArticlesBooksMore&adaptor=Local%20Search%20Engine&tab=Articles_books_more_slot&query=any,contains,exceptional%20C%2B%2B&offset=0) by Herb Sutter.
- [More exceptional C++ : 40 new engineering puzzles, programming problems, and solutions](https://search.library.ucla.edu/discovery/fulldisplay?vid=01UCS_LAL:UCLA&search_scope=ArticlesBooksMore&tab=Articles_books_more_slot&docid=alma99588043606533&lang=en&context=L&adaptor=Local%20Search%20Engine&query=any,contains,exceptional%20C%2B%2B&offset=0&virtualBrowse=true) by Herb Sutter.


### Effective STL

The only Scott Meyers book I would not recommend entirely heartily: as always it offers interesting insight, but it tries a bit too hard to sell the horrendous syntax required by many algorithms prior to the C++ 11. Still a good reading, but don't start by this one.

- [Effective STL : 50 specific ways to improve your use of the standard template library](https://search.library.ucla.edu/discovery/fulldisplay?vid=01UCS_LAL:UCLA&search_scope=ArticlesBooksMore&tab=Articles_books_more_slot&docid=alma99587823606533&lang=en&context=L&adaptor=Local%20Search%20Engine&query=any,contains,scott%20meyers&offset=0&virtualBrowse=true) by Scott Meyers.


### Functional Programming in C++

A very interesting book arguing C++ may be a rather good fit for the functional paradigm, especially in its later versions.

I (Sébastien) learnt a lot reading it; however the difficulty threshold is a wild ride here (some chapters are rather trivial and others... not so much...)

- [Functional programming in C++](https://search.library.ucla.edu/discovery/fulldisplay?docid=alma9914812800006531&context=L&vid=01UCS_LAL:UCLA&lang=en&search_scope=ArticlesBooksMore&adaptor=Local%20Search%20Engine&tab=Articles_books_more_slot&query=any,contains,functional%20programming%20in%20C%2B%2B&offset=0) by Ivan Čukić.


## Online


### [CppReference](https://en.cppreference.com)

Provides a dictionary entry with the API explained for every functions/algorithms/class/you name it from the language and the standard library. Usually you will find them upfront if you Google a term from C++ (e.g. `std::vector`). Explanation might be a bit terse, but there is now in most cases an example powered by Coliru which present a concrete (albeit simple - but you're accustomed to that if you followed this tutorial!) use case.

### [isocpp](https://isocpp.org/faq)

A gigantic FAQ that covers many aspects of C++; this site is in fact a merge from two previously independent FAQ, one of which was maintained by Bjarne Stroustrup, creator of the language.

### [FluentCpp](https://www.fluentcpp.com)

A very interesting blog with lots of interesting content, unfortunately not updated since June 2022 (but it's still a gold mine)

### [SED Saclay technology watch](https://sed.saclay.inria.fr/kw_c++.html)

In the experimentation and development departement ([SED](http://sed.saclay.inria.fr/)) at [Inria Saclay](https://www.inria.fr/centre/saclay), we publish about each week few links we found interesting. C++ is rather often mentioned; the link above provides all such links related to C++ since we started doing so.

### [C++ Weekly With Jason Turner](https://www.youtube.com/@cppweekly)

A youtube channel covering various aspects of very modern C++, with a new episode uploaded weekly. Each episode focuses on one specific topic and keeps it short (5 to 10mn videos).


[© Copyright](../COPYRIGHT.md)   
