© _CNRS 2016_ - _Inria 2018-2024_   

This notebook is an adaptation of a [lecture](https://gitlab.inria.fr/formations/cpp/DebuterEnCpp) prepared by David Chamont (CNRS) with the help of Vincent Rouvreau (Inria) under the terms of the licence [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

Present version has been written by several Inria engineers that acted as lecturers:

- Sébastien Gilles (SED Saclay): _2018 - present_
- Vincent Rouvreau (SED Saclay): _2018 - present_
- Vicente Mataix Ferrandiz (SED Paris): _2021_
- Laurent Steff (SED Saclay): _2021 - 2023_
- Jérôme Diaz (M3DISIM team): _2024 - present_

and is using the same licence as the original training session.

Many thanks for contributors:

- Mathias Malandain (SED Rennes)
- Benjamin Nguyen van Yen (SED Saclay)

and the participants of former training sessions whose comments help us improve the support over time!