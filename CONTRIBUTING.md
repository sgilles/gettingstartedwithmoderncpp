# Contribution guide

All contributions are welcome, but please read this first.

## Git hook

If you intend to contribute, please install first the pre-commit hook:

```shell
conda install -c conda-forge pre-commit # or pip install pre-commit
pre-commit install
```

This hook clears executed cells when committing a notebook.


## Workflow

The project is led through an integration manager workflow, so when you have a contribution to share, just open a merge request.

The MR will be reviewed and integrated directly by one of the project managers.

If you're not accustomed to merge request, here is a step-by-step procedure to create one; the first steps (creating the fork and setting it up locally on your computer) need to be done only once:

- Fork the [project](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp) by clicking on the "Fork" button 
![](Images/Fork.png) 
- Choose where you want to fork the project (usually it's in your personal space in Gitlab, but you may choose another group at your convenience).
- Go to your fork, then click on "Code"
![](Images/Clone1.png)
and then copy one of the URL given in the resulting menu (I recommend SSH, but make sure you provided properly the appropriate key in your [Gitlab profile](https://gitlab.inria.fr/-/profile/keys)).
<img src="Images/Clone2.png" alt="image" width="50%" height="auto">
- Add locally a new remote by going to the local copy of the project on your computer and typing:
```shell
git remote add mine *url*
``` 

where *url* is the URL copied from your fork (`mine` is a mere suggestion; call it however you see fit).
- Push your branch to this new remote repository through
```shell
git push mine *your_branch_name*
```
- You may at last create the merge request by clicking on the banner in your fork:
![](Images/CreateMR.png)

By default it will propose to push it toward the main repository of the formation.

Please read [the CI README](./CI.md) to set up properly your fork!