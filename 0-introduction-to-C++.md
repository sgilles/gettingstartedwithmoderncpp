---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

<!-- #region slideshow={"slide_type": "-"} -->
# [Getting started in C++](./) - [A brief introduction](./0-introduction-to-C++.ipynb)
<!-- #endregion -->

## A very brief historic

- C++ was first published in 1985 by Bjarne Stroustrup with the idea of extending C with object programming; the first name of the language was _C with classes_
- The first standard was issued in 1998 and called _C++ 98_
- A first (minor) update was issued in 2003 (and dubbed *C++ 03*)
- The plan was to provide a major overhaul for the next version, which was called for a long time *C++ 0x*.
- The schedule failed, as the next standard turned out to be *C++ 11*. It is a major update, with lots of new features and syntactic sugar introduced.
- The plan was now to publish a release every three years, alternating minor and major ones. The committee followed the planned schedule more successfully than for *C++ 0x*, except for the minor/major:
    * *C++ 14* was a polishing of *C++ 11*, as intended.
    * *C++ 17* introduced more new stuff than *C++ 14*, but not as many as initially intended.
    * *C++ 20* is a major update, almost as groundbreaking as *C++ 11*.
    * *C++ 23* has been finalized in February 2023; but many features from both language and standard library aren't supported yet by compilers.
    * *C++ 26* is under discussed and some of the features are already available.


<!-- #region -->
## Which standard will be tackled in this lecture?


### C++ 11/14/17 rather than C++ 98/03

The new major standard is now widely supported by compilers, and introduces many features that are very useful. As it is much more pleasant to use, it would be a shame to restrict ourselves to the older versions of the standard.

However, you may have to tackle legacy code which is written in C++ 98/03, so we indicated as much as possible for each recent feature presented when it was actually introduced (if not specified assume it was already present in C++ 98).


### But which one should you use? 11, 14 or 17?

As indicated above, we tried to specify clearly in which standard specific features were introduced. Few guidelines:

- C++ 14 is now a safe bet for most compilers, so you should probably choose this one instead of C++ 11.
- C++ 17 support is now really widespread as well, but you may still lack some features if your distro is a bit backward (for instance default gcc compiler on still supported Ubuntu LTS 18.04 does not support the brand new filesystem library (yes C++ was not historically a _batteries included_ language...)). Vincent is working on a library named [Gudhi](https://gudhi.inria.fr/) that is rather conservative for the standard use (in the sense they want most users to be able to compile the code without having to install brand new environment) and they switched to C++ 17 in December 2022.


### And C++ 20?


C++ 20 will not be addressed much in this lecture... even though at least one of us (Sébastien) dabbled a lot with it since 2023.

It has been published officially at the end of 2020, but some of its features are still not widely supported by current compilers.

We will obviously update this lecture when it becomes widespread, as it will introduce very cool stuff (and the promise of much better compilation errors for templates...). We also have a technical difficulty in that Xeus-cling technology we're using is not really maintained currently, and the issue asking whether C++ 20 support is foreseen [remains unanswered](https://github.com/jupyter-xeus/xeus-cling/issues/510) when these lines are written (February 2024), while the underlying `cling` which is used to make interpreted-like C++ support it now (see for instance [this talk](https://indico.jlab.org/event/459/contributions/11563/attachments/9696/14191/CHEP_2023_cling_Interpreting_C__20.pdf)).

### Compiler support

We will mention it again later, but [following cppreference link for compiler support](https://en.cppreference.com/w/cpp/compiler_support) is a very handy link that tells which feature is supported by which compiler.

It may help you to choose which standard to use for your project depending on the expected user of the code (can you expect your users to have fairly up-to-date OS and compilers installed? Do you plan your code to be used only in a specific environment, or must it be cross-platform? etc...)

Don't worry we will tackle that again at the end of this training session, but if you intend to use C++ fairly regularly it's a good idea to bookmark this link!
<!-- #endregion -->

## A multi-paradigm language

C++ was originally created with the will to provide object programming to C, but it is truly now a multi-paradigm language.

This lecture will cover three of them:

- [Procedural programming](1-ProceduralProgramming/0-main.ipynb)
- [Object programming](2-ObjectProgramming/0-main.ipynb)
- [Generic programming](4-Templates/0-main.ipynb)

There are actually even more: functional programming for instance seems to be gaining traction at the moment and will be eased in C++ 20 standard (see [Ivan Čukić's book](../bibliography.ipynb#Functional-Programming-in-C++) for more about functional programming in C++).

This richness is not always perceived as a boon: there is a section in the [Wikipedia page](https://en.wikipedia.org/wiki/C%2B%2B#Criticism) dedicated to the criticisms addressed at C++ by notorious developers (but also a defense by Brian Kernighan!)


[© Copyright](COPYRIGHT.md)
