---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Templates](./0-main.ipynb)


* [Introduction to the concept of templates](./1-Intro.ipynb)
    * [Hands-on 12](./1b-hands-on.ipynb)
* [Specialization](./2-Specialization.ipynb)
* [Special syntax: typename, template and mandatory this](./3-Syntax.ipynb)
    * [Hands-on 13](./3b-hands-on.ipynb)
* [Metaprogramming](./4-Metaprogramming.ipynb)
* [Hints to more advanced concepts with templates](./5-MoreAdvanced.ipynb)



[© Copyright](../COPYRIGHT.md)   

