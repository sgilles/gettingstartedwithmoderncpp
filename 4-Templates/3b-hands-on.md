---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Templates](./0-main.ipynb) - [Hands-on 13](./3b-hands-on.ipynb)

<!-- #region -->
### **EXERCISE 33: make `TestDisplayPowerOfTwoApprox` a template class**

Now we will make `TestDisplayPowerOfTwoApprox` and its derived classes template classes. We will therefore remove the hardcoded `long` we introduced in previous exercise.

In the derived classes, you will need to help the compiler to figure out inheritance is at play, with `this` or an appropriate alias.

Expected should result the same... but now the end-user may modify at one place the type if he wants to try with another type.

New main is:

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    TestDisplayContainer container(3ul);

    using type = long;

    container.Register(new TestDisplayPowerOfTwoApprox065<type>(100000000));
    container.Register(new TestDisplayPowerOfTwoApprox035<type>(100000000));
    container.Register(new TestDisplaySumOfMultiply<type>(1000000));
    
    Loop(4, 32, 4, container);
    
    return EXIT_SUCCESS;
}
```



_Expected results (unmodified):_

     [With 4 bits]: 0.65 ~ 0.625  (10/2^4)  [error = 38462/1000000]
     [With 8 bits]: 0.65 ~ 0.648438  (166/2^8)  [error = 2404/1000000]
     [With 12 bits]: 0.65 ~ 0.649902  (2662/2^12)  [error = 150/1000000]
     [With 16 bits]: 0.65 ~ 0.649994  (42598/2^16)  [error = 9/1000000]
     [With 20 bits]: 0.65 ~ 0.65  (681574/2^20)  [error = 1/1000000]
     [With 24 bits]: 0.65 ~ 0.65  (10905190/2^24)  [error = 0/1000000]
     [With 28 bits]: 0.65 ~ 0.65  (174483046/2^28)  [error = 0/1000000]
     [With 32 bits]: 0.65 ~ 0.65  (2791728742/2^32)  [error = 0/1000000]

     [With 4 bits]: 0.35 ~ 0.34375  (11/2^5)  [error = 17857/1000000]
     [With 8 bits]: 0.35 ~ 0.349609  (179/2^9)  [error = 1116/1000000]
     [With 12 bits]: 0.35 ~ 0.349976  (2867/2^13)  [error = 70/1000000]
     [With 16 bits]: 0.35 ~ 0.349998  (45875/2^17)  [error = 4/1000000]
     [With 20 bits]: 0.35 ~ 0.35  (734003/2^21)  [error = 0/1000000]
     [With 24 bits]: 0.35 ~ 0.35  (11744051/2^25)  [error = 0/1000000]
     [With 28 bits]: 0.35 ~ 0.35  (187904819/2^29)  [error = 0/1000000]
     [With 32 bits]: 0.35 ~ 0.35  (3006477107/2^33)  [error = 0/1000000]

     [With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 299/10000]
     [With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 20/10000]
     [With 12 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 2/10000]
     [With 16 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 2/10000]
     [With 20 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 2/10000]
     [With 24 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 2/10000]
     [With 28 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 2/10000]
     [With 32 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 2/10000]
<!-- #endregion -->

<!-- #region -->
### **EXERCISE 34: `char`specialisation**

Let's consider now the following `main`, in which we consider the `char` case (`char` may be used to represent very small integer, from -128 to 127, as well as standard characters):

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    TestDisplayContainer container(2ul);

    using type = char;

    container.Register(new TestDisplayPowerOfTwoApprox065<type>(100));
    container.Register(new TestDisplayPowerOfTwoApprox035<type>(100));
    
    Loop(1, 4, 1, container);
    
    return EXIT_SUCCESS;
}
```
(`TestDisplaySumOfMultiply` is deactivated as it doesn't make sense for `char` - both 3515 and 4832 are well beyond the range covered by `char`)

You should observe weird results: the display is not great with gibberish provided where `char` values were expected to be computed. For instance I get with clang on Fedora:

```shell
[With 1 bits]: 0.65 ~ 0.5 ( / 2^1)  [error = ?/1000]
[With 2 bits]: 0.65 ~ 0.75 ( / 2^2)  [error = ?/1000]
[With 3 bits]: 0.65 ~ 0.625 ( / 2^3)  [error = &/1000]
[With 4 bits]: 0.65 ~ 0.625 (
 / 2^4)  [error = &/1000]

[With 1 bits]: 0.35 ~ 0.25 ( / 2^2)  [error = /1000]
[With 2 bits]: 0.35 ~ 0.375 ( / 2^3)  [error = G/1000]
[With 3 bits]: 0.35 ~ 0.375 ( / 2^4)  [error = G/1000]
[With 4 bits]: 0.35 ~ 0.34375 (
                                / 2^5)  [error = /1000]
```

(the spaces are really there).

The reason is that we need to tell explicitly we want to print `char` result as an integer.

There are better ways to do so (we'll see them in next part), but for the time being we will solve this through a template specialization:

- Introduce a free function `DisplayInteger()` that just print the integer on standard output in general case and converts it first into an integer if the type is a `char`.
- Use this function in `PrintLine()` method to display the computed `error`.
- Use this function in `std::ostream& operator<<(std::ostream& out, const PowerOfTwoApprox<IntT>& approximation)` method to display the numerator.

Expected result is:

```shell
    [With 1 bits]: 0.65 ~ 0.5 (1 / 2^1)  [error = 23/100]
    [With 2 bits]: 0.65 ~ 0.75 (3 / 2^2)  [error = 15/100]
    [With 3 bits]: 0.65 ~ 0.625 (5 / 2^3)  [error = 4/100]
    [With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 4/100]
    
    [With 1 bits]: 0.35 ~ 0.25 (1 / 2^2)  [error = 29/100]
    [With 2 bits]: 0.35 ~ 0.375 (3 / 2^3)  [error = 7/100]
    [With 3 bits]: 0.35 ~ 0.375 (6 / 2^4)  [error = 7/100]
    [With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 2/100]
```
<!-- #endregion -->

<!-- #region -->
### **EXERCISE 35: integer template argument**

We'll go back to our previous `main`:

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    TestDisplayContainer container(3ul);

    using type = long;

    container.Register(new TestDisplayPowerOfTwoApprox065<type>(100000000));
    container.Register(new TestDisplayPowerOfTwoApprox035<type>(100000000));
    container.Register(new TestDisplaySumOfMultiply<type>(1000000));
    
    Loop(4, 32, 4, container);
    
    return EXIT_SUCCESS;
}
```

Make `TestDisplayContainer` a template class which template argument is an integer `CapacityN` that specifies the number of elements that might be stored inside (data attribute `capacity_` is of course no longer required!)

There are several school of thoughts about the type you should give this integer:

* The inspiration for this lecture advised strongly to make it an `int`, to avoid conversions when the size was compared with other quantities that are represented by an `int`. You should in this case add a `static_assert` within the class declaration to ensure the quantity is positive:
```c++
static_assert(SizeN > 0)
```
* You may prefer to use an `unsigned int` - personally I like to use unsigned types when a quantity has no business being negative.
* You may even prefer `std::size_t`, which is for most distributions an alias for `unsigned long`. This might seem overkill, but it is the choice in the standard library containers; it's the choice we made in the proposed solution.

Expected result is the same as in exercise 33:

    [With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 3846154/100000000]
    [With 8 bits]: 0.65 ~ 0.648438 (166 / 2^8)  [error = 240385/100000000]
    [With 12 bits]: 0.65 ~ 0.649902 (2662 / 2^12)  [error = 15024/100000000]
    [With 16 bits]: 0.65 ~ 0.649994 (42598 / 2^16)  [error = 939/100000000]
    [With 20 bits]: 0.65 ~ 0.65 (681574 / 2^20)  [error = 59/100000000]
    [With 24 bits]: 0.65 ~ 0.65 (10905190 / 2^24)  [error = 4/100000000]
    [With 28 bits]: 0.65 ~ 0.65 (174483046 / 2^28)  [error = 0/100000000]
    [With 32 bits]: 0.65 ~ 0.65 (2791728742 / 2^32)  [error = 0/100000000]
    
    [With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 1785714/100000000]
    [With 8 bits]: 0.35 ~ 0.349609 (179 / 2^9)  [error = 111607/100000000]
    [With 12 bits]: 0.35 ~ 0.349976 (2867 / 2^13)  [error = 6975/100000000]
    [With 16 bits]: 0.35 ~ 0.349998 (45875 / 2^17)  [error = 436/100000000]
    [With 20 bits]: 0.35 ~ 0.35 (734003 / 2^21)  [error = 27/100000000]
    [With 24 bits]: 0.35 ~ 0.35 (11744051 / 2^25)  [error = 2/100000000]
    [With 28 bits]: 0.35 ~ 0.35 (187904819 / 2^29)  [error = 0/100000000]
    [With 32 bits]: 0.35 ~ 0.35 (3006477107 / 2^33)  [error = 0/100000000]
    
    [With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 29917/1000000]
    [With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2000/1000000]
    [With 12 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 16 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 20 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 24 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 28 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 32 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]

<!-- #endregion -->

[© Copyright](../COPYRIGHT.md)   

