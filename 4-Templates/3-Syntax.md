---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Templates](./0-main.ipynb) - [Special syntax: typename, template and mandatory this](./3-Syntax.ipynb)


In this chapter, we will review some cases involving templates in which additional syntax are required to help the compiler to do its job; it is useful to have a look because these not that difficult rules might be puzzling if their whereabouts have not been explained a bit.

## Mandatory `this` in calling base class methods




```c++
template<class T>
class Base
{
public:
    
    Base() = default;
    
    void BaseMethod()
    { }
    
};
```

```c++
template<class T>
class Derived : public Base<T>
{
public:
    
    Derived() = default;
    
    void DerivedMethod();

};
```

```c++
template<class T>
void Derived<T>::DerivedMethod()
{
    BaseMethod(); // Compilation error!
}
```

Same code without the template argument would be accepted without issue... but with templates the compiler doesn't manage to properly find the base method.

To circumvent this, you must explicitly tell `BaseMethod()` is a method; the most obvious way is to put explicit `this` (which we introduced [here](../2-ObjectProgramming/2-Member-functions.ipynb#The-this-keyword)):

```c++
template<class T>
void Derived<T>::DerivedMethod()
{
    this->BaseMethod(); // Now ok!
}
```

Personally, I even prefer to use an alias which points to the base class: this way it is even clearer where the method (even more so if there are several parent classes).

```c++
template<class T>
class Derived2 : public Base<T>
{
public:
    
    Derived2() = default;
    
    using parent = Base<T>;
    
    void DerivedMethod()
    {
        parent::BaseMethod(); // also ok!
    }

};
```

## Mandatory `typename` keyword

In a `std::vector`, as in several STL constructs, there are several **traits** which are defined: a trait is a type information concerning the class. [This page](https://en.cppreference.com/w/cpp/container/vector) mentions all of them in the `Member types` section.

The first one simply returns the type of the value stored in a `std::vector`, for instance for `std::vector<int>::value_type` is just `int`.

```c++
#include <vector>

{
    std::vector<int>::value_type i = 5; // convoluted way to just say int i = 5!
}
```

However, this simple construct fails in the following case:

```c++
#include <iostream>

template<class ContainerT>
void PrintFive()
{
    std::cout << static_cast<ContainerT::value_type>(5) << std::endl;
}
```

You might see it's just that it was never mentioned `ContainerT` was intended to be a `std::vector`... and you would be right. That's not the full story though: the point of a template function is to just work with any given class that just respects the interface (it's very similar to the [**duck typing**](https://en.wikipedia.org/wiki/Duck_typing) so dear to Pythonists: _If it walks like a duck and it quacks like a duck, then it must be a duck_).

The issue in fact here is that the compiler doesn't even know that `value_type` is expected to be a type and not an attribute. So you have to help it with the keyword `typename` just before the type in question (just as indicated by the compiler: fortunately they are now rather helpful in pointing out this kind of issue):

```c++
#include <iostream>

template<class ContainerT>
void PrintFive()
{
    std::cout << static_cast<typename ContainerT::value_type>(5) << std::endl;
}
```

```c++
PrintFive<int>(); // Won't compile: no trait 'value_type' for an 'int'!
```

```c++
PrintFive<std::vector<int>>(); // Ok!
```

C++ 20 alleviates the need for `typename` keyword in some cases (see for instance this [StackOverflow](https://stackoverflow.com/questions/61990971/why-dont-i-need-to-specify-typename-before-a-dependent-type-in-c20) post with two very interesting links related to that topic); at the moment I must admit I keep putting it everywhere as previously and I am not very knowledgeable when it can be avoided.


### [optional] A bit of wandering: how will C++ 20 concepts will help to enforce the constraint upon the template parameter

Once again, C++ 20 concept will be much appreciated here to provide more straightforward error messages (see [@Coliru](https://coliru.stacked-crooked.com/a/4cc87e5a41d1a94c)):

```c++
// C++ 20 code - run it in Coliru!

#include <iostream>
#include <vector>
#include <list>

template<typename T>
concept HasValueType = requires(T) 
{
    typename T::value_type;
};

template<class ContainerT>
requires HasValueType<ContainerT> 
void PrintFive()
{
    std::cout << static_cast<typename ContainerT::value_type>(5) << std::endl;
}


int main(int argc, char** argv)
{
    PrintFive<std::vector<int>>();
    
    PrintFive<std::list<int>>();
    
    // PrintFive<int>(); // COMPILATION ERROR! But with a clear error message.
    
    return EXIT_SUCCESS;   
}
```

The error message (with gcc) includes all the relevant information to understand what is wrong the the `int` case:

```shell
main.cpp: In function 'int main(int, char**)':
main.cpp:27:20: error: use of function 'void PrintFive() [with ContainerT = int]' with unsatisfied constraints
   27 |     PrintFive<int>(); // COMPILATION ERROR! But with a clear error message.  
...
main.cpp:8:14: note: the required type 'typename T::value_type' is invalid
    8 |     typename T::value_type;
```



## Mandatory `template` keyword


Likewise, compiler sometimes needs help to figure out a template is involved. For instance:

```c++
struct Foo
{
    template<int N>
    void PrintN() const;    
};
```

```c++
#include <iostream>

template<int N>
void Foo::PrintN() const
{
    std::cout << N << std::endl;
}
```

```c++ jupyter={"source_hidden": true}
template<class U>
void Print(const U& u)
{
    std::cout << u.PrintN<0>() << std::endl;
}
```

Here the compiler is confused and doesn't understand we mean `PrintN` to be a template method; there is in fact an ambiguity with operators `<` and `>`. These ambiguity cases arises when two templates are involved.

To help the compiler figure it out, the solution is very similar to the `typename` one: we specify an explicit template to help the compiler.

Fortunately, compilers (at least clang and gcc) became very recently much more helpful and gives away in the error message the probable fix. It wasn't the case until very recently (the "fortunately" at the beginning of this sentence was an "unfortunately" in 2022...); so keep it in mind as it could be quite puzzling the first time you encounter it if the compiler doesn't give you the hint, especially if you're working with a fairly old compiler.


```c++
template<class U>
void Print(const U& u)
{
    std::cout << u.template PrintN<0>() << std::endl; // Notice the template keyword!
}
```

## Good practice: always check obvious template instantiations in test!

If you have been very attentive, you might see `Print()` above is faulty: we ask to print on screen a void function... An instantiation reveals the issue:

```c++
{
    Foo foo;
    Print(foo); // Compilation error!
}
```

Without instantiation, the compiler can't easily figure out there is an issue; in `Print()` there are actually implicit conditions on the nature if the template parameter `U`:

* `U` must provide a template `PrintN` method with an integer parameter.
* An overload of operator `<<` must be provided for the return type of this method.

`Foo` doesn't check the second condition... but you can't really see it without checking it (once again C++ 20 concepts will help on that matter, but the advise here still stands).

So if the template is intended to work with a `Foo`, it is a good idea to provide a dedicated test that checks it actually works.

**Attention!** Code coverage tools (at least those we are aware of) won't help you here: a template that is not instantiated isn't considered as code to cover... So you may have a 100 % coverage and still gets a template function or class with a faulty implementation if you never instantiate it. 



## No virtual template method!

All is in the title here: a method can't be both virtual and template...

We are speaking specifically of the methods here: a template class may feature virtual (non template) methods without any issue.


[© Copyright](../COPYRIGHT.md)   

