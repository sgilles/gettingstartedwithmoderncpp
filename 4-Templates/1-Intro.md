---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Templates](./0-main.ipynb) - [Introduction](./1-Intro.ipynb)


## Motivation

The typical introduction to the need of generic programming is implementing a function that determines the minimum between two quantities (by the way don't do that: STL [provides it already](https://en.cppreference.com/w/cpp/algorithm/min)...)


```c++
int Min(int lhs, int rhs)
{
    return lhs < rhs ? lhs : rhs;
}
```

```c++
double Min(double lhs, double rhs)
{
    return lhs < rhs ? lhs : rhs;
}
```

```c++
float Min(float lhs, float rhs)
{
    return lhs < rhs ? lhs : rhs;
}
```

```c++
#include <iostream>

{
    std::cout << Min(5, -8) << std::endl;
    std::cout << Min(5., -8.) << std::endl;    
    std::cout << Min(5.f, -8.f) << std::endl;            
}
```

Already tired? Yet we have still to define the same for unsigned versions, other types such as `short` ou `long`... And it's not very [**DRY** (Don't Repeat Yourself)](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself): you may have noticed the implementation is exactly the same!

## Function templates (or methods)

A mechanism was therefore introduced in C++ to provide only one implementation: the **templates**:

```c++
template<class T>
T MinWithTemplate(T lhs, T rhs)
{
    return lhs < rhs ? lhs : rhs;
}
```

```c++
#include <iostream>

{
    std::cout << MinWithTemplate<int>(5, -8) << std::endl;
    std::cout << MinWithTemplate(5, -8) << std::endl;    
    std::cout << MinWithTemplate(5., -8.) << std::endl;    
    std::cout << MinWithTemplate(5.f, -8.f) << std::endl;            
}
```

As you can see:

* The type is replaced by a parameter (here called `T`)
* In the function call, you might specify the type explicitly between brackets (`<>`). If not specified, the compiler may figure it out if said type is used for one or several of the parameters. In other words, for the following case it won't work:

```c++
template<class T>
T Convert(int value)
{
    return static_cast<T>(value);
}
```

```c++
{
    double x = Convert(5); // Error: can't figure out which type `T` to use!
}
```

```c++
{
    float x = Convert<double>(5); // Ok
}
```

You may have noticed there are no constraints on `T` whatsoever. If you invoke a template and the implementation doesn't make sense for the chosen type the compiler will yell (no doubt you have already seen compilers are extremely talented to do so and it is even truer regarding templates...)

```c++
#include <string>

{
    Convert<std::string>(5); // Doesn't make sense so compiler will yell!
}
```

### `static_assert`

Of course, it would be nicer to get a clearer error message when an impossible type is provided... C++ 20 will introduce the [concept](https://en.cppreference.com/w/cpp/language/constraints) to constraint properly which type is acceptable, but C++ 11 already introduced a way slightly better than C++ 03 with `static_assert`:

```c++
#include <type_traits> // for std::is_arithmetic

template<class T>
T Convert2(int value)
{
    static_assert(std::is_arithmetic<T>(), "T must be an integer or a floating point!");
    return static_cast<T>(value);
}
```

```c++
#include <string>

{
    Convert2<std::string>(5); // Doesn't make sense so compiler will yell! 
                              // But first line is much clearer than previously...
}
```

<!-- #region -->
`static_assert` evolved in C++ 17:

* In C++ 11, it takes two arguments: the test and a string which features an explanation on the topic at hand.
* In C++ 17 and above, you might just give the test; it is actually handy when the test is already crystal clear:

```c++
static_assert(std::is_same<T, int>(), "Check T is an integer");
```

is a tad overkill!

That being said, if the test is not that trivial you should really use the possibility to add an explanation.


## Class templates

We have seen templates in the case of functions, but classes can be templated as well:
<!-- #endregion -->

```c++
template<class T>
class HoldAValue
{
    public:
        HoldAValue(T value);
    
        T GetValue() const;
    
    private:
        T value_;    
};
```

```c++
template<class T>
HoldAValue<T>::HoldAValue(T value) // the <T> is mandatory to identify properly the class
: value_(value)
{ }
```

```c++
template<class T>
T HoldAValue<T>::GetValue() const
{ 
    return value_;
}
```

```c++
#include <iostream>
#include <string>

{
    HoldAValue integer {5};
    std::cout << "Integer hold: " << integer.GetValue() << std::endl;

    HoldAValue<std::string> string {"Hello world!"}; // If type not specified explicitly it would have been char*...
    std::cout << "String hold: " << string.GetValue() << std::endl;
}
```

The template must be reminded in the definition as well; please notice before the `::` the brackets with the template parameters.

Spot the template type is not defined for the `integer` variable. The type is automatically deduced from the `5` argument value as an `int`. This variable could have been defined as `HoldAValue<int> integer {5};` (cf. [types deduction](../1-ProceduralProgramming/3-Types.ipynb#decltype-and-auto))

### Template method of a template class

Notice a template class may provide template methods:



```c++
template<class T>
class HoldAValue2
{
    public:
        HoldAValue2(T value);
    
        T GetValue() const;
    
        template<class U>
        U Convert() const;
    
    private:
        T value_;  
};
```

```c++
template<class T>
HoldAValue2<T>::HoldAValue2(T value)
: value_(value)
{ }
```

```c++
template<class T>
T HoldAValue2<T>::GetValue() const
{ 
    return value_;
}
```

In this case there are two `template` keyword for the definition: one for the class and the other for the method:

```c++
template<class T> // template type for the class first
template<class U> // then template type for the method
U HoldAValue2<T>::Convert() const
{
    return static_cast<U>(value_);
}
```

```c++
{
    HoldAValue2 hold(9);
    
    hold.Convert<double>();
}
```

### Friendship syntax

There is a weird and specific syntax that is expected if you want to declare a friendship to a function. Quite naturally you would probably write something like:

```c++
template<class T>
class HoldAValue3
{
    public:
    
    HoldAValue3(T value);
    
    friend void Print(const HoldAValue3<T>& obj);
    
    private:
    
    T value_;
};
```

```c++
template<class T>
HoldAValue3<T>::HoldAValue3(T value)
: value_(value)
{ }
```

```c++
#include <iostream>

template<class T>
void Print(const HoldAValue3<T>& obj)
{
    // Friendship required to access private data.
    // I wouldn't recommend friendship where an accessor would do the same task easily!
    std::cout << "Underlying value is " << obj.value_ << std::endl;
}
```

```c++
{
    HoldAValue3<int> hold(5);
    Print(hold); // LINK ERROR, and that is not something amiss in Xeus-cling!
}
```

To make the friendship work, you have to use in the friendship declaration another label for the template parameter:

```c++
template<class T>
class HoldAValue4
{
    public:
    
    HoldAValue4(T value);
    
    // 'Repeating' the list of template arguments and not using the ones from the class will fix the issue...
    // T wouldn't have work here; the label MUST differ.
    template<class U>
    friend void Print(const HoldAValue4<U>& obj);
    
    private:
    
    T value_;
};
```

```c++
template<class T>
HoldAValue4<T>::HoldAValue4(T value)
: value_(value)
{ }
```

```c++
#include <iostream>

// Notice it is only a label: in the definition I'm free to use the same label as for the class definitions!
template<class T>
void Print(const HoldAValue4<T>& obj)
{
    std::cout << "Underlying value is " << obj.value_ << std::endl;
}
```

```c++
{
    HoldAValue4<int> hold(5);
    Print(hold); // Ok!
}
```

This way of declaring friendship works but is not entirely fullproof: `Print<int>` is hence a friend of `HoldAValue4<double>`, which was not what was sought. Most of the time it's ok but there are 2 other ways to declare friendship; have a look at [this link](https://web.archive.org/web/20211003194958/https://web.mst.edu/~nmjxv3/articles/templates.html) if you want to learn more about it.


## Type or non-type template parameter

The examples so far are using **type** parameters: the `T` in the example stands for a type and is deemed to be substituted by a type. Templates may also use **non type** parameters, which are in most cases `enum` or `integral constant` types (beware: floating point types parameters or `std::string` **can't** be used as template parameters!)

Both can be mixed in a given template declaration:

```c++
template<class TypeT, std::size_t Nelts>
class MyArray
{
    public:
        
        explicit MyArray(TypeT initial_value);
    
    private:
    
        TypeT content_[Nelts];
};
```

```c++
template<class TypeT, std::size_t Nelts>
MyArray<TypeT, Nelts>::MyArray(TypeT initial_value)
{
    for (std::size_t i = 0; i < Nelts; ++i)
        content_[i] = initial_value;
}
```

```c++
{
    MyArray<int, 5ul> array1(2);
    MyArray<double, 2ul> array2(3.3);
}
```

However, you can't provide a type parameter where a non-type is expected (and vice-versa):

```c++
{
    MyArray<5ul, 5ul> array1(2); // COMPILATION ERROR!
}
```

```c++
{
    MyArray<int, int> array1(2); // COMPILATION ERROR!
}
```

<!-- #region -->


## Few precisions about templates

* Template parameters must be known or computed at **compile time**. You can't therefore instantiate a template from a value that was entered by the user of the program or computed at runtime.
* In the template syntax, `class` might be replaced by `typename`:


<!-- #endregion -->

```c++
template<typename T>
T Convert3(int value)
{
    return static_cast<T>(value);
}
```

Since C++ 17, there are exactly zero differences between both keywords (before C++ 17, they were almost everywhere equivalent except in a very specific case - called template template parameter that we'll see shortly - in which typename did not work. You can check [this link](https://stackoverflow.com/questions/2023977/what-is-the-difference-between-typename-and-class-template-parameters) if you want to know more); some authors suggest conventions (e.g. use `typename` for POD types and `class` for the more complicated types) but they are just that: conventions!

* The definition of the templates must be provided in header files, not in compiled files. The reason is that the compiler can't know all the types for which the template might be used: you may use `std::min` for your own defined types (provided they define a `<` operator...) and obviously STL writers can't foresee which type you will come up with!
* The compiler will generate the code for each type given to the template; for instance if `Convert<double>`, `Convert<unsigned int>` and `Convert<short>` are used in your code, the content of this template function will be instantiated thrice! This can lead to **code bloat**: lots of assembler code is generated, and compilation time may increase significantly. 
* So templates are a _very_ nice tool, but make sure to use them only when they're relevant, and you may employ techniques to limit the amount of code actually defined within the template definitions - the more straightforward being defining in a non template function the parts of the implementation that do not depend on the template parameter type. This way, this part of the code is not duplicated for each different type.





[© Copyright](../COPYRIGHT.md)   

