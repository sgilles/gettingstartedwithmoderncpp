---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Templates](./0-main.ipynb) - [Hands-on 12](./1b-hands-on.ipynb)


### Introduction

[This notebook](../HandsOn/HowTo.ipynb) explains very briefly your options to run the hands-ons.



###  Context: when `int` is not enough

So far, we have not put much stress on our representation: 
 
 - We have always used positive values (spoiler: negative don't work...)
 - We have also never try to reach the limit of the integer representation.
 
If we do that and ask the loop up to 32 bits, we'll see things fall apart (obviously faster with the sum of two products, as we handle greater values).
 
We'll see in the next chapter how to deal with errors, but first we will try to fix the 32 bits case to just change the internal integer representation we use.
 
We will therefore add a degree of freedom and let the end user choose the type of integer to use through the use of a template.

Replacing everything in one step is very error-prone and is definitively not what I do: I really recommend to change part of it, do a commit (in a real project) and thus proceed incrementally until you reach the target state. 

The compiler is really your friend here: make sure implicit conversions deliver a warning to help you check whether you didn't forget a substitution somewhere (compiler warnings will be addressed in a [further notebook](../6-InRealEnvironment/3-Compilers.ipynb); in the hands-ons this one is already activated in the CMake, and if you're using an online compiler utility make sure there is `-Wconversion` on the command line).


### **EXERCISE 31: make `PowerOfTwoApprox` a template class**

- Extend the loop to go up to 32 bits.

- Transform the type of `PowerOfTwoApprox::numerator_ ` into a class template parameter.

Only this attribute will be modified: the exponent and the number of bits remain `int` (remember: we proceed with incremental steps; we are fully aware the intermediate state is clunky...).

Make sure to modify:

* The type of the data attribute.
* The type returned by the `GetNumerator()` method.

We will start with an 'easy' case and call the instantiation for a `long`; warning conversions are fully expected (see explanation above).

**Note:** The `-Wshorten-64-to-32` warning you may get if you're using `clang++` is fully expected and will be dealt with in next exercise.

_Expected results (that don't fix anything to the issue found in preamble so far):_

    [With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 3846154/100000000]
    [With 8 bits]: 0.65 ~ 0.648438 (166 / 2^8)  [error = 240385/100000000]
    [With 12 bits]: 0.65 ~ 0.649902 (2662 / 2^12)  [error = 15024/100000000]
    [With 16 bits]: 0.65 ~ 0.649994 (42598 / 2^16)  [error = 939/100000000]
    [With 20 bits]: 0.65 ~ 0.65 (681574 / 2^20)  [error = 59/100000000]
    [With 24 bits]: 0.65 ~ 0.65 (10905190 / 2^24)  [error = 4/100000000]
    [With 28 bits]: 0.65 ~ 0.65 (174483046 / 2^28)  [error = 0/100000000]
    [With 32 bits]: 0.65 ~ 1 (1 / 2^0)  [error = 53846154/100000000]
    
    [With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 1785714/100000000]
    [With 8 bits]: 0.35 ~ 0.349609 (179 / 2^9)  [error = 111607/100000000]
    [With 12 bits]: 0.35 ~ 0.349976 (2867 / 2^13)  [error = 6975/100000000]
    [With 16 bits]: 0.35 ~ 0.349998 (45875 / 2^17)  [error = 436/100000000]
    [With 20 bits]: 0.35 ~ 0.35 (734003 / 2^21)  [error = 27/100000000]
    [With 24 bits]: 0.35 ~ 0.35 (11744051 / 2^25)  [error = 2/100000000]
    [With 28 bits]: 0.35 ~ 0.35 (187904819 / 2^29)  [error = 0/100000000]
    [With 32 bits]: 0.35 ~ 0 (0 / 2^0)  [error = 100000000/100000000]
    
    [With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 29917/1000000]
    [With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2000/1000000]
    [With 12 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 16 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 20 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ -2167  [error = 1545027/1000000]
    [With 24 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 8  [error = 997988/1000000]
    [With 28 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 0  [error = 1000000/1000000]
    [With 32 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3515  [error = 115935/1000000]

<!-- #region -->
### **EXERCISE 32: follow the thread...**

Your compiler should not be happy and tell you something like (here clang output with unfortunately the colors lost):

```shell
/home/dev_cpp/training_cpp/HandsOn/4-Templates/exercise31.cpp:70:48: warning: implicit conversion loses integer precision: 'long' to 'int' [-Wshorten-64-to-32]
   70 |     return TimesPowerOf2(approx.GetNumerator() * coefficient, -approx.GetExponent());
      |            ~~~~~~~~~~~~~ ~~~~~~~~~~~~~~~~~~~~~~^~~~~~~~~~~~~
/home/dev_cpp/training_cpp/HandsOn/4-Templates/exercise31.cpp:348:63: note: in instantiation of function template specialization 'operator*<long>' requested here
  348 |     int approx = approximation1 * coefficient1 + coefficient2 * approximation2;
```    

Let's humor the compiler and 'propagate' the template... which will not be a trivial journey:

* You will first have to modify once more `operator*` template functions not to use `int` at all.
* `TestDisplaySumOfMultiply` must also become a template class (remember one of the reason we're doing all this is to fix the problematic values due to the fact we reached the limits of `int` type...). Don't forget to replace the hardcoded `long` introduced in exercise 31!

At this point the code should be able to compile... but there are still loose ends, as your compiler will tell you (more or less clearly):

* `TimesPowerOf2()` should also be transformed into a template function. `MaxInt()` should as well.
* `RoundAsInt()` should also be modified as well. For the time being we won't dwell more on this one, but we shall see later that its implementation is not good enough for all types.
* And also `HelperComputePowerOf2Approx()` if you are using the proposed solution.

* And finally `TestDisplay::PrintLine()` as well (template only the method, not the entire class). At the moment, keep hardcoding the type for `TestDisplayPowerOfTwoApprox` which is not (yet) a template class.

The code should at this point compile again...

But there is still a line which might be wrong (unless you were very cautious) without compiler warning: make sure the template argument is used in `PowerOfTwoApprox<IntT>::operator double`. This one was tricky as the code was 

```c++
int denominator = TimesPowerOf2(1, exponent_);
```

which doesn't have to trigger a warning: `1` is an `int`... So the `int` specialization of `TimesPowerOf2`, and that is used to compute the floating point value returned! And you may end up with an unwanted `inf` instead of the expected numeric value. This is yet another reminder that magic numbers are dangerous, even if they look as innocuous as a 1...

**Warning:** if you're not careful enough, you may end up with the executable seemingly stuck. It's likely it is due to a type not changed properly somewhere and `TimesPowerOf2()` being as a consequence in an infinite loop (as we shall see in next part our error handling is currently rather sloppy...). 

_Expected results (with `long`):_

    [With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 3846154/100000000]
    [With 8 bits]: 0.65 ~ 0.648438 (166 / 2^8)  [error = 240385/100000000]
    [With 12 bits]: 0.65 ~ 0.649902 (2662 / 2^12)  [error = 15024/100000000]
    [With 16 bits]: 0.65 ~ 0.649994 (42598 / 2^16)  [error = 939/100000000]
    [With 20 bits]: 0.65 ~ 0.65 (681574 / 2^20)  [error = 59/100000000]
    [With 24 bits]: 0.65 ~ 0.65 (10905190 / 2^24)  [error = 4/100000000]
    [With 28 bits]: 0.65 ~ 0.65 (174483046 / 2^28)  [error = 0/100000000]
    [With 32 bits]: 0.65 ~ 0.65 (2791728742 / 2^32)  [error = 0/100000000]
    
    [With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 1785714/100000000]
    [With 8 bits]: 0.35 ~ 0.349609 (179 / 2^9)  [error = 111607/100000000]
    [With 12 bits]: 0.35 ~ 0.349976 (2867 / 2^13)  [error = 6975/100000000]
    [With 16 bits]: 0.35 ~ 0.349998 (45875 / 2^17)  [error = 436/100000000]
    [With 20 bits]: 0.35 ~ 0.35 (734003 / 2^21)  [error = 27/100000000]
    [With 24 bits]: 0.35 ~ 0.35 (11744051 / 2^25)  [error = 2/100000000]
    [With 28 bits]: 0.35 ~ 0.35 (187904819 / 2^29)  [error = 0/100000000]
    [With 32 bits]: 0.35 ~ 0.35 (3006477107 / 2^33)  [error = 0/100000000]
    
    [With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 29917/1000000]
    [With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2000/1000000]
    [With 12 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 16 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 20 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 24 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 28 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 32 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
<!-- #endregion -->

[© Copyright](../COPYRIGHT.md)   


