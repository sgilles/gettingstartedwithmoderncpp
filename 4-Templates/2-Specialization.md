---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Templates](./0-main.ipynb) - [Specialization](./2-Specialization.ipynb)


## Total specialization

### For template functions

Let's take back our previous `Convert` example: the conversion into a `std::string` wasn't valid with the proposed instantiation, but converting an integer into a `std::string` is not an outlandish idea either...

There is a mechanism to provide specific instantiation for a type: the **(total) specialization** (we'll see why _total_ shortly):


```c++
#include <iostream>

template<class T>
T Convert(int value)
{
    std::cout << "Generic instantiation called!" << std::endl;
    return static_cast<T>(value);
}
```

```c++
#include <string>

template<> // notice the empty brackets!
std::string Convert<std::string>(int value)
{
    std::cout << "std::string instantiation called!" << std::endl;    
    return std::to_string(value);
}
```

```c++
#include <iostream>

{
    Convert<double>(5);
    Convert<std::string>(5);    
}
```

Please notice the syntax: the definition provides template with no parameter in the brackets, and the `T` is replaced by the specialized type. As there are no template parameters left, the definition of the specialization must be put in a compiled file (or with an [`inline` keyword](../1-ProceduralProgramming/4-Functions.ipynb#inline-functions) in header file)

Of course, as many specialization as you wish may be provided:

```c++
class HoldAnInt
{
    public:
        explicit HoldAnInt(int value);
    
    private:
        int value_;
};
```

```c++
HoldAnInt::HoldAnInt(int value)
: value_(value)
{ }
```

```c++
template<>
HoldAnInt Convert(int value)
{
    std::cout << "HoldAnInt instantiation called!" << std::endl;    
    return HoldAnInt(value);
}
```

```c++
#include <iostream>

{
    Convert<double>(5);
    Convert<std::string>(5);    
    Convert<HoldAnInt>(5);        
}
```

### For template classes

Total specialization was illustrated on a function, but it works as well for a class. You may choose to:

* Specialize either the entire class (in which case you have to define the complete API for the specialized class) 
* Or just specialize a method.

The case below provides entire class specialization for `double` and just method specialization for `std::string`:


```c++
// First the generic class `HoldAValue`

#include <iostream>
#include <string>

template<class T>
class HoldAValue
{
    public:
    
        HoldAValue(T value);
        
        T GetValue() const;
        
    private:
    
        T value_;    
};


template<class T>
HoldAValue<T>::HoldAValue(T value)
: value_(value)
{ }


template<class T>
T HoldAValue<T>::GetValue() const
{ 
    return value_;    
}
```

```c++
// Method specialization for std::string

template<>
std::string HoldAValue<std::string>::GetValue() const
{ 
    return "String case (through method specialization): " + value_;    
}
```

```c++
// Class specialization for double

template<>
class HoldAValue<double>
{
    public:
    
        HoldAValue(std::string blah, double value);
        
        double GetValue() const;
        
    private:
    
        std::string blah_;
    
        double value_;   
};


HoldAValue<double>::HoldAValue(std::string blah, double value)
: blah_(blah),
value_(value)
{ }


double HoldAValue<double>::GetValue() const
{ 
    std::cout << blah_;
    return value_;    
}


```

```c++
{
    
    HoldAValue<int> integer(5);
    std::cout << integer.GetValue() << std::endl;
    
    HoldAValue<std::string> string("Hello world");
    std::cout << string.GetValue() << std::endl;
    
    HoldAValue<double> dbl("Double case (through class specialization): ", 3.14);
    std::cout << dbl.GetValue() << std::endl;
}
```

## Partial specialization

### For classes


It is also possible for classes to **specialize partially** a template class:

```c++
template<class TypeT, std::size_t Nelts>
class MyArray
{
    public:
        
        explicit MyArray(TypeT initial_value);
    
    private:
    
        TypeT content_[Nelts];
};
```

```c++
#include <iostream>

template<class TypeT, std::size_t Nelts>
MyArray<TypeT, Nelts>::MyArray(TypeT initial_value)
{
    std::cout << "Generic constructor" << std::endl;
    for (auto i = 0ul; i < Nelts; ++i)
        content_[i] = initial_value;
}
```

```c++
template<class TypeT>
class MyArray<TypeT, 10ul>
{
    public:
        
        explicit MyArray(TypeT initial_value);
    
    private:
    
        TypeT content_[10ul];
};
```

```c++
#include <iostream>

template<class TypeT>
MyArray<TypeT, 10ul>::MyArray(TypeT initial_value)
{
    std::cout << "Partial specialization constructor: type is still templated but size is fixed " << std::endl;
    for (auto i = 0ul; i < 10ul; ++i)
        content_[i] = initial_value;
}
```

```c++
{
    MyArray<int, 8ul> generic_array(2); 
    MyArray<int, 10ul> specific_array(2); 
}
```

### But not for functions!

However, partial specialization is **forbidden** for template functions:

```c++
#include <iostream>

template<class T, class U>
void PrintSquare(T t, U u)
{
    std::cout << "Generic instantiation" << std::endl;
    std::cout << "(t^2, u^2) = (" << t * t << ", " << u * u << ")" << std::endl;
}
```

```c++
{
    PrintSquare(5, 7.);
}
```

```c++
#include <string>

template<class T>
void PrintSquare<T, std::string>(T t, std::string u) // COMPILATION ERROR!
{
    std::cout << "Partial function specialization: doesn't compile!" << std::endl;    
    std::cout << "(t^2, u^2) = (" << t * t << ", " << (u + u) << ")" << std::endl;
}
```

You might have the impress it works if you try defining the function without specifying the brackets

_(please use [@Coliru](https://coliru.stacked-crooked.com/a/ad6d2ceca1d05b0f) - the code below no longer works in Xeus-cling as of September 2022)_

```c++
// DOESN'T WORK ON XEUS-CLING!

#include <iostream>
#include <string>

template<class T, class U>
void PrintSquare(T t, U u)
{
    std::cout << "Generic instantiation" << std::endl;
    std::cout << "(t^2, u^2) = (" << t * t << ", " << u * u << ")" << std::endl;
    // As '*' is not defined for std::string, one would expect a failure if invoked with
    // `std::string` for T or U 
}


template<class T>
void PrintSquare(T t, std::string u)
{
    std::cout << "Seemingly ok function template specialization " << std::endl;
    std::cout << "(t^2, u^2) = (" << t * t << ", " << (u + u) << ")" << std::endl;
}


int main(int argc, char** argv)
{
    std::string hello("Hello");
    PrintSquare(5., hello);
    
    // But if you uncomment this line which explicitly calls the template function
    // with both types explicitly given the compilation will fail...
    // PrintSquare<double, std::string>(5., std::string("Hello"));
    
    // ... whereas it is a perfectly legit way of calling a template instantiation, as you may check with:
    // PrintSquare<double, int>(5., 3);
    
    
    return EXIT_SUCCESS;  
}
```

To be honest, the reason partial specialization are forbidden are quite unclear for me; this seems to be internal reasons in the way compilers are parsing the C++ code. I've seen on forums discussions to lift this limitation over the years, but to my knowledge no concrete plan is in motion to do so.

### Mimicking the partial template specialization for functions

#### Using a class and a static method

There is a quite verbose way to circumvent the impossibility: use a template struct (which is partially specializable) with a static method...

```c++
#include <iostream>

template<class T, class U>
struct PrintSquareHelper
{
    
    static void Do(T t, U u)
    {
        std::cout << "Generic instantiation" << std::endl;
        std::cout << "(t^2, u^2) = (" << t * t << ", " << u * u << ")" << std::endl;
    }
    
};
```

```c++
#include <string>

template<class T>
struct PrintSquareHelper<T, std::string>
{
    
    static void Do(T t, std::string u)
    {
        std::cout << "Specialized instantiation" << std::endl;
        std::cout << "(t^2, u^2) = (" << t * t << ", " << (u + u) << ")" << std::endl;
    }
    
};
```

```c++
template<class T, class U>
void PrintSquare2(T t, U u)
{
    PrintSquareHelper<T, U>::Do(t, u);
}
```

```c++
{
    PrintSquare2<double, std::string>(5., std::string("Hello"));
}
```

### If constexpr

C++ 17 introduced (at last!) a special `if` that is explicitly checked at compile time.

This alleviates greatly the codes and is really something I yearned for years:

```c++
#include <iostream>
#include <string>

template<class T, class U>
void Print3(T t, U u)
{
    if constexpr (std::is_same<U, std::string>()) // Don't worry we will see STL algorithms later
    {
        std::cout << "Specialized instantiation" << std::endl;
        std::cout << "(t^2, u^2) = (" << t * t << ", " << (u + u) << ")" << std::endl;        
    }
    else
    {
        std::cout << "Generic  instantiation" << std::endl;
        std::cout << "(t^2, u^2) = (" << t * t << ", " << u * u << ")" << std::endl;        
    }
}

```

```c++
{
    Print3<double, std::string>(5., "hello");
}
```

**Note: and without constexpr?**

I haven't specified it so far, but the `constexpr` is crucial: without it the compiler must instantiate both branches, even if only one is kept at the end. And if one can't be instantiated we're screwed:

```c++
#include <iostream>

template<class T, class U>
void Print4(T t, U u)
{
    if (std::is_same<U, std::string>())
    {
        std::cout << "Specialized instantiation" << std::endl;
        std::cout << "(t^2, u^2) = (" << t * t << ", " << (u + u) << ")" << std::endl;        
    }
    else
    {
        std::cout << "Generic  instantiation" << std::endl;
        std::cout << "(t^2, u^2) = (" << t * t << ", " << u * u << ")" << std::endl;        
    }
}
```

```c++
{
    Print4<double, std::string>(5., "Hello"); // Compilation error!
}
```


[© Copyright](../COPYRIGHT.md)   

