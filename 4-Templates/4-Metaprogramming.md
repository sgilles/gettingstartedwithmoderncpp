---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Templates](./0-main.ipynb) - [Metaprogramming](./4-Metaprogramming.ipynb)

<!-- #region -->
## Introduction

We will no go very far in this direction: metaprogramming is really a very rich subset of C++ of its own, and one that is especially tricky to code.

You can be a very skilled C++ developer and never use this; however it opens some really interesting prospects that can't be achieved easily (or at all...) without it.

I recommend the reading of [Modern C++ design](../bibliography.ipynb#Modern-C++-Design) to get the gist of it: even if it relies upon older versions of C++ (and therefore some of its hand-made constructs are now in one form or another in modern C++ or STL) it is very insightful to understand the reasoning behind metaprogrammation.


## Example: same action upon a collection of heterogeneous objects

Let's say we want to put together in a same container multiple objects of heterogeneous type (one concrete case for which I have used that: reading an input data file with each entry is handled differently by a dedicated object).

In C++11, `std::tuple` was introduced for that purpose:

<!-- #endregion -->

```c++
#include <tuple>
   
std::tuple<int, std::string, double, float, long> tuple = 
    std::make_tuple(5, "hello", 5., 3.2f, -35l);

```

What if we want to apply the same treatment to all of the entries? 


In more usual containers, we would just write a `for` loop, but this is not an option here: to access the `I`-th element of the tuple syntax is `std::get<I>`, so `I` has to be known at compiled time... which is not the case for the mutable variable used in a typical `for` loop!


Let's roll with just printing each of them:

```c++
#include <iostream>

{
    std::cout << std::get<0>(tuple) << std::endl;
    std::cout << std::get<1>(tuple) << std::endl;
    std::cout << std::get<2>(tuple) << std::endl;
    std::cout << std::get<3>(tuple) << std::endl;
    std::cout << std::get<4>(tuple) << std::endl;
}

```

The treatment was rather simple here, but the code was duplicated manually for each of them. **Metaprogramming** is the art of making the compiler generate by itself the whole code. 

The syntax relies heavily on templates, and those of you familiar with functional programming will feel at ease here:


```c++
#include <iostream>

template <std::size_t IndexT, std::size_t TupleSizeT>
struct PrintTuple
{
        
    template<class TupleT> // I'm lazy I won't put there std::tuple<int, std::string, double, float, long> again... 
    static void Do(const TupleT& tuple)
    {
        std::cout << std::get<IndexT>(tuple) << std::endl;
        PrintTuple<IndexT + 1ul, TupleSizeT>::Do(tuple); // that's the catch: call recursively the next one!
    }  
    
};
```

A side reminder here: we need to use a combo of template specialization of `struct` and static method here to work around the fact template specialization of functions is not possible (see [here](2-Specialization.ipynb#Mimicking-the-partial-template-specialization-for-functions) for more details)


You may see the gist of it, but there is still an issue: the recursivity goes to the infinity... (don't worry your compiler will yell before that!). So you need a specialization to stop it - you may see now why I used a class template and not a function!

```c++
template <std::size_t TupleSizeT>
struct PrintTuple<TupleSizeT, TupleSizeT>
{
        
    template<class TupleT>
    static void Do(const TupleT& tuple)
    {
        // Do nothing!
    }    
    
};
```

With that, the code is properly generated:

```c++
{
    std::tuple<int, std::string, double, float, long> tuple = 
        std::make_tuple(5, "hello", 5., 3.2f, -35l);
    PrintTuple<0, std::tuple_size<decltype(tuple)>::value>::Do(tuple);
}
```

Of course, the call is not yet very easy: it's cumbersome to have to explicitly reach the tuple size... But as often in C++ an extra level of indirection may lift this issue:

```c++
template<class TupleT>
void PrintTupleWrapper(const TupleT& t)
{
    PrintTuple<0, std::tuple_size<TupleT>::value>::Do(t);
}
```

And then the call may be simply:

```c++
{
    std::tuple<int, std::string, double, float, long> tuple = std::make_tuple(5, "hello", 5., 3.2f, -35l);
    PrintTupleWrapper(tuple);
}
```

In fact, my laziness earlier when I used a template argument rather than the exact tuple type pays now as this function may be used with any tuple (or more precisely with any tuple for which all elements comply with `operator<<`):

```c++
{
    int a = 5;
    std::tuple<std::string, int*> tuple = std::make_tuple("Hello", &a);
    PrintTupleWrapper(tuple);
}
```

## Slight improvement with C++ 17 `if constexpr`

With C++ 17 compile-time check `if constexpr`, you may even do the same with much less boilerplate:

```c++
#include <iostream>

template <std::size_t IndexT, class TupleT>
void PrintTupleIfConstexpr(const TupleT& tuple)
{
    constexpr auto size = std::tuple_size<TupleT>();

    static_assert(IndexT <= size);
    
    if constexpr (IndexT < size)
    {
        std::cout << std::get<IndexT>(tuple) << std::endl;
        PrintTupleIfConstexpr<IndexT + 1, TupleT>(tuple);
    }
};
```

```c++
template<class TupleT>
void PrintTupleIfConstexprWrapper(const TupleT& tuple)
{
    PrintTupleIfConstexpr<0ul>(tuple);
}
```

```c++
{
    std::tuple<int, std::string, double, float, long> tuple = std::make_tuple(5, "hello", 5., 3.2f, -35l);
    PrintTupleIfConstexprWrapper(tuple);
}
```

```c++
{
    int a = 5;
    std::tuple<std::string, int*> tuple = std::make_tuple("Hello", &a);
    PrintTupleIfConstexprWrapper(tuple);
}
```

The gist of it remains the same (it amounts to a recursive call) but the compile-time check makes us avoid entirely the use of the stopping specialization and the use of a struct with `static` method.


## `std::apply`

Another option provided by C++ 17 is to use `std::apply`, which purpose is to apply upon all elements of a same tuple a same operation.

[Cppreference](https://en.cppreference.com/w/cpp/utility/apply) provides a snippet that solves the exact problem we tackled above in a slightly different way: they wrote a generic overload of `operator<<` for any instance of a `std::tuple` object.

I have simplified somehow their snippet as they complicated a bit the reading with unrelated niceties to handle the comma separators.

Don't bother if you do not understand all of it:

- The weird `...` syntax is for variadic templates, that we will present [briefly in next notebook](../5-MoreAdvanced.ipynb#Variadic-templates). Sorry we avoid as much as possible to refer to future stuff in the training session, but current paragraph is a late addition and doesn't mesh completely well with the structure of this document. You just have to know it's a way to handle a variable number of arguments (here template arguments of `std::tuple`).


```c++
template<typename... Ts>
std::ostream& operator<<(std::ostream& os, std::tuple<Ts...> const& theTuple)
{
    std::apply
    (
        [&os](Ts const&... tupleArgs)
        {            
            ((os << tupleArgs << std::endl), ...);            
        }, theTuple
    );
    return os;
}
```

```c++
std::cout << tuple << std::endl;
```

# Bonus: metaprogramming Fibonacci

In [notebook about constexpr](../1-ProceduralProgramming/7-StaticAndConstexpr.ipynb), I said implementing Fibonacci series before C++ 11 involved metaprogramming; here is an implementation (much more wordy than the `constexpr` one):



```c++
template<std::size_t N>
struct Fibonacci
{
 
    static std::size_t Do()
    {
        return Fibonacci<N-1>::Do() + Fibonacci<N-2>::Do();
    }
    
    
};
```

```c++
// Don't forget the specialization for 0 and 1!

template<>
struct Fibonacci<0ul>
{
 
    static std::size_t Do()
    {
        return 0ul;
    }
    
    
};


template<>
struct Fibonacci<1ul>
{
 
    static std::size_t Do()
    {
        return 1ul;
    }
    
    
};
```

```c++
#include <iostream>

std::cout << Fibonacci<5ul>::Do() << std::endl;
std::cout << Fibonacci<10ul>::Do() << std::endl;
```

And if the syntax doesn't suit you... you could always add an extra level of indirection to remove the `::Do()` part:

```c++
template<std::size_t N>
std::size_t FibonacciWrapper()
{
    return Fibonacci<N>::Do();
}
```

```c++
#include <iostream>

std::cout << FibonacciWrapper<5ul>() << std::endl;
std::cout << FibonacciWrapper<10ul>() << std::endl;

```

As you can see, in some cases `constexpr` really alleviates some tedious boilerplate... 

It should be noticed that although these computations really occur at compile time, they aren't nonetheless recognized automatically as `constexpr`: 

```c++
constexpr auto fibo_5 = FibonacciWrapper<5ul>(); // COMPILATION ERROR!
```

To fix that, you need to declare `constexpr`:
- Each of the `Do` static method (`static constexpr std::size_t Do()`)
- The `FibonacciWrapper` function (`template<std::size_t N> constexpr std::size_t FibonacciWrapper()`)

So in this specific case you should really go with the much less wordy and more expressive expression with `constexpr` given in [aforementioned notebook](../1-ProceduralProgramming/7-StaticAndConstexpr.ipynb)


[© Copyright](../COPYRIGHT.md)

