---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Templates](./0-main.ipynb) - [Hints to more advanced concepts with templates](./5-MoreAdvanced.ipynb)

<!-- #region -->
We have barely scratched the surface of what can be done with templates; I will here just drop few names and a very brief explanation to allow you to dig deeper if it might seem of interest for your codes (a Google search for either of them will give you plenty of references) and also avoid you frowing upon a seemingly daunting syntax...

## Curiously recurrent template pattern (CRTP)

One of my own favourite idiom (so much I didn't resist writing an [entry](../7-Appendix/Crtp.ipynb) about it in the appendix).

The idea behind it is to provide a same set of a given functionality to classes that have otherwise nothing in common.

The basic example is if you want to assign a unique identifier to a class of yours: the implementation would be exactly the same in each otherwise different class in which you need this:

* Initializing properly this identifier at construction.
* Check no other objects of the same class use it already.
* Provide an accessor `GetUniqueIdentifier()`.

Usual inheritance or composition aren't very appropriate to put in common once and for all (DRY principle!) these functionalities: either they may prove dangerous (inheritance) or be very wordy (composition).

The **curiously recurrent template pattern** is a very specific inheritance:

```c++
class MyClass : public UniqueIdentifier<MyClass>
```

where your class inherits from a template class which parameter is... your class itself.
<!-- #endregion -->

## Traits

A **trait** is a member of a class which gives exclusively an information about type. For instance let's go back to the `HoldAValue` class we wrote [earlier](./2-Specialization.ipynb) in our template presentation:

```c++
#include <iostream>
#include <string>

template<class T>
class HoldAValue
{
    public:
    
        HoldAValue(T value);
        
        T GetValue() const;
        
    private:
    
        T value_;    
};


template<class T>
HoldAValue<T>::HoldAValue(T value)
: value_(value)
{ }


template<class T>
T HoldAValue<T>::GetValue() const
{ 
    return value_;    
}
```

```c++
{
    HoldAValue<int> hint(5);   
    std::cout << hint.GetValue() << std::endl;
    
    HoldAValue<std::string> sint("Hello world!");
    std::cout << sint.GetValue() << std::endl;
}
```

This class was not especially efficient: the accessor `GetValue()`:

- Requires `T` is copyable.
- Copy `T`, which is potentially a time-consuming operator. 

We could replace by `const T& GetValue() const` to solve both those issues, but it's a bit on the nose (and less efficient) for plain old data type. The best of both world may be achieved by a trait:


```c++
#include <iostream>
#include <string>
#include <type_traits> // for std::conditional, std::is_trivial


template<class T>
class ImprovedHoldAValue
{
    public:
        
        // Traits: information about type!
        using return_value = 
            typename std::conditional<std::is_trivial<T>::value, T, const T&>::type;
    
        ImprovedHoldAValue(T value);
        
        return_value GetValue() const;
        
    private:
    
        T value_;    
};


template<class T>
ImprovedHoldAValue<T>::ImprovedHoldAValue(T value)
: value_(value)
{ }

```

Beware the trait that acts as the return value must be scoped correctly in the definition:

```c++
template<class T>
typename ImprovedHoldAValue<T>::return_value ImprovedHoldAValue<T>::GetValue() const
{ 
    return value_;    
}
```

(unless you use the alternate syntax for function):

```c++
// Doesn't work in Xeus-cling but completely valid in a real environment
template<class T>
auto ImprovedHoldAValue<T>::GetValue() const -> return_value
{ 
    return value_;    
}
```

And the result remain the same, albeit more efficient as a copy is avoided:

```c++
{    
    ImprovedHoldAValue<int> hint(5);   
    std::cout << hint.GetValue() << std::endl;
    
    ImprovedHoldAValue<std::string> sint("Hello world!");
    std::cout << sint.GetValue() << std::endl;
}
```

A more complete example with an uncopyable class and the proof the alternate syntax works is available [@Coliru](https://coliru.stacked-crooked.com/a/698eb583b5a94bc7).

In fact sometimes you may even have **traits class**: class which sole purpose is to provide type information! Such classes are often used as template parameters of other classes.


## Policies

Policies are a way to provide a class for which a given aspect is entirely configurable by another class you provide as a template parameter.

STL uses up policies: for instance there is a second optional template parameter to `std::vector` which deals with the way to allocate the memory (and only with that aspect). So you may provide your own way to allocate the memory and provide it to `std::vector`, which will use it instead of its default behaviour. [Modern C++ design](../bibliography.ipynb#Modern-C++-Design) dedicates a whole chapter of his book to this example: he wrote an allocator aimed at being more efficient for the allocation of small objects.

The syntax of a policy is a template class which also derives from at least one of its template parameter:




```c++
template<class ColorPolicyT>
class Car : public ColorPolicyT
{ };
```

```c++
#include <iostream>

struct Blue
{
    
    void Print() const
    {
        std::cout << "My color is blue!" << std::endl;
    }
    
};
```

```c++
#include <string>

// Let's assume in the future a car provides a mechanism to change its color at will:
class Changing
{
    public:
    void Display() const // I do not use `Print()` intentionally to illustrate there is no constraint
                         // but in true code it would be wise to use same naming scheme!
    {
        std::cout << "Current color is " << color_ << "!" << std::endl;
    }
    
    void ChangeColor(const std::string& new_color)
    {
        color_ = new_color;
    }
    
    private:
    
        std::string color_ = "white"; 
};
```

```c++
{
    Car<Blue> blue_car;    
    blue_car.Print();
    
    Car<Changing> future_car;
    future_car.Display();
    future_car.ChangeColor("black");
    future_car.Display();
    
}
```

## Variadic templates

If you have already written some C, you must be familiar with `printf`:


```c++
#include <cstdio>

{
    int i = 5;
    double d = 3.1415;
    
    printf("i = %d\n", i);
    printf("i = %d and d = %lf\n", i, d);        
}
```

This function is atypical as it may take an arbitrary number of arguments. You can devise similar function of your own in C (look for `va_arg` if you insist...) but it was not recommended: under the hood it is quite messy, and limits greatly the checks your compiler may perform on your code (especially regarding the type of the arguments).

C++ 11 introduced **variadic templates**, which provides a much neater way to provide this kind of functionality (albeit with a *very* tricky syntax: check all the `...` below... and it becomes worse if you need to propagate them).

```c++
#include <iostream>

// Overload when one value only.
template<class T>
void Print(T value)
{
    std::cout << value << std::endl;
}

// Overload with a variadic number of arguments
template<class T, class ...Args>
void Print(T value, Args... args)  // args here will be all parameters passed to the function from the 
                                   // second one onward.
{
    Print(value);
    Print(args...); // Will call recursively `Print()` with one less argument.
}

```

```c++
Print(5, "hello", "world", "ljksfo", 3.12);
```

```c++
Print("One");
```

```c++
Print(); // Compilation error: no arguments isn't accepted!
```

To learn more about them, I recommend [Effective Modern C++](../bibliography.ipynb#Effective-Modern-C++), which provides healthy explanations about `std::forward` and `std::move` you will probably need soon if you want to use these variadic templates.


## Template template parameters (not a mistake...)

You may want to be way more specific when defining a template parameter: instead of telling it might be whatever you want, you may impose that a specific template parameter should only be a type which is itself an instantiation of a template.

Let's consider a very dumb template function which purpose is to call print the value of `size()` for a STL container. We'll see them more extensively in a [dedicated notebook](../5-UsefulConceptsAndSTL/3-Containers.ipynb), but for now you just have to know that these containers take two template parameters:

- One that describe the type inside the container (e.g. `double` for `std::vector<double>`).
- Another optional one which specifies how the memory is allocated.

We could not bother and use directly a usual template parameter:



```c++
#include <iostream> 

template<class ContainerT>
void PrintSize1(const ContainerT& container)
{
    std::cout << container.size() << std::endl;
}
```

You may use it on seamlessly on usual STL containers:

```c++
#include <vector>
#include <list>
#include <deque>

{
    std::vector<double> vector { 3.54, -73.1, 1004. };
    
    std::list<int> list { 15, -87, 12, 12, 0, -445 };
    
    std::deque<unsigned int> deque { 2, 87, 95, 14, 451, 10, 100, 1000 };
    
    
    PrintSize1(vector);
    PrintSize1(list);    
    PrintSize1(deque);    
}
```

However, it would also work for any class that define a `size()` parameters, regardless of its nature.

```c++
#include <string>

struct NonTemplateClass
{
    std::string size() const
    {
        return "Might seem idiotic, but why not?";
    }
        
};
```

```c++
template<class U, class V, class W>
struct TemplateWithThreeParameters
{
  
    int size() const
    {
        return -99;
    }
    
};
```

```c++
{
    NonTemplateClass non_template_class;
    TemplateWithThreeParameters<int, float, double> template_with_three_parameters;
    
    PrintSize1(non_template_class);
    PrintSize1(template_with_three_parameters);
    
}
```

We see here with my rather dumb example that `PrintSize1()` also works for my own defined types, that are not following the expected prototype of a STL container (class with two template parameters). 

It may seem pointless in this example, but the worst is that the method might be used to represent something entirely different from what we expect when we call `size()` upon a STL container.

A possibility to limit the risk is to use a **template template parameter** in the function definition:

```c++
#include <iostream>

template<template <class, class> class ContainerT, class TypeT, class AllocatorT>
void PrintSize2(const ContainerT<TypeT, AllocatorT>& container)
{
    std::cout << container.size() << std::endl;
}
```

By doing so, we impose that the type of the argument is an instantiation of a class with two arguments. With that, STL containers work:

```c++
#include <vector>
#include <list>
#include <deque>

{
    std::vector<double> vector { 3.54, -73.1, 1004. };
    
    std::list<int> list { 15, -87, 12, 12, 0, -445 };
    
    std::deque<unsigned int> deque { 2, 87, 95, 14, 451, 10, 100, 1000 };
    
    // At call site, you don't have to specify the template arguments that are inferred.
    PrintSize2(vector);
    PrintSize2(list);    
    PrintSize2(deque);    
}
```

whereas my own defined types don't:

```c++
{
    NonTemplateClass non_template_class;
    TemplateWithThreeParameters<int, float, double> template_with_three_parameters;
    
    PrintSize2(non_template_class);
    PrintSize2(template_with_three_parameters);
    
}
```

In practice you shouldn't need to use that too often, but in the context of this notebook it is worth knowing that the possibility exists (it may help you understand an error message should you use a library using them). I had to resort to them a couple of times, especially along policies.

If you want to learn more about them, you should really read [Modern C++ design](../bibliography.ipynb#Modern-C++-Design).

Of course, `concept` introduced in C++ 20 are a much more refined tool to check template parameter type fulfills some constraints, but template template parameter are a much older feature that worked way back to pre-C++ 11 versions of the language.



[© Copyright](../COPYRIGHT.md)   

