---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Object programming](./0-main.ipynb) - [Hands-on 7](./4b-hands-on.ipynb)


### **EXERCISE 16: transform struct `PowerOfTwoApprox` into a class**

Make `PowerOfTwoApprox` into a class, with proper encapsulation:

* Both data attributes should be made private.
* Constant accessors will therefore be needed (non-constant ones should not be required here)

Expected output is the same as previously:

```
[With 2 bits]: 0.65 ~ 3 / 2^2 (0.75)  [error = 15/100]  
[With 4 bits]: 0.65 ~ 10 / 2^4 (0.625)  [error = 4/100]  
[With 6 bits]: 0.65 ~ 42 / 2^6 (0.65625)  [error = 1/100]  
[With 8 bits]: 0.65 ~ 166 / 2^8 (0.648438)  [error = 0/100]  
  
[With 2 bits]: 0.35 ~ 3 / 2^3 (0.375)  [error = 7/100]  
[With 4 bits]: 0.35 ~ 11 / 2^5 (0.34375)  [error = 2/100]  
[With 6 bits]: 0.35 ~ 45 / 2^7 (0.351562)  [error = 0/100]  
[With 8 bits]: 0.35 ~ 179 / 2^9 (0.349609)  [error = 0/100]  
   
[With 1 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 2965  [error = 254/1000]  
[With 2 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4448  [error = 119/1000]  
[With 3 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4008  [error = 8/1000]  
[With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 30/1000]  
[With 5 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3967  [error = 2/1000]  
[With 6 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4004  [error = 7/1000]  
[With 7 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3977  [error = 0/1000]  
[With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2/1000]  
```


### **EXERCISE 17: transform `Multiply()` into a method `Multiply()` of `PowerOfTwoApprox`**

The method will take as argument only the integer coefficient.

`DisplaySumOfMultiply()` will of course need also some light rewriting to accommodate that change.

Expected output is the same as previously.



### **EXERCISE 18: transform `DisplayPowerOf2Approx()` into a class**

Create a class `TestDisplayPowerOfTwoApprox` which will be in charge of printing the display for the 0.65 and 0.35 values.

Use two methods in this class:

* A public method `Do()` which in its implementation will call the test for 0.65 and 0.35. This method will take the number of bits as argument.
* A private method `Display()` which will provide the display for a given double value (and will therefore be called twice in `Do()`: once for 0.65 and once for 0.35).

New main should look like:

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    TestDisplayPowerOfTwoApprox test_display_approx;
     
    for (int nbits = 2; nbits <= 8; nbits += 2)
         test_display_approx.Do(nbits); 

    std::cout << std::endl;

    for (int nbits = 1; nbits <= 8; ++nbits)
        DisplaySumOfMultiply(nbits, 0.65, 3515, 0.35, 4832);

    return EXIT_SUCCESS;
}
```

Output will be ordered differently:

```
[With 2 bits]: 0.65 ~ 0.75 (3/2^2)  [error = 15/100]
[With 2 bits]: 0.35 ~ 0.375 (3/2^3)  [error = 7/100]
[With 4 bits]: 0.65 ~ 0.625 (10/2^4)  [error = 4/100]
[With 4 bits]: 0.35 ~ 0.34375 (11/2^5)  [error = 2/100]
[With 6 bits]: 0.65 ~ 0.65625 (42/2^6)  [error = 1/100]
[With 6 bits]: 0.35 ~ 0.351562 (45/2^7)  [error = 0/100]
[With 8 bits]: 0.65 ~ 0.648438 (166/2^8)  [error = 0/100]
[With 8 bits]: 0.35 ~ 0.349609 (179/2^9)  [error = 0/100]

[With 1 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 2965  [error = 254/1000]
[With 2 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4448  [error = 119/1000]
[With 3 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4008  [error = 8/1000]
[With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 30/1000]
[With 5 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3967  [error = 2/1000]
[With 6 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4004  [error = 7/1000]
[With 7 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3977  [error = 0/1000]
[With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2/1000]
```


### **EXERCISE 19: transform `DisplaySumOfMultiply()` into a class**

Likewise, create a class `TestDisplaySumOfMultiply` which will be in charge of printing the display for 0.65 * 3515 + 0.35 * 4832 with public method `Do(int nbits)` and private method `Display()` which will takes 5 arguments:

* Number of bits.
* The two floating point values.
* Their integer coefficient.

New `main()` is:

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    TestDisplayPowerOfTwoApprox test_display_approx;
     
    for (int nbits = 2; nbits <= 8; nbits += 2)
         test_display_approx.Do(nbits); 

    std::cout << std::endl;

    TestDisplaySumOfMultiply test_display_sum_of_multiply;

    for (int nbits = 1; nbits <= 8; ++nbits)
        test_display_sum_of_multiply.Do(nbits);

    return EXIT_SUCCESS;
}
```

<!-- #region -->
### **EXERCISE 20: introduce common `PrintLine()` function for outputs**

Both `TestDisplay` classes are rather similar in the line in charge of printing content on standard output - so we would like to uniformize the implementation.

We will therefore introduce a `PrintLine()` function which will be in charge of printing the line for a given number of bits; there are dedicated arguments to enable the differences in current displays (for instance one round the floating point values and not the other). We know this function renege the sound principle of separating the functionalities, but please humor us for the moment...

```c++
// Declarations

//! Convenient enum used in \a PrintLine().
enum class RoundToInteger { no, yes };

/*!
 * \brief  Print a line with information about error.
 *
 * \param[in] maximum_error_index The error is expressed as an integer over this quantity.
 * \param[in] optional_string1 String that might appear just after the "[With N bits]:".
 * \param[in] optional_string2 String that might appear just before the "[error = ...]".
 * \param[in] do_round_to_integer If yes, the exact result is approximated as an integer.
 */
void PrintLine(int Nbits, double exact, double approx, int maximum_error_index, 
               RoundToInteger do_round_to_integer,
               std::string optional_string1 = "", std::string optional_string2 = "");


// Definitions

void PrintLine(int Nbits, double exact, double approx, int maximum_error_index, 
               RoundToInteger do_round_to_integer,
               std::string optional_string1, std::string optional_string2)            
{
    int error = RoundAsInt(maximum_error_index * std::fabs(exact - approx) / exact);
    
    std::cout << "[With " << Nbits << " bits]: " << optional_string1
        << (do_round_to_integer == RoundToInteger::yes ? RoundAsInt(exact) : exact) << " ~ " << approx
        << optional_string2
        << "  [error = " << error << "/" << maximum_error_index << "]" 
        << std::endl;    
}
```

Copy this function in your exercise and use it in both `Display()` methods. Output should remain unchanged.
<!-- #endregion -->

[© Copyright](../COPYRIGHT.md)   


