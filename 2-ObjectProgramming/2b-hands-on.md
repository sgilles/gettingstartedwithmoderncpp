---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Object programming](./0-main.ipynb) - [Hands-on 5](./2b-hands-on.ipynb)


### __EXERCISE 14: transform `ComputePowerOf2Approx()` into a `PowerOfTwoApprox` method__

What we did in exercise 13 is a bit clunky: the object is introduced but we keep spending time fetching the data attribute to act upon them (even initialization to 0 is done manually several times).

We'll streamline this a bit by making some of the functions methods of the `PowerOfTwoApprox` struct; starting with `ComputePowerOf2Approx()` that will become a method named `Compute()`. This method will change the internal data attributes rather than changing an output argument.

Also consider renaming the data attributes with a given convention (using either `numerator_` or `m_numerator` depending on your taste).

Output should remain unchanged after this modification:

```
[With 2 bits]: 0.65 ~ 3 / 2^2 (0.75)  [error = 15/100]  
[With 4 bits]: 0.65 ~ 10 / 2^4 (0.625)  [error = 4/100]  
[With 6 bits]: 0.65 ~ 42 / 2^6 (0.65625)  [error = 1/100]  
[With 8 bits]: 0.65 ~ 166 / 2^8 (0.648438)  [error = 0/100]  
  
[With 2 bits]: 0.35 ~ 3 / 2^3 (0.375)  [error = 7/100]  
[With 4 bits]: 0.35 ~ 11 / 2^5 (0.34375)  [error = 2/100]  
[With 6 bits]: 0.35 ~ 45 / 2^7 (0.351562)  [error = 0/100]  
[With 8 bits]: 0.35 ~ 179 / 2^9 (0.349609)  [error = 0/100]  
   
[With 1 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 2965  [error = 254/1000]  
[With 2 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4448  [error = 119/1000]  
[With 3 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4008  [error = 8/1000]  
[With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 30/1000]  
[With 5 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3967  [error = 2/1000]  
[With 6 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4004  [error = 7/1000]  
[With 7 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3977  [error = 0/1000]  
[With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2/1000]  
```


[© Copyright](../COPYRIGHT.md)   

