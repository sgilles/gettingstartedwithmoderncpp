---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Object programming](./0-main.ipynb) - [Introduction to the concept of object](./1-Introduction.ipynb)


## Motivation


Sometimes, there are variables that are bound to be initialized and used together. Let's consider the coordinates of a vector in a three-dimensional space:

```c++
#include <iostream>
#include <cmath>  // For std::sqrt

double Norm(double v_x, double v_y, double v_z) 
{ 
    return std::sqrt( v_x * v_x + v_y * v_y + v_z * v_z ); 
}

{
    double v1_x, v1_y, v1_z;
    v1_x = 1.;
    v1_y = 5.;
    v1_z = -2.;

    std::cout << Norm(v1_x, v1_y, v1_z) << std::endl;

    double v2_x, v2_y, v2_z;
    v2_x = 2.;
    v2_y = 2.;
    v2_z = 4.;

    std::cout << Norm(v2_x, v2_y, v2_z) << std::endl;
}

```

The code above is completely oblivious of the close relationship between `x`, `y` and `z`, and for instance the `Norm` function takes three distinct arguments. 

This is not just an inconveniency: this can lead to mistake if there is an error in the variables passed:

```c++
{
    double v1_x, v1_y, v1_z;
    v1_x = 1.;
    v1_y = 5.;
    v1_z = -2.;
    double v2_x, v2_y, v2_z;
    v2_x = 2.;
    v2_y = 2.;
    v2_z = 4.;

    const double norm1 = Norm(v1_x, v1_y, v2_z); // probably not what was intended, but the program 
                                                 // has no way to figure out something is fishy!
}
```

## The C response: the `struct`

C introduced the `struct` to be able to group nicely data together and limit the risk I exposed above:


```c++
struct Vector
{
    double x;
    double y;
    double z;    
};
```

```c++
#include <iostream>

double Norm(Vector v)
{
    return std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z); 
}

{
    Vector v1;
    v1.x = 1.;
    v1.y = 5.;
    v1.z = -2.;

    std::cout << Norm(v1) << std::endl;

    Vector v2;
    v2.x = 2.;
    v2.y = 2.;
    v2.z = 4.;

    std::cout << Norm(v2) << std::endl;    
}
```

Calling `Norm` is now both more elegant (only one argument) and less dangerous (I can't mix by mistake coordinates from different objects).

Let's introduce at this point a bit of vocabulary:

- `x`, `y` and `z` in the structure are called **member variables** or **data attributes** (often shorten as **attributes** even if in a class this is actually not completely proper). On a side note: some C++ purists will be adamant only **member variables** should be used; but I rather use **data attributes** which is the term preferred in many others object programming languages.
- `Vector` is a **struct**, which is a somewhat simplified **class** (we will explain the difference when we'll introduce classes).
- `v1` and `v2` are **objects**.

Let's also highlight the `.` syntax which allows to access the attributes of an object (e.g `v1.x`).



### The semicolon at the end of a `struct`
This comes historically from the C, where a `struct` could be defined and initialized at the same time (or should - Xeus-cling doesn't manage it... As usual you may check a full-fledged compiler accepts it [@Coliru](http://coliru.stacked-crooked.com/a/3b77606ea8082485)):

```c++
// Xeus-cling issue (at least circa September 2022 and still there in February 2024)

struct VectorAndInstantiate
{
    double x;
    double y;
    double z;    
} v1; // Here the struct is declared and at the same time an object v1 is created

v1.x = 1.;
v1.y = 5.;
v1.z = -2.;
```

This is absolutely **not** encouraged in C++, but it may help you to remember always closing a `struct` (or later a `class`) with a semicolon.


## Passing a struct to a function

In the `norm` function above, we passed as argument an object of `Vector` type by value. When we introduced functions, we saw there were three ways to pass an argument:
* By value.
* By reference.
* By pointers.

I didn't mention there the copy cost of a pass-by-value: copying a plain old data (POD) type such as an `int` or a `double` is actually cheap, and is recommended over a reference. But the story is not the same for an object: the cost of copying the object in the case of a pass-by-value may actually be quite high (imagine if there were an array with thousands of `double` values inside for instance) - and that's supposing the object is copyable (but we're not quite ready yet to deal with [that aspect](../3-Operators/4-CanonicalForm.ipynb#Uncopyable-class)).

### Pass-by-const-reference

So most of the time it is advised to pass arguments by reference, often along a `const` qualifier if the object is not to be modified by the function:


```c++
double NormWithoutCopy(const Vector& v)
{
    return std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z); 
}

{
    Vector v1;
    v1.x = 1.;
    v1.y = 5.;
    v1.z = -2.;

    std::cout << NormWithoutCopy(v1) << std::endl;
}
```

### Pass-by-pointer

Of course, if for some reason you prefer to use pointers it is also possible:

```c++
double Norm(const Vector* const v) // can keep the name here: no possible ambiguity
{
    return std::sqrt((*v).x * (*v).x + (*v).y * (*v).y + (*v).z * (*v).z); 
}

{
    Vector v1;
    v1.x = 1.;
    v1.y = 5.;
    v1.z = -2.;

    std::cout << Norm(&v1) << std::endl;
}
```

This is more than little verbosy, so a shortcut has been introduced; `->` means you dereference a pointer and then calls the attribute:

```c++
double NormWithPointerShortcut(const Vector* const v)
{
    return std::sqrt(v->x * v->x + v->y * v->y + v->z * v->z); 
}

{
    Vector v1;
    v1.x = 1.;
    v1.y = 5.;
    v1.z = -2.;

    std::cout << NormWithPointerShortcut(&v1) << std::endl;
}
```

## Initialization of objects

So far, we have improved the way the `Norm` function is called, but the initialization of a vector is still a bit tedious. Let's wrap up a function to ease that:

```c++
void Init(Vector& v, double x, double y, double z) 
{
    v.x = x;
    v.y = y;
    v.z = z;
}
```

```c++
{
    Vector v1;
    Init(v1, 1., 5., -2.);
    std::cout << "Norm = " << Norm(v1) << std::endl;
}
```

[© Copyright](../COPYRIGHT.md)   

