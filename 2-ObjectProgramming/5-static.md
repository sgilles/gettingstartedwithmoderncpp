---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Object programming](./0-main.ipynb) - [Static attributes](./5-static.ipynb)


## Static in C

As a reminder, we have seen in a [previous notebook](../1-ProceduralProgramming/7-StaticAndConstexpr.ipynb#Static-keyword) the `static` keyword inherited from C. 

What follows is the same  keyword used in a very different context (even if the one we already know will pop up in an idiom presented here).



## Static methods


Sometimes, a data is related to the _class_ itself rather than to the object. The way to indicate this is to put a `static` keyword in front of the attribute that is not especially related to the instantiated object but rather common to all instances.

Static attributes are following the exact same rules as the standard ones regarding the access status (public or private).

```c++
#include <string>

struct Class
{
    static std::string ClassName();
    
    Class() = default;
    
};
```

```c++
std::string Class::ClassName()
{
    return "Class";
}
```

```c++
#include <iostream>

{
    std::cout << "A static method may be called without any object instantiated: " 
              << Class::ClassName() << std::endl;
    
    Class obj;
    
    std::cout << "But any object of the class may access it as if it was a regular method: "
              << obj.ClassName() << std::endl;
    
}
```

<!-- #region -->
## Static data attributes - to avoid... (see next section to understand why!)


**Xeus-Cling Warning:** cling doesn't enable proper initialization of a static data attribute... Please considered the following code, available [@Coliru](https://coliru.stacked-crooked.com/a/f43bcc4548a4f160):
<!-- #endregion -->

```c++
#include <iostream>
#include <vector>

struct Class
{
    Class();
    
    ~Class();
    
    static unsigned int Ninstance_;
};

Class::Class()
{
    ++Ninstance_;
}

Class::~Class()
{
    --Ninstance_;
}

// IMPORTANT: this line must be put in a compiled file!
unsigned int Class::Ninstance_ = 0;

void Print()
{
    std::cout << "There are " << Class::Ninstance_ << " of class Class." << std::endl;
}

int main(int argc, char** argv)
{
    Print();
    
    Class obj;
    std::cout << "Access by an object is still possible: " << obj.Ninstance_ << std::endl;
    
    Print();
    
    {
        std::vector<Class> vec(5);    
        Print();
    }
    
    Print();
    
    return EXIT_SUCCESS;
}
```

## Static order initialization fiasco - and its fix

However there is a possible problem not easy to show: when a program is compiled, there are no guarantee whatsoever about the order in which the source files will be compiled. It is therefore completely possible to use a static data attribute in a file *before* its initial value is actually given in another file. This lead to undefined behaviour... The way to fix it is to use a static method instead:

```c++
#include <iostream>
#include <vector>

struct Class3
{
    Class3();
    
    ~Class3();
    
    static unsigned int& Ninstance(); // notice the reference and the fact it's now a method
};
```

```c++
unsigned int& Class3::Ninstance()
{
    static unsigned int ret = 0; // the initial value, please notice the use of C static here!
    return ret;
}
```

```c++
Class3::Class3()
{
    Ninstance()++;
}
```

```c++
Class3::~Class3()
{
    Ninstance()--;
}
```

```c++
void Print3()
{
    std::cout << "There are " << Class3::Ninstance() << " of class Class." << std::endl;
}
```

```c++
{
    Print3();
    
    Class3 obj;
    std::cout << "Access by an object is still possible: " << obj.Ninstance() << std::endl;
    
    Print3();
    
    {
        Class3* vec = new Class3[5];    
        Print3();
        delete[] vec;
    }
    
    Print3();
}
```

To understand better the possible issue and the fix proposed, you may have a look at:

* Item 26 of [More effective C++](../bibliography.ipynb#Effective-C++-/-More-Effective-C++)
* The dedicated item on [isocpp FAQ](https://isocpp.org/wiki/faq/ctors#static-init-order)



### New C++ 17 fix

C++ 17 actually provides a way to define the value in the header file within the class declaration with the `inline` keyword:

```c++
struct Class4
{
    Class4();
    
    ~Class4();
    
    static inline unsigned int Ninstance_ = 0;
};
```

Thanks to this [FluentCpp post](https://www.fluentcpp.com/2019/07/23/how-to-define-a-global-constant-in-cpp/) that gave me the hint!


[© Copyright](../COPYRIGHT.md)   

