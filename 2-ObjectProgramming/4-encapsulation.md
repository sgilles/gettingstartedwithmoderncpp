---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Object programming](./0-main.ipynb) - [Encapsulation](./4-encapsulation.ipynb)


## Public and private

We've now played quite a bit with objects now, but we are still using `struct` and not `class` keyword. So it's time to try to define a class:

```c++
class FirstClass
{
    FirstClass();
};
```

```c++
#include <iostream>

FirstClass::FirstClass()
{
    std::cout << "Hello world!" << std::endl;
}
```

```c++
{
    FirstClass object; // COMPILATION ERROR!
}       
```

The compiler informs you that a **private** constructor was called.

In fact, in C++ you we may define the level of access of attributes with three levels (we'll let the third slip until the [chapter about inheritance](./6-inheritance.ipynb#Protected-status)):

* **private** means only the member functions of the class are entitled to access it (and [friends](./4-encapsulation.ipynb#Friendship) as we shall see later in this notebook)
* **public** means everyone may access it.

To determine which one is used, `public:` and `private:` may be added in a class declaration:

```c++
class SecondClass
{
    public:
    
        SecondClass(int a);
    
    private:
    
        int a_ { -9999999 }; // stupid default value.
    
        void SetValue(int a);
    
    public:
    
        void Print() const;    
};
```

```c++
SecondClass::SecondClass(int a)
{
    SetValue(a); // Ok: a member function (here the constructor) may call private method
}
```

```c++
#include <iostream>

void SecondClass::Print() const
{
    std::cout << "The value is " << a_ << std::endl; // Ok: a member function may use private data attribute.
}
```

```c++
void SecondClass::SetValue(int a)
{
    a_ = a; // Ok: a member function may use private data attribute.
}
```

```c++
{
    SecondClass object(5);
    object.Print(); // Ok: public method
}
```

```c++
{
    SecondClass object(5);
    object.SetValue(7); // COMPILATION ERROR: trying to call publicly a private method
}
```

<!-- #region -->
As you may see on our example, there might be as many public and private sections as you wish, and their ordering doesn't matter (often coding standards recommend such an ordering, saying for instance to put public interface first, but the language itself does not care in the least).

One side note for those accustomed to other languages: C++ is really hell bent about privacy status. It is not a gentleman's agreement as in Python where the `_` prefix is an indication an attribute should not be used publicly but a user may supersede the choice anyway; in C++ you can't call directly a private method of a class
without modifying the class interface yourself - which is ill-advised, especially if we're talking about code from a third-party library.


## Struct and class

The difference in a (C++) `struct` and a `class` is in fact thin-veiled:

* A `struct` assumes implicitly that attributes are public if nothing is specified.
* A `class` assumes implicitly that attributes are private if nothing is specified.

I would advise personally to use classes and specify explicitly the sections (as we shall see very soon it is advised to get at least some private parts) but you now understand why we stick with `struct` in the former chapters: it allowed not to meddle with public/private concerns.


## Why encapsulation?

So far we've described the mechanism, but not provided much insight on why such a hurdle was introduced in the first place. Let's see a concrete example in which the benefits of encapsulation appear clearly:
<!-- #endregion -->

```c++
struct Rectangle
{
    Rectangle(double length, double width);

    double length_;
    double width_;
    double area_;
    
    void Print() const;
};
```

```c++
Rectangle::Rectangle(double length, double width)
: length_(length),
width_(width),
area_(length * width)
{ }
```

```c++
#include <iostream>

void Rectangle::Print() const
{
    std::cout << "My rectangle is " << length_ << " x " << width_ << " so its area is " << area_ << std::endl;    
}
```

```c++
{
    Rectangle rect(5., 4.);    
    rect.Print(); // OK    
    rect.length_ = 23.;    
    rect.Print(); // Not so much...    
}
```

Encapsulation may protect from that:

```c++
class MoreSecureRectangle
{
    public:
    
        MoreSecureRectangle(double length, double width);
        void Print() const;

    private:
        
        double length_;
        double width_;
        double area_;
};
```

```c++
MoreSecureRectangle::MoreSecureRectangle(double length, double width)
: length_(length),
width_(width),
area_(length * width)
{ }
```

```c++
#include <iostream>

void MoreSecureRectangle::Print() const
{
    std::cout << "My rectangle is " << length_ << " x " << width_ << " so its area is " << area_ << std::endl;    
}
```

```c++
{
    MoreSecureRectangle rect(5., 4.);    
    rect.Print(); // OK    
    rect.length_ = 0.; // can't do that!   
}
```

Of course, we have lost functionality here... If we want to add the functionality to change the values, we need more member functions:

```c++
class Rectangle3
{
     public:
    
        Rectangle3(double length, double widgth);
        void Print() const;
    
        void SetLength(double x);
        void SetWidth(double x);

    private:
        
        double length_;
        double width_;
        double area_;
};
```

```c++
Rectangle3::Rectangle3(double length, double width)
: length_(length),
width_(width),
area_(length * width)
{ }
```

```c++
#include <iostream>

void Rectangle3::Print() const
{
    std::cout << "My rectangle is " << length_ << " x " << width_ << " so its area is " << area_ << std::endl;    
}
```

```c++
void Rectangle3::SetLength(double x)
{
    length_ = x;
    area_ = length_ * width_;
}
```

```c++
void Rectangle3::SetWidth(double x)
{
    width_ = x;
    area_ = length_ * width_;    
}
```

```c++
{
    Rectangle3 rect(5., 4.);    
    rect.Print(); // OK    
    rect.SetLength(23.);    
    rect.Print(); // OK    
}
```

It should be noticed the class above is safer, but not very good nonetheless:

* The computation of the area is written in three different places: constructor, `SetLength()` and `SetWidth()`... It would be better to get a private `ComputeArea()` method that does this task (granted here for this example it might be overkill, but in real cases the operation might be much more than a mere multiplication...)
* The idea to store the `area_` is not that right here: a public method `GetArea()` or `ComputeArea()` or whatever you want to call it would be preferable in this case. Of course in a more complex problem it might not: it depends whether the computation is costly or not (here: not) and how often you need to use the value in average before recomputing it.


## Good practice: data attributes should be private

In the example above, we saw that the publicly data attribute could lead to an inconsistent state within the object.

It is often advised (see for instance item 22 of [Effective C++](../bibliography.ipynb#Effective-C++-/-More-Effective-C++) to make all data attributes private, with dedicated methods to access and eventually modify those data. This approach enables fine-tuning of access: you may define whether a given **accessor** or **mutator** should be public or private.

```c++
class Rectangle4
{
     public:
    
        Rectangle4(double length, double width);
    
        // Mutators
        void SetLength(double x);
        void SetWidth(double x);
    
        // Accessors
        double GetLength() const;
        double GetWidth() const;

    private:
        
        double length_ { -1.e20 }; // a stupid value which at least is deterministically known...
        double width_ { -1.e20 };
};
```

```c++
Rectangle4:: Rectangle4(double length, double width)
: length_(length),
width_(width)
{ }
```

```c++
void Rectangle4::SetLength(double x)
{
    length_ = x;
}
```

```c++
void Rectangle4::SetWidth(double x)
{
    width_ = x;
}
```

```c++
double Rectangle4::GetLength() const
{
    return length_;
}
```

```c++
double Rectangle4::GetWidth() const
{
    return width_;
}
```

```c++
double ComputeArea(const Rectangle4& r) // free function
{
    return r.GetLength() * r.GetWidth(); // ok: public methods! And no risk to inadvertably change the values here.
}
```

```c++
#include <iostream>

{
    Rectangle4 rect(4., 6.5);
    std::cout << "Area = " << ComputeArea(rect) << std::endl;
    
    rect.SetWidth(5.);
    std::cout << "Area = " << ComputeArea(rect) << std::endl;    
}
```

## Good practice: the default status of a method should be private and you should use mutators for quantities that might vary

In our `Rectangle4` example, all mutators and accessors were public. It is clearly what is intended here so it's fine, but as a rule you should really put in the public area what is intended to be usable by an end-user of your class. Let's spin another variation of our `Rectangle` class to illustrate this:



```c++
class Rectangle5
{
     public:
    
        Rectangle5(double length, double width);
    
        // Mutators
        void SetLength(double x);
        void SetWidth(double x);
    
        // Accessor
        double GetArea() const; // others accessors are not defined to limit steps to define the class
    
    private:
    
        // Mutator for the area
        // Prototype is not bright (parameters are clearly intended to be the data attributes
        // and a prototype with no parameters would be much wiser!)
        // but is useful to make my point below.
        void SetArea(double length, double width);
    
    private:
        
        double area_ { -1.e20 }; // a stupid value which at least is deterministic...
        double length_ { -1.20 }; 
        double width_ { -1.20 };
};
```

```c++
void Rectangle5::SetArea(double length, double width)
{
    area_ = width * length;
}
```

```c++
Rectangle5:: Rectangle5(double length, double width)
: length_(length),
width_(width)
{
    SetArea(width_, length_);
}
```

```c++
double Rectangle5::GetArea() const
{
    return area_;
}
```

```c++
void Rectangle5::SetLength(double x)
{
    length_ = x;
    SetArea(width_, length_);
}
```

```c++
void Rectangle5::SetWidth(double x)
{
    width_ = x;
    SetArea(width_, length_);
}
```

```c++
#include <iostream>

void Print5(const Rectangle5& r)
{
    std::cout << "Area is " << r.GetArea() << std::endl;
}
```

```c++
{
    Rectangle5 rect(3., 4.);
    Print5(rect);
    
    rect.SetLength(8.);
    Print5(rect);
    
    rect.SetWidth(2.);
    Print5(rect);    
}
```

It's clear that `SetArea()` has no business being called publicly: this member function is introduced here to update the area each time a dimension has changed, but it is assumed to be called with very specific arguments (the data attributes).

A end-user of the class doesn't in fact even need to know there is such a method: for his purpose being able to change one dimension and to get the correct area is all that matters for them.

This rather dumb example illustrates the interest of a mutator: when `SetWidth()` or `SetLength()` are called, the value is assigned **and** another operation is also performed. If when extending a class you need another operations you didn't think of in the first place (imagine for instance you need for some reason to know how many times the length was modified) you have just to modify the code in one place: the definition of the mutator (plus defining a new data attribute to store this quantity and initializing it properly). If you didn't use a mutator, you would end-up search the code for all the locations the data attributes is modified and pray you didn't miss one...


## Friendship

Sometimes, you may have a need to open access to the private part of a class for a very specific other class or function. You may in this case use the keyword **friend** in the class declaration:

```c++
#include <limits>

class Vector
{
    public:
    
        Vector(double x, double y, double z);
    
        friend double Norm(const Vector&);
    
        friend class PrintVector; // In C++11 `class` became optional here.

        friend void Reset(Vector&); // inane design here, just to illustrate friendship grants write access as well - a method would be much more appropriate here!
    
    private:
    
        double x_ = std::numeric_limits<double>::min();
        double y_ = std::numeric_limits<double>::min();    
        double z_ = std::numeric_limits<double>::min();    
};
```

```c++
Vector::Vector(double x, double y, double z)
: x_(x),
y_(y),
z_(z)
{ }
```

```c++
#include <cmath>

double Norm(const Vector& v)
{
    return std::sqrt(v.x_ * v.x_ + v.y_ * v.y_ + v.z_ * v.z_); // OK!
}
```

```c++
class PrintVector // not the cleverest class we could write...
{
    public:
    
        PrintVector(const Vector& v);
    
        void Print() const;
    
    private:
    
        const Vector& vector_;
};
```

```c++
PrintVector::PrintVector(const Vector& v)
: vector_(v)
{}
```

```c++
#include <iostream>

void PrintVector::Print() const
{
    std::cout << "Content of the vector is (" << vector_.x_ << ", " << vector_.y_ 
        << ", " << vector_.z_ << ")." << std::endl; // OK because of friendship!
}
```

```c++
Vector v(2., 3., 5.);

PrintVector printer(v);
printer.Print();

```

```c++
void Reset(Vector& vector)
{
    vector.x_ = 0.;
    vector.y_ = 0.;
    vector.z_ = 0.;
}
```

```c++
printer.Print();
Reset(v);
printer.Print();
```

Obviously, friendship should be used with parsimony... But it's not that much of a deal-breaker as it may seem:

* The friendship must be defined in the class declaration. It means you can't use it to bypass a class encapsulation without modifying this code directly.
* The friendship is granted to a very specific function or class, and this class must be known when the class is defined. So an ill-intentioned user can't use the function prototype to sneak into your class private parts (in fact to be completely honest we will see an exception to this statement later with [forward declaration](../6-InRealEnvironment/2-FileStructure.ipynb#Forward-declaration)).


[© Copyright](../COPYRIGHT.md)   

