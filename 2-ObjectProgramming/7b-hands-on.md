---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Object programming](./0-main.ipynb) - [Hands-on 8](./7b-hands-on.ipynb)


### **EXERCISE 21: base class `TestDisplay`**

Create a base class `TestDisplay` from which both `TestDisplayPowerOfTwoApprox` and `TestDisplaySum` will inherit publicly.

This class:

* Should get a constructor which sets the resolution (respectively 100 or 1000 for our two derived classes)
* Should define the `RoundToInteger` enum (as protected)
* Includes a protected method named `PrintLine()` that will replace the `PrintLine()` we introduced in previous exercise.

The constructors of derived classes will of course have to be modified accordingly: so far we relied on default ones.

**Note:** If you're using clang with warnings activated, you may get this warning:

```shell
warning: 'TestDisplay' has no out-of-line virtual method definitions; its vtable will be emitted in every translation unit [-Wweak-vtables]
```

To fix it, you should define a virtual method that is not inlined - i.e. its definition is neither in the class declaration directly or defined with `inline` keyword.

In our case we define only one virtual method which doesn't really count as it is the virtual destructor. So we have to define one - even if it is not used. The proposed solution does so by adding a method not intended to be ever called.



### **EXERCISE 22: inherit from `TestDisplayPowerOfTwoApprox`**

We would like to get back former output in which we got first all outputs for 0.65, then all the ones for 0.35.

To do so, we will create two classes `TestDisplayPowerOfTwoApprox065` and `TestDisplayPowerOfTwoApprox035` that inherits from `TestDisplayPowerOfTwoApprox`.

Of course, we still abide by the DRY principle and we want to specialize only the code related to `Do()` method. 

**Note:** The design proposed here is clearly not the best - it is just a pretense to practise virtual methods and encapsulation.

The `main()` to use:

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    TestDisplayPowerOfTwoApprox065 test_display_approx065(100);     

    for (int nbits = 2; nbits <= 8; nbits += 2)
         test_display_approx065.Do(nbits); 

    std::cout << std::endl;
    
    TestDisplayPowerOfTwoApprox035 test_display_approx035(100);     

    for (int nbits = 2; nbits <= 8; nbits += 2)
         test_display_approx035.Do(nbits); 
    
    std::cout << std::endl;

    TestDisplaySumOfMultiply test_display_sum_of_multiply(1000);

    for (int nbits = 1; nbits <= 8; ++nbits)
        test_display_sum_of_multiply.Do(nbits);

    return EXIT_SUCCESS;
}
```

_Expected output:_

```
[With 2 bits]: 0.65 ~ 0.75  (3 / 2^2)  [error = 15/100]
[With 4 bits]: 0.65 ~ 0.625  (10 / 2^4)  [error = 4/100]
[With 6 bits]: 0.65 ~ 0.65625  (42 / 2^6)  [error = 1/100]
[With 8 bits]: 0.65 ~ 0.648438  (166 / 2^8)  [error = 0/100]

[With 2 bits]: 0.35 ~ 0.375  (3 / 2^3)  [error = 7/100]
[With 4 bits]: 0.35 ~ 0.34375  (11 / 2^5)  [error = 2/100]
[With 6 bits]: 0.35 ~ 0.351562  (45 / 2^7)  [error = 0/100]
[With 8 bits]: 0.35 ~ 0.349609  (179 / 2^9)  [error = 0/100]

[With 1 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 2965  [error = 254/1000]
[With 2 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4448  [error = 119/1000]
[With 3 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4008  [error = 8/1000]
[With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 30/1000]
[With 5 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3967  [error = 2/1000]
[With 6 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4004  [error = 7/1000]
[With 7 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3977  [error = 0/1000]
[With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2/1000]
```


### **EXERCISE 23: Toward a `TestDisplayContainer` class**

We would like to introduce an object whose purpose is to store the various `TestDisplay` class and call for each of them the `Do` method.

The declaration of the class should look like:


```c++
class TestDisplayContainer
{
public:

    //! Maximum number of objects that might be stored (to avoid magic number)
    static constexpr std::size_t MAX_ELTS { 3ul };

    //! Add a new test_display_register.
    //! At each call, the item to be registered is put at the first available position and internal current_position_
    //! is incremented. If the end-user attempts to register more than three items, the Error() function is called.
    void Register(TestDisplay* test_display);
    
    //! For each `TestDisplay` stored within the container, loop over all those bits and print the result on screen.
    void Do(int initial_Nbit, int final_Nbit, int increment_Nbit) const;
    
private:
    
    //! List of all known `TestDisplay` objects.
    TestDisplay* list_[MAX_ELTS];
    
    //! Index to place the next register object. If '3', no more object may be registered.
    std::size_t current_position_ {};
};
```

You will need to add a sanity check in constructor; in case of failure use at the moment the following function (that should already be in the file):

```c++
//! Function for error handling. We will see later how to fulfill the same functionality more properly.
[[noreturn]] void Error(std::string explanation)
{
    std::cout << "ERROR: " << explanation << std::endl;
    exit(EXIT_FAILURE);
}
```

The `main()` to use is:

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    TestDisplayContainer container;

    container.Register(new TestDisplayPowerOfTwoApprox065(100000000)); // we change the resolution 
    container.Register(new TestDisplayPowerOfTwoApprox035(100000000)); // we change the resolution 
    container.Register(new TestDisplaySumOfMultiply(1000000)); // we change the resolution 
    
    container.Do(4, 16, 4);
    
    return EXIT_SUCCESS;
}
```

_Expected result:_

```
    [With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 3846154/100000000]
    [With 8 bits]: 0.65 ~ 0.648438 (166 / 2^8)  [error = 240385/100000000]
    [With 12 bits]: 0.65 ~ 0.649902 (2662 / 2^12)  [error = 15024/100000000]
    [With 16 bits]: 0.65 ~ 0.649994 (42598 / 2^16)  [error = 939/100000000]
    
    [With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 1785714/100000000]
    [With 8 bits]: 0.35 ~ 0.349609 (179 / 2^9)  [error = 111607/100000000]
    [With 12 bits]: 0.35 ~ 0.349976 (2867 / 2^13)  [error = 6975/100000000]
    [With 16 bits]: 0.35 ~ 0.349998 (45875 / 2^17)  [error = 436/100000000]
    
    [With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 29917/1000000]
    [With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2000/1000000]
    [With 12 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 16 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
```


**Note:** If you're using clang and activate its warnings (should be the case if you're using our Fedora Docker image), you may get these warnings:

```shell
/home/dev_cpp/training_cpp/2-ObjectProgramming/exercise23.cpp:458:5: warning: unsafe buffer access [-Wunsafe-buffer-usage]
  458 |     list_[current_position_] = test_display;    
      |     ^~~~~
/home/dev_cpp/training_cpp/2-ObjectProgramming/exercise23.cpp:468:13: warning: unsafe buffer access [-Wunsafe-buffer-usage]
  468 |             list_[i]->Do(nbits);
```

The compiler is really right here; the code we proposed to limit the number objects is very clunky. We will for the sake of the tutorial keep them quite a while; however in a true codebase you should really strive to fix such warnings as soon as they pop up (we will eventually get there but once again for the purpose of the lecture it's not our top priority).


### **EXERCISE 24: dynamic allocation of array**

Instead of setting an arbitrary size of 3, we will now add a size dynamically in `TestDisplayContainer` constructor; the internal storage will now be:

```
TestDisplay** list_;
```

meaning we will store an array of pointers (don't worry, we will see later how to avoid such monstruosities... but it is useful nonetheless to try them a bit).

Constructor must now:

* Allocate the array of `TestDisplay*` with a **capacity** given as its argument (the capacity being the number of elements that *might* be stored inside - we'll see the chosen name is not a whim).
* Keep track of the capacity (the related data attribute should be constant: we don't intend to modify the capacity of the array after construction).
* Set each element to `nullptr`.

Destructor must of course take care of deallocating properly the memory.

<!-- #region -->
### **EXERCISE 25: transform `TestDisplayContainer::Do()` into a free function**

We probably went a bridge too far: it is useful to provide an object which contains several `TestDisplay` together, but making it take in charge the loop might not be that good an idea (in a real program you might for instance interact with this container by another mean than the pre-defined loop).

Replace the `TestDisplayContainer::Do()` method by a free function with signature:
```c++
void Loop(int initial_Nbit, int final_Nbit, int increment_Nbit, const TestDisplayContainer& container)
```

To do so, you will need to add several methods to `TestDisplayContainer`:

- A method that returns the **size** (i.e. the number of non nullptr `TestDisplay*` stored), which will be required to loop over the relevant elements.
- A method to access the `i`-th element stored in the table. Signature might be:
```c++
const TestDisplay& GetElement(std::size_t i) const
```
(but others are also possible - you may prefer to return a pointer rather than a reference).

New `main()` is:
<!-- #endregion -->

```c++
int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    TestDisplayContainer container(3ul);

    container.Register(new TestDisplayPowerOfTwoApprox065(100000000));
    container.Register(new TestDisplayPowerOfTwoApprox035(100000000));
    container.Register(new TestDisplaySumOfMultiply(1000000));
    
    Loop(4, 16, 4, container);
    
    return EXIT_SUCCESS;
}
```


[© Copyright](../COPYRIGHT.md)   


