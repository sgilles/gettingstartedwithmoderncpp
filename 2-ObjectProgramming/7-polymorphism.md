---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Object programming](./0-main.ipynb) - [Polymorphism](./7-polymorphism.ipynb)


## Polymorphism

### Naïve approach to underline the need

[So far](./6-inheritance.ipynb), I have not yet shown how objects could be stored in the same container, which was a justification I gave when introducing inheritance.

The first idea would be to cram all items in a container whose type is the base class:




```c++
#include <iostream>

class NotPolymorphicVehicle
{
    public:
    
        NotPolymorphicVehicle() = default;
};
```

```c++
#include <string>

class NotPolymorphicCar : public NotPolymorphicVehicle
{
    public:
    
        NotPolymorphicCar();
        
        void Print() const;
};
```

```c++
#include <iostream>

void NotPolymorphicCar::Print() const
{
    std::cout << "I'm a car!" << std::endl;
}
```

```c++
#include <string>

class NotPolymorphicBicycle : public NotPolymorphicVehicle
{
    public:
    
        NotPolymorphicBicycle();
        
        void Print() const;
};
```

```c++
#include <iostream>

void NotPolymorphicBicycle::Print() const
{
    std::cout << "I'm a bike!" << std::endl;
}
```

```c++
{
    NotPolymorphicBicycle b;
    NotPolymorphicCar c;
    
    NotPolymorphicVehicle list[2] = { b, c };
    list[0].Print(); // COMPILATION ERROR
}
```

The issue here is that the objects are stored as `NotPolymorphicVehicle`, and this class doesn't know any method called `Print()`, even if all its children do.

Defining `Print()` in the base class would not work either:

```c++
class AlsoNotPolymorphicVehicle
{
    public:

        AlsoNotPolymorphicVehicle() = default;
    
        void Print() const;
};

class AlsoNotPolymorphicCar : public AlsoNotPolymorphicVehicle
{
    public:
    
        AlsoNotPolymorphicCar() = default;

        void Print() const;
};


class AlsoNotPolymorphicBicycle : public AlsoNotPolymorphicVehicle
{
    public:
    
        AlsoNotPolymorphicBicycle() = default;

        void Print() const;
};


```

```c++
#include <iostream>

void AlsoNotPolymorphicVehicle::Print() const
{
    std::cout << "I am... hopefully a polymorphic vehicle?" << std::endl;
}
```

```c++
#include <iostream>

void AlsoNotPolymorphicCar::Print() const
{
    std::cout << "I'm a car!" << std::endl;
}
```

```c++
#include <iostream>

void AlsoNotPolymorphicBicycle::Print() const
{
    std::cout << "I'm a bike!" << std::endl;
}
```

```c++
#include <iostream>
{
    AlsoNotPolymorphicBicycle b;
    AlsoNotPolymorphicCar c;
    
    AlsoNotPolymorphicVehicle list[2] = { b, c };
    
    for (auto i = 0ul; i < 2ul; ++i)
        list[i].Print(); // No compilation error, but clearly not what we intended...
}
```

So far, the perspectives aren't rosy:

* The base class needs to know all the methods beforehand: `Print()` had to be defined in the base class to make it compile.
* And even so, the result was clearly not what we hoped for: the dumb value provided in the base class was actually returned.

### `virtual ` keyword

The second point is easy to solve: there is a dedicated keyword named **virtual** in the language that may qualify a method and tell it is likely to be adapted or superseded in a derived class. 

Almost all methods might be virtual, with very few exceptions:

* Static methods
* Constructors: no constructor can be virtual, and even more than that using a virtual method within a constructor won't work as expected (we'll come back to this [shortly](#Good-practice:-never-call-a-virtual-method-in-a-constructor))
* Template methods (we'll see that in part 4 of this lecture)

That means even the destructor may be virtual (and probably should - we'll go back to that as well...).

```c++
class PolymorphicButClumsyVehicle
{
    public:

        PolymorphicButClumsyVehicle() = default;
    
        virtual void Print() const;
};

class PolymorphicButClumsyCar : public PolymorphicButClumsyVehicle
{
    public:
    
        PolymorphicButClumsyCar() = default;

        virtual void Print() const;
};


class PolymorphicButClumsyBicycle : public PolymorphicButClumsyVehicle
{
    public:
    
        PolymorphicButClumsyBicycle() = default;

        virtual void Print() const;
};
```

```c++
#include <iostream>

void PolymorphicButClumsyVehicle::Print() const // Please notice: no `virtual` on definition!
{
    std::cout << "I am... hopefully a polymorphic vehicle?" << std::endl;
}
```

```c++
#include <iostream>

void PolymorphicButClumsyCar::Print() const
{
    std::cout << "I am a car!" << std::endl;
}
```

```c++
#include <iostream>

void PolymorphicButClumsyBicycle::Print() const
{
    std::cout << "I am a bike!" << std::endl;
}
```

### `virtual` work only with pointers or references

```c++
#include <iostream>
{
    PolymorphicButClumsyBicycle b;
    PolymorphicButClumsyCar c;
    
    PolymorphicButClumsyVehicle list[2] = { b, c };
    
    for (auto i = 0ul; i < 2ul; ++i)
        list[i].Print(); // No compilation error, but clearly not what we intended...
}
```

Still not what was intended... That's because when you use the objects directly as we do here, **static binding** is used: the definitions seen in the base class are used directly because the resolution occurs at compilation time.

To use the **dynamic binding**, we need to use either references or pointers. Let's do that:

```c++
#include <iostream>
{
    PolymorphicButClumsyVehicle* b = new PolymorphicButClumsyBicycle;
    PolymorphicButClumsyVehicle* c = new PolymorphicButClumsyCar;
    
    PolymorphicButClumsyVehicle* list[2] = { b, c };
    
    for (auto i = 0ul; i < 2ul; ++i)
        list[i]->Print();
    
    delete b;
    delete c;
}
```

The fact that a `PolymorphicButClumsyVehicle` pointer is able to properly call the derived classes version is what is called **polymorphism**; it's at runtime that the decision to call `PolymorphicButClumsyVehicle::Print()` or `PolymorphicButClumsyCar::Print()` is taken.

We're nonetheless not completely done yet:

```c++
#include <iostream>
{
    PolymorphicButClumsyVehicle* v = new PolymorphicButClumsyVehicle;
    PolymorphicButClumsyVehicle* b = new PolymorphicButClumsyBicycle;
    PolymorphicButClumsyVehicle* c = new PolymorphicButClumsyCar;
    
    PolymorphicButClumsyVehicle* list[3] = { v, b, c };
    
    for (auto i = 0ul; i < 3ul; ++i)
        list[i]->Print();
    
    delete v;
    delete b;
    delete c;
}
```

### Abstract class and pure virtual methods

Our issue here is that `PolymorphicButClumsyVehicle` has no business being instantiated directly: it is merely an **abstract class** which should never be instantiated but is there to provide a skeleton to more substantiated derived classes.

The mechanism to indicate that is to provide at least one **pure virtual method**: a method which prototype is given in the base class but that **must** be overridden in derived classes (at least if you want them to become concrete). The syntax to do so is to add `= 0` after the prototype.

```c++
class PolymorphicVehicle
{
    public:

        PolymorphicVehicle() = default;
    
        virtual void Print() const = 0; // the only change from PolymorphicButClumsyVehicle!
};

class PolymorphicCar : public PolymorphicVehicle
{
    public:
    
        PolymorphicCar() = default;

        virtual void Print() const;
};


class PolymorphicBicycle : public PolymorphicVehicle
{
    public:
    
        PolymorphicBicycle() = default;

        virtual void Print() const;
};
```

```c++
#include <iostream>

void PolymorphicCar::Print() const
{
    std::cout << "I am a car!" << std::endl;
}
```

```c++
#include <iostream>

void PolymorphicBicycle::Print() const
{
    std::cout << "I am a bike!" << std::endl;
}
```

```c++
{   
    PolymorphicVehicle v; // Compilation error: you can't instantiate an abstract class!
}
```

But the following is fine:

```c++
#include <iostream>
{
    PolymorphicVehicle* b = new PolymorphicBicycle;
    PolymorphicVehicle* c = new PolymorphicCar;
    
    PolymorphicVehicle* list[2] = { b, c };
    
    for (auto i = 0ul; i < 2ul; ++i)
        list[i]->Print();     

    delete b;
    delete c;
}
```

_(don't worry if you get a warning - if so the compiler does its job well and we'll see shortly why)_

**Beware:** You **must** provide a definition for all non pure-virtual methods in your class. Not doing so leads to a somewhat cryptic error at link-time.

You are not required to provide a definition for a pure virtual method, and you won't most of the time... But you might provide one if you want to do so, for instance to provide an optional default instantiation for the method in derived classes:

```c++
struct VirtualBase
{
     virtual void Method() = 0;
};
```

```c++
#include <iostream>

void VirtualBase::Method()
{
    std::cout << "Default implementation provided in abstract class." << std::endl;
}
```

```c++
struct Concrete1 : public VirtualBase
{
    virtual void Method() override;
};
```

```c++
struct Concrete2 : public VirtualBase
{
    virtual void Method() override;
};
```

```c++
void Concrete1::Method()
{
    VirtualBase::Method(); // call to the method defined in the base class
    std::cout << "This enables providing a base behaviour that might be completed if needed "
        "in derived classes, such as here by these lines you are reading!" << std::endl;
}
```

```c++
void Concrete2::Method()
{
    std::cout << "Overridden implementation." << std::endl;
}
```

```c++
{
    std::cout << "====== Concrete 1: uses up the definition provided in base class =====" << std::endl;
    Concrete1 concrete1;
    concrete1.Method();

    std::cout << "\n====== Concrete 2: doesn't use the definition provided in base class =====" << std::endl;
    Concrete2 concrete2;
    concrete2.Method();
}
```

### override keyword

We saw in previous section that to make a method virtual we need to add a `virtual` qualifier in front of its prototype. 

I put it both in the base class and in the derived one, but in fact it is entirely up to the developer concerning the derived classes:

```c++
class European
{
    public:
    
        European() = default;
    
        virtual void Print() const;
};
```

```c++
#include <iostream>

void European::Print() const
{
    std::cout << "I'm European!" << std::endl;
};
```

```c++
class French : public European
{
    public:
        
        French() = default;
    
        void Print() const; // virtual keyword is skipped here - and it's fine by the standard
};
```

```c++
#include <iostream>

void French::Print() const
{
    std::cout << "I'm French!" << std::endl;
};
```

```c++
{
    European* european = new European;
    european->Print();
    
    European* french = new French;
    french->Print();
}
```

But the drawback doing so is that we may forget the method is virtual... or we might do a typo when writing it! And in this case, the result is not what is expected:

```c++
class ThirstyFrench : public European
{
    public:
        
        ThirstyFrench() = default;
    
        virtual void Pint() const; // typo here! And the optional `virtual` doesn't help avoid it...
};
```

```c++
void ThirstyFrench::Pint() const
{
     std::cout << "I'm French!" << std::endl;
}
```

```c++
{   
    ThirstyFrench* french = new ThirstyFrench;
    french->Print();    
}
```

What would be nice is for the compiler to provide a way to secure against such errors... and that exactly what C++ 11 introduced with the `override` keyword. This keyword explicitly says we are declaring an override of a virtual method, and the code won't compile if the prototype doesn't match:

```c++
class ThirstyButCarefulFrench : public European
{
    public:
        
        ThirstyButCarefulFrench() = default;
    
        void Pint() const override; // COMPILATION ERROR!
};
```

```c++
class ForgetfulFrench : public European
{
    public:
        
        ForgetfulFrench() = default;
    
        virtual void Print() override; // COMPILATION ERROR! `const` is missing and therefore prototype doesn't match.
};
```

<!-- #region -->
### Cost of virtuality

You have to keep in mind there is a small cost related to the virtual behaviour: at each method call the program has to figure out which dynamic type to use. To be honest the true effective cost is quite blurry for me: some says it's not that important (see for instance [isocpp FAQ](https://isocpp.org/wiki/faq/virtual-functions)) while others will say you can't be serious if you're using them. I tend personally to avoid them in the part of my code where I want to crunch numbers fast and I use them preferably in the initialization phase.



## `dynamic_cast`

There is yet a question to ask: what if we want to use a method only defined in the derived class? For instance, if we add an attribute `oil_type_` that is not meaningful for all types of vehicles, can we access it?
<!-- #endregion -->

```c++
#include <string>

class PolymorphicVehicle
{
    public:

        PolymorphicVehicle() = default;
    
        virtual void Print() const = 0;
};



class PolymorphicThermicCar : public PolymorphicVehicle
{
    public:
    
        PolymorphicThermicCar(std::string&& oil_type);

        virtual void Print() const;
        
        const std::string& GetOilType() const;
        
    private:
    
        std::string oil_type_;
};
```

```c++
PolymorphicThermicCar::PolymorphicThermicCar(std::string&& oil_type)
: PolymorphicVehicle(), 
oil_type_(oil_type)
{ }
```

```c++
#include <iostream>
void PolymorphicThermicCar::Print() const
{
    std::cout << "I'm a car that uses up " << GetOilType() << std::endl;
}
```

```c++
const std::string& PolymorphicThermicCar::GetOilType() const
{
    return oil_type_;
}
```

```c++
#include <iostream>

{
    PolymorphicVehicle* c = new PolymorphicThermicCar("gazole");
    std::cout << "Oil type = " << c->GetOilType() << std::endl; // COMPILATION ERROR: been there before...
}
```

You may in fact explicitly tells the `c` pointer needs to be interpreted as a `PolymorphicThermicCar`:

**Xeus-cling issue:** Here cling may not be able to run it (depends on the version used - it didn't in 2021 but seems to work in 2022 and 2024) but it is accepted rightfully by a full-fledged compiler (see for instance [@Coliru](http://coliru.stacked-crooked.com/a/22fff15d28c93a17)):

```c++
// Xeus-cling issue!

#include <iostream>

{
    PolymorphicThermicCar* c = new PolymorphicThermicCar("gazole");
    
    PolymorphicVehicle* list[1] = { c }; // Ok!
        
    auto c_corrected = dynamic_cast<PolymorphicThermicCar*>(list[0]);
    
    std::cout << "Oil type = " << c_corrected->GetOilType() << std::endl;
}
```

So you could devise a way to identify which is the dynamic type of your `PolymorphicVehicle` pointer and cast it dynamically to the rightful type so that extended API offered by the derived class is accessible.

If you find this clunky, you are not alone: by experience if you really need to resort to **dynamic_cast** it's probably that your data architecture needs some revision. But maybe the mileage vary for other developers, and it's useful to know the possibility exists.


## `final` keyword

If you need to specify a class that can't be derived, you may stick a `final` keyword in its declaration (from C++11 onward):


```c++
class UnderivableClass final
{ };
```

```c++
class ITryAnyway : public UnderivableClass // COMPILATION ERROR!
{ };
```

The keyword may also be used only for one or several virtual methods:

```c++
struct DerivableClass
{
    virtual void Method1();
    virtual void Method2();
};
```

```c++
struct DerivedClassFirstLevel : public DerivableClass
{
    virtual void Method1() override final;
    virtual void Method2() override;
};
```

```c++
struct DerivedClassSecondLevel : public DerivedClassFirstLevel
{
    virtual void Method2() override; // ok
};
```

```c++
struct DerivedClassSecondLevelIgnoreFinal : public DerivedClassFirstLevel
{
    virtual void Method1() override; // COMPILATION ERROR!
};
```

## Virtual destructor to avoid partial destruction

I indicated earlier that destructors might be virtual, but in fact I should have said that for most of the non final classes the destructor should be virtual:

```c++
struct BaseClass3
{
    BaseClass3();
    
    ~BaseClass3();
};
```

```c++
#include <string>
#include <iostream>

BaseClass3::BaseClass3()
{
    std::cout << "BaseClass3 constructor" << std::endl;
}
```

```c++
BaseClass3::~BaseClass3()
{
    std::cout << "BaseClass3 destructor" << std::endl;
}
```

```c++
struct DerivedClass3 : public BaseClass3
{
    DerivedClass3();
    
    ~DerivedClass3();
};
```

```c++
DerivedClass3::DerivedClass3()
{
    std::cout << "DerivedClass3 constructor" << std::endl;
}
```

```c++
DerivedClass3::~DerivedClass3()
{
    std::cout << "DerivedClass3 destructor" << std::endl;
}
```

```c++
{
    std::cout << "Here all should be well: " << std::endl;
    {
        DerivedClass3 obj;        
    }
    
    std::cout << std::endl << "But there not so much, see the missing destructor call! " << std::endl;
  
    {
        BaseClass3* obj = new DerivedClass3;
        delete obj;
    }
    
}
```

You can see here the derived class destructor is not called! This means if you're for instance deallocating memory in this destructor, the memory will remain unduly allocated.

To circumvent this, you need to declare the destructor virtual in the base class. This way, the derived destructor will be properly called.

### Good practice: should my destructor be virtual?

So to put in a nutshell, 99 % of the time:

* If a class of yours is intended to be inherited from, make its destructor `virtual`.
* If not, mark the class as `final`.



## Good practice: never call a virtual method in a constructor

A very important point: I lost time years ago with this because I didn't read carefully enough item 9 of [Effective C++](../bibliography.ipynb#Effective-C++-/-More-Effective-C++)...

Due to the way construction occurs, never call a virtual method in a constructor: it won't perform the dynamic binding as you would like it to (and your compiler won't help you here).



```c++
#include <string>

struct BaseClass4
{
    BaseClass4();    
    
    virtual std::string ClassName() const;
};
```

```c++
struct DerivedClass4 : public BaseClass4
{
    DerivedClass4() = default;
    
    virtual std::string ClassName() const;
};
```

```c++
#include <iostream>

BaseClass4::BaseClass4()
{
    std::cout << "Hello! I'm " << ClassName() << std::endl;
}
```

```c++
std::string BaseClass4::ClassName() const
{
    return "BaseClass4";
}
```

```c++
std::string DerivedClass4::ClassName() const
{
    return "DerivedClass4";
}
```

```c++
{
    DerivedClass4 object; // not working by stack allocation...
    
    DerivedClass4* object2 = new DerivedClass4; // neither by heap allocation!
}
```

There is unfortunately no way to circumvent this; just some hack tactics (see for instance the _VirtualConstructor_ idiom on the [isocpp FAQ](https://isocpp.org/wiki/faq/virtual-functions)).

When I need this functionality, I usually define an `Init()` method to call after the constructor, which includes the calls to virtual methods:

```c++
#include <string>

struct BaseClass5
{
    BaseClass5() = default;
    
    void Init();
    
    virtual std::string ClassName() const;
};
```

```c++
struct DerivedClass5 : public BaseClass5
{
    DerivedClass5() = default; 
    
    virtual std::string ClassName() const;
};
```

```c++
#include <iostream>

void BaseClass5::Init()
{
    std::cout << "Hello! I'm " << ClassName() << std::endl;
}
```

```c++
std::string BaseClass5::ClassName() const
{
    return "BaseClass5";
}
```

```c++
std::string DerivedClass5::ClassName() const
{
    return "DerivedClass5";
}
```

```c++
{
    DerivedClass5 object; // ok by stack allocation
    object.Init();
    
    DerivedClass5* object2 = new DerivedClass5; // same by heap allocation
    object2->Init();
}
```

But it's not perfect either: if the end-user forget to call the `Init()` method the class could be ill-constructed (to avoid this either I provide manually a check mechanism or I make sure the class is private stuff not intended to be used directly by the end-user).


[© Copyright](../COPYRIGHT.md)   

