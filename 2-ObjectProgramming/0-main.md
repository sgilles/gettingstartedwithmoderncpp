---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Object programming](./0-main.ipynb)


* [Introduction to the concept of object](./1-Introduction.ipynb)
    * [Hands-on 4](./1b-hands-on.ipynb)
* [Member functions](./2-Member-functions.ipynb)
    * [Hands-on 5](./2b-hands-on.ipynb)
* [Base constructors and destructor](./3-constructors-destructor.ipynb)
    * [Hands-on 6](./3b-hands-on.ipynb)
* [Encapsulation](./4-encapsulation.ipynb)
    * [Hands-on 7](./4b-hands-on.ipynb)
* [Static attributes](./5-static.ipynb)
* [Inheritance](./6-inheritance.ipynb)
* [Polymorphism](./7-polymorphism.ipynb)
    * [Hands-on 8](./7b-hands-on.ipynb)



[© Copyright](../COPYRIGHT.md)   

