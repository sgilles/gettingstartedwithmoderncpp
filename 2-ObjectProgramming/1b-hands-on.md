---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Object programming](./0-main.ipynb) - [Hands-on 4](./1b-hands-on.ipynb)

<!-- #region -->
### Introduction

[This notebook](../HandsOn/HowTo.ipynb) explains very briefly your options to run the hands-ons.

**Note:** `initial_file.cpp` is the solution to the exercise 10 (we drop the writing into an external file and the function pointers).


### **EXERCISE 13: introduce `PowerOfTwoApprox` structure**

We will tackle the same problem with a dedicated `struct` called `PowerOfTwoApprox` which will be used to represent a real by two integers: the numerator and the exponent to the power of 2 used in the denominator.

Create this structure and use it throughout the program (`ComputePowerOf2Approx()` signature should be modified accordingly).

**Note:** if you're using the `initial_file.cpp` provided, you don't have to modify `HelperComputePowerOf2Approx()` function.

The expected output is unchanged from the initial file:

```
[With 2 bits]: 0.65 ~ 3 / 2^2 (0.75)  [error = 15/100]  
[With 4 bits]: 0.65 ~ 10 / 2^4 (0.625)  [error = 4/100]  
[With 6 bits]: 0.65 ~ 42 / 2^6 (0.65625)  [error = 1/100]  
[With 8 bits]: 0.65 ~ 166 / 2^8 (0.648438)  [error = 0/100]  
  
[With 2 bits]: 0.35 ~ 3 / 2^3 (0.375)  [error = 7/100]  
[With 4 bits]: 0.35 ~ 11 / 2^5 (0.34375)  [error = 2/100]  
[With 6 bits]: 0.35 ~ 45 / 2^7 (0.351562)  [error = 0/100]  
[With 8 bits]: 0.35 ~ 179 / 2^9 (0.349609)  [error = 0/100]  
   
[With 1 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 2965  [error = 254/1000]  
[With 2 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4448  [error = 119/1000]  
[With 3 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4008  [error = 8/1000]  
[With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 30/1000]  
[With 5 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3967  [error = 2/1000]  
[With 6 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 4004  [error = 7/1000]  
[With 7 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3977  [error = 0/1000]  
[With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2/1000]  
```
<!-- #endregion -->

[© Copyright](../COPYRIGHT.md)   

