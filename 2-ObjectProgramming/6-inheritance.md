---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Object programming](./0-main.ipynb) - [Inheritance](./6-inheritance.ipynb)


## Introduction to inheritance

Sometimes, you might want to define two types that are related: one might be an extension of the other, or they may share some similarities we would like to put in common.

Let's suppose for instance you are asked to register all vehicles owned by your company. You could define independent classes `Bicycle`, `Scooter` and `Car`, but as a result storing them in a same array (or even better `std::vector`) would be impossible. 

The idea of inheritance is to provide a **base class** from which our classes are derived:

```c++
#include <iostream>

enum class motor_type { none, thermic, electric };

class Vehicle
{
    public:
    
        Vehicle(motor_type type);
    
        void Print() const { std::cout << "I'm a Vehicle!" << std::endl; }
    
    private:
    
        motor_type type_;
};
```

```c++
Vehicle::Vehicle(motor_type type)
: type_(type)
{ }
```

```c++
class ElectricBicycle : public Vehicle
{
    public:
    
        ElectricBicycle();

};
```

```c++
class ThermicCar : public Vehicle
{
    public:
    
        ThermicCar();

};
```

```c++
ElectricBicycle::ElectricBicycle()
: Vehicle(motor_type::electric)
{ }
```

```c++
ThermicCar::ThermicCar()
: Vehicle(motor_type::thermic)
{ }
```

A bit of syntax first:

* See the structure in declaring the derived classes: there is a `:` followed by the keyword `public` and the name of the class.
* The derived constructors must first call one of the base class constructor. If none specified, the default one without arguments is called *if existing*...
    
The base class part is constructed first, and then the elements specific to the derived class are added (that will be important - we will come back to that).
    
Likewise, destruction is performed in reverse order: first the specific parts of the derived class, then the base class (in most cases you don't have to care about that).

### Multiple layer of inheritance

A child class may also be the parent of another class (unless `final` keyword is used - we'll see this keyword in [polymorphism notebook](./7-polymorphism.ipynb#final-keyword)).



```c++
class ElectricVehicle : public Vehicle
{
    public:
        
        ElectricVehicle();


};
```

```c++
ElectricVehicle::ElectricVehicle()
: Vehicle(motor_type::electric)
{ }
```

```c++
class ElectricCar : public ElectricVehicle
{
    public:
        
        ElectricCar() = default;

};
```

### Multiple inheritance

It is also possible for a class to inherit from several parents:

```c++
class BlueObjects
{
    public:
    
        BlueObjects() = default;
    
        int Print()
        {
            std::cout << "I'm blue!" << std::endl;
            return 42;
        }
}
```

```c++
class BlueVehicle : public Vehicle, public BlueObjects
{
    public:
    
        BlueVehicle(motor_type type);
};
```

```c++
BlueVehicle::BlueVehicle(motor_type type)
: Vehicle(type), // mandatory call of the non-default constructor 
BlueObjects() // not mandatory: default constructor called anyway if not specified explicitly
{ }
```

```c++
BlueVehicle blue_bike(motor_type::none);
```

**Beware:** there might be ambiguity between some methods names, regardless of prototype:

```c++
blue_bike.Print(); // COMPILATION ERROR: should it call int BlueObjects::Print() or void Vehicle::Print() const ?
```

It is possible to lift the ambiguity, but the syntax is not very pretty and you should strive to avoid the case that requires it in the first place:

```c++
blue_bike.BlueObjects::Print();
```

```c++
blue_bike.Vehicle::Print();
```

### Diamond inheritance

In C++ you may also define so-called diamond inheritance, where an object `D` inherits from the same base class `A` twice through different parents `B` and `C`:


<img src="./Images/DiamondInheritance.png" alt="Diamond inheritance diagram, from Wikipedia page (public domain)" width="150"/>


I really advise you to avoid this kind of things whenever possible, but if you _really_ need this:

- First think again, do you _really really_ need this, or could you reorganize your code differently to avoid this situation? (keep in mind other languages chose - wisely to my mind - not to support this, so it is possible to work around it!)
- Have a look [here](https://www.cprogramming.com/tutorial/virtual_inheritance.html) to understand the caveats and do it properly (one key is not to forget the keywords `virtual` in the inheritance line).


## Public inheritance, private inheritance and composition

### IS-A relationship of public inheritance

So far, we have derived **publicly** the base class (hence the **public** keyword in the inheritance declaration), and this defines a **IS-A** relationship: the derived object is expected to be acceptable *everywhere* a base object is deemed acceptable.

This might seem trivial, but is not as obvious at it seems. Let's consider a counter-intuitive example:


```c++
#include <string>

class Rectangle
{
    public:
    
        Rectangle(double width, double length);
    
        void SetWidth(double x);
    
        void SetLength(double x);    
    
        double GetWidth() const;
        double GetLength() const;    
    
        void Print() const;
    
    private:
    
        double width_ { -1.e20 } ; // Stupid value that would play havoc if not properly initialized
                                // - std::optional (C++17) would probably a better choice.
        double length_ { -1.e20 } ;      
};
```

```c++
Rectangle::Rectangle(double width, double length)
: width_(width),
length_(length)
{ }
```

```c++
double Rectangle::GetWidth() const
{
    return width_;
}
```

```c++
double Rectangle::GetLength() const
{
    return length_;
}
```

```c++
void Rectangle::SetWidth(double x)
{
    width_ = x;
}
```

```c++
void Rectangle::SetLength(double x)
{
    length_ = x;
}
```

```c++
#include <iostream>

void Rectangle::Print() const
{
    std::cout << "My rectangle gets a length of " << GetLength() << " and a width of " << GetWidth() << std::endl;
}
```

```c++
class Square : public Rectangle // BAD IDEA!
{
    public:
        
        Square(double side_dimension);

};
```

```c++
Square::Square(double side_dimension)
: Rectangle(side_dimension, side_dimension)
{ }
```

This is perfectly valid C++ code, and you might be happy with that... Now let's add a free-function that changes the shape of a rectangle:

```c++
void ModifyRectangle(Rectangle& r)
{
    r.SetWidth(r.GetWidth() * 2.);
    r.SetLength(r.GetLength() * .5);    
}
```

```c++
{
    std::cout << " ==== RECTANGLE ==== " << std::endl;
    Rectangle r(3., 5.);
    r.Print();
    
    ModifyRectangle(r); // ok
    r.Print();
    
    std::cout << " ==== SQUARE ==== " << std::endl;
    Square c(4.);
    c.Print(); // ok
    
    ModifyRectangle(c); // ok from compiler standpoint... not so much from consistency!
    c.Print(); // urgh...    
}
```

<!-- #region -->
So the language allows you to define this public relationship between `Rectangle` and `Square`, but you can see it is not a very bright idea... (this example is more detailed in item 32 of [Effective C++](../bibliography.ipynb#Effective-C++-/-More-Effective-C++)).

Don't get me wrong: public inheritance is very handy, as we shall see more below with the introduction of polymorphism. It's just that you need to assess properly first what your needs are, and decide which is the more appropriate answer - and sometimes the most obvious one is not the best.

The public inheritance is an application of the [Liskov substitution principle](https://en.wikipedia.org/wiki/Liskov_substitution_principle).

We will now "fix" our `Square` problem with two different idioms: private inheritance and composition.


### IS-IMPLEMENTED-IN-TERMS-OF relationship of private inheritance

What you might look for in fact is **private** inheritance, in which all the inherited attributes are considered private:
<!-- #endregion -->

```c++
class Square2 : private Rectangle
{
    public:
    
        Square2(double side_dimension);

};
```

```c++
Square2::Square2(double side_dimension)
: Rectangle(side_dimension, side_dimension)
{ }
```

```c++
{
    Square2 square(4.);
    square.SetWidth(5.); // COMPILATION ERROR!
}
```

And from there:

```c++
{
    Square2 square(4.);
    ModifyRectangle(square); // COMPILATION ERROR!
}
```

So this way, there is no assumption a `Square2` might pass any call a `Rectangle` would accept.

And of course the point is to avoid redefining stuff and relying upon what was already implemented in the first class (it won't be impressive here but in other more complex cases it might prove extremely handy):

```c++
class Square3 : private Rectangle
{
    public:
    
        Square3(double side_dimension);
        
        void SetSideDimension(double x);
        
        using Rectangle::Print; // special syntax to make explicitly available the Print() method from Rectangle
};
```

```c++
Square3::Square3(double side_dimension)
: Rectangle(side_dimension, side_dimension)
{ }
```

```c++
void Square3::SetSideDimension(double x)
{
    SetWidth(x); // the methods from Rectangle, accessible privately
    SetLength(x);
}
```

```c++
{
    Square3 square(4.);
    square.Print();
    square.SetSideDimension(3.);
    square.Print();    
}
```

### CONTAINS-A relationship of composition

Private inheritance is not the only way to provide this kind of behaviour; it is possible as well to use **composition**, which is defining a data attribute object that handles part of the computation.

I won't dwelve into too much details here, but the general idea is that composition is often preferable as the binding is less strong and you should strive for the looser relationship possible. Here is the same example as above implemented with composition:

```c++
class Square4
{
    public:
    
        Square4(double side_dimension);
    
        ~Square4() = default;
    
        void Print() const;
        
        void SetSideDimension(double x);
    
    private:
    
        const Rectangle& GetRectangle() const;

        Rectangle& GetNonConstantRectangle(); // we could have named it `GetRectangle()` as well, but a really distinct name is more expressive!
    
    private:
    
        Rectangle rectangle_;
};
```

```c++
const Rectangle& Square4::GetRectangle() const
{
    return rectangle_;
}
```

```c++
Rectangle& Square4::GetNonConstantRectangle()
{
    return rectangle_;
}
```

```c++
Square4::Square4(double side_dimension)
: rectangle_(Rectangle(side_dimension, side_dimension)) // you need the `Rectangle` constructor call here, as there are no default constructor in `Rectangle` class
{ }
```

```c++
void Square4::SetSideDimension(double x)
{
    auto& rectangle = GetNonConstantRectangle();
    rectangle.SetWidth(x);
    rectangle.SetLength(x);    
}
```

```c++
 void Square4::Print() const
 {
    GetRectangle().Print();
 }
```

```c++
{
    Square4 square(4.);
    square.Print();
    square.SetSideDimension(3.);
    square.Print();
}
```

### Private inheritance vs composition

So what are the pros and cons of private inheritance and composition?
    
- No granularity with private inheritance: you get all the interface available (privately) in the derived class. On the other hand, with composition you can choose which method you want to expose publicly in your new class (only `Print()` here).
- But composition is more verbose: if you want most of the interface, you need for each method to define a method which under the hood calls your data attribute (as did `Square4::Print()` above). So if you need most of the API of the base class, private inheritance is less work.

As indicated before, I tend to use composition more, but it's still nice to know both are available.



## Protected status

Our discussion about public and private inheritance has highlighted the importance of the propagation of the status of public members:

* With public inheritance, public members of the base class remains public.
* With private inheritance, public members of the base class become private.

So far, we have not talked about what happens to private members... Now is the time; let's find out!


```c++
class BaseClass
{
    public:
        
        BaseClass() = default;
    
    private:
    
        void DoNothing() { }
    
};
```

```c++
class DerivedClass : public BaseClass
{
    public:
    
        DerivedClass();
};
```

```c++
DerivedClass::DerivedClass()
{
    DoNothing(); // COMPILATION ERROR
}
```

So a private method is not accessible, even to the derived class. If you need such a functionality (and you should!), there is actually a third keyword: **protected**. This status means basically private except for the derived classes:

```c++
class BaseClass2
{
    public:
        
        BaseClass2() = default;
    
    protected:
    
        void DoNothing() { }
    
};
```

```c++
class DerivedClass2 : public BaseClass2
{
    public:
    
        DerivedClass2();
};
```

```c++
DerivedClass2::DerivedClass2()
{
    DoNothing(); // Ok
}
```

```c++
auto object = DerivedClass2();
```

```c++
object.DoNothing(); // COMPILATION ERROR!
```

Here `DoNothing` behaves exactly as a private method of `DerivedClass2`, but it was defined in `BaseClass2`.


There is as well a **protected** status for inheritance even if I must admit I have never needed it. A contributor to [StackOverflow](https://stackoverflow.com/questions/860339/difference-between-private-public-and-protected-inheritance) summed this up with this nice cheatsheet:


![Diagram](Images/PublicProtectedPrivateDiagram.jpg "Inheritance diagram, courtesy of [StackOverflow](https://stackoverflow.com/questions/860339/difference-between-private-public-and-protected-inheritance)")


## **[WARNING]** Be wary of the slicing effect

A note of caution about public inheritance:

```c++
class Base { };
```

```c++
#include <iostream>

class Derived : public Base
{
public:
    Derived(int val)
    : value_ { val }
    { }

private:
    int value_;
}
```

```c++
{
    Base base;
    Derived derived { 5 };
    base = derived; // dangerous line...
}
```

This code is perfectly valid, but there is a fat chance it's absolutely not what you intended: all the information that is in `Derived` but not in `Base` is lost in `base` object...

This is the so-called **slicing effect**.


Here it may seem obvious, but it might be insidious if you use functions with pass-by-value:

```c++
void ComputePoorlyStuff(Base base)
{
    // ...
}
```

```c++
{
    Derived derived { 5 };

    ComputePoorlyStuff(derived); // slicing effect: all defined only in `Derived` class is lost as far as `ComputePoorlyStuff` function is concerned!
}
```

you're actually slicing the `derived` object and only the `Base` part of it is copied and passed in the function!

The patch is rather easy and match what we already told for different reasons previously: don't pass objects by copy!

Indeed, the same with pass-by-reference (or pass-by-pointer if you like these sort of things...) works like a charm:

```c++
void ComputeStuff(const Base& base) // ok, no slicing effect!
{
    // ...
}
```

If you want to learn more about this effect, you should read this [great (and short) tutorial](https://www.learncpp.com/cpp-tutorial/object-slicing).


[© Copyright](../COPYRIGHT.md)   

