---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Operators](./0-main.ipynb)


* [Introduction to the concept of operator overload](./1-Intro.ipynb)
    * [Hands-on 9](./1b-hands-on.ipynb)
* [Comparison operators](./2-Comparison.ipynb)
* [Stream operators](./3-Stream.ipynb)
    * [Hands-on 10](./3b-hands-on.ipynb)
* [Assignment operator and the canonical form of a class](./4-CanonicalForm.ipynb)
* [Functors](./5-Functors.ipynb)
    * [Hands-on 11](./5b-hands-on.ipynb)



[© Copyright](../COPYRIGHT.md)   


