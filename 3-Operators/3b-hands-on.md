---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Operators](./0-main.ipynb) - [Hands-on 10](./3b-hands-on.ipynb)


### **EXERCISE 29: `operator<<`**

Introduce an `operator<<` for class `PowerOfTwoApprox` which provides the numerator and exponent used, e.g. `10 / 2^4`.

Use it in `TestDisplayPowerOfTwoApprox::Display()`.

_Expected result: (no modification)_

```
    [With 4 bits]: 0.65 ~ 0.625 (10 / 2^4)  [error = 3846154/100000000]
    [With 8 bits]: 0.65 ~ 0.648438 (166 / 2^8)  [error = 240385/100000000]
    [With 12 bits]: 0.65 ~ 0.649902 (2662 / 2^12)  [error = 15024/100000000]
    [With 16 bits]: 0.65 ~ 0.649994 (42598 / 2^16)  [error = 939/100000000]
    
    [With 4 bits]: 0.35 ~ 0.34375 (11 / 2^5)  [error = 1785714/100000000]
    [With 8 bits]: 0.35 ~ 0.349609 (179 / 2^9)  [error = 111607/100000000]
    [With 12 bits]: 0.35 ~ 0.349976 (2867 / 2^13)  [error = 6975/100000000]
    [With 16 bits]: 0.35 ~ 0.349998 (45875 / 2^17)  [error = 436/100000000]
    
    [With 4 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3857  [error = 29917/1000000]
    [With 8 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3968  [error = 2000/1000000]
    [With 12 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
    [With 16 bits]: 0.65 * 3515 + 0.35 * 4832 = 3976 ~ 3975  [error = 239/1000000]
```


[© Copyright](../COPYRIGHT.md)   

