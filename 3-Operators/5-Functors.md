---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Operators](./0-main.ipynb) - [Functors](./5-Functors.ipynb)


## What is a functor?

### Disclaimer: not a functional programming functor!

First of call, for those of you with a background in functional programming, what C++ calls a `functor` is [not what you are used to](https://en.wikipedia.org/wiki/Functor_(functional_programming)).

### Functor in C++

We can provide a class with an `operator()` that allows to give its objects a function behavior. In other words: if you write an expression in which an object is used as if it were a function, it is its execution operator that is invoked. 

We can also see these *functional objects*, or *functors*, as functions to which we would have added state parameters from one call to another.

The return type and the arguments are not constrained and might be defined as you wish in the class (`int` is used here for return type, and an `int` is required as argument):

```c++
class LinearFunction
{

    public :

        LinearFunction(int constant);

        int operator()(int value)
        {
            return constant_ * value; // here due to usual Xeus-cling issue when out of class 
                                      // definition is used for operators
        }

    private :

        int constant_ = 0.;

 } ;

```

```c++
LinearFunction::LinearFunction(int constant)
: constant_(constant)
{ }
```

```c++
#include <iostream>

{
    int values[10] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
  
    LinearFunction Double(2);
    LinearFunction Triple(3);

    for (int value : values)
        std::cout << value << "\t" << Double(value) << "\t" << Triple(value) << std::endl;
}
```

This might seem inconsequential, but they are sometimes extremely useful. 

## Functors in STL

STL itself defines some functors: imagine you want to sort a `std::vector` decreasingly; you can't directly put `>` as the comparison operation in `sort` and therefore may use a specific STL-defined `greater`: 

```c++
#include <algorithm>
#include <iostream>
#include <vector>

{
    std::vector<int> values { -980, 12, 2987, -8323, 4, 275, -8936, 27, -988, 92784 };
      
    std::sort(values.begin(), values.end(), std::greater<int>());
    
    for (auto value : values)
        std::cout << value << " ";    
}
```

C++ 11 and above limits the need for functors with lambda functions (see [earlier](../1-ProceduralProgramming/4-Functions.ipynb#Lambda-functions)), but in older codes, functors were one of the way to pass your own rules to some STL algorithms, along with pointer to functions.



[© Copyright](../COPYRIGHT.md)   

