---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [Getting started with the tutorial](./getting_started_with_tutorial.ipynb)


## About the choice of a Jupyter notebook

This notebook uses up [xeus-cling](https://xeus-cling.readthedocs.io/en/latest/), a special instance of Jupyter able to run C++ code based upon xeus (tool to build Jupyter kernels for any language) and cling (a creation from CERN to be able to run C++ as an interpreted language).

The reasons for these choices is to really access directly to handle C++ code without the hassle of explaining how to compile and run stuff, which is an especially cumbersome way to start with this (or any really...) language.

This is not to say this tutorial will ignore entirely these topics (see the dedicated [chapter](./6-InRealEnvironment/0-main.ipynb)), just that we will first focus on C++ code. However keep in mind that this notebook's fancy interpreter is not a typical C++ environment.

## When the notebook is not enough...

As we shall see repeatedly, Xeus-cling notebooks are far from being full-proof: some stuff that are perfectly acceptable C++ aren't accepted in them, and some others required work-arounds. When such an issue appears:

* It will be indicated explicitly in the notebook if a specific work around is used. We do not want you to take Jupyter work-arounds as a legit advice on how to write proper C++.
* If Jupyter can't deal with the code, we will use [Coliru](https://coliru.stacked-crooked.com/). Coliru is a C++ online compiler; others are listed [here]([GitHub page](https://arnemertz.github.io/online-compilers/)) ([Wandbox](https://wandbox.org/) deserves a shout out as it enables testing the same code with a great variety of compiler versions).

We're not sure we'll keep using Jupyter notebooks in the future: development of Xeus-cling has a bit stalled in the recent years and support of more recent versions of C++ (20 and more) is still unclear.


<!-- #region -->
## Few guidelines about Jupyter

You might not be familiar with Jupyter notebooks, so here are few tips to run it smoothly (the _Help_ menu will help you find more if you need it).

In a Jupyter notebook the content is divided into _cells_, in our case we are using two kind of cells:
* Markdown cells, such as the ones into these very words are written.
* Code cells, which are running code. In these notebooks the chosen kernel is C++17, so the code is C++17 which is interpreted by cling.

There are two modes:
* Edit mode, in which you might change the content of a cell. In this mode the left part of the cell is in green.
* Command mode, in which you might take actions such as changing the type of a cell, create or delete a new one, etc...

To enter in edit mode, simply type on 'Enter'.   
To enter in command mode, type 'Esc'.

To execute a cell, type 'Shift + Enter'. For a markdown cell it will edit nicely the content by interpreting the markdown, and for a code cell it will run the code.

In command mode, several handy shortcuts are there; I would recommend especially:
* `a` (add a cell above)
* `b` (add a cell below)
* `x` (cut a cell)
* `M` (change cell mode to Markdown)

The complete list is available in _Help_ > _Keyboard_ shortcut.

If for some reason the code in the notebook seems stuck, you might try to restart the kernel with one of the restart option in the _Kernel_ menu.

### Restarting the kernel

Sometimes something that should work doesn't... In this case try restarting the kernel: it might fix your issue!


### Table of contents

The table of content for a given notebook is available as a side panel if you go to _View_ > _Table of contents_ or if you click on the third item on the leftmost panel.
<!-- #endregion -->

## Very basic C++ syntax (in notebook and in general)

### Semicolons

In C++ most instructions end by a semicolon `;`. If you forget it, the underlying compiler  doesn't understand the syntax.

```c++
{
    int foo = 5 // COMPILATION ERROR!
}
```

```c++
{
    int foo = 5; // OK
}
```

Spaces, end lines and tabulations act as word separators; utterly unreadable code as the one below is perfectly fine from the compiler standpoint:

```c++
  #   include <string>
{
    int number ; number       = 1
    ;         std::string           name;
    name=
    "truc" ;
    number      =          2
    ;
}
```

### Input / output

Inputs and outputs aren't directly a part of the language itself, but are in the standard library (often abbreviated as STL for *Standard Template Library* even if some purist may yell and explain it's not 100 % the same thing...). You therefore need to __include__ a file named `iostream`; doing so will enable the use of the input / output facilities.

```c++
{
    std::cout << "Hello world!" << std::endl; // Should fail (unless you run a cell that includes iostream before)
}
```

```c++
#include <iostream>

{
    std::cout << "Hello world!" << std::endl; // Should work: std::cout and std::endl are now known.
}
```

- `std::cout` is the symbol to designate the standard output (i.e. your screen...)
- `std::endl` is the symbol to clean-up the stream and go to next line.

The operator `<<` is used to indicate what you direct toward the stream; here `std::cout << "Hello world!"` tells to redirect the string toward the standard output.

We will see that a bit more in detail in [a later chapter](./1-ProceduralProgramming/6-Streams.ipynb), but printing something is really helpful early on hence this brief introduction here.



### Comments

There are two ways to comment code in C++:

- `//` which comments all that is after this symbol on the same line.
- `/*` ... `*/` which comments everything between the symbols.


```c++
{
    int i = 0; // Everything after // is commented until the end of the line

    /*

    commented...

    also commented...
    */

    int j = 5; // no longer commented
    
    /*
    
    // This type of comment might be used inside the other style
    
    */
}
```


[© Copyright](COPYRIGHT.md)
