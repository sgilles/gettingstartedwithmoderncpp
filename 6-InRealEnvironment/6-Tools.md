---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [C++ in a real environment](./0-main.ipynb) - [External tools](./6-Tools.ipynb)

<!-- #region -->
## Introduction

The purpose of this notebook is just to namedrop briefly some facilities related to C++ that might be useful.
It's far from exhaustive - I won't present debuggers as usually they are provided directly with your IDE (and if you're a Vim or Emacs user you're probably already familiar with [gdb](https://www.gnu.org/software/gdb/)).

Some tools are redundant, but they are complementary: the more you use the better. The constraint is time: it is not straightforward to use some of those, and even for the user-friendly ones there is a non negligible set-up time. But for projects that have gained enough traction, at some point it becomes a no-brainer to set them up properly once and for all - usually part of the [continuous integration](https://gitlab.inria.fr/FormationIntegrationContinue/gitlabciintroduction) process.


## Online compilers

This [GitHub page](https://arnemertz.github.io/online-compilers/) gives a list of online compilers we mentioned several times in this tutorial.

I have used only [Coliru](http://coliru.stacked-crooked.com/) which provides an API to incorporate directly some interactive code in a Web page (disclaimer - I have not used that feature yet) and [Wandbox](http://melpon.org/wandbox) which may be useful in day-to-day work as you may try a snippet of code with many configurations. It's useful for instance when you intend to use a bleeding-edge feature to see which version of compilers support it or not. There are more advanced options as [Compiler Explorer](https://godbolt.org/) which allows to explore the intermediate Assembly code generated, this is a very advanced utility that may be useful to optimize your code and to understand how a compiler internally works.

## Static analyzer tools

[cppcheck](http://cppcheck.sourceforge.net/) is a static analyser program; it might really help you to find some questionable constructs in your code. As many tools, it's not 100 % accurate and sometimes it may raise false positives, but I nonetheless recommend it warmly.

[cpplint](https://github.com/cpplint/cpplint) is also worth a look, we mentioned it for instance in [this chapter](./2-FileStructure.ipynb) to track down missing includes in files.

[clang-tidy](https://clang.llvm.org/extra/clang-tidy/) is also used by some of us. [clang-tidy](https://clang.llvm.org/extra/clang-tidy/) also includes advanced utilities to automatically refactor/correct/improve code. See the following [link](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/-/wikis/How-to-use-Clang-Tidy-to-automatically-correct-code).

## Valgrind

[Valgrind](valgrind.org/) is mostly known as a leak checker, but is really a jack-of-all-trade tool; callgrind for instance may be used to profile your code and see where most of the computation time is spent. Some lesser-known tools are also incredibly useful: [Verrou](https://github.com/edf-hpc/verrou) for instance, developed at EDF, is a floating-point checker which helps you figure out if at some point some of your computations might be skewed by the way the computer approximates the real numbers.

Unfortunately, macOS support is scarse: sometimes they plainly say it is not up-to-date, but even when they say it works the outputs were never satisfactory for me (always check `valgrind ls` first as they recommend on their website...). So even if I develop mostly in macOS I always fire up valgrind in a Linux environment.

## Address sanitizer

A [recent competitor](https://github.com/google/sanitizers/wiki/AddressSanitizer) (once again use both if possible!) to Valgrind, which has the advantage of running with a much better runtime (Valgrind slows down your program tremendously). Might be integrated in some IDEs (it is in XCode on macOS).


## Sonarqube

[Sonarqube](https://www.sonarqube.org/) is a development platform which inspects your code and helps to figure out where there are issues. It enables integration of multiple tools such as cppcheck mentioned earlier. If you're Inria staff, an instance was set by our [Bordeaux colleagues](http://sed.bordeaux.inria.fr/) and is [available](https://sonarqube.inria.fr/) for you.

For open-source projects, the company behind Sonarqube provides a [freely accessible platform]( https://sonarcloud.io/about).


## Tests

These are frameworks to write your tests; to actually run them you should see what your build system provides (for instance CMake comes with CTest which takes gracefully the management of tests in your project).

### Google test

[GoogleTest](https://google.github.io/googletest/) includes everything that is needed for testing: [xUnit](https://en.wikipedia.org/wiki/XUnit), test discovery, rich test assertions, death tests, XML test report generation, ... 
It is also the only one we know that provide mocks. It is worth saying that mocking in C++ requires your application to be specially designed for it, more information are available on the [gMock cookbook](https://google.github.io/googletest/gmock_cook_book.html).


### Doctest

[doctest](https://github.com/onqtam/doctest) is another C++ testing framework that was very appreciated by one of the teacher (Vicente) in the 2021 session of this tutorial.


### BoostTest

[Boost test](https://www.boost.org/doc/libs/1_69_0/libs/test/doc/html/index.html) is a widely used facility to write unit and integration tests with your code.

It might be used as a header-only or as a compiled library; for big projects they recommend using the latter.

### Catch2

[Catch2](https://github.com/catchorg/Catch2) is a more recent test facility which is aimed at being user-friendly.


## Build system

Already mentioned [here](./1-SetUpEnvironment.ipynb#Build-system).

## Code formatters

It is useful to delegate the check on your code format to an external tool, which may take a bit of time to configurate to suit your needs. 

I can mention [Uncrustify](http://uncrustify.sourceforge.net/): plenty of options to configure, even if they're not easy to figure out.

[clang-format](https://clang.llvm.org/docs/ClangFormat.html) probably provides a better trade-off power/complexity but requires LLVM clang to be installed on your system (AppleClang won't do, but you can easily install it on macOS with Homebrew).


## Doxygen

[Doxygen](http://www.doxygen.nl/) is a software to write an automatic documentation for your functions / classes / types / you name it. 

It is rather easy to set up to easy task but may become a tad more difficult once you want to tackle more advanced features. Nonetheless it is the de-facto standard for C++ documentation and it's really something you should set up quite early if you're working on a project meant to stay for a while: it's really a hassle to spend countless hours providing after the fact such guidance in the code. As for compilers, you should strive to provide a documentation without any warning.

## Codespell

[codespell](https://github.com/codespell-project/codespell) is a very neat project to check english errors in your code. It may be used upon code directly or upon the associated README and is rather helpful to notice typos or some english errors (for instance it found out we used _informations_ a lot in these notebooks whereas it is incorrect and should be _information_).

A strong point for this tool is that it is very easy to use; no steep learning curve for it. 

We wouldn't however advice to use the options to automatically correct your documents; there might be some false positives.

## Ccache

[Ccache](https://ccache.dev/) is a great tool that caches compilation results and does it very well even when you work with different git branches, for instance. If your compilation time, even if you compile in parallel mode, is getting too annoyingly long, this tool is recommended. Ccache is also quite well integrated with CMake.

## More...

Our colleagues at Inria Bordeaux also recommend some additional tools in the Inria SonarQube
[documentation](https://sonarqube.inria.fr/pages/documentation.html#orgd4ab5b1).

We also mentioned in a notebook [include-what-you-use](https://include-what-you-use.org/) to help figure out which include declarations are truly required and when forward declaration may be used.



<!-- #endregion -->


[© Copyright](../COPYRIGHT.md)   

