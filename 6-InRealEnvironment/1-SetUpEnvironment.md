---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [C++ in a real environment](./0-main.ipynb) - [Set up environment](./1-SetUpEnvironment.ipynb)

<!-- #region -->
## Introduction 

I will present here briefly how to set up a minimal development environment... only in Unix-like systems: sorry for Windows developers, but I have never set up a Windows environment for development. You may have a look at WSL, which is gaining traction and enables you to use Linux inside Windows.

This will explain installation for two mainstreams compilers: [GNU compiler for C++](https://en.wikipedia.org/wiki/GNU_Compiler_Collection) and [clang++](https://en.wikipedia.org/wiki/Clang).

## Installing a compiler

**Note:** Compilers themselves will be addressed more in depth in an [upcoming notebook](3-Compilers.ipynb).


### Ubuntu / Debian
<!-- #endregion -->

#### Clang

To install `clang` you need to specify explicitly the required version, e.g.:

```c++
// In a terminal
sudo apt-get install -y clang++-13
```

#### Default g++

```c++
// In a terminal
sudo apt-get install -y g++
```

#### More recent gcc

However, Ubuntu is rather conservative and the version you get might be a bit dated and it might be problematic if you intend to use the bleeding-edged features from the latest C++ standard (even if it's now better than what it used to be).

In February 2024, default Ubuntu provided gcc 12; if you want gcc 13 you need to use the following [PPA](https://launchpad.net/ubuntu/+ppas):

(_disclaimer_: the instructions below have been tested in a Docker image - with `RUN` instead of `sudo` of course - so all the lines might not be necessary in a full-fledged Ubuntu distro)

```c++
// In a terminal

// Update the Ubuntu environment
sudo apt-get update && sudo apt-get upgrade -y -q && sudo  apt-get dist-upgrade -y -q && sudo  apt-get -y -q autoclean && sudo  apt-get -y -q autoremove)

// To enable `add-apt-repository` command; probably not required in a full-fledged installation
// but you will need it in a Docker image for instance
sudo apt-get install --no-install-recommends -y software-properties-common  gpg-agent wget

// Adding PPA and making its content available to `apt`.
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt-get update -y

// Installing the more recent gcc
sudo apt-get install -y g++-13
```

And to tell the system which version to use, the command is:

```c++
// In a terminal
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-13 100
```

More realistically, you will install gcc and perhaps gfortran as well; the following command make sure all are kept consistent (you do not want to mesh gcc 12 with g++ 13 for instance...):

```c++
// In a terminal
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-13 100  
    --slave /usr/bin/g++ g++ /usr/bin/g++-13
    --slave /usr/bin/gfortran gfortran /usr/bin/gfortran-13
```

### Fedora

For development purposes I rather like Fedora, which provides more recent versions than Ubuntu and also a simple clang installation.


#### g++

```c++
// In a terminal
sudo dnf install -y gcc gcc-c++ gcc-gfortran
```

#### clang++

```c++
// In a terminal
sudo dnf install -y clang
```

### macOS

* Install XCode from the AppStore
* Install if requested the command line tools

This will install _Apple Clang_ which is a customized version of `clang`. Unfortunately, Apple stopped providing the version of mainstream clang it is built upon; it's therefore more difficult to check if a new standard feature is already supported or not.

It is also possible to install gcc on macOS; I personally use [Homebrew](https://brew.sh) to do so. However using it is not always reliable (on STL side at least - see below), especially shortly after a new version of XCode / Command Line Tools has been published; I would advise to use primarily clang.



## STL

Besides the compiler, you may also choose which implementation of the STL you want to use. There are two mainstream choices:

- `libstdc++`, which is the STL provided along gcc by GNU. This is the default choice for many Linux distro, and there is a good chance the binaries, libraries and share libraries in your package system were compiled with this one.

- `libc++`, which is the STL provided along clang by LLVM. It is the default choice on macOS, and was until recently a pain to use with Ubuntu (according to Laurent it is much better now in Ubuntu 20.04 and more recent versions).

Both are pretty solid choices:

- Going with `libstdc++` is not a bad idea if you're using with your code libraries from your package manager that use this STL implementation (likely in a Linux distro).
- Going with `libc++` along with clang++ seems rather natural as well.


Just a note for compatibility: `libc++` tends to provide more `include` directive in its header files than `libstdc++`. So don't be astonished if your code compiles well with `libc++` but complains about an unknown symbol from STL with `libstdc++` (and the patch is simply to use the missing include - a tool such as IncludeWhatYouUse would have underlined the missing include even when using `libc++`).


## Editor


You will need a [source code editor](https://en.wikipedia.org/wiki/Source_code_editor) to write your code; usually your system will provide a very basic one and you may be able to install another on your own.

From my experience, there are essentially two types of developers:

- Those that revel in using [VIM](https://en.wikipedia.org/wiki/Vim_(text_editor)) **or** [emacs](https://en.wikipedia.org/wiki/Emacs), which are lightweight editors that have been around for decades and are rather powerful once you've climbed a (very) steep learning curve.
- Those that will like [integrated development environment](https://en.wikipedia.org/wiki/Integrated_development_environment) more, which provides more easily some facilities (that can often be configured the hard way in the aforementioned venerable text editors) but are more resource-intensive.

I suggest you take whichever you're the most comfortable with and don't bother about the zealots that tell you their way is absolutely the best one. 

_Vim_ and _emacs_ are often either already installed or available easily with a distribution package (_apt_, _dnf_, etc...); for IDEs here are few of them:

* [Visual Studio Code](https://code.visualstudio.com/), which gained traction in last few years and is one of the most sizeable GitHub project. This is an open-source and multi-platform editor maintained by Microsoft, not to be confused with [Visual Studio](https://visualstudio.microsoft.com/?rr=https%3A%2F%2Fwww.google.com%2F) - also provided by Microsoft on Windows (and with a fee).
* [CLion](https://www.jetbrains.com/clion/) by JetBrains is also a rising star in IDEs; a free version is available for students (you may know PyCharm by the same company)
* [Eclipse CDT](https://www.eclipse.org/cdt/) and [NetBeans](https://netbeans.org/) are other IDEs with more mileage.
* [QtCreator](https://www.qt.io/qt-features-libraries-apis-tools-and-ide) is not just for Qt edition and might be used as a C++ IDE as well.
* [XCode](https://developer.apple.com/xcode) is the editor provided by Apple on macOS.
* [KDevelop](https://www.kdevelop.org/) is the IDE from the KDE project.
* [JupyterLab](https://jupyter.org/) this very same notebook lab can be used as IDE after the last improvements and extensions added, [see this](https://towardsdatascience.com/jupyter-is-now-a-full-fledged-ide-c99218d33095) and how include the [VS Code Monaco Editor](https://imfing.medium.com/bring-vs-code-to-your-jupyterlab-187e59dd1c1b). I wouldn't advise it but if you're really keen to roll with it.



## Software configuration manager

A [software configuration manager](https://en.wikipedia.org/wiki/Software_configuration_management), sometimes abbreviated as **SCM**, is important when you're writing code that is meant to stay at least a while (and very handy even if that is not the case).

It is useful not only when you're working with someone else: if at some point you're lost in your code and don't understand why what was working perfectly few hours ago is now utterly broken it is really helpful to be able to compare what has changed since this last commit.

The most obvious choice for a SCM is [git](https://git-scm.com) which is now widely abroad and has become the _de facto_ standard. _git_ is extremely versatile but you can already do a lot of version control with around 10 commands so the learning curve is not as steep as you may fear.

git is generally already installed on your system or readily available through your package manager (or by installing XCode and its tools on macOS).

There are many tutorials about how to use Git; including some from Inria:

- [Support](https://gitlab.inria.fr/aabadie/formation-git) used in the training sessions by Alexandre Abadie (SED Paris).
- A [nice tutorial](https://tutorial.gitlabpages.inria.fr/git/) from Mathias Malandain (SED Rennes) to learn to use Git and Gitlab at the same time.

There are also fairly frequent training sessions organized in the Inria centers; ask your local SED if you're interested by one!


## Build system

Handling properly the compilation of the code is not an easy task: many tutorial skip entirely the topic or just show a very basic example that is very far removed from a real project with potentially many third-party dependencies. This is understandable (and I will mostly do the same): using properly a build system is not trivial and may be the topic on a full lecture of its own.

The usual possibilities are:

* Build system provided by your IDE. Might be easier to use (definitely the case for XCode which I'm familiar with once you grasp how it is intended to work) but you bind your potential users to use the same IDE (even if now some relies upon CMake).
* [Makefile](https://en.wikipedia.org/wiki/Makefile) is the venerable ancestor, which is really too painful to write and not automated enough for my taste.
* [Ninja](https://ninja-build.org) is presented on this website as _a small build system with a focus on speed. It differs from other build systems in two major respects: it is designed to have its input files generated by a higher-level build system, and it is designed to run builds as fast as possible_. It is my favorite generator to use with CMake; meson also enables usage of Ninja under the hood.
* [CMake](https://cmake.org) is the build system probably with the more traction now; it is a cross-platform build system which is rather powerful but not that easy to learn. Official documentation is terse; you may try [this](https://cliutils.gitlab.io/modern-cmake/) or [that](https://cgold.readthedocs.io/en/latest/) to understand it better. Please notice CMake was heavily changed when switching from version2 to version 3; take a recent documentation if you want to learn "modern" CMake. The principle of CMake is to provide a generic configuration that may be used for different build tools: by default you generate a Makefile, but you may choose another generator such as Ninja (see above) or a specific IDE.
* [meson](https://mesonbuild.com/) is a more recent alternative which aims to be simpler to use than CMake. Never used it so can't say much about it.
* [SCons](https://www.scons.org/) is a build system built upon Python which lets you write your own Python functions in the build system. The concept is appealing, but the actual use is actually dreadful and the provided build is much slower than what other build system provides. Avoid it!

We will illustrate in next notebook a basic use of CMake.

**Important:** Nowadays build systems can leverage the powerful computer on which they run and use several processors at the same time. Depending on the tool you use, the default build might be sequential or parallel (on the few I have used, only `ninja` assumes a parallel build by default). Make sure you know how your build tool works and that you're leveraging parallel builds!




[© Copyright](../COPYRIGHT.md)   

