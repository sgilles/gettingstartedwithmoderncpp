---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [C++ in a real environment](./0-main.ipynb) - [Clang ang gcc compilers](./3-Compilers.ipynb)

<!-- #region -->
## Introduction

I will present here briefly some characteristics of both gcc and clang compilers.

I recommend using them both (and more if you can!): each compiler gets its own spin on the standard, and sometimes a perfectly valid code will be refused by one... whereas (more often) invalid code will unduly get a free pass with one of the compilers.

So the more compilers with which you may test your code, the merrier!

If you're Inria staff, Inria also provides a licence to [Intel compiler](https://software.intel.com/en-us/c-compilers).


## GNU compiler 

[GCC](http://gcc.gnu.org/) is a free-to-use compiler which has now been around for decades; it is mostly for Unix systems but may be used with Windows with some additional set-up (I don't master this but see for instance this [StackOverflow question](https://stackoverflow.com/questions/771756/what-is-the-difference-between-cygwin-and-mingw)).

As many others software, GCC changed its version system: gcc 3 and 4 were there for decades, and now the versions change more swiftly, with gcc 13.2 the current stable version (as of February 2024; it was published on the 27th of July 2023).

`gcc` was long known for its terse user interface: until recently color were not provided in outputs, and error messages were a bit cryptic for the beotians. It changed though when `clang` appeared and it is now much more user-friendly.


### Debug and release flags

#### Debug mode

As a reminder, debug mode is the one you should use to develop: it is intended to compile code as fast as possible, and doesn't spend time performing some optimizations. Typical debug flags are:

`-O0 -g`

where:

* `-O0` means no optimization is applied. It might be skipped: it is the default behaviour.
* `-g` means symbols are kept for debug purposes, enabling a debugger to do its bidding.

#### Release mode

On release mode, the goal is more to provide an efficient code, at the cost of higher compilation time. Typical flags are:

`-O3 -DNDEBUG`

where:

* `-DNDEBUG` means the macro `NDEBUG` is defined; this deactivates all asserts in the code.
* `-O3` means as many optimizations as possible should be applied.

You may sometimes find on the Web advocates of `-O2` flag, which performs slightly less optimization than `-O3`, on the ground that `-O3` breaks some code. It was true many years ago... but now if your code breaks under `-O3` it probably means it's buggy, not that optimization is! You may read [this thread](https://stackoverflow.com/questions/11546075/is-optimisation-level-o3-dangerous-in-g) for more about the question; I raised this point because you might be surprised by the number of libraries which still use `-O2` in their release mode.


### Warnings

To my mind, `gcc` is a bit of a mess on the warning side...

There is the flag:

`-Wall`... which activates _some_ warnings, and not all as its name suggests.

You may add:

`-Wextra`... which add some others.

So how to activate them all? No reliable way; my current batch of gcc warnings is (brace yourself!):

`-Wall -Wextra -Wcast-align -Wcast-qual -Wconversion -Wdisabled-optimization -Wfloat-equal -Wformat=2 -Wformat-nonliteral -Wformat-security -Wformat-y2k -Wimport -Winit-self -Winvalid-pch -Wmissing-field-initializers -Wmissing-format-attribute -Wmissing-include-dirs -Wpacked -Wpointer-arith -Wredundant-decls -Wstack-protector -Wstrict-aliasing=2 -Wswitch-enum -Wunreachable-code -Wunused -Wunused-parameter -Wvariadic-macros -Wwrite-strings`

Some are intentionally deactivated, and plenty others I probably don't know - especially if they were introduced since I established this list (by adapting one provided on StackOverflow...) some years ago...

An answer to this [StackOverflow post](https://stackoverflow.com/questions/11714827/how-can-i-turn-on-literally-all-of-gccs-warnings) provides a command to extract the list of all warnings used by gcc (unfortunately without filtering for the language used):

```shell
gcc -Q --help=warning | sed -e 's/^\s*\(\-\S*\)\s*\[\w*\]/\1 /gp;d' | tr -d '\n'
```



### Standard library

GNU also provides its implementation of the standard C++ library, which is called **libstdc++**.

### Fortran support

GNU compiler suite also provides a Fortran compiler, which is often required for many mathematical libraries that use part of this language in their implementation.

## clang

[clang](http://clang.llvm.org/) is a much more recent project (2007) that proposes an interface mostly similar to the one provided by gcc.

This is the compiler I recommend for your development: interface is much more user friendly - even if gcc took note and made progress on that front. But clang gets for itself:

* Default syntax coloring of the output.
* More helpful compilation error: there is even an arrow to indicate where in a line the syntax is problematic.
* Faster compilation in debug mode.

### Debug and release mode

See gcc: they took the same.

### Warnings

clang provides many warnings that are the same as gcc... but:

* Some do not behave exactly the same way. For instance I activate `-Wshadow` in clang but not in gcc where it is too cumbersome for my taste.
* Some are specific to a compiler... and it becomes truer with each new version of clang, which often introduces a new warning.

But what I really like with clang is they took the opposite approach warning side: they provide a `-Weverything` which really activates all of them! (`-Wall -Wextra` are also supported but similar to their gcc counterparts).

Of course, you may deactivate a warning you do not want by adding -Wno-foo where _foo_ is the warning not to consider.

In my code, compilation warnings I use are:

```
-Weverything
-Wno-c++98-compat // I assume my code is not C++ 03 compatible
-Wno-c++98-compat-pedantic // same for pedantic warnings
-Wno-padded // I don't want to add char data attributes to make the static size of a class a multiple of 4
-Wno-exit-time-destructors // I use an advanced pattern that doesn't mesh with this one.
-Wno-global-constructors // same
-Wno-documentation // Some Doxygen were incorrectly indicated as inadequate
-Wno-documentation-unknown-command // A valid Doxygen command I used was not recognized
-Wno-undefined-func-template // Requires weird code in header file; didn't understand this one to be honest
-Wno-c++1z-extensions // I assume my code uses C++ 20
```

What's neat is that your build remains up-to-date with `-Weverything`: if a new warning is added you will possibly see it if your code is affected and then decide if you want to keep it or not.

### Standard library

A new implementation of the standard library is also delivered with clang; it is named **libc++**. I advise you to use it rather than libstdc++ with clang; however it is easier said than done on Ubuntu (at least the last time I tried).

To ensure that, add in your command line:

```
-stdlib=libc++
```

Your build system may already take care of this automatically.

### Fortran support

For a long time, there was no Fortran compiler with LLVM or clang; you therefore had to use something as gfortran if a third-party library you use require it - usually the most recent you may find.

As of 2022, there is something called Flang that exists, but my attempt to use it didn't go far as they chose contrary to clang did years ago to use their own options without pseudo backward compatibility with gfortran's ones. As a result, compilation of third party libraries is tricky as they often assume interface provided by gfortran (Openblas and PETSc for instance won't compile with Flang). So for the time being the best is probably to stick with gfortran if as myself you aren't a Fortran developer but may need it for your third party dependencies.

### Apple Clang

As a side note: macOS provides for few years now a customized clang with its developer environment. This one is not the standard clang and is usually slightly older than the bleeding-edge clang you may find on LLVM site. The drawback is that they stopped indicating the base version upon which their version is built; so it's not that easy to find on the Web whether a feature is supported or not.
<!-- #endregion -->

## State of compiler support

There is currently a new C++ standard every three years, but there is a very noticeable lag for all features to be supported by both the compilers and their associated standard library (some C++ 20 features such as `std::format` library still aren't supported in February 2024).

You may check [here](https://en.cppreference.com/w/cpp/compiler_support) what is the state of support for any given features; each compiler also gets its own page but this one has the merit of providing information for many compilers at once.


## c++filt

We won't delve at all into the details, but just know that to allow stuff such as function overload, namespaces (that we'll cover [shortly](../5-Namespace.ipynb)) and so on, C++ does something called _mangling_ (see [here](http://web.mit.edu/tibbetts/Public/inside-c/www/mangling.html) for instance if you're curious about this process). That's the reason in linker error messages you may see stuff such as `_ZN9cdnalizer11rewriteHTMLINS_6apache8IteratorEcEET_RKSsRKNS_6ConfigES3_S3_St8functionIFS3_RKS3_SB_EES9_IFvSsEE`

The least we can say is that's it's not very user friendly; sometimes what is behind the symbol will be clear enough, sometimes not that much....

Luckily, both clang and gcc ship with a nifty tool named `c++filt` to decipher it, with same name and interface:


```shell
c++filt -n _ZN9cdnalizer11rewriteHTMLINS_6apache8IteratorEcEET_RKSsRKNS_6ConfigES3_S3_St8functionIFS3_RKS3_SB_EES9_IFvSsEE
```


which returns the signature for humans:

<!-- #region -->
```c++
cdnalizer::apache::Iterator cdnalizer::rewriteHTML<cdnalizer::apache::Iterator, char>(std::string const&, cdnalizer::Config const&, cdnalizer::apache::Iterator, cdnalizer::apache::Iterator, std::function<cdnalizer::apache::Iterator (cdnalizer::apache::Iterator const&, cdnalizer::apache::Iterator const&)>, std::function<void (std::string)>)
```
<!-- #endregion -->

## Wandbox

If you need to know whether some small code is supported by a specific version of clang or gcc, you may use the online compiler facility [Wandbox](https://wandbox.org/) which provides many gcc and clang versions (and even Boost library for good measure).



[© Copyright](../COPYRIGHT.md)   

