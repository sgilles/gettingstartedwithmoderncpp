#ifndef BAR_HPP // If this macro is not yet defined, proceed to the rest of the file.
#define BAR_HPP // Immediately define it so next call won't include again the file content.

#include "foo.hpp"

struct Bar
{
    Foo foo_;
};

#endif // BAR_HPP // End of the macro block that begun with #ifndef