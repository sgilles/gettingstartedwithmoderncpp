#include <cstdlib>
#include <iostream>

#include "clean_boost_filesystem.hpp"


int main()
{
    try
    {
        boost::filesystem::path source_path("/Codes/ThirdPartyWarning/source.txt");
        boost::filesystem::path target_path("target.txt");

        boost::filesystem::copy_file(source_path, target_path);
    }
    catch (const boost::filesystem::filesystem_error& e)
    {
        std::cerr << "Exception with Boost filesystem: " << boost::diagnostic_information(e) << std::endl;
        return EXIT_FAILURE;
    }
    
    int a; // variable intentionally left to underline my point about warnings...
    
    std::cout << "Value is " << a << std::endl;
    
    return EXIT_SUCCESS;
}
