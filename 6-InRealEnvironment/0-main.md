---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [C++ in a real environment](./0-main.ipynb)

<!-- #region -->
## Introduction: C++ is not an interpreted language!

We have used so far mostly (except for few snippets of code present in [Coliru](https://coliru.stacked-crooked.com/)) a very unusual flavor of C++: the interpreter cling, which makes C++ behaves rather like an interpreted language ~~(without some caveats: running twice the same cell with a function or class declaration or definition is not possible)~~ *no longer true in 2021!*.

The idea was to highlight first the language syntax and philosophy, and not add the pain of explaining the excruciating details of how to build it - which may not be exactly the same from one environment or another!

We won't do that in detail (CMake tool for instance would deserve its own dedicated lecture...) but give the hints required to understand how C++ is operated in more realistic settings than a Jupyter notebook.


* [Set up a minimal environment](./1-SetUpEnvironment.ipynb)
* [File structure of a C++ program](./2-FileStructure.ipynb)
    * [Hands-on 17](./2b-hands-on.ipynb)
* [Compilers](./3-Compilers.ipynb)
* [Third-party libraries: how to include them without getting their compilation warnings](./4-ThirdParty.ipynb)
* [Namespaces](./5-Namespace.ipynb)
* [Tools](./6-Tools.ipynb)


<!-- #endregion -->


[© Copyright](../COPYRIGHT.md)   

