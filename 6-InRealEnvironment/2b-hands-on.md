---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [C++ in a real environment](./0-main.ipynb) - [Hands-on 18](./2b-hands-on.ipynb)


### Introduction

[This notebook](../HandsOn/HowTo.ipynb) explains very briefly your options to run the hands-ons.

**WARNING:** Coliru is not an option here: you need to go the local or Docker way!


### **EXERCISE 44**

Reorganize the project in several files:

- Only `Loop()` and `main()` functions should be put in _main.cpp_.
- `Exception` should be declared in `Exception.hpp` and defined in `Exception.cpp`.
- Free functions such as `TimesPowerOf2` and `RoundAsInt` should be put in `Tools` files. As all these definitions are template, they should be in a header file (that might be the same as the one used for declaration, or a different one if you want to split declaration and definition - in this case don't forget to include it at the end of the hpp file!)
- Likewise for `PowerOfTwoApprox`. Closely related free functions (such as `operator<<`) should be put in the same file(s).
- Put `TestDisplay` and derived classes in the appropriate files. Beware: some definitions are template and must be in header files, whereas some aren't and should therefore be compiler.

To do the exercise, create an _Exercice44_ directory in the _HandsOn_ root directory (and include it in your main CMakeLists.txt).

Create the files in this directory, and use the following CMakeLists.txt:

```cmake
cmake_minimum_required(VERSION 3.20)

project(Exercise44)

set(CMAKE_CXX_STANDARD 17 CACHE STRING "C++ standard; at least 17 is expected.")

add_library(exercise44_lib
            SHARED
            # Header files are not strictly necessary but may be useful for some CMake generators.
            ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt # same for CMakteLists if you for instance want it in your IDE!
            ${CMAKE_CURRENT_LIST_DIR}/Exception.cpp
            ${CMAKE_CURRENT_LIST_DIR}/Exception.hpp
            ${CMAKE_CURRENT_LIST_DIR}/PowerOfTwoApprox.hpp
            ${CMAKE_CURRENT_LIST_DIR}/PowerOfTwoApprox.hxx
            ${CMAKE_CURRENT_LIST_DIR}/TestDisplay.cpp
            ${CMAKE_CURRENT_LIST_DIR}/TestDisplay.hpp
            ${CMAKE_CURRENT_LIST_DIR}/TestDisplay.hxx
            ${CMAKE_CURRENT_LIST_DIR}/TestDisplayPowerOfTwoApprox.hpp
            ${CMAKE_CURRENT_LIST_DIR}/TestDisplayPowerOfTwoApprox.hxx
            ${CMAKE_CURRENT_LIST_DIR}/TestDisplaySumOfMultiply.hpp
            ${CMAKE_CURRENT_LIST_DIR}/TestDisplaySumOfMultiply.hxx
            ${CMAKE_CURRENT_LIST_DIR}/Tools.hpp
            ${CMAKE_CURRENT_LIST_DIR}/Tools.hxx
)


add_executable(exercise44
               ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
               
target_link_libraries(exercise44 
                      exercise44_lib)
```                     



[© Copyright](../COPYRIGHT.md)   

