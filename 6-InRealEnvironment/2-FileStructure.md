---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.16.1
  kernelspec:
    display_name: C++17
    language: C++17
    name: xcpp17
---

# [Getting started in C++](./) - [C++ in a real environment](/notebooks/6-InRealEnvironment/0-main.ipynb) - [File structure in a C++ program](/notebooks/6-InRealEnvironment/2-FileStructure.ipynb)


## Library and program

Contrary to for instance Python or Ruby, C++ is not a scripting language: it is intended to build either an **executable** or **library**.

To summarize:

* An **executable** runs the content of the [`main() function`](../1-ProceduralProgramming/4-Functions.ipynb#A-very-special-function:-main). There should be exactly one such function in all the compiled files; the file with this `main` must be compiled.
* A **library** is a collection of functions, classes and so on that might be used in a program. A library may be **header-only**: in this case it is just an ensemble of header files with no file compiled. In this case all the definitions must be either **inline** or **template** (and possibly both of course).

### Static and shared libraries

A (non header) library may be constructed as one of the following type:

* A **static** library, usually with a **.a** extension, is actually included directly into any executable that requires it. The advantage is that you just need the bare executable to run your code: the library is no longer required at runtime. The inconvenient is that the storage space may balloon up rather quickly: each executable will contain the whole library! 
* A **shared** library, which extension may vary wildly from one OS to another (**.dylib**, **.so**, **.dll**, etc...), is on the other hand required at runtime by the executable that was built with it. The advantage is that executables are thus much smaller. They are often described on the Web as the way to go; my personal experience with them is however less rosy as each OS handles them differently (noticeably the way to indicate in which location the dynamic libraries should be looked at differ rather wildly...)

The best if possible is to enable generation of your library in either type... but it requires a bit of work with your build system.

## Source file

Contrary to most of more modern languages, C++ relies upon two very specific kind of files, each of which with their own extension schemes. We will introduce first the source file, with which basic programs might be achieved, and then show why header files are also needed.

### Compilation of _Hello world!_

A source file is a type of file intended to be **compiled**.

Let's consider the seminal _Hello world_ in a dedicated source file named _hello.cpp_ (all the examples here are made available in `2c-Demo` directory; this one is `01-HelloWorld`):


```c++
// File hello.cpp - I put "Code" as cell type in Jupyter to get nice colors but it's not intended
// to be executed in the cell!
#include <iostream>

int main(int argc, char** argv)
{
    std::cout << "Hello world!" << std::endl;
    
    return EXIT_SUCCESS;
}
```

To compile it on a Unix system, you will need to type in your terminal a line that looks like (with at least [GNU compiler for C++](https://en.wikipedia.org/wiki/GNU_Compiler_Collection) and [clang++](https://en.wikipedia.org/wiki/Clang)):

```c++
// In a terminal
g++ -std=c++17 hello.cpp -o hello
```

where:

- `g++` is the name of the compiler. You may provide `clang++` if you wish.
- `-std=c++17` tells to use this version of the standard. If not specified the compilers tend to assume C++ 11 but may issue warnings if some features introduced with this standard are used.
- `hello.cpp` is the name of the source file.
- `hello` is the name of the executable produced. If the `-o hello` is omitted, the executable is arbitrarily named `a.out`, exactly as in C.


The executable may then be used with:

```c++
// In a terminal
./hello
```

The `./` is there to specify the executable is to be looked at in current path; it may be omitted if `.` is present in the system `PATH` environment variable.

Please notice the name of the file with the `main()` function and the name of the executable are completely custom; you have no requirement on the names of files and executable.


If your current machine has the compilers installed it is possible to execute these compilation commands instead of opening the terminal use the ! symbol as follows:

```c++
!g++ -std=c++17 ./2c-Demo/01-HelloWorld/hello.cpp -o hello
```

```c++
!./hello
```

### Source files extensions

The plural is not a mistake: unfortunately, contrary to many languages, there is no universal convention upon the extensions to use for C++ files. There are widely spread conventions, but a library may choose not to follow them. 

Editors and IDE know the most common ones and usually provide a way to add your own spin so that they may provide language recognition and all that goes with it (colored syntax, completion helper and so on).

The most common extensions are **.cpp**, **.cc**, **.C** and more seldom **.cxx**.

My advice would be to choose one and stick to it; the only one I warn against is **.C** because some operating systems (such as macOS) are case-insensitive by default and **.c** is a more common convention for C programs.


### Expanding our hello program with two source files: one for main, one for the function

This code is not very subtle: everything is in the same file, so we are in a very simplistic case in which only one file is compiled, and there are no need to find ways to specify how several files relate to each other.

You may imagine working with a single file is not a very common option: it hinders reusability, and it would be cumbersome to navigate in a file with thousands or more lines of code (if you're curious about an extreme case, have a look at the amalgamation ([2.28 Mo zip here](https://www.sqlite.org/2020/sqlite-amalgamation-3310100.zip)) of sqlite code, in which all the code is put in a same source file...)

We now want to separate the main() and the actual content of the code (also in `2c-Demo/02-InTwoFilesWithoutHeader`): 

```c++
// File hello.cpp - no main inside
#include <iostream>

void hello()
{
    std::cout << "Hello world!" << std::endl;
}

```

```c++
// File main.cpp
#include <cstdlib> // for EXIT_SUCCESS

int main(int argc, char** argv)
{
    hello();
    
    return EXIT_SUCCESS;
}
```

This brute force method is not working: a line on a terminal like:

```c++
// In a terminal
clang++ -std=c++17 hello.cpp main.cpp -o hello
```

would yield something like:

```verbatim
main.cpp:5:5: error: use of undeclared identifier 'hello'
    hello();
    ^
1 error generated.
```


## Header file

The issue above is that we need to inform the compiler when it attempts to compile `main.cpp` that `hello()` function is something that exists. We need to **declare** it in a dedicated **header file** and **include** this file in each source file that needs it (also in `2c-Demo/03-InTwoFilesWithHeader`):

```c++
// File hello.hpp
void hello();
```

```c++
// File main.cpp
#include <cstdlib> // for EXIT_SUCCESS
#include "hello.hpp"

int main(int argc, char** argv)
{
    hello();
    
    return EXIT_SUCCESS;
}
```

```c++
// File hello.cpp - no main inside
#include <iostream>
#include "hello.hpp"

void hello()
{
    std::cout << "Hello world!" << std::endl;
}
```

With this few changes, the command line:

```c++
// In a terminal
clang++ -std=c++17 hello.cpp main.cpp -o hello
```

works as expected and creates a valid `hello` executable (also note the header file is not required explicitly in this build command). 

As in the previous case we may directly compile from here using the ! symbol as follows (if compilers are present in the environment):

```c++
!g++ -std=c++17 2c-Demo/03-InTwoFilesWithHeader/hello.cpp 2c-Demo/03-InTwoFilesWithHeader/main.cpp -o hello
```

```c++
!./hello
```

<!-- #region -->
### Header location

You may have noticed that in the previous call to compile the executable the header file wasn't provided explicitly.

`hello.hpp` was found because it was in the current folder. Let's suppose now we want to put include files in a directory named `incl`; to make it work we have actually two ways:

* Either modifying the path in the source file. We would get

```c++
#include "incl/hello.hpp"
``` 
in both `hello.cpp` and `main.cpp`.

* Or by giving to the command line the `-I` instruction to indicate which path to look for (`2c-Demo/04-SpecifyHeaderDirectory`):




<!-- #endregion -->

```c++
// In a terminal
clang++ -std=c++17 -Iincl hello.cpp main.cpp -o hello
```

<!-- #region -->
As many `-I` as you wish may be provided on the command line; I would recommend not providing too many as it increases the risk of an ambiguity if two header files at different path are named likewise:


```verbatim
incl/foo.hpp
bar/incl/foo.hpp
```

and 

```shell
clang++ -Iincl -Ibar/incl main.cpp
```

leads to an ambiguity if there is `#include "foo.hpp"` in the `main.cpp`...

<!-- #endregion -->

### `""`  or `<>`?

You may have noticed I sometimes used `<>` and sometimes `""` to specify the path for the include.

The details don't matter that much in most cases, but it is better to:

* Use `<>` only for the system libraries, typically STL or C headers should be this form.
* Use `""` for your headers or for third-party libraries installed in specific locations.

If you want a bit more details:

* `""` will look first in the current directory, and then in the header files directories.
* `<>` will look only in the header files directories.


### Header guards and #pragma once

During compilation, the `#include` command is actually replaced by the content of the file which path is provided here. We therefore may quickly include twice the same content (`2c-Demo/05-NoHeaderGuards`):

```c++
// File foo.hpp
class Foo
{ };
```

```c++
// File main.cpp
#include <cstdlib>
#include "foo.hpp"
#include "foo.hpp" // Oops...

int main()
{
    return EXIT_SUCCESS;
}
```

```c++
// In terminal
clang++ -std=c++17 main.cpp -o does_not_compile   
```

doesn't compile: the translation unit provides two declarations of class Foo!

This might seem a simple enough mistake to fix it, but in a project with few header files that might be intricated it becomes quickly too much a hassle (`2c-Demo/06-MoreSubtleNoHeaderGuards`):

```c++
// File foo.hpp
class Foo
{ };
```

```c++
// File bar.hpp

#include "foo.hpp"

struct Bar
{
    Foo foo_;
};
```

```c++
// File main.cpp
#include <cstdlib>
#include "foo.hpp"
#include "bar.hpp" // Compilation error: "foo.hpp" is sneakily included here as well!

int main()
{
    Bar bar;
    return EXIT_SUCCESS;
}
```

```c++
// In terminal
clang++ -std=c++17 main.cpp -o does_not_compile
```

The patch is to indicate in each header file that it should be included **only once**. 

#### #pragma once

There is the easy but non standard approach that is nonetheless [widely supported](https://en.wikipedia.org/wiki/Pragma_once#Portability) by compilers (`2c-Demo/07-PragmaOnce`):

```c++
// File foo.hpp
#pragma once

class Foo
{ };
```

```c++
// File bar.hpp
#pragma once

#include "foo.hpp"

struct Bar
{
    Foo foo_;
};
```

```c++
// File main.cpp
#include <cstdlib>
#include "foo.hpp"
#include "bar.hpp"

int main()
{    
    return EXIT_SUCCESS;
}
```

This prevents the inclusion of `foo.hpp` twice; and now `clang++ -std=c++17 main.cpp -o do_nothing` compiles correctly.


#### Header guards

The "official" way to protect files - the use of so-called **header guards** - fully supported by the standard, is much more clunky (`2c-Demo/08-HeaderGuards`):

```c++
// File foo.hpp

#ifndef FOO_HPP // If this macro is not yet defined, proceed to the rest of the file.
#define FOO_HPP // Immediately define it so next call won't include again the file content.

class Foo
{ };

#endif // FOO_HPP // End of the macro block that begun with #ifndef
```

```c++
// File bar.hpp

#ifndef BAR_HPP // If this macro is not yet defined, proceed to the rest of the file.
#define BAR_HPP // Immediately define it so next call won't include again the file content.

#include "foo.hpp"

struct Bar
{
    Foo foo_;
};

#endif // BAR_HPP // End of the macro block that begun with #ifndef
```

You may check that `clang++ -std=c++17 main.cpp -o do_nothing` compiles properly as well.


##### **[WARNING]** Ensure unicity of header guards

There is however a catch with header guards: you must ensure that the macro for a given file is used only once. Let's consider the previous case, but with a bug (`2c-Demo/09-HeaderGuardsBug`):

```c++
// File foo.hpp

#ifndef FOO_HPP // If this macro is not yet defined, proceed to the rest of the file.
#define FOO_HPP // Immediately define it so next call won't include again the file content.

class Foo
{ };

#endif // FOO_HPP // End of the macro block that begun with #ifndef
```

```c++
// File bar.hpp

#ifndef FOO_HPP // bug here!
#define FOO_HPP

#include "foo.hpp"

struct Bar
{
    Foo foo_;
};

#endif // FOO_HPP
```

`clang++ -std=c++17 main.cpp` does not compile, with the terse message:

```shell
main.cpp:7:5: error: unknown type name 'Bar'
    Bar bar;
```    


And in a more developed code, it might be a nightmare to identify this kind of bug...

A common strategy is to define a header guard name based on the location of the source file in the tree; this circumvent the case in which two files share a same name (quite common in a large codebase...)

One of us (Sébastien) uses up a [Python script](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/raw/master/Scripts/header_guards.py) which iterates through all the C++ files in his library, identify the header guards of each header file and check they are a mix of the project name and the path of the file. This is definitely much more clunky than **#pragma once** !

But as we said the latter is non standard and there are hot discussions about whether it is safe or not for all set-ups (at some point it was complicated to use if there were symbolic or hard links in the project).


### Header files extensions

The most current header files extensions are **.hpp**, **.h**, **.hh** and more seldom **.hxx**. I definitely do not recommend **.h**: this is also the extension used for C header files, and some compiler even issue a warning if you're using it in a C++ context.

#### My personal convention (Sébastien)

Personally I am using both **.hpp** and **.hxx**:

* **.hpp** is for the declaration of functions, classes, and so on.
* **.hxx** is for the definitions of inline functions and templates.

The **.hxx** is included at the end of **.hpp** file; this way:

* End-user just includes the **.hpp** files in his code; he **never** needs to bother about including **.hxx** or not.
* The **hpp** file is not too long and includes only declarations with additionally Doxygen comments to explain the API.

And you may have noticed that standard library headers get no extension at all!



## Why a build system: very basic CMake demonstration

Let's take back our mighty "Hello world" example with a slight extension: we want to query the identity of the user and print that instead. We will foolishly add this new function in yet another file for the sake of illustration only (`2c-Demo/10-CMake`):



```c++
// File hello.hpp
#ifndef HELLO_HPP
#define HELLO_HPP

void Hello();

#endif // HELLO_HPP
```

```c++
// File who-are-you.hpp
#ifndef WHO_ARE_YOU_H
#define WHO_ARE_YOU_H

#include <string>

std::string WhoAreYou();

#endif // WHO_ARE_YOU_H
```

```c++
// File hello.cpp
#include <iostream>
#include "hello.hpp"
#include "who-are-you.hpp"

void hello()
{
    auto identity = WhoAreYou();
    std::cout << "Hello " << identity << '!' << std::endl;
}

```

```c++
// File who-are-you.cpp
#include <iostream>
#include "who-are-you.hpp"

std::string WhoAreYou()
{
    std::string name;
    std::cout << "What's your name? ";
    std::cin >> name; // not much safety here but this is not the current point!
    return name;
}
```

```c++
// File main.cpp
#include <cstdlib> // For EXIT_SUCCESS
#include "hello.hpp"

int main(int argc, char** argv)
{
    Hello();
    
    return EXIT_SUCCESS;
}
```

Up to now, we compiled such a program with manually:

```c++
// In terminal
clang++ -std=c++17 -c hello.cpp
clang++ -std=c++17 -c main.cpp
clang++ -std=c++17 -c who-are-you.cpp
clang++ -std=c++17 *.o -o hello          
```

The issue with that is that it's not robust at all: either you recompile everything all the time (and let's face it: it's tedious even with our limited number of files...) or you have to keep track of which should be recompiled. For instance if `who-are-you.hpp` is modified all source files include it and must be recompiled, but if it is `hello.hpp` `who_are_you.cpp` is not modified.

It is to handle automatically this and limit the compilation to only what is required that build systems (which we talked about briefly [here](./1-SetUpEnvironment.ipynb#Build-system)) were introduced. Let's see a brief CMake configuration file named by convention `CMakeLists.txt`:

```c++
# CMakeLists.txt
# Ensure the cmake used is compatible with the CMake functions that are used
cmake_minimum_required(VERSION 3.20) 

# A project name is mandatory, preferably right after cmake_minimum_required call
project(Hello)

set(CMAKE_CXX_STANDARD 17 CACHE STRING "C++ standard; at least 17 is expected.")

add_executable(hello
               main.cpp 
               hello.cpp 
               who-are-you.cpp)
```

```c++
// In terminal
mkdir build // create a directory to separate build from source files and so on
cd build
cmake .. // will create the Makefile; as no generator was provided with -G Unix makefile is chosen.
         // The directory indicated by .. MUST include the main CMakeLists.txt of the project.
make
```

This command creates the executable in current directory; now if we modified one file the build system will rebuild all that needs it and nothing more.


If `main.cpp` and `hello.cpp` may also be used jointly for another executable, they may be put together in a library; replace the former `add_executable` command by:

```c++
add_library(hello_lib
            SHARED
            hello.cpp 
            who-are-you.cpp)


add_executable(hello
               main.cpp)
               
target_link_libraries(hello 
                      hello_lib)   
```

SHARED may be replaced by STATIC to use a static library instead.


You can run these commands directly with the ! symbol as follows:

```c++
!cd ./2c-Demo/7-CMake/ && mkdir build && cd build && cmake .. && make
```

```c++
!cd ./2c-Demo/7-CMake/build && ./hello 
```

## Where should the headers be included?

* Each time a header is modified, all the source files that include it directly or indirectly are recompiled. 
* Each time a source file is modified, only this source file is modified; some relinking for the libraries and executables that depend on it will also occur (linking is the step that glue together the object files and libraries; the term _compilation_ is often - included in this very tutorial - abusively used to encompass both compilation and link phases).

Thus it might seem a good idea to put as much as possible `#include` directives in the source files **rather than in include files**... hence limiting the compilation time. This is a generally very good advice... provided we do not err on the wrong side and put enough in the header file:

```c++
// File foo.hpp
#ifndef FOO_HPP
# define FOO_HPP

#include <string>

void Print(std::string text);

#endif // FOO_HPP
```

```c++
// File foo.cpp
#include <iostream>
#include "foo.hpp"

void Print(std::string text)
{
    std::cout << "The text to be printed is: \"" << text << "\"." << std::endl;
}
```

```c++
// File main.cpp
#include <cstdlib>
#include "foo.hpp"

int main()
{
    Print("Hello world!");
    
    return EXIT_SUCCESS;
}
```

You may have noticed `string` and `iostream` are not dealt with the same way... and rightly so:

* `#include <iostream>` is only in the source file: it is actually needed only for `std::cout` and `std::endl`, which are implementation details of `Print()` function: neither appears in the signature of the function.
* `#include <string>` is present in `foo.hpp` as it is required to give the information about the type of the prototype to be used. If you do not do that, each time you include `foo.hpp` you would need to include as well `string`; doing so leads to unmaintainable code as you would have to track down all the includes that are required with each include...

So to put in a nutshell:

* Put in the header files all the includes that are mandatory to make the prototypes understandable. A rule of thumb is that a source file that would only include the header file should be compilable:


```c++
// File foo.hpp
std::string Print();
```

```c++
// File check_foo.cpp
#include <cstdlib>
#include "foo.hpp"

int main(int, char**)
{

    return EXIT_SUCCESS;
} // DOES NOT COMPILE => header is ill-formed!
```

* Include that are here for implementation details should on the other hand be preferably in source files. Of course, you may not be able to do that in any case: for instance templates are by construction defined in header files!


Some tools such as [include-what-you-use](https://include-what-you-use.org/) are rather helpful to help cut off the unrequired includes in file, but they need a bit of time to configure and set up properly, especially on an already large codebase.


## Forward declaration

There is actually an exception to the first rule I've just given: **forward declaration**. This is really a trick that may be used to reduce compilation time, with some caveats.

The idea is that if a type intervenes in a header file **only as a reference and/or as a (smart) pointer**, it might be forward-declared: its type is merely given in the header (`2c-Demo/11-Forward`)



```c++
// File foo.hpp

#ifndef FOO_HPP
# define FOO_HPP

// Forward declaration: we say a class Bar is meant to exist...
class Bar;

struct Foo
{
    Foo(int n);
    
    void Print() const;

    Bar* bar_ = nullptr;
};

#endif // FOO_HPP
```

```c++
// File check_header_ok.cpp
#include <cstdlib>
#include "foo.hpp"

int main(int, char**)
{

    return EXIT_SUCCESS;
}
```

and `clang++ -std=c++17 check_header_ok.cpp` compiles properly (you may try commenting out the forward declaration line to check it does not without it)

This is not without cost: obviously in a file where `Bar` is actually needed you will need to include it properly: with just `#include "foo.hpp"` you can't for instance call a method of `Bar` class. 

Typically the `include "bar.hpp"` will be located in the `foo.cpp` file, in which you will probably need the `Bar` object interface to define your `Foo` object (or if not you may question why you chose to put the `bar_` data attribute in the first place)

It is nonetheless a very nice trick to know; there is even an idiom call [Pimpl idiom](https://arne-mertz.de/2019/01/the-pimpl-idiom/) that relies upon forward declaration.

This is however not the only use for it though: to define a shared_ptr/weak_ptr you [also need](../7-Appendix/WeakPtr.ipynb) to use this capability.

The tool [include-what-you-use](https://include-what-you-use.org/) mentioned earlier is able to suggest as well what should be forward-declared.



[© Copyright](../COPYRIGHT.md)   

